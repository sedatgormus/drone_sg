/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef PROJECT_ROUTER_CONF_H_
#define PROJECT_ROUTER_CONF_H_

#define IPV6_RPL_BORDER_ROUTER 1

#define FOURE_DEBUG DEBUG_PRINT

/* Select CC2538 devices
#define CC2538_DEV_CC2538SF53   CC2538_DEV_DEF(0, 512, 32, 1, 1)
#define CC2538_DEV_CC2538SF23   CC2538_DEV_DEF(1, 256, 32, 1, 1)
#define CC2538_DEV_CC2538NF53   CC2538_DEV_DEF(2, 512, 32, 1, 0)
#define CC2538_DEV_CC2538NF23   CC2538_DEV_DEF(3, 256, 32, 1, 0)
#define CC2538_DEV_CC2538NF11   CC2538_DEV_DEF(4, 128, 16, 1, 0)
*/
#define CC2538_DEV_CONF         CC2538_DEV_CC2538NF53

#ifndef UIP_FALLBACK_INTERFACE
#define UIP_FALLBACK_INTERFACE  rpl_interface
#endif

#ifndef QUEUEBUF_CONF_NUM
#define QUEUEBUF_CONF_NUM       2
#endif

#ifndef UIP_CONF_BUFFER_SIZE
#define UIP_CONF_BUFFER_SIZE    140
#endif

#ifndef UIP_CONF_RECEIVE_WINDOW
#define UIP_CONF_RECEIVE_WINDOW  60
#endif

#ifndef WEBSERVER_CONF_CFS_CONNS
#define WEBSERVER_CONF_CFS_CONNS 2
#endif

#define UART0_CONF_BAUD_RATE     921600

#define RPL_ADD_ROUTE_ENABLED    1

#define TSCH_TIME_SYNCH                      1

#define FRAME802154_CONF_VERSION             FRAME802154_IEEE802154E_2012

#if TSCH_TIME_SYNCH
#define TSCH_CONF_SIX_TISCH_MINIMAL          1
#define TSCH_CONF_CENTRAL_SCHEDULER_ENABLED  1

#define FOURE_CONF_MAX_SHARED_SLOT   2
#define FOURE_CONF_MAX_CONTENT       5
#define FOURE_CONF_MAX_SLOTS         15
#endif /* TSCH_TIME_SYNCH */

#endif /* PROJECT_ROUTER_CONF_H_ */
