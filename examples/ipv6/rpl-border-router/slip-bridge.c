/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

/**
 * \file
 *         Slip fallback interface
 * \author
 *         Niclas Finne <nfi@sics.se>
 *         Joakim Eriksson <joakime@sics.se>
 *         Joel Hoglund <joel@sics.se>
 *         Nicolas Tsiftes <nvt@sics.se>
 */

#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "dev/slip.h"
#include "dev/uart1.h"
#include "lib/memb.h"
#include <string.h>

#define UIP_IP_BUF        ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#ifndef DEBUG
#define DEBUG DEBUG_NONE /* debug messages must be disabled */
#endif
#include "net/ip/uip-debug.h"

void set_prefix_64(uip_ipaddr_t *);

static uip_ipaddr_t last_sender;

#if RPL_ADD_ROUTE_ENABLED
#define SLIP_END     0300
#define SLIP_ESC     0333
#define SLIP_ESC_END 0334
#define SLIP_ESC_ESC 0335

struct slip_list{
  struct slip_list *next;
  uint8_t buf[UIP_CONF_BUFFER_SIZE];
  uint8_t buf_len;
};

MEMB(slip_list_mem, struct slip_list, 10);
LIST(slip_list);

#define GET_HEAD_MSG()  list_head(slip_list)

PROCESS(slip_send_process, "SLIP driver");

/*---------------------------------------------------------------------------*/
static uint8_t
add_slip_msg()
{
  struct slip_list *data;

  data = memb_alloc(&slip_list_mem);
  if(data == NULL){ /* no more space left in the buffer */
    PRINTF("Slip List full!\n");
    return 0;
  }

  data->buf_len = uip_len;
  memcpy(data->buf, uip_buf, data->buf_len * sizeof(uint8_t));

  list_add(slip_list, data);
  return 1;
}

/*---------------------------------------------------------------------------*/
static void
delete_slip_msg(struct slip_list *data)
{
  if(data != NULL){
    list_remove(slip_list, data);
    memb_free(&slip_list_mem, data);
  }
}
#endif /* RPL_ADD_ROUTE_ENABLED */
/*---------------------------------------------------------------------------*/
static void
slip_input_callback(void)
{
 // PRINTF("SIN: %u\n", uip_len);
  if(uip_buf[0] == '!') {
    PRINTF("Got configuration message of type %c\n", uip_buf[1]);
    uip_len = 0;
    if(uip_buf[1] == 'P') {
      uip_ipaddr_t prefix;
      /* Here we set a prefix !!! */
      memset(&prefix, 0, 16);
      memcpy(&prefix, &uip_buf[2], 8);
      PRINTF("Setting prefix ");
      PRINT6ADDR(&prefix);
      PRINTF("\n");
      set_prefix_64(&prefix);
    }
  } else if (uip_buf[0] == '?') {
    PRINTF("Got request message of type %c\n", uip_buf[1]);
    if(uip_buf[1] == 'M') {
      char* hexchar = "0123456789abcdef";
      int j;
      /* this is just a test so far... just to see if it works */
      uip_buf[0] = '!';
      for(j = 0; j < 8; j++) {
        uip_buf[2 + j * 2] = hexchar[uip_lladdr.addr[j] >> 4];
        uip_buf[3 + j * 2] = hexchar[uip_lladdr.addr[j] & 15];
      }
      uip_len = 18;
      slip_send();
    }
    uip_len = 0;
  }
  /* Save the last sender received over SLIP to avoid bouncing the
     packet back if no route is found */
  uip_ipaddr_copy(&last_sender, &UIP_IP_BUF->srcipaddr);
}

/*---------------------------------------------------------------------------*/
static void
init(void)
{
  slip_arch_init(BAUD2UBR(115200));
  process_start(&slip_process, NULL);
#if RPL_ADD_ROUTE_ENABLED
  process_start(&slip_send_process, NULL);
#endif
  slip_set_input_callback(slip_input_callback);
}

/*---------------------------------------------------------------------------*/
static void
output(void)
{
  if(uip_ipaddr_cmp(&last_sender, &UIP_IP_BUF->srcipaddr)) {
    /* Do not bounce packets back over SLIP if the packet was received
       over SLIP */
    PRINTF("slip-bridge: Destination off-link but no route src=");
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF(" dst=");
    PRINT6ADDR(&UIP_IP_BUF->destipaddr);
    PRINTF("\n");
  } else {
    PRINTF("SUT: %u\n", uip_len);
#if RPL_ADD_ROUTE_ENABLED
    add_slip_msg();
#else
    slip_send();
#endif
  }
}

/*---------------------------------------------------------------------------*/
#if !SLIP_BRIDGE_CONF_NO_PUTCHAR
#undef putchar
int
putchar(int c)
{
  #define SLIP_END     0300
  static char debug_frame = 0;

  if(!debug_frame) {            /* Start of debug output */
    slip_arch_writeb(SLIP_END);
    slip_arch_writeb('\r');     /* Type debug line == '\r' */
    debug_frame = 1;
  }

  /* Need to also print '\n' because for example COOJA will not show
     any output before line end */
  slip_arch_writeb((char)c);

  /*
   * Line buffered output, a newline marks the end of debug output and
   * implicitly flushes debug output.
   */
  if(c == '\n') {
    slip_arch_writeb(SLIP_END);
    debug_frame = 0;
  }
  return c;
}
#endif

/*---------------------------------------------------------------------------*/
const struct uip_fallback_interface rpl_interface = {
  init, output
};

#if RPL_ADD_ROUTE_ENABLED
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(slip_send_process, ev, data)
{
  static uint16_t i;
  static uint8_t *ptr;
  static uint8_t c;
  static struct slip_list *msg;

  PROCESS_BEGIN();
  PRINTF("Slip Process Started\n");

  while(1){
    msg = GET_HEAD_MSG();
    if(msg == NULL){
      PROCESS_PAUSE();
      continue;
    }

    slip_arch_writeb(SLIP_END);

    ptr = &msg->buf[UIP_LLH_LEN];
    for(i = 0; i < msg->buf_len; ++i){
      if(NETSTACK_RADIO.pending_packet() == 1){
        PROCESS_PAUSE();
      }
      if(i == UIP_TCPIP_HLEN){
        ptr = (uint8_t *)&msg->buf[UIP_TCPIP_HLEN + UIP_LLH_LEN];
      }
      c = *ptr++;
      if(c == SLIP_END){
        slip_arch_writeb(SLIP_ESC);
        c = SLIP_ESC_END;
      }else if(c == SLIP_ESC){
        slip_arch_writeb(SLIP_ESC);
        c = SLIP_ESC_ESC;
      }
      slip_arch_writeb(c);
    }
    slip_arch_writeb(SLIP_END);
    delete_slip_msg(msg);
  }
  PROCESS_END();
}
#endif /* RPL_ADD_ROUTE_ENABLED */