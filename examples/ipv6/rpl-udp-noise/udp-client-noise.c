/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#ifdef WITH_COMPOWER
#include "powertrace.h"
#endif

#include <stdio.h>
#include <string.h>

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define UDP_CLIENT_PORT 8765
#define UDP_SERVER_PORT 5678

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#ifndef PERIOD
#define PERIOD  1
#endif

#define SEND_INTERVAL     (PERIOD * CLOCK_SECOND) >> 0
#define SEND_TIME         (random_rand() % (SEND_INTERVAL))
#define MAX_PAYLOAD_LEN   64

static struct uip_udp_conn *client_conn;
static uip_ipaddr_t server_ipaddr;

/*---------------------------------------------------------------------------*/
PROCESS(udp_client_process, "UDP Client Process");
AUTOSTART_PROCESSES(&udp_client_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  char *str;

  if(uip_newdata()){
    str = uip_appdata;
    str[uip_datalen()] = '\0';
    PRINT("DATA recv '%s'\n", str);
  }
}
/*---------------------------------------------------------------------------*/
static void
send_packet(void *ptr)
{
  static uint16_t seq_id;
  uint8_t buf[MAX_PAYLOAD_LEN], buf_len = 0;
  uint8_t i, slot_frame[SLOT_FRAME_SIZE];

  seq_id++;
  PRINT("DATA send to %d 'Hello %d'\n", server_ipaddr.u8[sizeof(server_ipaddr.u8) - 1], seq_id);

#if RPL_ADD_ROUTE_ENABLED 
  buf[buf_len++] = '#';
  buf[buf_len++] = '!';

  slot_frame[0] = 0x40; /* EB slot is used = used, slot type = shared, channel offset = 0 */
  for(i = 1; i < SLOT_FRAME_SIZE; i++)
    slot_frame[i] = 0x80; /* 1 slot unused(default), 0 slot used */

  foure_mac_buf_get_slot_frame_from_lladdr(nbr_table_get_lladdr(rpl_parents, default_instance->current_dag->preferred_parent), slot_frame);
  memcpy(&buf[buf_len], slot_frame, SLOT_FRAME_SIZE * sizeof(uint8_t));
  buf_len += SLOT_FRAME_SIZE * sizeof(uint8_t);
  
  memcpy(&buf[buf_len], &seq_id, sizeof(uint16_t));
  buf_len += sizeof(uint16_t);

  memcpy(&buf[buf_len], &default_instance->current_dag->rank, sizeof(uint16_t));
  buf_len += sizeof(uint16_t);
  /* Parent fill rssi */
  buf[buf_len++] = 0; 
  buf[buf_len++] = 0;

  /* parent fill asn */
  buf[buf_len++] = 0;
  buf[buf_len++] = 0;

  buf[buf_len++] = uip_lladdr.addr[7];
#else
  sprintf((char *)buf, "Hello %d from the client", seq_id);
  buf_len += strlen((char *)buf);
#endif

  uip_udp_packet_sendto(client_conn, buf, buf_len, &server_ipaddr, UIP_HTONS(UDP_SERVER_PORT));
}

/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xdaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

/* The choice of server address determines its 6LoPAN header compression.
 * (Our address will be compressed Mode 3 since it is derived from our link-local address)
 * Obviously the choice made here must also be selected in udp-server.c.
 *
 * For correct Wireshark decoding using a sniffer, add the /64 prefix to the 6LowPAN protocol preferences,
 * e.g. set Context 0 to aaaa::.  At present Wireshark copies Context/128 and then overwrites it.
 * (Setting Context 0 to aaaa::1111:2222:3333:4444 will report a 16 bit compressed address of aaaa::1111:22ff:fe33:xxxx)
 *
 * Note the IPCMV6 checksum verification depends on the correct uncompressed addresses.
 */
 
#if 1
/* Mode 1 - 64 bits inline */
   uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 5);
#elif 0
/* Mode 2 - 16 bits inline */
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0, 0x00ff, 0xfe00, 1);
#else
/* Mode 3 - derived from server link-local (MAC) address */
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0x0250, 0xc2ff, 0xfea8, 0xcd1a); //redbee-econotag
#endif
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_client_process, ev, data)
{
#if WITH_COMPOWER
  static int print = 0;
#endif

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  set_global_address();

  PRINT("UDP noise process started\n");

#if TSCH_TIME_SYNCH
  foure_timesynch_init(1);
#endif /* TIMESYNCH_CONF_ENABLED */

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
