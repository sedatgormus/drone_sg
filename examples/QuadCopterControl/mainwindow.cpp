#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fix16.h"
#include "packet_parser.h"

#define QUAD_CONTROL_PORT    3461

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupPlot();

    /////////////////// Server ////////////////////////
    const QString quadCopter_address= "daaa::212:4b00:430:569c";
    quadAddress.setAddress(quadCopter_address);

    // create a QUDP socket
    socket = new QUdpSocket(this);

    QHostAddress server_address("::");
    socket->bind(server_address, QUAD_CONTROL_PORT);

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

    handshake = false;

    setWindowTitle("Quadcopter Stabilization Center");
    ui->powerSlider->installEventFilter(this);
}

bool MainWindow::eventFilter(QObject* watched, QEvent* event)
{
    if (event->type() == QEvent::KeyPress )
    {
        QKeyEvent* keyEvent = (QKeyEvent*)event;
        if (keyEvent->key()== Qt::Key_W)
        {
            //qDebug() << "move slider";
            ui->powerSlider->setSliderPosition(ui->powerSlider->sliderPosition()+1);
            on_powerSlider_sliderPressed();
        }
        else if (keyEvent->key()== Qt::Key_S)
        {
            //qDebug() << "down slider";
            ui->powerSlider->setSliderPosition(ui->powerSlider->sliderPosition()-1);
            on_powerSlider_sliderPressed();
        }
        else if(keyEvent->key()== Qt::Key_Space){
            on_emergencyButton_clicked();
        }
        ///
        else if(keyEvent->key()== Qt::Key_8){
            if(handshake){
                uint8_t buffer[ANGLE_PACKET_SIZE];
                quad_control_packet pck;

                pck.angle_x = 2;
                pck.angle_y = 0;
                pck.angle_z = 0;
                pck.packet_type = ANGLE_PACKET;

                create_quad_packet(&pck, buffer);
                SendtoQuadCopter(buffer, ANGLE_PACKET_SIZE);
             }
        }
        else if(keyEvent->key()== Qt::Key_2){
            if(handshake){
                uint8_t buffer[ANGLE_PACKET_SIZE];
                quad_control_packet pck;

                pck.angle_x = -2;
                pck.angle_y = 0;
                pck.angle_z = 0;
                pck.packet_type = ANGLE_PACKET;

                create_quad_packet(&pck, buffer);
                SendtoQuadCopter(buffer, ANGLE_PACKET_SIZE);
             }
        }
        else if(keyEvent->key()== Qt::Key_6){
            if(handshake){
                uint8_t buffer[ANGLE_PACKET_SIZE];
                quad_control_packet pck;

                pck.angle_x = 0;
                pck.angle_y = 0;
                pck.angle_z = 2;
                pck.packet_type = ANGLE_PACKET;

                create_quad_packet(&pck, buffer);
                SendtoQuadCopter(buffer, ANGLE_PACKET_SIZE);
             }
        }
        else if(keyEvent->key()== Qt::Key_4){
            if(handshake){
                uint8_t buffer[ANGLE_PACKET_SIZE];
                quad_control_packet pck;

                pck.angle_x = 0;
                pck.angle_y = 0;
                pck.angle_z = -2;
                pck.packet_type = ANGLE_PACKET;

                create_quad_packet(&pck, buffer);
                SendtoQuadCopter(buffer, ANGLE_PACKET_SIZE);
             }
        }
        else if(keyEvent->key()== Qt::Key_5){
            if(handshake){
                uint8_t buffer[ANGLE_PACKET_SIZE];
                quad_control_packet pck;

                pck.angle_x = 0;
                pck.angle_y = 0;
                pck.angle_z = 0;
                pck.packet_type = ANGLE_PACKET;

                create_quad_packet(&pck, buffer);
                SendtoQuadCopter(buffer, ANGLE_PACKET_SIZE);
             }
        }

    }
    return QMainWindow::eventFilter(watched, event);
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupPlot(void){
    /*QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));

    ui->customPlot->setBackground(plotGradient);*/
    ui->customPlot->xAxis->setLabel("TIME");
    ui->customPlot->yAxis->setLabel("ERROR");

    ui->customPlot->legend->setVisible(true);
    ui->customPlot->legend->setFont(QFont("Helvetica",9));

    ui->customPlot->addGraph(); // blue line
    ui->customPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->customPlot->graph(0)->setName("Pitch Error");

    ui-> customPlot->addGraph(); // red line
    ui->customPlot->graph(1)->setPen(QPen(QColor(255, 110, 40)));
    ui->customPlot->graph(1)->setName("Roll Error");

    ui-> customPlot->addGraph(); // green line
    ui->customPlot->graph(2)->setPen(QPen(Qt::green));
    ui->customPlot->graph(2)->setName("Yaw Error");

    ui->customPlot->axisRect()->setupFullAxesBox();
    ui->customPlot->yAxis->setRange(-8, 8);

    // make left and bottom axes transfer their ranges to right and top axes:
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));

    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}

void MainWindow::on_pidXButton_clicked()
{
    if(handshake){
        uint8_t buffer[PARAMETER_PACKET_SIZE];

        quad_control_packet pck;
        pck.pid_type = 1;
        pck.pk = fix16_from_float(ui->Kpx->text().toFloat());
        pck.ik = fix16_from_float(ui->Kix->text().toFloat());
        pck.dk = fix16_from_float(ui->Kdx->text().toFloat());
        pck.packet_type = PARAMETER_PACKET;

        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, PARAMETER_PACKET_SIZE);
    }
}

void MainWindow::on_pidYButton_clicked()
{
    if(handshake){
        uint8_t buffer[PARAMETER_PACKET_SIZE];
        quad_control_packet pck;

        pck.pid_type = 2;
        pck.pk = fix16_from_float(ui->Kpy->text().toFloat());
        pck.ik = fix16_from_float(ui->Kiy->text().toFloat());
        pck.dk = fix16_from_float(ui->Kdy->text().toFloat());
        pck.packet_type = PARAMETER_PACKET;

        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, PARAMETER_PACKET_SIZE);
    }
}

void MainWindow::on_pidZButton_clicked()
{
    if(handshake){
        uint8_t buffer[PARAMETER_PACKET_SIZE];
        quad_control_packet pck;

        pck.pid_type = 3;
        pck.pk = fix16_from_float(ui->Kpz->text().toFloat());
        pck.ik = fix16_from_float(ui->Kiz->text().toFloat());
        pck.dk = fix16_from_float(ui->Kdz->text().toFloat());
        pck.packet_type = PARAMETER_PACKET;

        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, PARAMETER_PACKET_SIZE);
    }
}


void MainWindow::on_setPointButton_clicked()
{
    if(handshake){
        uint8_t buffer[ANGLE_PACKET_SIZE];
        quad_control_packet pck;

        pck.angle_x = ui->setPointX->text().toInt();
        pck.angle_y = ui->setPointY->text().toInt();
        pck.angle_z = ui->setPointZ->text().toInt();
        pck.packet_type = ANGLE_PACKET;

        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, ANGLE_PACKET_SIZE);
     }
}

void MainWindow::on_emergencyButton_clicked()
{
    if(handshake){
        uint8_t buffer[EMERGENCY_PACKET_SIZE];
        quad_control_packet pck;
        pck.packet_type = EMERGENCY_PACKET;
        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, EMERGENCY_PACKET_SIZE);
        /*SendtoQuadCopter(buffer, PACKET_SIZE);
        SendtoQuadCopter(buffer, PACKET_SIZE);*/
    }

}


void MainWindow::on_powerSlider_sliderPressed()
{
    if(handshake){
        uint8_t buffer[POWER_PACKET_SIZE];
        quad_control_packet pck;

        pck.power = ui->powerSlider->value();

        pck.packet_type = POWER_PACKET;

        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, POWER_PACKET_SIZE);
    }
}



void MainWindow::replot(float time,int8_t pitch,int8_t roll,int8_t yaw)
{

    ui->customPlot->graph(0)->addData(time, pitch);
   // ui->customPlot->graph(1)->addData(time, roll);
    //ui->customPlot->graph(2)->addData(time, yaw);
    // rescale value (vertical) axis to fit the current data:
    ui->customPlot->graph(0)->rescaleValueAxis(true);
    ui->customPlot->graph(1)->rescaleValueAxis(true);
    ui->customPlot->graph(2)->rescaleValueAxis();


    // make key axis range scroll with the data (at a constant range size of 8):
    ui->customPlot->xAxis->setRange(time, 8, Qt::AlignRight);
    ui->customPlot->replot();

}


void MainWindow::on_connectButton_clicked()
{
    const int quad_id = 613468;
    QByteArray ba((const char *)&quad_id, sizeof(quad_id));

    socket->writeDatagram(ba, quadAddress, QUAD_CONTROL_PORT);
    /*if(udpserver->create_server() == true)
        ui->statusBar->showMessage("Connection Established",2000);
    else
        ui->statusBar->showMessage("Connection Failed",2000);*/
}

void MainWindow::SendtoQuadCopter(uint8_t * buffer,int size)
{
    QByteArray Data;

    Data.append((char *)buffer,size);

    // Sends the datagram datagram
    // to the host address and at port.
    // qint64 QUdpSocket::writeDatagram(const QByteArray & datagram,
    //                      const QHostAddress & host, quint16 port)
    socket->writeDatagram(Data, quadAddress, QUAD_CONTROL_PORT);
}



void MainWindow::readyRead()
{
    static float time = 0;
    // when data comes in
    QByteArray buffer;
    buffer.resize(ERROR_PACKET_SIZE);

    // qint64 QUdpSocket::readDatagram(char * data, qint64 maxSize,
    //                 QHostAddress * address = 0, quint16 * port = 0)
    // Receives a datagram no larger than maxSize bytes and stores it in data.
    // The sender's host address and port is stored in *address and *port
    // (unless the pointers are 0).

    socket->readDatagram(buffer.data(), buffer.size());
    if(handshake){
        quad_control_packet pck;
        parse_quad_packet((uint8_t *)buffer.data(),&pck,ERROR_PACKET_SIZE);
        for(int i=0; i < ERROR_PACKET_SIZE-1; i++){
            time += 0.05;
            replot(time,pck.error[i],0,0);
            /*ui->progressBarM1->setValue(pck.pow_M1[i]);
            ui->progressBarM2->setValue(pck.pow_M2[i]);
            ui->progressBarM3->setValue(pck.pow_M3[i]);
            ui->progressBarM4->setValue(pck.pow_M4[i]);*/
        }
    }else{
        handshake = true;
    }
}





void MainWindow::on_powerSlider_valueChanged(int value)
{

}

void MainWindow::on_powerSlider_sliderMoved(int position)
{

}


void MainWindow::on_backButton_clicked()
{
    if(handshake){
        uint8_t buffer[BACK_PACKET_SIZE];
        quad_control_packet pck;

        pck.back = fix16_from_float(ui->lineBack->text().toFloat());
        pck.packet_type = BACK_PACKET;

        create_quad_packet(&pck, buffer);
        SendtoQuadCopter(buffer, BACK_PACKET_SIZE);
    }
}

void MainWindow::on_powerSlider_sliderReleased()
{

}
