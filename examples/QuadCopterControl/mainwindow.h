#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

    void replot(float time,int8_t pitch,int8_t roll,int8_t yaw);

    bool eventFilter(QObject* obj, QEvent* event);

    void SendtoQuadCopter(uint8_t * buffer,int size);

private slots:
    void setupPlot();

    void on_pidXButton_clicked();

    void on_connectButton_clicked();

    void on_pidYButton_clicked();

    void on_emergencyButton_clicked();

    void on_powerSlider_sliderPressed();

    void on_powerSlider_sliderMoved(int position);

    void on_powerSlider_valueChanged(int value);

    void on_pidZButton_clicked();

    void on_setPointButton_clicked();

    void on_recordButton_clicked();

    void on_backButton_clicked();

    void on_powerSlider_sliderReleased();

public slots:
    void readyRead();

private:
    Ui::MainWindow *ui;
    QUdpSocket *socket;
    QHostAddress quadAddress;
    bool handshake;
};


#endif // MAINWINDOW_H
