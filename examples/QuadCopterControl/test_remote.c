#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include "fix16.h"
#include "packet_parser.h"

#define QUAD_CONTROL_PORT    3461

static quad_control_packet pck;
static quad_control_packet pck_recv;

void send_test(void *);
void recv_test(void *);

uint8_t packet[PACKET_SIZE];
uint8_t packet_recv[PACKET_SIZE];

int sockfd;
struct sockaddr_in6 quad_conn,remote_conn;
FILE *fp,*fp1;

int
main(int argc, char *argv[])
{  
 
  const int quad_id = 613468;
  const char *quad_adress = "daaa::212:4b00:430:569c"; // For Mahmote 156
  
  
  sockfd = socket(AF_INET6, SOCK_DGRAM, 0);
  if(sockfd < 0){
    fprintf(stderr, "ERROR opening socket!\n");
    exit(0);
  }

  memset((char *)&remote_conn, 0, sizeof(remote_conn));
  remote_conn.sin6_flowinfo = 0;
  remote_conn.sin6_family = AF_INET6;
  remote_conn.sin6_addr = in6addr_any;
  remote_conn.sin6_port = htons((int)QUAD_CONTROL_PORT);

  memset((char *)&quad_conn, 0, sizeof(quad_conn));
  quad_conn.sin6_flowinfo = 0;
  quad_conn.sin6_family = AF_INET6;
  quad_conn.sin6_addr = in6addr_any;
  quad_conn.sin6_port = htons(QUAD_CONTROL_PORT);

  if(inet_pton(AF_INET6,quad_adress,&quad_conn.sin6_addr) == 0){
    fprintf(stderr, "inet_pton error!\n");
    exit(0);
  }

  if( bind(sockfd, (struct sockaddr *) &remote_conn, sizeof(remote_conn)) < 0){
   
    printf("Error: When remote binding\n");
    exit(-1);
  }

  sendto(sockfd, &quad_id, 1, 0, (struct sockaddr*) &quad_conn, sizeof(quad_conn));
  
  
  short ack_value;
  //int n = recv(sockfd, &ack_value, sizeof(ack_value), 0);
  ssize_t n = recvfrom(sockfd,&ack_value,sizeof(ack_value),0,
                        (struct sockaddr *)quad_conn, sizeof(quad_conn));

  if(n < 0){
    printf("ERROR reading from socket");
  }
  printf("ack_value: %u\n",ack_value);

  remove("gnuplot/hata.txt");
  remove("gnuplot/aci.txt");

  pthread_t id[2];
  if(pthread_create(&id[0],NULL,send_test,NULL) != 0){
    exit(1);
  }
      
  if(pthread_create(&id[1],NULL,recv_test,NULL) != 0){
    exit(1);
  }
  
  pthread_join(&id[0],NULL);
  pthread_join(&id[1],NULL);

  close(sockfd);
  close(fp);
  close(fp1);
 
  return 0;
}

void send_test(void * x){
  int type;
  int p_type;
  float pk,dk,ik;
  int power;
    int16_t angle_x;
    int16_t angle_y;
    int16_t angle_z;
  while(1){
    printf("paket türü normal -->1 ,test -->2, acil durum -->3 ,güc --> 4\n");
    scanf("%d",&type);
    if(type==3){
        packet[PACKET_SIZE-1] = EMERGENCY_PACKET;
        sendto(sockfd, packet, sizeof(packet), 0, (struct sockaddr*)&quad_conn, sizeof(quad_conn));
        sendto(sockfd, packet, sizeof(packet), 0, (struct sockaddr*)&quad_conn, sizeof(quad_conn));
        sendto(sockfd, packet, sizeof(packet), 0, (struct sockaddr*)&quad_conn, sizeof(quad_conn));
        
    }
    else if(type==1){
       printf("sirasi ile acilar x y z \n");
       scanf("%d %d %d",&angle_x,&angle_y,&angle_z);
       pck.angle_x = angle_x;
       pck.angle_y = angle_y;
       pck.angle_z = angle_z;
       pck.packet_type = NORMAL_PACKET;
       create_quad_packet(&pck,packet);
       sendto(sockfd, packet, sizeof(packet), 0, (struct sockaddr*)&quad_conn, sizeof(quad_conn));
       
    }
    else if(type ==2)
    {
      printf("pid türünü girin x -->1 , y-->2 ");
      scanf("%d",&p_type);
      pck.pid_type = p_type;
      printf("sıra ile pk  ik dk\n");
      scanf("%f %f %f",&pk,&ik,&dk);
      pck.pk = fix16_from_float(pk);
      pck.dk = fix16_from_float(dk);
      pck.ik = fix16_from_float(ik);
      pck.packet_type = TEST_PACKET;
      printf("pk-> %u dk-> %u ik-> %u \n",pck.pk,pck.dk,pck.ik);
      create_quad_packet(&pck, packet);
      sendto(sockfd, packet, sizeof(packet), 0, (struct sockaddr*) &quad_conn, sizeof(quad_conn));
    }
    
    else if(type ==4){
        printf("motor gücünü giriniz 13-45\n");
       scanf("%d",&power);
       pck.power = power;
       pck.packet_type = POWER_PACKET;
       create_quad_packet(&pck,packet);
       sendto(sockfd, packet, sizeof(packet), 0, (struct sockaddr*)&quad_conn, sizeof(quad_conn));
    }
  }
}

void recv_test(void * y)
{
  float time = 0; 
  if ((fp = fopen ("gnuplot/hata.txt", "a+")) == NULL) {
      //printf("Dosya açma hatası!");
      pthread_exit(NULL);
  }
  if ((fp1 = fopen ("gnuplot/aci.txt", "a+")) == NULL) {
      //printf("Dosya açma hatası!");
      pthread_exit(NULL);
  }
  printf("File Pointer:  %d\n",fp);
  while(1){
    recvfrom(sockfd,packet_recv,sizeof(packet_recv),0,
                        (struct sockaddr *)quad_conn, sizeof(quad_conn));
    parse_quad_packet(packet_recv ,&pck_recv,PACKET_SIZE);
    if(pck_recv.packet_type == ERROR_PACKET){
   // printf("alinan %u \n",pck_recv.time);
   // printf("time %f \n",fix16_to_float(pck_recv.time));
    time+=fix16_to_float(pck_recv.time);
    fprintf (fp,"%f %f %f %f \n",time , fix16_to_float(pck_recv.error_x), fix16_to_float(pck_recv.error_y), fix16_to_float(pck_recv.error_z));
    fflush(fp);
    }else if (pck_recv.packet_type == SENSOR_PACKET){
        time+=fix16_to_float(pck_recv.time);
        fprintf (fp1,"%f %f %f %f \n",time , fix16_to_float(pck_recv.filter_x), fix16_to_float(pck_recv.filter_y), fix16_to_float(pck_recv.filter_z));
        fflush(fp1);
    
    }
  }
}