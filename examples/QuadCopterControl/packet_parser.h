#ifndef PACKET_PARSER_H_
#define PACKET_PARSER_H_

#include "fix16.h"


// PACKET TYPE
#define ANGLE_PACKET      0
#define PARAMETER_PACKET  1
#define POWER_PACKET      2
#define EMERGENCY_PACKET  3
#define FEEDBACK_PACKET	  4
#define ERROR_PACKET      5
#define BACK_PACKET	      7


// PACKET SIZE

#define ANGLE_PACKET_SIZE 	   4
#define PARAMETER_PACKET_SIZE 14
#define POWER_PACKET_SIZE	   3
#define EMERGENCY_PACKET_SIZE  1
#define FEEDBACK_PACKET_SIZE  56
#define ERROR_PACKET_SIZE	  51
#define BACK_PACKET_SIZE	   5

typedef struct quad_control {

    uint8_t packet_type;

    //ANGLE PACKET
    int8_t angle_x;
    int8_t angle_y;
    int8_t angle_z;

    //PARAMETER PACKET

    fix16_t pk;
    fix16_t dk;
    fix16_t ik;
    uint8_t pid_type;	// 0 -> X  1 -> Y // 2 -> Z

    //POWER PACKET

    uint16_t power;

    // FEEDBACK_PACKET

    uint16_t pow_M1[5];
    uint16_t pow_M2[5];
    uint16_t pow_M3[5];
    uint16_t pow_M4[5];

    int8_t err_x[5];
    int8_t err_y[5];
    int8_t err_z[5];

    // ERROR_PACKET

    int8_t error[50];

    fix16_t back;

} quad_control_packet;


void create_quad_packet(const quad_control_packet *packet, uint8_t *buffer);
void parse_quad_packet(const uint8_t * buffer ,quad_control_packet *packet ,int len);


#endif /* PACKET_PARSER_H_ */
