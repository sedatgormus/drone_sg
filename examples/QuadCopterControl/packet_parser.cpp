#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "packet_parser.h"

void
create_quad_packet(const quad_control_packet *packet, uint8_t *buffer)
{
    switch(packet->packet_type)
    {


        case ANGLE_PACKET:

            memset(buffer,0,ANGLE_PACKET_SIZE);

            buffer[ANGLE_PACKET_SIZE-1] = ANGLE_PACKET;

            buffer[ANGLE_PACKET_SIZE -2] = packet->angle_z;
            buffer[ANGLE_PACKET_SIZE -3] = packet->angle_y;
            buffer[ANGLE_PACKET_SIZE -4] = packet->angle_x;

            break;

        case PARAMETER_PACKET:

            memset(buffer,0,PARAMETER_PACKET_SIZE);

            buffer[PARAMETER_PACKET_SIZE-1] = PARAMETER_PACKET;

            buffer[PARAMETER_PACKET_SIZE -2] = packet->pk;
            buffer[PARAMETER_PACKET_SIZE -3] = (packet->pk >> 8);
            buffer[PARAMETER_PACKET_SIZE -4] = (packet->pk >> 16);
            buffer[PARAMETER_PACKET_SIZE -5] = (packet->pk >> 24);

            buffer[PARAMETER_PACKET_SIZE -6] = packet->ik;
            buffer[PARAMETER_PACKET_SIZE -7] = (packet->ik >> 8);
            buffer[PARAMETER_PACKET_SIZE -8] = (packet->ik >> 16);
            buffer[PARAMETER_PACKET_SIZE -9] = (packet->ik >> 24);

            buffer[PARAMETER_PACKET_SIZE -10] = packet->dk;
            buffer[PARAMETER_PACKET_SIZE -11] = (packet->dk >> 8);
            buffer[PARAMETER_PACKET_SIZE -12] = (packet->dk >> 16);
            buffer[PARAMETER_PACKET_SIZE -13] = (packet->dk >> 24);

            buffer[PARAMETER_PACKET_SIZE -14] = packet->pid_type;

            break;

        case POWER_PACKET:

            memset(buffer,0,POWER_PACKET_SIZE);

            buffer[POWER_PACKET_SIZE-1] = POWER_PACKET;

            buffer[POWER_PACKET_SIZE -2] = packet->power;
            buffer[POWER_PACKET_SIZE -3] = (packet->power >> 8);

            break;

        case EMERGENCY_PACKET:

            memset(buffer,0,EMERGENCY_PACKET_SIZE);

            buffer[EMERGENCY_PACKET_SIZE-1] = EMERGENCY_PACKET;

            break;

        case FEEDBACK_PACKET:

            memset(buffer,0,FEEDBACK_PACKET_SIZE);

            buffer[FEEDBACK_PACKET_SIZE-1] = FEEDBACK_PACKET;

            uint8_t i;
            for(i=0;i<5;i++)
            {

                buffer[FEEDBACK_PACKET_SIZE - (i+2)]  = packet->err_x[i];
                buffer[FEEDBACK_PACKET_SIZE - (i+7)]  = packet->err_y[i];
                buffer[FEEDBACK_PACKET_SIZE - (i+12)] = packet->err_z[i];

                buffer[FEEDBACK_PACKET_SIZE -(2*i+17)] = packet->pow_M1[i];
                buffer[FEEDBACK_PACKET_SIZE -(2*i+18)] = (packet->pow_M1[i] >> 8);

                buffer[FEEDBACK_PACKET_SIZE -(2*i+27)] = packet->pow_M2[i];
                buffer[FEEDBACK_PACKET_SIZE -(2*i+28)] = (packet->pow_M2[i] >> 8);

                buffer[FEEDBACK_PACKET_SIZE -(2*i+37)] = packet->pow_M3[i];
                buffer[FEEDBACK_PACKET_SIZE -(2*i+38)] = (packet->pow_M3[i] >> 8);

                buffer[FEEDBACK_PACKET_SIZE -(2*i+47)] = packet->pow_M4[i];
                buffer[FEEDBACK_PACKET_SIZE -(2*i+48)] = (packet->pow_M4[i] >> 8);
            }

            break;

        case ERROR_PACKET:

            memset(buffer,0,ERROR_PACKET_SIZE);

            buffer[ERROR_PACKET_SIZE-1] = ERROR_PACKET;
                        uint8_t j;
            for(j=0;j<ERROR_PACKET_SIZE-1;j++)
            {
                buffer[ERROR_PACKET_SIZE - (j+2)] = packet->error[j];

            }
            break;

        case BACK_PACKET:

            memset(buffer,0,BACK_PACKET_SIZE);

            buffer[BACK_PACKET_SIZE-1] = BACK_PACKET;

            buffer[BACK_PACKET_SIZE -2] = packet->back;
            buffer[BACK_PACKET_SIZE -3] = (packet->back >> 8);
            buffer[BACK_PACKET_SIZE -4] = (packet->back >> 16);
            buffer[BACK_PACKET_SIZE -5] = (packet->back >> 24);
            break;
    }
}



void
parse_quad_packet(const uint8_t * buffer ,quad_control_packet *packet, int len)
{
    switch((buffer[len -1]))
    {

        case ANGLE_PACKET:

            packet->packet_type = ANGLE_PACKET;

            packet->angle_z = buffer[ANGLE_PACKET_SIZE -2];
            packet->angle_y = buffer[ANGLE_PACKET_SIZE -3];
            packet->angle_x = buffer[ANGLE_PACKET_SIZE -4];

            break;

        case PARAMETER_PACKET:

            packet->packet_type = PARAMETER_PACKET;

            packet->pk = buffer[PARAMETER_PACKET_SIZE - 5];
            packet->pk = (packet->pk << 8) | buffer[PARAMETER_PACKET_SIZE - 4];
            packet->pk = (packet->pk << 8) | buffer[PARAMETER_PACKET_SIZE - 3];
            packet->pk = (packet->pk << 8) | buffer[PARAMETER_PACKET_SIZE - 2];

            packet->ik = buffer[PARAMETER_PACKET_SIZE - 9];
            packet->ik = (packet->ik << 8) | buffer[PARAMETER_PACKET_SIZE - 8];
            packet->ik = (packet->ik << 8) | buffer[PARAMETER_PACKET_SIZE - 7];
            packet->ik = (packet->ik << 8) | buffer[PARAMETER_PACKET_SIZE - 6];

            packet->dk = buffer[PARAMETER_PACKET_SIZE - 13];
            packet->dk = (packet->dk << 8) | buffer[PARAMETER_PACKET_SIZE - 12];
            packet->dk = (packet->dk << 8) | buffer[PARAMETER_PACKET_SIZE - 11];
            packet->dk = (packet->dk << 8) | buffer[PARAMETER_PACKET_SIZE - 10];

            packet->pid_type = buffer[PARAMETER_PACKET_SIZE - 14];

            break;

        case POWER_PACKET:

            packet->packet_type = POWER_PACKET;

            packet->power = buffer[POWER_PACKET_SIZE - 3];
            packet->power = (packet->power << 8) | buffer[POWER_PACKET_SIZE - 2];

            break;

        case EMERGENCY_PACKET:

            packet->packet_type = EMERGENCY_PACKET;

            break;


        case  FEEDBACK_PACKET:

            packet->packet_type = FEEDBACK_PACKET;

            uint8_t i;
            for(i=0;i<5;i++)
            {
                packet->err_x[i] = buffer[FEEDBACK_PACKET_SIZE - (i+2)];
                packet->err_y[i] = buffer[FEEDBACK_PACKET_SIZE - (i+7)];
                packet->err_z[i] = buffer[FEEDBACK_PACKET_SIZE - (i+12)];

                packet->pow_M1[i] = buffer[FEEDBACK_PACKET_SIZE -(2*i+18)];
                packet->pow_M1[i] = (packet->pow_M1[i] << 8) | buffer[FEEDBACK_PACKET_SIZE -(2*i+17)];

                packet->pow_M2[i] = buffer[FEEDBACK_PACKET_SIZE -(2*i+28)];
                packet->pow_M2[i] = (packet->pow_M2[i] << 8) | buffer[FEEDBACK_PACKET_SIZE -(2*i+27)];

                packet->pow_M3[i] = buffer[FEEDBACK_PACKET_SIZE -(2*i+38)];
                packet->pow_M3[i] = (packet->pow_M3[i] << 8) | buffer[FEEDBACK_PACKET_SIZE -(2*i+37)];

                packet->pow_M4[i] = buffer[FEEDBACK_PACKET_SIZE -(2*i+48)];
                packet->pow_M4[i] = (packet->pow_M4[i] << 8) | buffer[FEEDBACK_PACKET_SIZE -(2*i+47)];
            }

            break;

        case ERROR_PACKET:

            packet->packet_type = ERROR_PACKET;
                        uint8_t j;
            for(j=0;j<ERROR_PACKET_SIZE-1;j++)
            {
                packet->error[j] = buffer[ERROR_PACKET_SIZE - (j+2)] ;

            }
            break;

        case BACK_PACKET:

            packet->packet_type = BACK_PACKET;

            packet->back = buffer[BACK_PACKET_SIZE - 5];
            packet->back = (packet->back << 8) | buffer[BACK_PACKET_SIZE - 4];
            packet->back = (packet->back << 8) | buffer[BACK_PACKET_SIZE - 3];
            packet->back = (packet->back << 8) | buffer[BACK_PACKET_SIZE - 2];
            break;
    }
}
















































