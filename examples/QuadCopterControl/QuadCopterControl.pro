#-------------------------------------------------
#
# Project created by QtCreator 2016-12-02T10:16:33
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = QuadCopterControl
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    packet_parser.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    packet_parser.h \
    fix16.h

FORMS    += mainwindow.ui
