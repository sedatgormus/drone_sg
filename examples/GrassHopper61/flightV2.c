#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include "dev/pwm.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>

#include "mpudriver.h"
#include "math.h"
#include "pid.h"
#include "fix16.h"
#include "control.h"
#include "packet_parser.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define QUAD_CONTROL_PORT    3461

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"

#define MOTOR_FRONT_LEFT  1
#define MOTOR_FRONT_RIGHT 2
#define MOTOR_BACK_LEFT   3
#define MOTOR_BACK_RIGHT  4
#define MOTOR_ALL         5

static struct uip_udp_conn *quad_conn;

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define PWM_FREQUENCY 400
#define DUTY_MAX 959
#define DUTY_MIN 5


#define SAMPLING_TIME_7MS ((clock_time_t)1)            // ~7ms

static  uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
static  uint16_t fifoCount;     // count of all bytes currently in FIFO

static uint8_t hand_shake = 0;
static uint8_t set_yaw_axis = 0;
static uip_ipaddr_t remote_ipaddr;
static euler_t set_point;
static euler_t input;
static quad_control_packet error_packet;

static void fly_control(euler_t set,euler_t reference);
static euler_t get_angles();
static void motor_pwm(uint16_t duty,uint8_t motor);
static void pwm_calibration();

static uint16_t sample_count = 0;
static uint8_t kere = 0;
static uint8_t sample =0;

static struct etimer pwm_calib_timer;

/********************** Eski Pervane Icin PID Katsayilari *********************************/


//static  float rollKp = 1.2, rollKi = 0.45, rollKd = 0.25;

//static  float pitchKp = 1.5f, pitchKi = 0.35f, pitchKd = 0.25f; // backCoff = 1.23 
/******************************************************************************************/

/******************** Yeni Pervane Icin PID Katsayilari *********************************/

/*static  float rollKp = 1.0f, rollKi = 0.3f, rollKd = 0.19f;

static  float pitchKp = 1.0f, pitchKi = 0.35f, pitchKd = 0.2f;  

static  float yawKp = 1.0f, yawKi = 0.15f, yawKd = 0.22f;*/

//static  float rollKp = 1, rollKi = 0.3, rollKd = 0.19;

//static  float pitchKp = 1.0f, pitchKi = 0.35f, pitchKd = 0.2f;  // backCoff = 1.18

//static  float yawKp = 1.0f, yawKi = 0.15f, yawKd = 0.22f;
/******************************************************************************************/

static  float rollKp = 1.0f, rollKi = 0.25f, rollKd = 0.3f;  //back_y = 1

static  float pitchKp = 1.55f, pitchKi = 0.25f, pitchKd = 0.31f;  // backCoff = 1.2

static  float yawKp = 1.55f, yawKi = 0.25f, yawKd = 0.3f;

/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  quad_control_packet pac;
  char * buffer = uip_appdata;
  if(uip_newdata()){  
    parse_quad_packet(buffer,&pac,uip_datalen());  
    if(hand_shake){
      switch(pac.packet_type)
      {      
        case POWER_PACKET:
          PRINTF("POWER Packet is arrived\n");
          if(pac.power < MIN_THROTTLE){
            pid_set_coefficients(pitchKp,0,pitchKd,1);
            pid_set_coefficients(rollKp,0,rollKd,2);
            pid_set_coefficients(yawKp,0,yawKd,3);
            pid_reset();
          }else{
            pid_set_coefficients(pitchKp,pitchKi,pitchKd,1);
            pid_set_coefficients(rollKp,rollKi,rollKd,2);
            pid_set_coefficients(yawKp,yawKi,yawKd,3);
          }
          control_set_throttle((float)pac.power);
          PRINTF("POWER: %d\n",pac.power);
          break;

        case PARAMETER_PACKET:
          PRINTF("PARAMETER PACKET is arrived\n");
          switch(pac.pid_type){
            case 1:
              pitchKp = fix16_to_float(pac.pk);
              pitchKi = fix16_to_float(pac.ik);
              pitchKd = fix16_to_float(pac.dk);
              pid_set_coefficients(pitchKp,pitchKi,pitchKd,1);
              PRINTF("Pitch Gains: p: %d i: %d d: %d\n",(int16_t)(pitchKp*1000.0f), (int16_t)(pitchKi*1000.0f),(int16_t)(pitchKd*1000.0f));
              break;
            case 2:
              rollKp = fix16_to_float(pac.pk);
              rollKi = fix16_to_float(pac.ik);
              rollKd = fix16_to_float(pac.dk);
              pid_set_coefficients(rollKp,rollKi,rollKd,2);
              PRINTF("Roll Gains: p: %d i: %d d: %d\n",(int16_t)(rollKp*1000.0f), (int16_t)(rollKi*1000.0f),(int16_t)(rollKd*1000.0f));
              break;
            case 3:
              yawKp = fix16_to_float(pac.pk);
              yawKi = fix16_to_float(pac.ik);
              yawKd = fix16_to_float(pac.dk);
              pid_set_coefficients(yawKp,yawKi,yawKd,3);
              PRINTF("Yaw Gains: p: %d i: %d d: %d\n",(int16_t)(yawKp*1000.0f), (int16_t)(yawKi*1000.0f),(int16_t)(yawKd*1000.0f));
              break;
            default:
              PRINTF("UNKNOWN\n");
              break;
          }
          pid_reset();
          break;

        case EMERGENCY_PACKET:
          PRINTF("EMERGENCY PACKET is arrived\n");
          pid_set_coefficients(pitchKp,0,pitchKd,1);
          pid_set_coefficients(rollKp,0,rollKd,2);
          pid_set_coefficients(yawKp,0,yawKd,3);
          pid_reset();
          control_set_throttle((float)STOP_THROTTLE);
          break;

        case ANGLE_PACKET:
          PRINTF("ANGLE PACKET is arrived\n");
          if(pac.angle_x == 0 && pac.angle_y == 0 && pac.angle_z == 0){
            set_point.x = 0;
            set_point.y = 0;
            set_point.z = 0;
          }else{
            set_point.x = fix16_to_float(pac.angle_x);
            set_point.y = fix16_to_float(pac.angle_y);
            set_point.z += fix16_to_float(pac.angle_z); 
          }
          PRINTF("Set P: x %d y %d z %d\n", (int)(set_point.x*100.0f),(int)(set_point.y*100.0f),(int)(set_point.z*100.0f));
          break; 

        case BACK_PACKET:
         PRINTF("BACK PACKET is arrived\n");
         PRINTF("Type: %d Back coff: %d\n",pac.backType,(uint16_t)(fix16_to_float(pac.backValue)*1000.0f));
         setBackCoff(fix16_to_float(pac.backValue),pac.backType);
         break; 
        
        default:
          PRINTF("UNKNOWN PACKET TYPE\n");
          break;
      }
    }else{
      PRINTF("HandShake Packet is send\n");
      uip_ipaddr_copy(&remote_ipaddr, &UIP_IP_BUF->srcipaddr);
      uint8_t response = 12;
      pwm_calibration();
      etimer_set(&pwm_calib_timer,CLOCK_SECOND*3);
      uip_udp_packet_sendto(quad_conn,(void *)&response,sizeof(uint8_t), &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xdaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
  PRINTF("IP ADDRESS: ");
  PRINT6ADDR(&ipaddr);
  PRINTF("\n");
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  static struct etimer periodic;
  
  PROCESS_BEGIN();

  PROCESS_PAUSE();

  set_global_address();

  PRINTF("GRASSHOPPER61 FLIGHT CONTROLLER TEST\n");

 if(!mpu6050_testConnection()){
    // Can not connect to MPU6050 sensor. 
    PRINTF("Cannot Connect Mpu6050\n");
    PROCESS_EXIT();                          
  }


  if(mpu6050_getDMPEnabled()){
    PRINTF("Resetting DMP\n");
    packetSize = MPU6050_DEFAULT_PACKET_SIZE;
    fifoCount = 0;
    mpu6050_resetFIFO();
  }else{
    mpu6050_initialize();
    uint8_t devStatus = mpu6050_dmpInitialize();
  
    if(devStatus == 0) {
      // turn on the DMP, now that it's ready
      PRINTF("Enabling DMP...\n");
      mpu6050_setDMPEnabled(1);
      fifoCount = 0;
      // get expected DMP packet size for later comparison
      packetSize = mpu6050_dmpGetFIFOPacketSize();
    }else {
      PRINTF("DMP Initilaze Failed\n");
      PROCESS_EXIT();
    }
  }

  
  /* new connection with remote host */
  quad_conn = udp_new(NULL, UIP_HTONS(0), NULL); 
  if(quad_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(quad_conn, UIP_HTONS(QUAD_CONTROL_PORT)); 
  
  PRINTF("PID COEFFICIENTS ARE SET\n");
  pid_initilize(pitchKp, pitchKi, pitchKd, rollKp, rollKi, rollKd, yawKp , yawKi ,yawKd); 

  control_initilize();

  set_point.x = 0.5f;
  set_point.y = 0;
  set_point.z = 0;
 
 
#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */

  etimer_set(&periodic,CLOCK_SECOND*5);
  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
    }else if(data == &periodic){

      input = get_angles();
      fly_control(set_point,input);
      etimer_set(&periodic,SAMPLING_TIME_7MS);

    }else if(data == &pwm_calib_timer){

      motor_pwm(DUTY_MIN,MOTOR_ALL);
      etimer_stop(&pwm_calib_timer);
      hand_shake = 1;
      set_yaw_axis = 1;
    }
  }

  PROCESS_END();
}

static void 
send_to_remote(unsigned char * data,int len)
{
  uip_udp_packet_sendto(quad_conn,data, len, &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
}


static euler_t
get_angles()
{
  euler_t result;
  quaternion_t q;           // [w, x, y, z]         quaternion container
  vector_float_t gravity;    // [x, y, z]            gravity vector
  float ypr[3];
  uint8_t fifoBuffer[packetSize]; // FIFO storage buffer

  fifoCount = mpu6050_getFIFOCount();
    // wait for MPU interrupt or extra packet(s) available
  while (fifoCount < packetSize) {
    //PRINTF("FIFO nun dolması bekleniyor\n");
    fifoCount = mpu6050_getFIFOCount();
  }

    // check for overflow (this should never happen unless our code is too inefficient)
  if (fifoCount >= 1024 || packetSize > (1024-fifoCount) ) {
        // reset so we can continue cleanly
    mpu6050_resetFIFO();
    PRINTF("FIFO overflow!\n");

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  }else{
      // read a packet from FIFO
    mpu6050_getFIFOBytes(fifoBuffer, packetSize);
    
    fifoCount = mpu6050_getFIFOCount();
    if(fifoCount % packetSize == 0 && fifoCount/packetSize>=1){
      mpu6050_getFIFOBytes(fifoBuffer, packetSize);
    }
      
    mpu6050_dmpGetQuaternion_q(&q, fifoBuffer);
    mpu6050_dmpGetGravity(&gravity, &q);
    mpu6050_dmpGetYawPitchRoll(ypr, &q, &gravity);

    //PRINTF("yaw : %d roll %d pitch %d\n",(int16_t)ypr[0],(int16_t)-ypr[1],(int16_t)ypr[2]);
    if(set_yaw_axis){
      set_yaw_axis = 0;
      set_point.z = ypr[0];
      //printf("set yaw axis : %d\n", (int16_t)ypr[0]);
    }

    result.z = ypr[0];
    result.y = -ypr[1];
    result.x = ypr[2];

    return result;
  }
  
}

static uint32_t last_time_pid = 0;
  
static void 
fly_control(euler_t set_point, euler_t reference)
{
  uint32_t now = (uint32_t)clock_time();
  //PRINTF("PID TIME : %d\n",(now*1000-last_time_pid*1000)/128);
  float dt = ((float)(now - last_time_pid)) / 128.0f;
  //PRINTF("dt : %d\n", (uint16_t)(dt*10000.0f));
  last_time_pid = now;

  pidout_t rate = pid_system_calculation(set_point,reference,dt);
  
  error_packet.packet_type = ERROR_PACKET;
  if((sample % 2) == 0){
    error_packet.error[sample/2] = (int8_t)rate.error;
  }
 
  sample++; 

  if(sample == 100){
    if(hand_shake){
      uint8_t send_buffer[ERROR_PACKET_SIZE];
      create_quad_packet(&error_packet, send_buffer);
      send_to_remote(send_buffer,ERROR_PACKET_SIZE);
    }    
    sample = 0;    
  }
  /*
    Motorlara Guc verme
  */ 
  if(hand_shake) {
    //PRINTF("x: %d y :%d z: %d\n",(int16_t)rate.pidX,(int16_t)rate.pidY,(int16_t)rate.pidZ);  
    motor_t motors = control_motors(rate.pidX,rate.pidY,rate.pidZ);
    motor_pwm(motors.M1,MOTOR_FRONT_LEFT);
    motor_pwm(motors.M2,MOTOR_FRONT_RIGHT);
    motor_pwm(motors.M3,MOTOR_BACK_LEFT);
    motor_pwm(motors.M4,MOTOR_BACK_RIGHT);
    //PRINTF("M1 %d M2 :%d M3 %d M4 %d\n",motors.M1,motors.M2,motors.M3,motors.M4 );
  }
}


static void 
pwm_calibration()
{   
  
  INTERRUPTS_DISABLE();
  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_0, PWM_TIMER_B, GPIO_C_NUM, 5) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_1, PWM_TIMER_B, GPIO_C_NUM, 1) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_2, PWM_TIMER_B, GPIO_C_NUM, 6) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_3, PWM_TIMER_B, GPIO_C_NUM, 7) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  INTERRUPTS_ENABLE();
}

static void 
motor_pwm(uint16_t duty,uint8_t motor)
{
  switch(motor){
    case MOTOR_FRONT_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

   case MOTOR_FRONT_RIGHT:
      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_RIGHT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_ALL:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;
  }
}






