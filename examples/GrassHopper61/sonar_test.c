#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include <stdio.h>
#include <string.h>
#include "sonar.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  
  PROCESS_BEGIN();
  static struct etimer sonar_timer;
  PROCESS_PAUSE();
 
  PRINTF("SONAR TEST\n");
  sonar_init();

#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */
  etimer_set(&sonar_timer,CLOCK_SECOND/2);
  while(1){
    PROCESS_YIELD();
    if(data == &sonar_timer){
      uint8_t i = 0;
      float value = 0;
      for(;i<5;i++)
        value += sonar_get_value();
      value/= 5;
      printf("Value: %d\n",(int)value);
      etimer_reset(&sonar_timer);
	}
}

  PROCESS_END();
}





