#ifndef PACKET_PARSER_H_
#define PACKET_PARSER_H_

#include "fix16.h"

// PACKET TYPE
#define HANDSHAKE_PAC 0
#define CONTROL_PAC   1
#define EMERGENCY_PAC 2
#define POWER_PAC     3
#define PID_PAC       4
#define ALTITUTE_HOLD       15

// PACKET SIZE

#define HAND_PAC_SIZE           1
#define CONTROL_PAC_SIZE        17
#define EMRG_PAC_SIZE           1
#define POWER_PAC_SIZE          2//boyut belirle (10.3.17)
#define PID_PAC_SIZE            1//boyut belirle (10.3.17)
#define ALTITUTE_HOLD_PACKET_SIZE   6  // 1-TYPE 1-ıs_hold 4-YÜKSEKLİK

#define HOLD_ALT_TRUE     0
#define HOLD_ALT_FALSE    1

typedef struct quad_control {

    uint8_t packet_type;

    float pitch;
    float yaw;
    float roll;
    float power;

    //CONTROL PACKET
    fix16_t fyaw; //z
    fix16_t froll; //y
    fix16_t fpitch;//x
    fix16_t fpower;
    
    //POWER PACKET
    uint8_t on_off;

    //altitute
    uint8_t ıs_hold;
    uint16_t altitute;


} quad_control_packet;

void create_quad_packet(const quad_control_packet *packet, uint8_t *buffer);
void parse_android_packet(char * buffer ,quad_control_packet *packet);

#endif /* PACKET_PARSER_H_ */