#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "packet_parser.h"

void
create_quad_packet(const quad_control_packet *packet, uint8_t *buffer)
{
    switch(packet->packet_type)
    {

        case EMERGENCY_PAC:

            memset(buffer,0,EMRG_PAC_SIZE);

            buffer[EMRG_PAC_SIZE-1] = EMERGENCY_PAC;

            break;
            
        case HANDSHAKE_PAC:

            memset(buffer,0,HAND_PAC_SIZE);
            buffer[HAND_PAC_SIZE-1] = HANDSHAKE_PAC;
            
            break;
        
        case CONTROL_PAC:
            memset(buffer,0,CONTROL_PAC_SIZE);
            buffer[CONTROL_PAC_SIZE-1] = CONTROL_PAC;
            
            buffer[CONTROL_PAC_SIZE -2] = packet->fpitch;
            buffer[CONTROL_PAC_SIZE -3] = (packet->fpitch >> 8);
            buffer[CONTROL_PAC_SIZE -4] = (packet->fpitch >> 16);
            buffer[CONTROL_PAC_SIZE -5] = (packet->fpitch >> 24);

            buffer[CONTROL_PAC_SIZE -6] = packet->froll;
            buffer[CONTROL_PAC_SIZE -7] = (packet->froll >> 8);
            buffer[CONTROL_PAC_SIZE -8] = (packet->froll >> 16);
            buffer[CONTROL_PAC_SIZE -9] = (packet->froll >> 24);

            buffer[CONTROL_PAC_SIZE -10] = packet->fyaw;
            buffer[CONTROL_PAC_SIZE -11] = (packet->fyaw >> 8);
            buffer[CONTROL_PAC_SIZE -12] = (packet->fyaw >> 16);
            buffer[CONTROL_PAC_SIZE -13] = (packet->fyaw >> 24);
            
            buffer[CONTROL_PAC_SIZE -14] = packet->fpower;
            buffer[CONTROL_PAC_SIZE -15] = (packet->fpower >> 8);
            buffer[CONTROL_PAC_SIZE -16] = (packet->fpower >> 16);
            buffer[CONTROL_PAC_SIZE -17] = (packet->fpower >> 24);
            break;

        case POWER_PAC:
            memset(buffer,0,POWER_PAC_SIZE);

            buffer[POWER_PAC_SIZE-1] = POWER_PAC;
            buffer[POWER_PAC_SIZE-2] = packet->on_off;

        break;


        case ALTITUTE_HOLD:

            memset(buffer,0,ALTITUTE_HOLD_PACKET_SIZE);
            
            buffer[ALTITUTE_HOLD_PACKET_SIZE-1] = ALTITUTE_HOLD;
            buffer[ALTITUTE_HOLD_PACKET_SIZE-2] = packet->ıs_hold;

            buffer[ALTITUTE_HOLD_PACKET_SIZE -3] = packet->altitute;
            buffer[ALTITUTE_HOLD_PACKET_SIZE -4] = (packet->altitute >> 8);
        
            break;






    }
}



void
parse_android_packet(char * buffer ,quad_control_packet *packet)
{
    const char slide[1] = "*";
    char *token;
    int counter = 0; 
    token = strtok(buffer,slide);
    packet->packet_type = atoi(token);    
        
    switch(packet->packet_type){
    
        case HANDSHAKE_PAC:
            packet->packet_type = HANDSHAKE_PAC;
            break;
        
        case CONTROL_PAC:
            
            packet->packet_type = CONTROL_PAC;
            while(token != NULL){
                counter++;
                token = strtok(NULL,slide);
                if(counter == 1)
                    packet->power = (float)atof(token);
                else if(counter == 2)
                    packet->yaw = (float)atof(token);
                else if(counter == 3)
                    packet->pitch = (float)atof(token);
                else if(counter == 4)
                    packet->roll = (float)atof(token);
                
            }
            break;
            
        case EMERGENCY_PAC:
            packet->packet_type = EMERGENCY_PAC;
            break;

        case POWER_PAC:
            packet->packet_type = POWER_PAC;
            while(token != NULL){
                counter++;
                token = strtok(NULL,slide);
                if(counter == 1)
                    packet->on_off = (uint8_t)atoi(token);  
            }            
            break;

        case ALTITUTE_HOLD:

            packet->packet_type = ALTITUTE_HOLD;
            
            while(token != NULL){
                counter++;
                token = strtok(NULL,slide);
                if(counter == 1)
                    packet->ıs_hold = (uint8_t)atoi(token);
                else if(counter == 2)
                    packet->altitute = (uint32_t)atoi(token);
                }
            break;

            
            //kalan android paket formatlarına göre parserlar yazılacak (03.05.17)
    }
}
