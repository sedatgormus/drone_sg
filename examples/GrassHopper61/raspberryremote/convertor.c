#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <errno.h>
#include <sys/time.h>

#include <pthread.h>
#include "fix16.h"
#include "packet_parser.h"

#define AND_RECEIVE_PAC_SIZE 50
#define QUAD_PORT    3461
#define ANDROID_PORT 6134

static quad_control_packet pck;


void *quad_process(void *);
void *android_process(void *);

static char    android_pac [AND_RECEIVE_PAC_SIZE];
static uint8_t quad_hand   [HAND_PAC_SIZE];
static uint8_t quad_control[CONTROL_PAC_SIZE];
static uint8_t quad_emerg  [EMRG_PAC_SIZE];
static uint8_t quad_power  [POWER_PAC_SIZE];
static uint8_t quad_altitute[ALTITUTE_HOLD_PACKET_SIZE];

static int quad_socket,android_socket;
struct sockaddr_in6 server_quad,quad_address;
struct sockaddr_in server_android,android_address;

const char *drone_address = "daaa::212:4b00:430:569c"; // For Mahmote 156

static socklen_t and_add_len;

int input_timeout (int filedes, unsigned int seconds);

int
main(int argc, char *argv[])
{  
  /*char str[80] = "1*105.5523*20.2425*30.55542*40.5325";
  parse_android_packet(str, &pck);
  printf(" power -> %f yaw -> %f pitch -> %f roll -> %f ",pck.power1,pck.yaw,pck.pitch,pck.roll);*/
  quad_socket = socket(AF_INET6, SOCK_DGRAM, 0);
  if(quad_socket < 0){
    printf( "ERROR opening quad socket!\n");
    exit(0);
  }
  
  android_socket = socket(AF_INET, SOCK_STREAM, 0);
  if(android_socket < 0){
    printf("ERROR opening android socket!\n");
    exit(0);
  }
  
  //SERVER QUAD 
  memset((char *)&server_quad, 0, sizeof(server_quad));
  server_quad.sin6_flowinfo = 0;
  server_quad.sin6_family = AF_INET6;
  server_quad.sin6_addr = in6addr_any;
  server_quad.sin6_port = htons(QUAD_PORT);
 
  if( bind(quad_socket, (struct sockaddr *) &server_quad, sizeof(server_quad)) < 0){
   
    printf("Error: When server binding\n");
    exit(-1);
  }
  
 // SERVER ANDROID
  memset((char *)&server_android, 0, sizeof(server_android));
  
  server_android.sin_family = AF_INET;
  server_android.sin_addr.s_addr = INADDR_ANY;
  server_android.sin_port = htons(ANDROID_PORT);
 
  if( bind(android_socket, (struct sockaddr *) &server_android, sizeof(server_android)) < 0){
   
    printf("Error: When server binding\n");
    exit(-1);
  }
  
  // QUAD
  
  memset((char *)&quad_address, 0, sizeof(quad_address));
  quad_address.sin6_flowinfo = 0;
  quad_address.sin6_family = AF_INET6;
  quad_address.sin6_addr = in6addr_any;
  quad_address.sin6_port = htons(QUAD_PORT);
  
  if(inet_pton(AF_INET6,drone_address,&quad_address.sin6_addr) == 0){
    printf("inet_pton error android !\n");
    exit(0);
  }
 
  //ANDROİD
  
  memset((char *)&android_address, 0, sizeof(android_address));
  android_address.sin_family = AF_INET;
  android_address.sin_addr.s_addr  = INADDR_ANY;
  android_address.sin_port = htons(ANDROID_PORT);
 

  
  pthread_t id[2];
  if(pthread_create(&id[0],NULL,quad_process,NULL) != 0){
    exit(1);
  }
      
  if(pthread_create(&id[1],NULL,android_process,NULL) != 0){
    exit(1);
  }
  
  pthread_join(id[0],NULL);
  pthread_join(id[1],NULL);

  close(socket);

  return 0;
}

void *quad_process(void * x){
  
}

void *android_process(void * y)
{
   int8_t time_out = 1,hand = 0;
   int8_t handshake =0;
   listen(android_socket,2);
   and_add_len = sizeof(android_address);
   int and_soc = accept(android_socket, (struct sockaddr *) &android_address, &and_add_len);
   if (and_soc < 0) 
         printf("ERROR on accept !\n");
  
  while(1)
  { 
     bzero(android_pac,AND_RECEIVE_PAC_SIZE);
     int n = read(and_soc,android_pac,AND_RECEIVE_PAC_SIZE);
     if (n < 0) printf("ERROR reading from socket !\n");
     parse_android_packet(android_pac, &pck);
     if(pck.packet_type == HANDSHAKE_PAC){
        printf("handshake geldi \n");
        create_quad_packet(&pck,quad_hand);
        sendto(quad_socket,&quad_hand,HAND_PAC_SIZE,0,(struct sockaddr *)&quad_address,sizeof(quad_address));
        int8_t state = input_timeout (quad_socket, 10);
        if( state == 1)
        {
            int temp = ntohl(hand);
            printf("android handshake gönderdi \n");
           send(and_soc , &temp , sizeof(int) , 0);
           handshake = 1;
        }else{
            int temp = ntohl(time_out);
            printf("timeout mesaj gönder \n");
           send(and_soc , &temp , sizeof(int) , 0);
        }
     }else if(handshake == 1 && pck.packet_type == CONTROL_PAC){
        printf("control packet geldi \n");
        printf("power---> %f pitch --> %f yaw --> %f roll---> %f ",pck.power,pck.pitch,pck.yaw,pck.roll);
        pck.fyaw   = fix16_from_float(pck.yaw);
        pck.fpitch = fix16_from_float(pck.pitch);
        pck.froll  = fix16_from_float(pck.roll);
        pck.fpower = fix16_from_float(pck.power);       
        create_quad_packet(&pck,quad_control);
        sendto(quad_socket,&quad_control,CONTROL_PAC_SIZE,0,(struct sockaddr *)&quad_address,sizeof(quad_address));
   
     }else if(handshake == 1 && pck.packet_type == EMERGENCY_PAC){
        printf("emergency geldi \n");
        create_quad_packet(&pck,quad_emerg);
        sendto(quad_socket,&quad_emerg,EMRG_PAC_SIZE,0,(struct sockaddr *)&quad_address,sizeof(quad_address));
     }else if(handshake == 1 && pck.packet_type == POWER_PAC){
        printf("power geldi \n");
        create_quad_packet(&pck,quad_power);
        sendto(quad_socket,&quad_power,POWER_PAC_SIZE,0,(struct sockaddr *)&quad_address,sizeof(quad_address));
        
     }else if(handshake == 1 && pck.packet_type == ALTITUTE_HOLD){
        printf("ALTITUTE_HOLD geldi\n");
        create_quad_packet(&pck,quad_altitute);
        sendto(quad_socket,&quad_altitute,ALTITUTE_HOLD_PACKET_SIZE,0,(struct sockaddr *)&quad_address,sizeof(quad_address));
     }
     
  }
    close(quad_socket);
    close(and_soc);
   
}


int
input_timeout (int filedes, unsigned int seconds)
{
  fd_set set;
  struct timeval timeout;


  /* Initialize the file descriptor set. */
  FD_ZERO (&set);
  FD_SET (filedes, &set);

  /* Initialize the timeout data structure. */
  timeout.tv_sec = seconds;
  timeout.tv_usec = 0;

  /* select returns 0 if timeout, 1 if input available, -1 if error. */
  return select (FD_SETSIZE,&set, NULL, NULL,&timeout);
}
