#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include "dev/pwm.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>

#include "mpudriver.h"
#include "math.h"
#include "pid.h"
#include "fix16.h"
#include "control.h"
#include "parse_packet.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define QUAD_CONTROL_PORT    3461

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"

#define MOTOR_FRONT_LEFT  1
#define MOTOR_FRONT_RIGHT 2
#define MOTOR_BACK_LEFT   3
#define MOTOR_BACK_RIGHT  4
#define MOTOR_ALL         5

static struct uip_udp_conn *quad_conn;

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define PWM_FREQUENCY 400
#define DUTY_MAX 959
#define DUTY_MIN 5

#define SAMPLING_TIME_7MS ((clock_time_t)1)            // ~7ms

static  uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
static  uint16_t fifoCount;     // count of all bytes currently in FIFO

static uint8_t hand_shake = 0;
static uint8_t set_yaw_axis = 0;
static uint8_t pid_block  = 0;
static uint32_t last_time_pid = 0;

static struct etimer pwm_calib_timer;

static uip_ipaddr_t remote_ipaddr;
static euler_t set_point;
static euler_t input;

static void fly_control(euler_t set,euler_t reference);
static euler_t get_angles();
static void motor_pwm(uint16_t duty,uint8_t motor);
static void pwm_calibration();

static  float rollKp = 1.0f, rollKi = 0.25f, rollKd = 0.3f;  //back_y = 1

static  float pitchKp = 1.55f, pitchKi = 0.25f, pitchKd = 0.31f;  // backCoff = 1.2

static  float yawKp = 1.55f, yawKi = 0.25f, yawKd = 0.3f;

/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  quad_control_packet pac;
  char * buffer = uip_appdata;
  if(uip_newdata()){  
    parse_packet(buffer,&pac,uip_datalen());  
    if(pac.packet_type == HANDSHAKE_PAC ){
      PRINTF("Handshake Packet is arrived\n");
      uip_ipaddr_copy(&remote_ipaddr, &UIP_IP_BUF->srcipaddr);
      uint8_t response = 12;
      if(!hand_shake){
        pwm_calibration();
        etimer_set(&pwm_calib_timer,CLOCK_SECOND*3);
      }
      uip_udp_packet_sendto(quad_conn,(void *)&response,sizeof(uint8_t), &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
    }else if(hand_shake){

      switch(pac.packet_type)
      {  
        case POWER_PAC:
          PRINTF("Power Packet is arrived\n");
          if(pac.on_off){
            PRINTF("PID ACTİVE\n");
            pid_block = 1;
            control_set_throttle(MIN_THROTTLE);
          }else{
            PRINTF("PID CLOSE\n");
            last_time_pid = 0;
            pid_block = 0;
            pid_reset();
            control_set_throttle(STOP_THROTTLE);
          }
          break;  
        
        case CONTROL_PAC:
          PRINTF("Control Packet is arrived\n");
          if(pid_block){
            control_set_throttle(fix16_to_float(pac.fpower) + MIN_THROTTLE); 
            set_point.x = fix16_to_float(pac.fpitch)+ 0.5f;
            set_point.y = fix16_to_float(pac.froll);
            set_point.z += fix16_to_float(pac.fyaw); 
            PRINTF("Throttle: %d\n",(int)((fix16_to_float(pac.fpower) + MIN_THROTTLE)*1000.0f));
            PRINTF("SetPoint : %d %d %d\n",(int)(set_point.x*1000.0f),(int) (set_point.y*1000.0f),(int)(set_point.z*1000.0f));
          }
          break;

        case EMERGENCY_PAC:
          PRINTF("Emergency Packet is arrived\n");
          control_set_throttle(STOP_THROTTLE);
          pid_reset();
          pid_block = 0;
          last_time_pid = 0;
          break;

        default:
          PRINTF("Unknown Packet Type\n");
          break;
      }
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xdaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
  PRINTF("IP ADDRESS: ");
  PRINT6ADDR(&ipaddr);
  PRINTF("\n");
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer set;

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  set_global_address();

  /*etimer_set(&set,CLOCK_SECOND*2);
  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&set));
  etimer_stop(&set);*/

  PRINTF("GRASSHOPPER61 FLIGHT CONTROLLER TEST\n");

  if(!mpu6050_testConnection()){
    // Can not connect to MPU6050 sensor. 
    PRINTF("Cannot Connect Mpu6050\n");
    PROCESS_EXIT();                          
  }

  
  if(mpu6050_getDMPEnabled()){
    PRINTF("Resetting DMP\n");   // TODO:
    packetSize = MPU6050_DEFAULT_PACKET_SIZE;
    fifoCount = 0;
    mpu6050_resetFIFO();
  }else{
    mpu6050_initialize();
    uint8_t devStatus = mpu6050_dmpInitialize();
  
    if(devStatus == 0) {
      PRINTF("Enabling DMP...\n");
      mpu6050_setDMPEnabled(1);
      fifoCount = 0;
      packetSize = mpu6050_dmpGetFIFOPacketSize();
    }else {
      PRINTF("DMP Initilaze Failed\n");
      PROCESS_EXIT();
    }
  }

  /* new connection with remote host */
  quad_conn = udp_new(NULL, UIP_HTONS(0), NULL); 
  if(quad_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(quad_conn, UIP_HTONS(QUAD_CONTROL_PORT)); 
  
  PRINTF("PID COEFFICIENTS ARE SET\n");
  pid_initilize(pitchKp, pitchKi, pitchKd, rollKp, rollKi, rollKd, yawKp , yawKi ,yawKd); 

  control_initilize();

  set_point.x = 0;
  set_point.y = 0;
  set_point.z = 0;
 
 
#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */

  etimer_set(&periodic,CLOCK_SECOND*2);
  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
    }
    else if(data == &periodic){
      input = get_angles();
      if(pid_block ){
        fly_control(set_point,input);
      }else if(hand_shake){
        motor_t motors = control_motors(0,0,0);
        motor_pwm(motors.M1,MOTOR_FRONT_LEFT);
        motor_pwm(motors.M2,MOTOR_FRONT_RIGHT);
        motor_pwm(motors.M3,MOTOR_BACK_LEFT);
        motor_pwm(motors.M4,MOTOR_BACK_RIGHT);
      }
      etimer_set(&periodic,SAMPLING_TIME_7MS);
    }else if(data == &pwm_calib_timer){
      motor_pwm(DUTY_MIN,MOTOR_ALL);
      etimer_stop(&pwm_calib_timer);
      set_yaw_axis = 1;
      hand_shake = 1;
    }
  }

  PROCESS_END();
}

static void 
send_to_remote(unsigned char * data,int len)
{
  uip_udp_packet_sendto(quad_conn,data, len, &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
}


static euler_t
get_angles()
{
  euler_t result;
  quaternion_t q;           // [w, x, y, z]         quaternion container
  vector_float_t gravity;    // [x, y, z]            gravity vector
  float ypr[3];
  uint8_t fifoBuffer[packetSize]; 

  fifoCount = mpu6050_getFIFOCount();
  
  while (fifoCount < packetSize) {
    fifoCount = mpu6050_getFIFOCount();
  }
   
  if (fifoCount >= 1024 || packetSize > (1024-fifoCount) ) {
    mpu6050_resetFIFO();
    PRINTF("FIFO overflow!\n");
  }else{
    mpu6050_getFIFOBytes(fifoBuffer, packetSize);
    
    fifoCount = mpu6050_getFIFOCount();
    if(fifoCount % packetSize == 0 && fifoCount/packetSize>=1){
      mpu6050_getFIFOBytes(fifoBuffer, packetSize);
    }
      
    mpu6050_dmpGetQuaternion_q(&q, fifoBuffer);
    mpu6050_dmpGetGravity(&gravity, &q);
    mpu6050_dmpGetYawPitchRoll(ypr, &q, &gravity);


    if(set_yaw_axis){
      set_yaw_axis = 0;
      set_point.z = ypr[0];
    }
    //PRINTF("yaw : %d roll %d pitch %d\n",(int16_t)ypr[0],(int16_t)-ypr[1],(int16_t)ypr[2]);
    //PRINTF("yaw : %d \n",(int16_t)ypr[0]);
    
    result.z = ypr[0];
    result.y = -ypr[1];
    result.x = ypr[2];

    return result;
  }
}
  
static void 
fly_control(euler_t set_point, euler_t reference)
{
  uint32_t now = (uint32_t)clock_time();
  //PRINTF("PID TIME : %d\n",(now*1000-last_time_pid*1000)/128);
  float dt = ((float)(now - last_time_pid)) / 128.0f;
  last_time_pid = now;

  pidout_t rate = pid_system_calculation(set_point,reference,dt);
  
  
  motor_t motors = control_motors(rate.pidX,rate.pidY,rate.pidZ);
  motor_pwm(motors.M1,MOTOR_FRONT_LEFT);
  motor_pwm(motors.M2,MOTOR_FRONT_RIGHT);
  motor_pwm(motors.M3,MOTOR_BACK_LEFT);
  motor_pwm(motors.M4,MOTOR_BACK_RIGHT);
  //PRINTF("M1 %d M2 :%d M3 %d M4 %d\n",motors.M1,motors.M2,motors.M3,motors.M4 );
  
}

static void 
pwm_calibration()
{   
  
  INTERRUPTS_DISABLE();
  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_0, PWM_TIMER_B, GPIO_C_NUM, 5) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_1, PWM_TIMER_B, GPIO_C_NUM, 1) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_2, PWM_TIMER_B, GPIO_C_NUM, 6) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_3, PWM_TIMER_B, GPIO_C_NUM, 7) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  INTERRUPTS_ENABLE();
}

static void 
motor_pwm(uint16_t duty,uint8_t motor)
{
  switch(motor){
    case MOTOR_FRONT_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

   case MOTOR_FRONT_RIGHT:
      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_RIGHT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_ALL:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;
  }
}




