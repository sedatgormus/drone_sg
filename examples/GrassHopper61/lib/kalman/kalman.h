#ifndef KALMAN_H
#define KALMAN_H

// One dimensional kalman filter implentation

typedef struct {
	float q; //process noise covariance
    float r; //measurement noise covariance

    float x; //value
    float p; //estimation error covariance
    
    float k; //kalman gain
} kalman_t;

void kalman_init(float process_noise, float sensor_noise, float estimated_error, float intial_value , kalman_t * info);

float kalman_getFilteredValue(float measurement,kalman_t * info);

void setParametersQRP(float process_noise, float sensor_noise, float estimated_error,kalman_t * info);

void setParametersQR(float process_noise, float sensor_noise,kalman_t * info);
  
float getProcessNoise(const kalman_t info);
  
float getSensorNoise(const kalman_t info);
    
float getEstimatedError(const kalman_t info);
   

#endif 