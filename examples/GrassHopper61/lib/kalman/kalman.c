#include "kalman.h"

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"


void kalman_init(float process_noise, float sensor_noise, float estimated_error, float intial_value , kalman_t * info){
	info->q = process_noise;
	info->r = sensor_noise;
	info->x = intial_value;
	info->p = estimated_error;
}

float kalman_getFilteredValue(float measurement,kalman_t * info){
	//prediction update
	info->p = info->p + info->q;
    
    //measurement update
    info->k = info->p / (info->p + info->r);
    info->x = info->x + info->k * (measurement - info->x);
    info->p = (1 - info->k) * info->p;

    return info->x;
}

void setParametersQRP(float process_noise, float sensor_noise, float estimated_error,kalman_t * info){
	info->q = process_noise;
    info->r = sensor_noise;
    info->p = estimated_error;
}

void setParametersQR(float process_noise, float sensor_noise,kalman_t * info){
	info->q = process_noise;
    info->r = sensor_noise;
}

float getProcessNoise(const kalman_t info){
	return info.q;
}
  
float getSensorNoise(const kalman_t info){
	return info.r;
}
    
float getEstimatedError(const kalman_t info){
	return info.p;
}