#include "math.h"
#include "contiki.h"


static double
asin_acos(double x, int cosfl) 
{
    int negative = x < 0;
    int i;
    double g;
    static double p[] = {
        -0.27368494524164255994e+2,
        0.57208227877891731407e+2,
        -0.39688862997540877339e+2,
        0.10152522233806463645e+2,
        -0.69674573447350646411e+0
    };
    static double q[] = {
        -0.16421096714498560795e+3,
        0.41714430248260412556e+3,
        -0.38186303361750149284e+3,
        0.15095270841030604719e+3,
        -0.23823859153670238830e+2,
        1.0
    };

    if (__IsNan(x)) {
	printf("Error 1\n");
        return x;
    }

    if (negative) {
        x = -x;
    }
    if (x > 0.5) {
        i = 1;
        if (x > 1) {
            printf("Error 2\n");
            return 0;
        }
        g = 0.5 - 0.5 * x;
        x = -sqrt(g);
        x += x;
    } else {
        /* ??? avoid underflow ??? */
        i = 0;
        g = x * x;
    }
    x += x * g * POLYNOM4(g, p) / POLYNOM5(g, q);
    if (cosfl) {
        if (!negative) x = -x;
    }
    if ((cosfl == 0) == (i == 1)) {
        x = (x + M_PI_4) + M_PI_4;
    } else if (cosfl && negative && i == 1) {
        x = (x + M_PI_2) + M_PI_2;
    }
    if (!cosfl && negative) x = -x;
    return x;
}

int 
__IsNan(double d) 
{
    #if defined(vax) || defined(pdp)
    #else
        float f = d;
        if ((*((long *) &f) & 0x7f800000) == 0x7f800000 &&
                (*((long *) &f) & 0x007fffff) != 0) return 1;
    #endif
        return 0;
}

double
asin(double x)
{
    return asin_acos(x, 0);
}

double
acos(double x)
{
    return asin_acos(x, 1);
}

#define SQRT_MAGIC_F 0x5f3759df 

float
sqrt(const float x)
{
  const float xhalf = 0.5f*x;
 
  union // get bits for floating value
  {
    float x;
    int i;
  } u;

  u.x = x;
  u.i = SQRT_MAGIC_F - (u.i >> 1);  // gives initial guess y0

  return x*u.x*(1.5f - xhalf*u.x*u.x);// Newton step, repeating increases accuracy 
}


double
atan2(double y, double x)
{
    double absx, absy, val;
    if (x == 0 && y == 0) {
        printf("Error 3\n");
        return 0;
    }
    absy = y < 0 ? -y : y;
    absx = x < 0 ? -x : x;
    if (absy - absx == absy) {
        /* x negligible compared to y */
        return y < 0 ? -M_PI_2 : M_PI_2;
    }
    if (absx - absy == absx) {
        /* y negligible compared to x */
        val = 0.0;
    } else val = atan(y / x);
    if (x > 0) {
        /* first or fourth quadrant; already correct */
        return val;
    }
    if (y < 0) {
        /* third quadrant */
        return val - M_PI;
    }
    return val + M_PI;
}


double
atan(double x)
{
    /*      Algorithm and coefficients from:
                 "Software manual for the elementary functions"
                 by W.J. Cody and W. Waite, Prentice-Hall, 1980
     */

    static double p[] = {
        -0.13688768894191926929e+2,
        -0.20505855195861651981e+2,
        -0.84946240351320683534e+1,
        -0.83758299368150059274e+0
    };
    static double q[] = {
        0.41066306682575781263e+2,
        0.86157349597130242515e+2,
        0.59578436142597344465e+2,
        0.15024001160028576121e+2,
        1.0
    };
    static double a[] = {
        0.0,
        0.52359877559829887307710723554658381, /* pi/6 */
        M_PI_2,
        1.04719755119659774615421446109316763 /* pi/3 */
    };

    int neg = x < 0;
    int n;
    double g;

    if (__IsNan(x)) {
        printf("Error 4\n");
        return x;
    }
    if (neg) {
        x = -x;
    }
    if (x > 1.0) {
        x = 1.0 / x;
        n = 2;
    } else n = 0;

    if (x > 0.26794919243112270647) { /* 2-sqtr(3) */
        n = n + 1;
        x = (((0.73205080756887729353 * x - 0.5) - 0.5) + x) /
                (1.73205080756887729353 + x);
    }

    /* ??? avoid underflow ??? */

    g = x * x;
    x += x * g * POLYNOM3(g, p) / POLYNOM4(g, q);
    if (n > 1) x = -x;
    x += a[n];
    return neg ? -x : x;
}


