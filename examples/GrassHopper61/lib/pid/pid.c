#include "pid.h"

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

static pidout_t  pid_last_out;


void 
pid_initilize(float CpX, float CiX, float CdX,float CpY, float CiY, float CdY,float CpZ, float CiZ, float CdZ,
  float CpU, float CiU, float CdU){

	Kpx = CpX;
	Kix = CiX;
	Kdx = CdX;

	Kpy = CpY;
	Kiy = CiY;
	Kdy = CdY;

  Kpz = CpZ;
  Kiz = CiZ;
  Kdz = CdZ;

  Kpu = CpU;
  Kiu = CiU;
  Kdu = CdU;

  sum_errX = 0.0f;
  lastErrorX = 0.0f;

  sum_errY= 0.0f;
  lastErrorY = 0.0f;

  sum_errZ= 0.0f;
  lastErrorZ = 0.0f;

  sum_errU= 0.0f;
  lastErrorU = 0.0f;
}


pidout_t 
pid_system_calculation(euler_t setpoint, euler_t input, float dt)
{
  //PRINTF("Kpx : %d ,Kix: %d\n", Kpx,Kix);
  //PRINTF("Kpy : %d ,Kiy: %d\n", Kpy,Kiy);
  if(dt == 0){
   return pid_last_out;
  }
   //////////////////////  FOR PITCH AXIS        //////////////////////////////


  //PRINTF("setpoint %d\n",fix16_to_int(setpoint.y));
  //Computes error
  float errX = setpoint.x -input.x;
 //printf("errX2 : %d\n", fix16_to_int(fix16_sadd(errX,32768)));
  //Integrating errors
  if(Kix > 0)
    sum_errX += errX * dt;

  //calculating error derivative
  //Input derivative is used to avoid derivative kick
  float ddt_errX = (input.x - lastErrorX) / dt;

  //Calculation of the output
  //winds up boundaries

  if(sum_errX > WINDUP_MAX) {
    sum_errX  = WINDUP_MAX;
  }else if (sum_errX < WINDUP_MIN) {
    sum_errX  = WINDUP_MIN;
  }

  float outputX = (Kpx*errX) + (Kix*sum_errX) + (-Kdx*ddt_errX); 
  

  lastErrorX = input.x;


   //////////////////////  FOR ROLL AXIS        //////////////////////////////


  float errY = setpoint.y -input.y;
 //printf("errX2 : %d\n", fix16_to_int(fix16_sadd(errX,32768)));
  //Integrating errors
  if(Kiy > 0)
    sum_errY += errY * dt;

  //calculating error derivative
  //Input derivative is used to avoid derivative kick
  float ddt_errY = (input.y - lastErrorY) / dt;

  //Calculation of the output
  //winds up boundaries
  if(sum_errY > WINDUP_MAX) {
    sum_errY  = WINDUP_MAX;
  }else if (sum_errY < WINDUP_MIN) {
    sum_errY  = WINDUP_MIN;
  }

  float outputY = (Kpy * errY) + (Kiy * sum_errY) + (-Kdy * ddt_errY); 
  
  lastErrorY = input.y;
  
   //////////////////////      FOR YAW AXIS        //////////////////////////////

  float errZ = setpoint.z -input.z;
 //printf("errX2 : %d\n", fix16_to_int(fix16_sadd(errX,32768)));
  //Integrating errors
  if(Kiz > 0)
    sum_errZ += errZ * dt;

  //calculating error derivative
  //Input derivative is used to avoid derivative kick
  float ddt_errZ = (input.z - lastErrorZ) / dt;

  //Calculation of the output
  //winds up boundaries
  if(sum_errZ > WINDUP_MAX) {
    sum_errZ  = WINDUP_MAX;
  }else if (sum_errZ < WINDUP_MIN) {
    sum_errZ  = WINDUP_MIN;
  }

  float outputZ = (Kpz*errZ) + (Kiz*sum_errZ) + (-Kdz*ddt_errZ); 
  
  lastErrorZ = input.z;


  //////////////////////      FOR ULTRASOUND        //////////////////////////////

  float errU = setpoint.altitute -input.altitute;
 //printf("errX2 : %d\n", fix16_to_int(fix16_sadd(errX,32768)));
  //Integrating errors
  if(Kiu > 0)
    sum_errU += errU * dt;

  //calculating error derivative
  //Input derivative is used to avoid derivative kick
  float ddt_errU = (input.altitute - lastErrorU) / dt;

  //Calculation of the output
  //winds up boundaries
  if(sum_errU > WINDUP_MAX) {
    sum_errU  = WINDUP_MAX;
  }else if (sum_errU < WINDUP_MIN) {
    sum_errU  = WINDUP_MIN;
  }

  float outputU = (Kpu*errU) + (Kiu*sum_errU) + (-Kdu*ddt_errU); 
  
  lastErrorU = input.altitute;


  pidout_t result;
  result.pidX = outputX ;
  result.pidY = outputY ;
  result.pidZ = outputZ ;       
  result.pidU = outputU ;  

  pid_last_out = result;
  
  result.error = errU;
  result.integral = sum_errU;

  return result;
}


void 
pid_set_coefficients(float Cp, float Ci, float Cd,uint8_t pid_type)
{
  if(pid_type == 1){
	   Kpx = Cp;
	   Kix = Ci;
	   Kdx = Cd;
  }else if(pid_type == 2){
	   Kpy = Cp;
	   Kiy = Ci;
	   Kdy = Cd;
  }
  else if(pid_type == 3){
     Kpz = Cp;
     Kiz = Ci;
     Kdz = Cd;
  }else if(pid_type == 4){
     Kpu = Cp;
     Kiu = Ci;
     Kdu = Cd;
  }
}


void 
pid_change_bounds(float max, float min)
{

	WINDUP_MAX = max;
	WINDUP_MIN = min;
}

void 
pid_reset()
{
	sum_errX = 0;
	sum_errY = 0;
  sum_errZ = 0;
  sum_errU = 0;  
}