#ifndef PID_H
#define PID_H

#include "math.h"

static  float WINDUP_MAX = 300.0f;
static  float WINDUP_MIN = -300.0f;

static	float Kpx;
static	float Kix;
static	float Kdx;

static	float Kpy;
static	float Kiy;
static	float Kdy;

static	float Kpz;
static	float Kiz;
static	float Kdz;

static	float Kpu;
static	float Kiu;
static	float Kdu;

static float  sum_errX;
static float  lastErrorX;

static float  sum_errY;
static float  lastErrorY;

static float  sum_errZ;
static float  lastErrorZ;

static float  sum_errU;
static float  lastErrorU;


typedef struct 
{
	float pidX;
	float pidY;
	float pidZ;
	float pidU;		

	float error;
	float integral;
} pidout_t;


void pid_initilize(float CpX, float CiX, float CdX,float CpY, float CiY, float CdY,float CpZ, float CiZ, float CdZ,
	float CpU, float CiU, float CdU);

pidout_t pid_system_calculation(euler_t setpoint, euler_t input, float dt);

void pid_set_coefficients(float Cp, float Ci, float Cd,uint8_t pid_type);

void pid_change_bounds(float max,float min);

void pid_reset();

#endif 	/* PID_H */
