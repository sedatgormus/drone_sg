#ifndef EMA_H
#define EMA_H

#include "contiki.h"

typedef struct  
{
	uint8_t turn;
	float last_value;
} ema_t;

void exponantial_seed(const float seed, ema_t *info);

float exponantial_move_average(const float value, ema_t *info);

#endif // EMA_H
