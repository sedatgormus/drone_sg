#include "ema.h"
#include "contiki.h"

#define ALPHA 0.63f

void 
exponantial_seed(const float seed, ema_t *info)
{
	info->turn = 0;
    info->last_value = seed;
}

float
exponantial_move_average(const float value, ema_t *info)
{   
    float result = ALPHA*value + (1-ALPHA)*info->last_value;
    info->last_value = result;
    return result;
}
