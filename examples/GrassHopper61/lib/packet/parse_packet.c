#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "parse_packet.h"


#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

void
create_packet(const quad_control_packet *packet, uint8_t *buffer)
{
    switch(packet->packet_type)
    {
        case HANDSHAKE_PAC:
            memset(buffer,0,HAND_PAC_SIZE);
            buffer[HAND_PAC_SIZE -1] = HANDSHAKE_PAC;
            break;

        case CONTROL_PAC:
            memset(buffer,0,CONTROL_PAC_SIZE);
            buffer[CONTROL_PAC_SIZE -1 ] = CONTROL_PAC;

            buffer[CONTROL_PAC_SIZE -2] = packet->fpitch;
            buffer[CONTROL_PAC_SIZE -3] = (packet->fpitch >> 8);
            buffer[CONTROL_PAC_SIZE -4] = (packet->fpitch >> 16);
            buffer[CONTROL_PAC_SIZE -5] = (packet->fpitch >> 24);

            buffer[CONTROL_PAC_SIZE -6] = packet->froll;
            buffer[CONTROL_PAC_SIZE -7] = (packet->froll >> 8);
            buffer[CONTROL_PAC_SIZE -8] = (packet->froll >> 16);
            buffer[CONTROL_PAC_SIZE -9] = (packet->froll >> 24);

            buffer[CONTROL_PAC_SIZE -10] = packet->fyaw;
            buffer[CONTROL_PAC_SIZE -11] = (packet->fyaw >> 8);
            buffer[CONTROL_PAC_SIZE -12] = (packet->fyaw >> 16);
            buffer[CONTROL_PAC_SIZE -13] = (packet->fyaw >> 24);

            buffer[CONTROL_PAC_SIZE -14] = packet->fpower;
            buffer[CONTROL_PAC_SIZE -15] = (packet->fpower >> 8);
            buffer[CONTROL_PAC_SIZE -16] = (packet->fpower >> 16);
            buffer[CONTROL_PAC_SIZE -17] = (packet->fpower >> 24);

            break;

          case EMERGENCY_PAC:
            memset(buffer,0,EMRG_PAC_SIZE);
            buffer[EMRG_PAC_SIZE -1] = EMERGENCY_PAC;
            break; 
            
         case POWER_PAC:
            memset(buffer,0,POWER_PAC_SIZE);
            buffer[POWER_PAC_SIZE -1] = POWER_PAC;
            buffer[POWER_PAC_SIZE -2] = packet->on_off ;
            break; 

        case PING_PAC:

            memset(buffer,0,PING_PAC_SIZE);

            buffer[PING_PAC_SIZE -1 ] = PING_PAC;

            buffer[PING_PAC_SIZE -2 ] = packet->is_move;

            break;

        case MOVE_REQUEST_PAC:

            memset(buffer,0,MOVE_REQUEST_PAC_SIZE);

            buffer[MOVE_REQUEST_PAC_SIZE -1 ] = MOVE_REQUEST_PAC;

            buffer[MOVE_REQUEST_PAC_SIZE -2 ] = packet->move_type;

            buffer[MOVE_REQUEST_PAC_SIZE -3] = packet->altitute;
            buffer[MOVE_REQUEST_PAC_SIZE -4] = (packet->altitute >> 8);

            buffer[MOVE_REQUEST_PAC_SIZE -5] = packet->fyaw;
            buffer[MOVE_REQUEST_PAC_SIZE -6] = (packet->fyaw >> 8);
            buffer[MOVE_REQUEST_PAC_SIZE -7] = (packet->fyaw >> 16);
            buffer[MOVE_REQUEST_PAC_SIZE -8] = (packet->fyaw >> 24);

            break;

        case REPLY_PAC:

            memset(buffer,0,REPLY_PAC_SIZE);

            buffer[REPLY_PAC_SIZE -1] = REPLY_PAC;

            buffer[REPLY_PAC_SIZE -2] = packet->move_type;

            buffer[REPLY_PAC_SIZE -3] = packet->reply_type;

            break;


        case FORM_FLIGHT_PAC:

            memset(buffer,0,FORM_FLIGHT_PAC_SIZE);

            buffer[FORM_FLIGHT_PAC_SIZE -1] = FORM_FLIGHT_PAC;
            buffer[FORM_FLIGHT_PAC_SIZE -2 ] = packet->is_form;

            buffer[FORM_FLIGHT_PAC_SIZE -3] = packet->fyaw;
            buffer[FORM_FLIGHT_PAC_SIZE -4] = (packet->fyaw >> 8);
            buffer[FORM_FLIGHT_PAC_SIZE -5] = (packet->fyaw >> 16);
            buffer[FORM_FLIGHT_PAC_SIZE -6] = (packet->fyaw >> 24);
            break;
            
        case ALTITUTE_HOLD_PAC:

            memset(buffer,0,ALTITUTE_HOLD_PAC_SIZE);
            
            buffer[ALTITUTE_HOLD_PAC_SIZE-1] = ALTITUTE_HOLD_PAC;
            buffer[ALTITUTE_HOLD_PAC_SIZE-2] = packet->is_hold;

            buffer[ALTITUTE_HOLD_PAC_SIZE -3] = packet->altitute;
            buffer[ALTITUTE_HOLD_PAC_SIZE -4] = (packet->altitute >> 8);

            break;

        default:
            PRINTF("Packet Creator :There is No packet type as you enter!!\n");
            break;
    }
}


void
parse_packet(const uint8_t * buffer ,quad_control_packet *packet, int len)
{
    switch((buffer[len -1]))
    {
        
        case HANDSHAKE_PAC:
            packet->packet_type = HANDSHAKE_PAC;
            break;

        case CONTROL_PAC:
            
            packet->packet_type = CONTROL_PAC;

            packet->fpitch = buffer[CONTROL_PAC_SIZE - 5];
            packet->fpitch = (packet->fpitch << 8) | buffer[CONTROL_PAC_SIZE - 4];
            packet->fpitch = (packet->fpitch << 8) | buffer[CONTROL_PAC_SIZE - 3];
            packet->fpitch = (packet->fpitch << 8) | buffer[CONTROL_PAC_SIZE - 2];

            packet->froll = buffer[CONTROL_PAC_SIZE - 9];
            packet->froll = (packet->froll << 8) | buffer[CONTROL_PAC_SIZE - 8];
            packet->froll = (packet->froll << 8) | buffer[CONTROL_PAC_SIZE - 7];
            packet->froll = (packet->froll << 8) | buffer[CONTROL_PAC_SIZE - 6];

            packet->fyaw = buffer[CONTROL_PAC_SIZE - 13];
            packet->fyaw = (packet->fyaw << 8) | buffer[CONTROL_PAC_SIZE - 12];
            packet->fyaw = (packet->fyaw << 8) | buffer[CONTROL_PAC_SIZE - 11];
            packet->fyaw = (packet->fyaw << 8) | buffer[CONTROL_PAC_SIZE - 10];

            packet->fpower = buffer[CONTROL_PAC_SIZE - 17];
            packet->fpower = (packet->fpower << 8) | buffer[CONTROL_PAC_SIZE - 16];
            packet->fpower = (packet->fpower << 8) | buffer[CONTROL_PAC_SIZE - 15];
            packet->fpower = (packet->fpower << 8) | buffer[CONTROL_PAC_SIZE - 14];
            break;

        case EMERGENCY_PAC:
            packet->packet_type = EMERGENCY_PAC;
            break; 
            
         case POWER_PAC:
            packet->packet_type = POWER_PAC;
            packet->on_off = buffer[POWER_PAC_SIZE - 2];
            break; 

        case PING_PAC:
            packet->packet_type = PING_PAC;
            packet->is_move = buffer[PING_PAC_SIZE -2 ];

            break;


        case MOVE_REQUEST_PAC:
            packet->packet_type = MOVE_REQUEST_PAC;

            packet->move_type = buffer[MOVE_REQUEST_PAC_SIZE -2 ];

            packet->altitute = buffer[MOVE_REQUEST_PAC_SIZE - 4];
            packet->altitute = (packet->altitute << 8) | buffer[MOVE_REQUEST_PAC_SIZE - 3];

            packet->fyaw = buffer[MOVE_REQUEST_PAC_SIZE - 8];
            packet->fyaw = (packet->fyaw << 8) | buffer[MOVE_REQUEST_PAC_SIZE - 7];
            packet->fyaw = (packet->fyaw << 8) | buffer[MOVE_REQUEST_PAC_SIZE - 6];
            packet->fyaw = (packet->fyaw << 8) | buffer[MOVE_REQUEST_PAC_SIZE - 5];

            break;

        case REPLY_PAC:

            packet->packet_type = REPLY_PAC;

            packet->move_type = buffer[REPLY_PAC_SIZE -2];

            packet->reply_type = buffer[REPLY_PAC_SIZE -3];

            break;

        case FORM_FLIGHT_PAC:

            packet->packet_type = FORM_FLIGHT_PAC;
            
            packet->is_form =  buffer[FORM_FLIGHT_PAC_SIZE -2];

            packet->fyaw = buffer[FORM_FLIGHT_PAC_SIZE - 6];
            packet->fyaw = (packet->fyaw << 8) | buffer[FORM_FLIGHT_PAC_SIZE - 5];
            packet->fyaw = (packet->fyaw << 8) | buffer[FORM_FLIGHT_PAC_SIZE - 4];
            packet->fyaw = (packet->fyaw << 8) | buffer[FORM_FLIGHT_PAC_SIZE - 3];

            break;

        case ALTITUTE_HOLD_PAC:

            packet->packet_type = ALTITUTE_HOLD_PAC;

            packet->is_hold = buffer[ALTITUTE_HOLD_PAC_SIZE -2 ];

            packet->altitute = buffer[ALTITUTE_HOLD_PAC_SIZE - 4];
            packet->altitute = (packet->altitute << 8) | buffer[ALTITUTE_HOLD_PAC_SIZE - 3];
            break;
        
        default:
            PRINTF("Packet Parser :There is No packet type as you wish!!\n");
            break;
    }
    }
}