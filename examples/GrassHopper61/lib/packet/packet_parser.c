#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "packet_parser.h"


#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

void
create_quad_packet(const quad_control_packet *packet, uint8_t *buffer)
{
    switch(packet->packet_type)
    {

        case ANGLE_PACKET:
            memset(buffer,0,ANGLE_PACKET_SIZE);

            buffer[ANGLE_PACKET_SIZE-1] = ANGLE_PACKET;

            buffer[ANGLE_PACKET_SIZE -2] = packet->angle_x;
            buffer[ANGLE_PACKET_SIZE -3] = (packet->angle_x >> 8);
            buffer[ANGLE_PACKET_SIZE -4] = (packet->angle_x >> 16);
            buffer[ANGLE_PACKET_SIZE -5] = (packet->angle_x >> 24);

            buffer[ANGLE_PACKET_SIZE -6] = packet->angle_y;
            buffer[ANGLE_PACKET_SIZE -7] = (packet->angle_y >> 8);
            buffer[ANGLE_PACKET_SIZE -8] = (packet->angle_y >> 16);
            buffer[ANGLE_PACKET_SIZE -9] = (packet->angle_y >> 24);

            buffer[ANGLE_PACKET_SIZE -10] = packet->angle_z;
            buffer[ANGLE_PACKET_SIZE -11] = (packet->angle_z >> 8);
            buffer[ANGLE_PACKET_SIZE -12] = (packet->angle_z >> 16);
            buffer[ANGLE_PACKET_SIZE -13] = (packet->angle_z >> 24);
            break;

        case PARAMETER_PACKET:

            memset(buffer,0,PARAMETER_PACKET_SIZE);

            buffer[PARAMETER_PACKET_SIZE-1] = PARAMETER_PACKET;

            buffer[PARAMETER_PACKET_SIZE -2] = packet->pk;
            buffer[PARAMETER_PACKET_SIZE -3] = (packet->pk >> 8);
            buffer[PARAMETER_PACKET_SIZE -4] = (packet->pk >> 16);
            buffer[PARAMETER_PACKET_SIZE -5] = (packet->pk >> 24);

            buffer[PARAMETER_PACKET_SIZE -6] = packet->ik;
            buffer[PARAMETER_PACKET_SIZE -7] = (packet->ik >> 8);
            buffer[PARAMETER_PACKET_SIZE -8] = (packet->ik >> 16);
            buffer[PARAMETER_PACKET_SIZE -9] = (packet->ik >> 24);

            buffer[PARAMETER_PACKET_SIZE -10] = packet->dk;
            buffer[PARAMETER_PACKET_SIZE -11] = (packet->dk >> 8);
            buffer[PARAMETER_PACKET_SIZE -12] = (packet->dk >> 16);
            buffer[PARAMETER_PACKET_SIZE -13] = (packet->dk >> 24);

            buffer[PARAMETER_PACKET_SIZE -14] = packet->pid_type;
            break;

        case POWER_PACKET:

            memset(buffer,0,POWER_PACKET_SIZE);

            buffer[POWER_PACKET_SIZE-1] = POWER_PACKET;

            buffer[POWER_PACKET_SIZE -2] = packet->power;
            buffer[POWER_PACKET_SIZE -3] = (packet->power >> 8);

            break;

        case EMERGENCY_PACKET:

            memset(buffer,0,EMERGENCY_PACKET_SIZE);

            buffer[EMERGENCY_PACKET_SIZE-1] = EMERGENCY_PACKET;

            break;

        case ERROR_PACKET:

            memset(buffer,0,ERROR_PACKET_SIZE);

            buffer[ERROR_PACKET_SIZE-1] = ERROR_PACKET;
            uint8_t j;
            for(j=0;j<ERROR_PACKET_SIZE-1;j++)
                buffer[ERROR_PACKET_SIZE - (j+2)] = packet->error[j];
            break;

        case PING_PACKET:
            memset(buffer,0,PING_PACKET_SIZE);
            buffer[PING_PACKET_SIZE-1] = PING_PACKET;
            break;

        case HANDSHAKE_PACKET:
            memset(buffer,0,HANDSHAKE_PACKET_SIZE);
            buffer[HANDSHAKE_PACKET_SIZE-1] = HANDSHAKE_PACKET;
            break;

        case ALTITUDE_PACKET:
            memset(buffer,0,ALTITUDE_PACKET_SIZE);
            buffer[ALTITUDE_PACKET_SIZE-1] = ALTITUDE_PACKET;
            buffer[ALTITUDE_PACKET_SIZE-2] = packet->hold_altitude;
            break; 

        case BACK_PACKET:

            memset(buffer,0,BACK_PACKET_SIZE);

            buffer[BACK_PACKET_SIZE-1] = BACK_PACKET;

            buffer[BACK_PACKET_SIZE -2] = packet->backValue;
            buffer[BACK_PACKET_SIZE -3] = (packet->backValue >> 8);
            buffer[BACK_PACKET_SIZE -4] = (packet->backValue >> 16);
            buffer[BACK_PACKET_SIZE -5] = (packet->backValue >> 24);

            buffer[BACK_PACKET_SIZE -6] = packet->backType;
            break;

        default:
            PRINTF("Packet Creator :There is No packet type as you enter!!\n");
            break;
    }
}


void
parse_quad_packet(const uint8_t * buffer ,quad_control_packet *packet, int len)
{
    switch((buffer[len -1]))
    {

        case ANGLE_PACKET:

            packet->packet_type = ANGLE_PACKET;

            packet->angle_x = buffer[ANGLE_PACKET_SIZE - 5];
            packet->angle_x = (packet->angle_x << 8) | buffer[ANGLE_PACKET_SIZE - 4];
            packet->angle_x = (packet->angle_x << 8) | buffer[ANGLE_PACKET_SIZE - 3];
            packet->angle_x = (packet->angle_x << 8) | buffer[ANGLE_PACKET_SIZE - 2];

            packet->angle_y = buffer[ANGLE_PACKET_SIZE - 9];
            packet->angle_y = (packet->angle_y << 8) | buffer[ANGLE_PACKET_SIZE - 8];
            packet->angle_y = (packet->angle_y << 8) | buffer[ANGLE_PACKET_SIZE - 7];
            packet->angle_y = (packet->angle_y << 8) | buffer[ANGLE_PACKET_SIZE - 6];

            packet->angle_z = buffer[ANGLE_PACKET_SIZE - 13];
            packet->angle_z = (packet->angle_z << 8) | buffer[ANGLE_PACKET_SIZE - 12];
            packet->angle_z = (packet->angle_z << 8) | buffer[ANGLE_PACKET_SIZE - 11];
            packet->angle_z = (packet->angle_z << 8) | buffer[ANGLE_PACKET_SIZE - 10];

            break;

        case PARAMETER_PACKET:

            packet->packet_type = PARAMETER_PACKET;

            packet->pk = buffer[PARAMETER_PACKET_SIZE - 5];
            packet->pk = (packet->pk << 8) | buffer[PARAMETER_PACKET_SIZE - 4];
            packet->pk = (packet->pk << 8) | buffer[PARAMETER_PACKET_SIZE - 3];
            packet->pk = (packet->pk << 8) | buffer[PARAMETER_PACKET_SIZE - 2];

            packet->ik = buffer[PARAMETER_PACKET_SIZE - 9];
            packet->ik = (packet->ik << 8) | buffer[PARAMETER_PACKET_SIZE - 8];
            packet->ik = (packet->ik << 8) | buffer[PARAMETER_PACKET_SIZE - 7];
            packet->ik = (packet->ik << 8) | buffer[PARAMETER_PACKET_SIZE - 6];

            packet->dk = buffer[PARAMETER_PACKET_SIZE - 13];
            packet->dk = (packet->dk << 8) | buffer[PARAMETER_PACKET_SIZE - 12];
            packet->dk = (packet->dk << 8) | buffer[PARAMETER_PACKET_SIZE - 11];
            packet->dk = (packet->dk << 8) | buffer[PARAMETER_PACKET_SIZE - 10];

            packet->pid_type = buffer[PARAMETER_PACKET_SIZE - 14];

            break;

        case POWER_PACKET:
            packet->packet_type = POWER_PACKET;
            packet->power = buffer[POWER_PACKET_SIZE - 3];
            packet->power = (packet->power << 8) | buffer[POWER_PACKET_SIZE - 2];
            break;

        case EMERGENCY_PACKET:
            packet->packet_type = EMERGENCY_PACKET;
            break;

        case ERROR_PACKET:

            packet->packet_type = ERROR_PACKET;
            uint8_t j;
            for(j=0;j<ERROR_PACKET_SIZE-1;j++)
                packet->error[j] = buffer[ERROR_PACKET_SIZE - (j+2)] ;
            break;

        case PING_PACKET:
            packet->packet_type = PING_PACKET;
            break;

        case HANDSHAKE_PACKET:
            packet->packet_type = HANDSHAKE_PACKET;
            break;

        case ALTITUDE_PACKET:
            packet->packet_type = ALTITUDE_PACKET;
            packet->hold_altitude = buffer[ALTITUDE_PACKET_SIZE-2];
            break;

        case BACK_PACKET:

            packet->packet_type = BACK_PACKET;

            packet->backType = buffer[BACK_PACKET_SIZE - 6];

            packet->backValue = buffer[BACK_PACKET_SIZE - 5];
            packet->backValue = (packet->backValue << 8) | buffer[BACK_PACKET_SIZE - 4];
            packet->backValue = (packet->backValue << 8) | buffer[BACK_PACKET_SIZE - 3];
            packet->backValue = (packet->backValue << 8) | buffer[BACK_PACKET_SIZE - 2];
            break;    

        default:
            PRINTF("Packet Parser :There is No packet type as you wish!!\n");
            break;
    }
}














































