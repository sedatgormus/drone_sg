#ifndef PARSE_PACKET_H_
#define PARSE_PACKET_H_

#include "fix16.h"

// PACKET TYPE
#define HANDSHAKE_PAC 0
#define CONTROL_PAC   1
#define EMERGENCY_PAC 2
#define POWER_PAC     3
#define PID_PAC       4
#define PING_PAC            5
#define MOVE_REQUEST_PAC    6
#define REPLY_PAC           7
#define FORM_FLIGHT_PAC     8
#define ALTITUTE_HOLD_PAC   9


// PACKET SIZE

#define HAND_PAC_SIZE           1
#define CONTROL_PAC_SIZE        17
#define EMRG_PAC_SIZE           1
#define POWER_PAC_SIZE          2
#define PID_PAC_SIZE            1

#define PING_PAC_SIZE            2  // 1 - TYPE  1-HAREKET VAR MI ? 
#define MOVE_REQUEST_PAC_SIZE    8  // 1 - TYPE  1-HAREKET TYPE      4-YÜKSEKLİK  4-Z AÇISI
#define REPLY_PAC_SIZE           3   // 1 - TYPE  1-HAREKET TYPE  1-İZİN
#define FORM_FLIGHT_PAC_SIZE     6   // 1-TYPE 1-FORM_START ? 4-ANGLE_Z
#define ALTITUTE_HOLD_PAC_SIZE   4  // 1-TYPE 1-ıs_hold 4-YÜKSEKLİK

//reply type
#define REPLY_FALSE       1
#define REPLY_TRUE        0
// ıs_move type
#define MOVE_TRUE         0
#define MOVE_FALSE        1
// move type
#define MOVE_ALTITUTE     1
#define MOVE_ANGLE        2
#define REPLY_FORM_START  3
#define REPLY_FORM_STOP   4
// ıs_form type
#define FORM_START        0
#define FORM_STOP         1
//ıs_hold type
#define HOLD_ALT_TRUE     0
#define HOLD_ALT_FALSE    1

typedef struct  quad_control
{
    uint8_t packet_type;

    //CONTROL PACKET
    fix16_t fyaw; 
    fix16_t froll;
    fix16_t fpitch;
    fix16_t fpower;

    uint8_t on_off;
    uint8_t is_move;
    uint8_t move_type;
    uint16_t altitute;
    uint8_t reply_type;
    uint8_t is_form;
    uint8_t is_hold;
}quad_control_packet;

void parse_packet(const uint8_t * buffer, quad_control_packet * packet, int len);
void create_packet(const quad_control_packet *packet,uint8_t *buffer);

#endif