#ifndef PACKET_PARSER_H_
#define PACKET_PARSER_H_

#include "fix16.h"


// PACKET TYPE
#define ANGLE_PACKET      0
#define PARAMETER_PACKET  1
#define POWER_PACKET      2
#define EMERGENCY_PACKET  3
#define ERROR_PACKET      4
#define PING_PACKET       5
#define HANDSHAKE_PACKET  6
#define ALTITUDE_PACKET   7
#define BACK_PACKET       8



// PACKET SIZE
#define ANGLE_PACKET_SIZE     13
#define PARAMETER_PACKET_SIZE 14
#define POWER_PACKET_SIZE      3
#define EMERGENCY_PACKET_SIZE  1
#define ERROR_PACKET_SIZE     51
#define PING_PACKET_SIZE       1
#define HANDSHAKE_PACKET_SIZE  1
#define ALTITUDE_PACKET_SIZE   2
#define BACK_PACKET_SIZE       6


typedef struct quad_control {

    uint8_t packet_type;

    //ANGLE PACKET
    fix16_t angle_x;
    fix16_t angle_y;
    fix16_t angle_z;

    //PARAMETER PACKET

    fix16_t pk;
    fix16_t dk;
    fix16_t ik;
    uint8_t pid_type;   // 0 -> X  1 -> Y // 2 -> Z

    //POWER PACKET

    uint16_t power;

    // ERROR_PACKET

    int8_t error[50];
    //ALTITUDE PACKET
    uint8_t hold_altitude;

    // BACK PACKET
    fix16_t backValue;
    uint8_t backType;
} quad_control_packet;


void create_quad_packet(const quad_control_packet *packet, uint8_t *buffer);
void parse_quad_packet(const uint8_t * buffer ,quad_control_packet *packet ,int len);


#endif /* PACKET_PARSER_H_ */