#ifndef CONTROL_H
#define CONTROL_H

#include "contiki.h"

#define MIN_THROTTLE 7.0f
#define MAX_THROTTLE 707.0f
#define STOP_THROTTLE 2.0f

static float throttle;
static float back_x = 1.0f;
static float back_y = 1.0;
	
typedef struct{
	int16_t M1;   	// MOTOR_FRONT_LEFT  
	int16_t M2;		// MOTOR_FRONT_RIGHT
	int16_t M3;		// MOTOR_BACK_LEFT 
	int16_t M4;		// MOTOR_BACK_RIGHT
} motor_t;


void control_initilize();

void setBackCoff(float value,uint8_t type);

void control_set_throttle(float th);

motor_t control_motors(float pitchPid,float rollPid,float yawPid,float ultPid);

#endif