#include "control.h"

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"


void 
control_initilize()
{
	throttle = 1;
}

void 
setBackCoff(float value,uint8_t type)
{
	if(value > 1.0f){
		switch(type){
			case 0:
				back_x = value;
				break;
			case 1:
				back_y = value;
				break;
			default:
				break;
		}
	}
}

void 
control_set_throttle(float th)
{

	if (th <= MAX_THROTTLE){
		throttle = th;
	}
	else{
		throttle = MAX_THROTTLE;
	}
}


motor_t 
control_motors(float pitch,float roll,float yaw,float ultPid)
{	
	
	motor_t result;
	//PRINTF(" throttle %d \n",throttle );
	if(throttle == STOP_THROTTLE || throttle < MIN_THROTTLE){
		result.M1 = STOP_THROTTLE;
		result.M2 = STOP_THROTTLE;
		result.M3 = STOP_THROTTLE;
		result.M4 = STOP_THROTTLE;

		return result;
	}
	
	
	//result.M1 = throttle + fix16_to_int(fix16_sadd( rollPid,-pitchPid));
	result.M1 = (int16_t)((throttle*back_y) - roll - pitch + yaw + ultPid + 0.5f);
	if(result.M1 < MIN_THROTTLE){
		result.M1 = MIN_THROTTLE;
	}
	else if(result.M1 > MAX_THROTTLE){
		result.M1 = MAX_THROTTLE;
	}

	//result.M2 = throttle + fix16_to_int(fix16_sadd( -rollPid,-pitchPid));
	result.M2 = (int16_t)((throttle) + roll - pitch - yaw + ultPid + 0.5f) ;
	if(result.M2 < MIN_THROTTLE){
		result.M2 = MIN_THROTTLE;
	}
	else if(result.M2 > MAX_THROTTLE){
		result.M2 = MAX_THROTTLE;
	}

	//result.M3 = throttle + fix16_to_int(fix16_sadd( rollPid,pitchPid));
	result.M3 = (int16_t)((throttle*back_x*back_y) - roll + pitch - yaw + ultPid + 0.5f);
	if(result.M3 < MIN_THROTTLE){
		result.M3 = MIN_THROTTLE;
	}
	else if(result.M3 > MAX_THROTTLE){
		result.M3 = MAX_THROTTLE;
	}

	result.M4 = (int16_t)((throttle*back_x) + roll + pitch + yaw + ultPid + 0.5f);
	if(result.M4 < MIN_THROTTLE){
		result.M4 = MIN_THROTTLE;
	}
	else if(result.M4 > MAX_THROTTLE){
		result.M4 = MAX_THROTTLE;
	}

	return result;
}	
