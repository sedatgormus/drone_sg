#ifndef SONAR_H
#define SONAR_H

#define TRIGER_PIN   0
#define ECHO_PIN     1 
#define SPEED_OF_SOUND 34300.0f			// 34300 cm/s


void sonar_init();

float sonar_get_value();

#endif 	/* SONAR_H */
