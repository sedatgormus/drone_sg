#include "contiki.h"
#include "sonar.h"


#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"

void 
sonar_init()
{

  GPIO_SET_OUTPUT(GPIO_D_BASE, GPIO_PIN_MASK(TRIGER_PIN));
  GPIO_SET_INPUT(GPIO_D_BASE, GPIO_PIN_MASK(ECHO_PIN));
  GPIO_CLR_PIN(GPIO_D_BASE, GPIO_PIN_MASK(TRIGER_PIN));
}


float 
sonar_get_value()
{
  rtimer_clock_t rt_trig_st = RTIMER_NOW();
  GPIO_SET_PIN(GPIO_D_BASE, GPIO_PIN_MASK(TRIGER_PIN));

  while( (RTIMER_NOW()-rt_trig_st) <= 100);
  GPIO_CLR_PIN(GPIO_D_BASE, GPIO_PIN_MASK(TRIGER_PIN));
      
  while(GPIO_READ_PIN(GPIO_D_BASE, GPIO_PIN_MASK(ECHO_PIN)) == 0);
  rtimer_clock_t rt_echo_start = RTIMER_NOW();  
  while(GPIO_READ_PIN(GPIO_D_BASE, GPIO_PIN_MASK(ECHO_PIN)) == GPIO_PIN_MASK(ECHO_PIN));
  
  rtimer_clock_t rt_echo_end = RTIMER_NOW(); 
  
  float distance = SPEED_OF_SOUND / 2.0f * ((float)(rt_echo_end - rt_echo_start)/(float)RTIMER_SECOND);
  PRINTF("Distance %d\n",(uint16_t)distance);
  return distance;
}







