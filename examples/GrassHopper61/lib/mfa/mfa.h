#ifndef MFA_H
#define MFA_H

#include "contiki.h"

#define ARRAY_SIZE 10

typedef struct  
{
	uint8_t turn;
	float array[ARRAY_SIZE];;
} mfa_t;

void mfa_seed(const float seed, mfa_t *info);

float get_mfa(const float value, mfa_t *info);

#endif // EMA_H
