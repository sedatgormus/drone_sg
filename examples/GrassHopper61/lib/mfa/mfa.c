#include "mfa.h"
#include "contiki.h"


void 
mfa_seed(const float seed, mfa_t *info)
{
	info->turn = 0;
    int i = 0;
  	for(; i < ARRAY_SIZE; i++)
    	info->array[i] = seed;
  
}

float
get_mfa(const float value, mfa_t *info)
{   
  float result = 0;
  int i = 0;
  info->array[info->turn] = value;
  for(; i < ARRAY_SIZE; i++){
    result+= info->array[i];
  }

  result /= ARRAY_SIZE;
  
  if(info->turn == ARRAY_SIZE-1)
    info->turn = 0;
  else
    info->turn++;

  return result;
}
