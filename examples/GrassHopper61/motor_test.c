#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include "dev/pwm.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>

#include "packet_parser.h"


#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define QUAD_CONTROL_PORT    3461

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define MOTOR_FRONT_LEFT  1
#define MOTOR_FRONT_RIGHT 2
#define MOTOR_BACK_LEFT   3
#define MOTOR_BACK_RIGHT  4
#define MOTOR_ALL         5

static struct uip_udp_conn *quad_conn;

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define PWM_FREQUENCY 400
#define DUTY_MAX 959
#define DUTY_MIN 5

static uint8_t hand_shake = 0;

static uip_ipaddr_t remote_ipaddr;

static void motor_pwm(uint16_t duty,uint8_t motor);
static void pwm_calibration();

/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
 quad_control_packet pac;
  char * buffer = uip_appdata;
  if(uip_newdata()){  
    parse_quad_packet(buffer,&pac,uip_datalen());  
    if(hand_shake){
      switch(pac.packet_type)
      {     
        case POWER_PACKET:
          PRINTF("POWER Packet is arrived\n");
          motor_pwm(pac.power,MOTOR_ALL);
          PRINTF("POWER: %d\n",pac.power);
          break;

        case EMERGENCY_PACKET:
          PRINTF("EMERGENCY PACKET is arrived\n");
          motor_pwm(2,MOTOR_ALL);
          break;
        
        default:
          PRINTF("UNKNOWN PACKET TYPE\n");
          break;
      }
    }else{
      PRINTF("HandShake Packet is send\n");
      uip_ipaddr_copy(&remote_ipaddr, &UIP_IP_BUF->srcipaddr);
      uint8_t response = 12;
      motor_pwm(DUTY_MIN,MOTOR_ALL);
      PRINTF("ESC CALIBRATION END\n");
      hand_shake = 1; 
      uip_udp_packet_sendto(quad_conn,(void *)&response,sizeof(uint8_t), &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xdaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
  PRINTF("IP ADDRESS: ");
  PRINT6ADDR(&ipaddr);
  PRINTF("\n");
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  
  PROCESS_BEGIN();
  static struct etimer pwm_timer;
  PROCESS_PAUSE();

  set_global_address();
 
  PRINTF("GRASSHOPPER61 FLIGHT CONTROLLER TEST\n");
  
  PRINTF("ESC CALIBRATION START\n");
  
  pwm_calibration();
  
  quad_conn = udp_new(NULL, UIP_HTONS(0), NULL); 
  if(quad_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(quad_conn, UIP_HTONS(QUAD_CONTROL_PORT)); 
  
  
#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */

  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
	}
}

  PROCESS_END();
}



static void 
pwm_calibration()
{   
     INTERRUPTS_DISABLE();
    if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
      PRINTF("PWM ERROR\n");
    if( pwm_start(PWM_TIMER_0, PWM_TIMER_B, GPIO_C_NUM, 5) != PWM_SUCCESS )
      PRINTF("PWM ERROR 2\n");

    if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
      PRINTF("PWM ERROR\n");
    if( pwm_start(PWM_TIMER_1, PWM_TIMER_B, GPIO_C_NUM, 1) != PWM_SUCCESS )
      PRINTF("PWM ERROR 2\n");

    if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
      PRINTF("PWM ERROR\n");
    if( pwm_start(PWM_TIMER_2, PWM_TIMER_B, GPIO_C_NUM, 6) != PWM_SUCCESS )
      PRINTF("PWM ERROR 2\n");

    if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
      PRINTF("PWM ERROR\n");
    if( pwm_start(PWM_TIMER_3, PWM_TIMER_B, GPIO_C_NUM, 7) != PWM_SUCCESS )
      PRINTF("PWM ERROR 2\n");

    INTERRUPTS_ENABLE();
}

static void 
motor_pwm(uint16_t duty,uint8_t motor)
{
  switch(motor){
    case MOTOR_FRONT_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

   case MOTOR_FRONT_RIGHT:
      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_RIGHT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_ALL:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;
  }
}





