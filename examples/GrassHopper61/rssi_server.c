#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/rpl/rpl.h"
#include "net/netstack.h"
#if PLATFORM_HAS_BUTTON
#include "dev/button-sensor.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "packet_parser.h"
#include "kalman.h"
#include "ema.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#endif /* TSCH_TIME_SYNCH */


#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
static uip_ipaddr_t remote_ipaddr;

#define UDP_CLIENT_PORT 8765
#define UDP_SERVER_PORT 5678

static struct uip_udp_conn *server_conn;

uint16_t ping_count = 0;
float   rssi = 0;

static kalman_t filter_info;
uint8_t init = 0;

static ema_t ema_info;
static uint16_t turn_mfa = 0;
const static int wind_size = 10;
float array_mfa[1000];

void send_handshake();
/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/

static float 
get_mfa(float *array , float value )
{
  
  float result = 0;
  int i = 0;
  array[turn_mfa] = value;
  for(; i<wind_size; i++){
    result+= array[i];
  }

  result/=wind_size;
  
  if(turn_mfa == wind_size-1)
    turn_mfa = 0;
  else
    turn_mfa++;

  return result;
}

static void
tcpip_handler(void)
{  
  int pac_rssi;
  float temp_ema;
  float temp_kalman;
  float temp_mfa;
  char *appdata;
  quad_control_packet pac;
  if(uip_newdata()){
    appdata = (char *)uip_appdata;
    parse_quad_packet(appdata,&pac,uip_datalen()); 
    switch(pac.packet_type)
    {      
      case HANDSHAKE_PACKET:
        PRINTF("Handshake Packet arrived\n ");
        uip_ipaddr_copy(&remote_ipaddr, &UIP_IP_BUF->srcipaddr);
        send_handshake();
        break;

      case PING_PACKET:
        pac_rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);
        if(!init){
          kalman_init(0.02,3,1,(float)pac_rssi,&filter_info);
          exponantial_seed((float)pac_rssi,&ema_info);
          int j = 0;
          for(;j<wind_size;j++){
            array_mfa[j] = (float)pac_rssi;
          }
          init = 1;
          PRINTF("RAW VALUES    FILTERED VALUES\n");
        }
        temp_ema = exponantial_move_average((float)pac_rssi,&ema_info);
        temp_kalman = kalman_getFilteredValue((float)pac_rssi,&filter_info);
        temp_mfa = get_mfa(array_mfa,(float)pac_rssi);
        /*if(temp <(-33.0f)){
          PRINTF("DIŞARDA :%d\n",(int)(temp*1000.0f));
        }else{
          PRINTF("1m ICINDE \n");
        }*/
        PRINTF("%d %d %d %d\n",pac_rssi,(int)(temp_kalman*1000.0f),(int)(temp_ema*1000.0f),(int)(temp_mfa*1000.0f));
        if(ping_count > 200)
          rssi+= temp_kalman;
        ping_count++;
        /*rssi += (float)pac_rssi;
        PRINTF("PING Packet arrived %d \n",pac_rssi);
        ping_count++;*/
        break;

      case END_PACKET:
        PRINTF("END PACKET Packet arrived\n ");
        float record = rssi/((float)ping_count-200);
        PRINTF("RSSI ANALYZE %d\n count: %d\n",(int)(record*1000.0f),ping_count);
        break;
    }

    /*PRINTF("DATA recv from ");
    PRINTF("%d",
           UIP_IP_BUF->srcipaddr.u8[sizeof(UIP_IP_BUF->srcipaddr.u8) - 1]);
    PRINTF("\nRSSI of the last Received Frame is %d\n", packetbuf_attr(PACKETBUF_ATTR_RSSI));*/
  } 

}
/*---------------------------------------------------------------------------*/
static void
set_own_addresses(void)
{

  uip_ipaddr_t ipaddr;
  struct uip_ds6_addr *root_if;

  #if 1
/* Mode 1 - 64 bits inline */
   uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 5);
#elif 0
/* Mode 2 - 16 bits inline */
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0x00ff, 0xfe00, 1);
#else
/* Mode 3 - derived from link local (MAC) address */
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
#endif

  uip_ds6_addr_add(&ipaddr, 0, ADDR_MANUAL);
  root_if = uip_ds6_addr_lookup(&ipaddr);
  if(root_if != NULL){
    rpl_dag_t *dag;
    dag = rpl_set_root(RPL_DEFAULT_INSTANCE,(uip_ip6addr_t *)&ipaddr);
    uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
    rpl_set_prefix(dag, &ipaddr, 64);
    PRINTF("created a new RPL dag\n");
  }else{
    PRINTF("failed to create a new RPL DAG\n");
  }
  
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{

  PROCESS_BEGIN();
  PROCESS_PAUSE();

  set_own_addresses();
 
  server_conn = udp_new(NULL, UIP_HTONS(UDP_CLIENT_PORT), NULL);
  if(server_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(server_conn, UIP_HTONS(UDP_SERVER_PORT));
  
  
#if TSCH_TIME_SYNCH
  foure_timesynch_init(1);
#endif /* TIMESYNCH_CONF_ENABLED */

  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
	  }
  }

  PROCESS_END();
}

static void 
send_to_remote(unsigned char * data,int len)
{
  uip_udp_packet_sendto(server_conn,data, len, &remote_ipaddr, UIP_HTONS(UDP_CLIENT_PORT));
}

void send_handshake(){
  quad_control_packet pac2;
  pac2.packet_type = HANDSHAKE_PACKET;
  uint8_t sbuffer[HANDSHAKE_PACKET_SIZE];
  create_quad_packet(&pac2, sbuffer);
  send_to_remote(sbuffer,HANDSHAKE_PACKET_SIZE);
}







