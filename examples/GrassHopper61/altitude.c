#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include "dev/pwm.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>

#include "mpudriver.h"
#include "math.h"
#include "pid.h"
#include "fix16.h"
#include "control.h"
#include "packet_parser.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define QUAD_CONTROL_PORT    3461

#define DEBUG DEBUG_PRINT 
#include "net/ip/uip-debug.h"

#define MOTOR_FRONT_LEFT  1
#define MOTOR_FRONT_RIGHT 2
#define MOTOR_BACK_LEFT   3
#define MOTOR_BACK_RIGHT  4
#define MOTOR_ALL         5

static struct uip_udp_conn *quad_conn;

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define PWM_FREQUENCY 400

#define SAMPLING_TIME_7MS ((clock_time_t)1)            // ~7ms

static  uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
static  uint16_t fifoCount;     // count of all bytes currently in FIFO

static uint8_t hand_shake = 1;
static uint8_t set_yaw_axis = 0;
static uip_ipaddr_t remote_ipaddr;
static euler_t set_point;
static euler_t input;
static quad_control_packet error_packet;

static void fly_control(float accel);
static float get_angles();
static void motor_pwm(uint16_t duty,uint8_t motor);
static void pwm_calibration();

static uint16_t sample_count = 0;
static uint8_t kere = 0;
static uint8_t sample =0;

static float mesafe = 0;

/********************** Eski Pervane Icin PID Katsayilari *********************************/


//static  float rollKp = 1.2, rollKi = 0.45, rollKd = 0.25;

//static  float pitchKp = 1.5f, pitchKi = 0.35f, pitchKd = 0.25f; // backCoff = 1.23 
/******************************************************************************************/

/******************** Yeni Pervane Icin PID Katsayilari *********************************/


//static  float rollKp = 1, rollKi = 0.3, rollKd = 0.19;

//static  float pitchKp = 1.0f, pitchKi = 0.35f, pitchKd = 0.2f;  // backCoff = 1.18

//static  float yawKp = 1.0f, yawKi = 0.15f, yawKd = 0.22f;
/******************************************************************************************/

static  float rollKp = 1.0f, rollKi = 0.3f, rollKd = 0.19f;

static  float pitchKp = 1.0f, pitchKi = 0.35f, pitchKd = 0.2f;  // backCoff = 1.35

static  float yawKp = 1.0f, yawKi = 0.15f, yawKd = 0.22f;

/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  quad_control_packet pac;
  char * buffer = uip_appdata;
  if(uip_newdata()){  
    parse_quad_packet(buffer,&pac,uip_datalen());  
    if(hand_shake){
      switch(pac.packet_type)
      {      
        case POWER_PACKET:
          PRINTF("POWER Packet is arrived\n");
          if(pac.power <= MIN_THROTTLE-1 && pac.power >= 0){
            pid_set_coefficients(pitchKp,0,pitchKd,1);
            pid_set_coefficients(rollKp,0,rollKd,2);
            pid_set_coefficients(yawKp,0,yawKd,3);
            pid_reset();
          }else{
            pid_set_coefficients(pitchKp,pitchKi,pitchKd,1);
            pid_set_coefficients(rollKp,rollKi,rollKd,2);
            pid_set_coefficients(yawKp,yawKi,yawKd,3);
          }
          control_set_throttle(pac.power);
          PRINTF("POWER: %d\n",pac.power);
          break;

        case PARAMETER_PACKET:
          PRINTF("PARAMETER PACKET is arrived\n");
          switch(pac.pid_type){
            case 1:
              pitchKp = fix16_to_float(pac.pk);
              pitchKi = fix16_to_float(pac.ik);
              pitchKd = fix16_to_float(pac.dk);
              pid_set_coefficients(pitchKp,pitchKi,pitchKd,1);
              PRINTF("Pitch Gains: p: %d i: %d d: %d\n",(int16_t)(pitchKp*1000.0f), (int16_t)(pitchKi*1000.0f),(int16_t)(pitchKd*1000.0f));
              break;
            case 2:
              rollKp = fix16_to_float(pac.pk);
              rollKi = fix16_to_float(pac.ik);
              rollKd = fix16_to_float(pac.dk);
              pid_set_coefficients(rollKp,rollKi,rollKd,2);
              PRINTF("Roll Gains: p: %d i: %d d: %d\n",(int16_t)(rollKp*1000.0f), (int16_t)(rollKi*1000.0f),(int16_t)(rollKd*1000.0f));
              break;
            case 3:
              yawKp = fix16_to_float(pac.pk);
              yawKi = fix16_to_float(pac.ik);
              yawKd = fix16_to_float(pac.dk);
              pid_set_coefficients(yawKp,yawKi,yawKd,3);
              PRINTF("Yaw Gains: p: %d i: %d d: %d\n",(int16_t)(yawKp*1000.0f), (int16_t)(yawKi*1000.0f),(int16_t)(yawKd*1000.0f));
              break;
            default:
              PRINTF("UNKNOWN\n");
              break;
          }
          pid_reset();
          break;

        case EMERGENCY_PACKET:
          PRINTF("EMERGENCY PACKET is arrived\n");
          pid_set_coefficients(pitchKp,0,pitchKd,1);
          pid_set_coefficients(rollKp,0,rollKd,2);
          pid_set_coefficients(yawKp,0,yawKd,3);
          pid_reset();
          control_set_throttle(MIN_THROTTLE - 10);
          break;

        case ANGLE_PACKET:
          PRINTF("ANGLE PACKET is arrived\n");
          if(pac.angle_x == 0 && pac.angle_y == 0 && pac.angle_z == 0){
            set_point.x = 0;
            set_point.y = 0;
          }else{
            set_point.x += fix16_to_float(pac.angle_x);
            set_point.y += fix16_to_float(pac.angle_y);
            set_point.z += fix16_to_float(pac.angle_z); 
          }
          PRINTF("Set P: x %d y %d z %d\n", (int)set_point.x,(int)set_point.y,(int)set_point.z);
          break; 

        case BACK_PACKET:
         PRINTF("BACK PACKET is arrived\n");
         PRINTF("Type: %d Back coff: %d\n",pac.backType,(uint16_t)(fix16_to_float(pac.backValue)*1000.0f));
         setBackCoff(fix16_to_float(pac.backValue),pac.backType);
         break; 
        
        default:
          PRINTF("UNKNOWN PACKET TYPE\n");
          break;
      }
    }else{
      PRINTF("HandShake Packet is send\n");
      uip_ipaddr_copy(&remote_ipaddr, &UIP_IP_BUF->srcipaddr);
      uint8_t response = 12;
      motor_pwm(PWM_DUTY_MIN,MOTOR_ALL);
      hand_shake = 1;
      uip_udp_packet_sendto(quad_conn,(void *)&response,sizeof(uint8_t), &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xdaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
  PRINTF("IP ADDRESS: ");
  PRINT6ADDR(&ipaddr);
  PRINTF("\n");
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer set;

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  set_global_address();

  PRINTF("GRASSHOPPER61 FLIGHT CONTROLLER TEST\n");

 if(!mpu6050_testConnection()){
    // Can not connect to MPU6050 sensor. 
    PRINTF("Cannot Connect Mpu6050\n");
    PROCESS_EXIT();                          
  }

  mpu6050_initialize();
  pwm_calibration();
  
  if(mpu6050_getDMPEnabled()){
    PRINTF("Resetting DMP\n");
    packetSize = MPU6050_DEFAULT_PACKET_SIZE;
    fifoCount = 0;
    mpu6050_resetFIFO();
  }else{
    uint8_t devStatus = mpu6050_dmpInitialize();
  
    if(devStatus == 0) {
      // turn on the DMP, now that it's ready
      PRINTF("Enabling DMP...\n");
      mpu6050_setDMPEnabled(1);
      fifoCount = 0;
      // get expected DMP packet size for later comparison
      packetSize = mpu6050_dmpGetFIFOPacketSize();
    }else {
      PRINTF("DMP Initilaze Failed\n");
      PROCESS_EXIT();
    }
  }

  
  /* new connection with remote host */
  quad_conn = udp_new(NULL, UIP_HTONS(0), NULL); 
  if(quad_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(quad_conn, UIP_HTONS(QUAD_CONTROL_PORT)); 
  
  PRINTF("PID COEFFICIENTS ARE SET\n");
  pid_initilize(pitchKp, pitchKi, pitchKd, rollKp, rollKi, rollKd, yawKp , yawKi ,yawKd); 

  control_initilize();

  set_point.x = 0;
  set_point.y = 0;
  set_point.z = 0;
 
 
#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */

  etimer_set(&periodic,CLOCK_SECOND*5);
  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
    }
    else if(data == &periodic){
      ;
      fly_control(get_angles());
      etimer_set(&periodic,SAMPLING_TIME_7MS);
      /*if(kere == 0){
        kere=1;
         etimer_set(&set,CLOCK_SECOND);

      }
      sample_count++;*/
    }
    /*else if(data == &set){
      PRINTF("sample count -> %d\n",sample_count );
      sample_count=0;
      etimer_reset(&set);
    }*/
  }

  PROCESS_END();
}

static void 
send_to_remote(unsigned char * data,int len)
{
  uip_udp_packet_sendto(quad_conn,data, len, &remote_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
}


static float 
get_angles()
{
  int16_t accel = mpu6050_getAccelerationZ();
  float acceleration_z_axis = ((float)accel / 16384.0f - 1.0f)* 9.8f;
  //PRINTF("Ivmelenme: %d\n",(int)(acceleration_z_axis*1000.0f));
  return acceleration_z_axis;
}

static uint32_t last_time_pid = 0;
  
static void 
fly_control(float accel)
{
  uint32_t now = (uint32_t)clock_time();
  //PRINTF("PID TIME : %d\n",(now*1000-last_time_pid*1000)/128);
  float dt = ((float)(now - last_time_pid)) / CLOCK_SECOND;
  //PRINTF("dt : %d\n", (uint16_t)(dt*1000.0f));
  last_time_pid = now;

  mesafe += accel*dt;
  PRINTF("Mesafe : %d \n",(int)(mesafe*1000.0f));
}

static void 
pwm_calibration()
{
    INTERRUPTS_DISABLE();
    pwm_enable(PWM_FREQUENCY, PWM_DUTY_MAX, PWM_TIMER_0, PWM_TIMER_B);
    pwm_start(PWM_TIMER_0, PWM_TIMER_B, GPIO_C_NUM, 1);

    pwm_enable(PWM_FREQUENCY, PWM_DUTY_MAX, PWM_TIMER_1, PWM_TIMER_B);
    pwm_start(PWM_TIMER_1, PWM_TIMER_B, GPIO_C_NUM, 5);

    pwm_enable(PWM_FREQUENCY, PWM_DUTY_MAX, PWM_TIMER_2, PWM_TIMER_B);
    pwm_start(PWM_TIMER_2, PWM_TIMER_B, GPIO_C_NUM, 6);

    pwm_enable(PWM_FREQUENCY, PWM_DUTY_MAX, PWM_TIMER_3, PWM_TIMER_B);
    pwm_start(PWM_TIMER_3, PWM_TIMER_B, GPIO_C_NUM, 7);
    INTERRUPTS_ENABLE();  
}

static void 
motor_pwm(uint16_t duty,uint8_t motor)
{
  switch(motor){
    case MOTOR_FRONT_LEFT:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B); 
      INTERRUPTS_ENABLE();
      break;

   case MOTOR_FRONT_RIGHT:
      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_LEFT:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_RIGHT:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_ALL:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;
  }
}





