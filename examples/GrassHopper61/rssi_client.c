#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include "dev/pwm.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>
#include "packet_parser.h"


#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define DEBUG DEBUG_NONE
#include "net/ip/uip-debug.h"

#define UDP_CLIENT_PORT 8765
#define UDP_SERVER_PORT 5678

static struct uip_udp_conn *client_conn;


#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

uint16_t ping_count = 0;


static struct etimer handshake_timer;
static struct etimer ping_timer;

void send_handshake();
void send_ping();
void send_end();


/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/

static void
tcpip_handler(void)
{  
  char *appdata;
  quad_control_packet pac;
  if(uip_newdata()){
    appdata = (char *)uip_appdata;
    parse_quad_packet(appdata,&pac,uip_datalen()); 
    switch(pac.packet_type)
    {      
      case HANDSHAKE_PACKET:
        PRINTF("Handshake Packet is  arrived\n ");
        etimer_stop(&handshake_timer);
        etimer_set(&ping_timer,CLOCK_SECOND/2);
        break;
    }

    /*PRINTF("DATA recv from ");
    PRINTF("%d",
           UIP_IP_BUF->srcipaddr.u8[sizeof(UIP_IP_BUF->srcipaddr.u8) - 1]);
    PRINTF("\nRSSI of the last Received Frame is %d\n", packetbuf_attr(PACKETBUF_ATTR_RSSI));*/
  } 

}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xdaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
  PRINTF("IP ADDRESS: ");
  PRINT6ADDR(&ipaddr);
  PRINTF("\n");
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  PROCESS_BEGIN();
  PROCESS_PAUSE();

  set_global_address();
 
  /* new connection with remote host */
  client_conn = udp_new(NULL, UIP_HTONS(UDP_SERVER_PORT), NULL); 
  if(client_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(client_conn, UIP_HTONS(UDP_CLIENT_PORT)); 
  
  etimer_set(&handshake_timer,CLOCK_SECOND*2);
#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */

  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
	  }else if(data == &handshake_timer){
      //PRINTF("HANDSHAKE PACKET IS SEND\n");
      send_handshake();
      etimer_reset(&handshake_timer);
    }else if(data == &ping_timer){
      //PRINTF("PING PACKET IS SEND\n");
      if(ping_count<500){
        send_ping();
        ping_count++;
        etimer_reset(&ping_timer);
      }
      else{
        PRINTF("END PACKET IS SEND\n");
        send_end();
        etimer_stop(&ping_timer);
      }
    }
}

  PROCESS_END();
}



static void 
send_to_remote(unsigned char * data,int len)
{
  static uip_ipaddr_t server_ipaddr;
  //daaa::212:4b00:432:8bbf
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 5);
  uip_udp_packet_sendto(client_conn,data, len, &server_ipaddr, UIP_HTONS(UDP_SERVER_PORT));
}

void send_handshake(){
  quad_control_packet pac2;
  pac2.packet_type = HANDSHAKE_PACKET;
  uint8_t sbuffer[HANDSHAKE_PACKET_SIZE];
  create_quad_packet(&pac2, sbuffer);
  send_to_remote(sbuffer,HANDSHAKE_PACKET_SIZE);
}


void send_ping(){
  quad_control_packet pac2;
  pac2.packet_type = PING_PACKET;
  uint8_t sbuffer[PING_PACKET_SIZE];
  create_quad_packet(&pac2, sbuffer);
  send_to_remote(sbuffer,PING_PACKET_SIZE);
}

void send_end(){
  quad_control_packet pac2;
  pac2.packet_type = END_PACKET;
  uint8_t sbuffer[END_PACKET_SIZE];
  create_quad_packet(&pac2, sbuffer);
  send_to_remote(sbuffer,END_PACKET_SIZE);
}




