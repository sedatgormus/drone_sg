#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mpudriver.h"
#include "clock.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#endif /* TSCH_TIME_SYNCH */

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

static int32_t ax_off=0,ay_off=0,az_off=0,gx_off=0,gy_off=0,gz_off=0;
static struct etimer et;

static uint16_t i=0;
static uint16_t k=0;
static uint16_t bound = 1000;
/*---------------------------------------------------------------------------*/
PROCESS(test_process,"Test Process");
AUTOSTART_PROCESSES(&test_process);
/*---------------------------------------------------------------------------*/

uint8_t calib(){

 

  int16_t ax,ay,az,gx,gy,gz;
  
  if(i < bound){
    mpu6050_getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    i++;
    etimer_set(&et,1);
    return 0;
  }
     

  if(i>=bound && i< bound+1001){
      mpu6050_getMotion6(&ax, &ay, &az, &gx, &gy, &gz); 
      ax_off+=ax;
      ay_off+=ay;
      az_off+=az;
      gx_off+=gx;
      gy_off+=gy;
      gz_off+=gz;

      i++;
      k++;
      etimer_set(&et,1);
      return 0;
  }

  if(i == bound+1001){
    ax_off = ax_off/bound;
    ay_off = ay_off/bound;
    az_off = az_off/bound;
    gy_off = gy_off/bound;
    gx_off = gx_off/bound;
    gz_off = gz_off/bound;  

    printf("ax_off : %d ay_off %d az_off %d gx_off %d gy_off %d gz_off %d\n",ax_off,ay_off,az_off,gx_off,gy_off,gz_off);
    printf("k: %d\n",k);
    etimer_stop(&et);
    return 1;
  }
    
  return 0;
  
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(test_process, ev, data)
{
  PROCESS_BEGIN();

  PROCESS_PAUSE();
  
  static struct etimer et2;
  PRINTF("MPU 6050 TEST\n");
  #if PLATFORM_HAS_BUTTON
    SENSORS_ACTIVATE(button_sensor);
  #endif

  #if TSCH_TIME_SYNCH
    foure_timesynch_init(0);                                              
  #endif /* TIMESYNCH_CONF_ENABLED */
  

  
 if(!mpu6050_testConnection()){
    // Can not connect to MPU6050 sensor. 
    PRINTF("Cannot Connect Mpu6050\n");
    PROCESS_EXIT();                          
  }
  

  mpu6050_initialize(); 

  mpu6050_setXGyroOffset(0);
  mpu6050_setYGyroOffset(0);
  mpu6050_setZGyroOffset(0);
  mpu6050_setXAccelOffset(0);
  mpu6050_setYAccelOffset(0);
  mpu6050_setZAccelOffset(0);

  etimer_set(&et,CLOCK_SECOND*5);
  while(1){
    PROCESS_YIELD();
    if(data == &et){
      if(calib() == 1){
        mpu6050_setXGyroOffset(-gx_off/4);
        mpu6050_setYGyroOffset(-gy_off/4);
        mpu6050_setZGyroOffset(-gz_off/4);
        mpu6050_setXAccelOffset(15700/8);
        mpu6050_setYAccelOffset(27200/8);
        mpu6050_setZAccelOffset((16384-9930)/8);
        // ASıl const static int32_t ax_off = -15700, ay_off= -27200, az_off= 9930, gx_off= --146, gy_off= -54, gz_off = -253;

        etimer_set(&et2,CLOCK_SECOND*2);
      } 
    }
    else if(data == &et2){
      int16_t ax,ay,az,gx,gy,gz;
      mpu6050_getMotion6(&ax, &ay, &az, &gx, &gy, &gz); 
      printf("ax : %d\n",(ax));
      printf("ay : %d\n",(ay));
      printf("az : %d\n",(az));
      printf("gx : %d\n",(gx));
      printf("gy : %d\n",(gy));
      printf("gz : %d\n",(gz));
      etimer_set(&et2,CLOCK_SECOND/2);
    }
     
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
