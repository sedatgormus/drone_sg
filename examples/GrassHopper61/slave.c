#include "contiki.h"
#include "contiki-net.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
#include "sys/node-id.h"
#include "dev/pwm.h"
#include "clock.h"
#include "cpu.h"
#include <stdio.h>
#include <string.h>

#include "mpudriver.h"
#include "math.h"
#include "pid.h"
#include "fix16.h"
#include "control.h"
#include "parse_packet.h"
#include "kalman.h"
#include "sonar.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#endif /* TSCH_TIME_SYNCH */

#define QUAD_CONTROL_PORT    3461

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define MOTOR_FRONT_LEFT  1
#define MOTOR_FRONT_RIGHT 2
#define MOTOR_BACK_LEFT   3
#define MOTOR_BACK_RIGHT  4
#define MOTOR_ALL         5

static struct uip_udp_conn *quad_conn;
static uip_ipaddr_t master_ipaddr;

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])
#define PWM_FREQUENCY 400
#define DUTY_MAX 959
#define DUTY_MIN 5

#define SAMPLING_TIME_7MS ((clock_time_t)1)            // ~7ms

static  uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
static  uint16_t fifoCount;     // count of all bytes currently in FIFO

static uint8_t hand_shake = 0;
static uint8_t set_yaw_axis = 0;
static uint8_t pid_block  = 0;
static uint32_t last_time_pid = 0;

static struct etimer pwm_calib_timer;

//static uip_ipaddr_t remote_ipaddr;
static euler_t set_point;
static euler_t input;

static void fly_control(euler_t set,euler_t reference);
static euler_t get_angles();
static void motor_pwm(uint16_t duty,uint8_t motor);
static void pwm_calibration();

static  float rollKp = 1.0f, rollKi = 0.25f, rollKd = 0.3f;  //back_y = 1

static  float pitchKp = 1.55f, pitchKi = 0.25f, pitchKd = 0.31f;  // backCoff = 1.2

static  float yawKp = 1.55f, yawKi = 0.25f, yawKd = 0.3f;


//Formation flight eklenenler
static uint8_t altitute_control();
static uint8_t angle_control();
static uint8_t formation_flight(const uint8_t move ,const float rssi);
static float last_rssi;
static int temp_rssi;
static float pac_rssi;
static float default_rssi = -33.0f;
static uint8_t reply_buffer[REPLY_PAC_SIZE];


static kalman_t rssi_info;

static uint8_t hold_altitude = 0;
static struct etimer landing_timer;
static void landing();
static float get_altitude();
static void send_to_master(uint8_t* data,int len);


static float m_default_z;
static float s_default_z;
static uint8_t start_form;

/*---------------------------------------------------------------------------*/
PROCESS(quad_control_process, "Quad Control Process");
AUTOSTART_PROCESSES(&quad_control_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  quad_control_packet pac;
  char * buffer = uip_appdata;
  if(uip_newdata()){  
    parse_packet((uint8_t*)buffer,&pac,uip_datalen());  
    if(pac.packet_type == FORM_FLIGHT_PAC ){
      PRINTF("FORM_FLIGHT Packet is arrived\n");
      uip_ipaddr_copy(&master_ipaddr, &UIP_IP_BUF->srcipaddr);

      if(pac.is_form == FORM_START){
        PRINTF("Form Flight Start \n");
        if(!start_form){
          m_default_z = fix16_to_float(pac.fyaw);
          s_default_z = input.z;
          pwm_calibration();
          etimer_set(&pwm_calib_timer,CLOCK_SECOND*3);
          PRINTF("Kalibre oldu slave default z = %d master default z = %d \n",(int16_t)s_default_z,(int16_t)m_default_z);
          kalman_getFilteredValue((default_rssi-5.0f),&rssi_info);  // En basta gurultulu sinyal gelirse diye
        }
        start_form = 1;
        pac.packet_type = REPLY_PAC;
        pac.reply_type = REPLY_TRUE;
        pac.move_type = REPLY_FORM_START;
        create_packet(&pac, reply_buffer);
        send_to_master(reply_buffer,REPLY_PAC_SIZE);
          
      }else if(pac.is_form == FORM_STOP && start_form == 1){
        PRINTF("Form flight Stop and working\n");
        start_form = 0;
        etimer_set(&landing_timer,CLOCK_SECOND*3);
        pac.packet_type = REPLY_PAC;
        pac.reply_type = REPLY_TRUE;
        pac.move_type = REPLY_FORM_STOP;
        create_packet(&pac, reply_buffer);
        send_to_master(reply_buffer,REPLY_PAC_SIZE);
      }else if(pac.is_form == FORM_STOP && start_form == 0){
        PRINTF("Form flight Stop and working\n");
        pac.packet_type = REPLY_PAC;
        pac.reply_type = REPLY_TRUE;
        pac.move_type = REPLY_FORM_STOP;
        create_packet(&pac, reply_buffer);
        send_to_master(reply_buffer,REPLY_PAC_SIZE);
      }

    }else if(start_form){

      switch(pac.packet_type){  
        case POWER_PAC:
          PRINTF("Power Packet is arrived\n");
          if(pac.on_off){
            PRINTF("PID ACTİVE\n");
            pid_block = 1;
            control_set_throttle(MIN_THROTTLE);
          }else{
            PRINTF("PID CLOSE\n");
            last_time_pid = 0;
            pid_block = 0;
            pid_reset();
            control_set_throttle(STOP_THROTTLE);
          }
          break;  
        
        case MOVE_REQUEST_PAC:
          PRINTF("Move Request is arrived\n");
          if(pid_block){
            PRINTF("Move Request PID BLOCK ACIK \n"); 
            temp_rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);
            pac_rssi = kalman_getFilteredValue((float)temp_rssi,&rssi_info);
            PRINTF("RSSI DEGERI: %d \n",(int16_t)pac_rssi);
            if(pac_rssi >= default_rssi){
              if(pac.move_type == MOVE_ALTITUTE){
                PRINTF("1 m içinde yukselme istegi\n");
                pac.packet_type = REPLY_PAC;
                pac.reply_type = REPLY_TRUE;
                pac.move_type = MOVE_ALTITUTE; 
                create_packet(&pac, reply_buffer);
                send_to_master(reply_buffer,REPLY_PAC_SIZE);
                set_point.altitute = (float) pac.altitute;
                PRINTF("altitute istegi %d \n",(int16_t)set_point.altitute);
                hold_altitude = 1;          
              }else if(pac.move_type == MOVE_ANGLE){
                PRINTF("1 m icinde donme istegi\n");
                pac.packet_type = REPLY_PAC;
                pac.reply_type = REPLY_TRUE;
                pac.move_type = MOVE_ANGLE; 
                create_packet(&pac, reply_buffer);
                send_to_master(reply_buffer,REPLY_PAC_SIZE);
                set_point.z = s_default_z + fix16_to_float(pac.fyaw)-m_default_z;
                PRINTF("aci istegi: %d \n",(int16_t)set_point.z);
              }
            }else{
              PRINTF("1 m dışında istek ret\n");
              pac.packet_type = REPLY_PAC;
              pac.reply_type = REPLY_FALSE;
              pac.move_type = pac.move_type; // ?
              create_packet(&pac, reply_buffer);
              send_to_master(reply_buffer,REPLY_PAC_SIZE);
              pac.is_move = MOVE_FALSE;
              formation_flight(pac.is_move,pac_rssi);
            }
          }else
            PRINTF("PID BLOCK KAPALI \n");
          break;

        case PING_PAC:
          PRINTF("PING_PACKET is arrived\n");
          if(pid_block){
            PRINTF("PID BLOCK ACIK \n");
            temp_rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);
            pac_rssi = kalman_getFilteredValue((float)temp_rssi,&rssi_info);
            PRINTF("RSSI degeri: %d \n",(int16_t)pac_rssi);
            formation_flight(pac.is_move,pac_rssi);
          }else
            PRINTF("PID BLOCK KAPALI \n");
          break;
        
        case EMERGENCY_PAC:
          PRINTF("Emergency Packet is arrived\n");
          control_set_throttle(STOP_THROTTLE);
          pid_reset();
          pid_block = 0;
          last_time_pid = 0;
          break;

        default:
          PRINTF("Unknown Packet Type\n");
          break;
      }
    }else{
      PRINTF("Unknown Packet Type OUT: %d\n",pac.packet_type);

    }
  }
}
/*--------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xfe80, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
  PRINTF("IP ADDRESS: ");
  PRINT6ADDR(&ipaddr);
  PRINTF("\n");
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(quad_control_process, ev, data)
{
  static struct etimer periodic;

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  set_global_address();


  PRINTF("GRASSHOPPER61 FLIGHT CONTROLLER TEST\n");

  /*if(!mpu6050_testConnection()){
    // Can not connect to MPU6050 sensor. 
    PRINTF("Cannot Connect Mpu6050\n");
    PROCESS_EXIT();                          
  }

  
  if(mpu6050_getDMPEnabled()){
    PRINTF("Resetting DMP\n");   // TODO:
    packetSize = MPU6050_DEFAULT_PACKET_SIZE;
    fifoCount = 0;
    mpu6050_resetFIFO();
  }else{
    mpu6050_initialize();
    uint8_t devStatus = mpu6050_dmpInitialize();
  
    if(devStatus == 0) {
      PRINTF("Enabling DMP...\n");
      mpu6050_setDMPEnabled(1);
      fifoCount = 0;
      packetSize = mpu6050_dmpGetFIFOPacketSize();
    }else {
      PRINTF("DMP Initilaze Failed\n");
      PROCESS_EXIT();
    }
  }*/

  /* new connection with remote host */
  quad_conn = udp_new(NULL, UIP_HTONS(0), NULL); 
  if(quad_conn == NULL){
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  
  udp_bind(quad_conn, UIP_HTONS(QUAD_CONTROL_PORT)); 
  
  PRINTF("PID COEFFICIENTS ARE SET\n");
  pid_initilize(pitchKp, pitchKi, pitchKd, rollKp, rollKi, rollKd, yawKp , yawKi ,yawKd); 
  sonar_init();

  kalman_init(0.01,1,1,(float)default_rssi,&rssi_info);
  
  control_initilize();

  set_point.x = 0;
  set_point.y = 0;
  set_point.z = 0;
  set_point.altitute = 0;
 
 
#if TSCH_TIME_SYNCH
  foure_timesynch_init(0);
#endif /* TIMESYNCH_CONF_ENABLED */

  etimer_set(&periodic,CLOCK_SECOND*2);
  while(1){
    PROCESS_YIELD();
    if(ev == tcpip_event){
      tcpip_handler();
    }
    else if(data == &periodic){
      //input = get_angles();
      if(pid_block ){
        if(hold_altitude){
          input.altitute = get_altitude();
        }else{
          set_point.altitute = 0;
          input.altitute = 0;
        }
        fly_control(set_point,input);
      }else if(hand_shake){
        motor_t motors = control_motors(0,0,0);
        motor_pwm(motors.M1,MOTOR_FRONT_LEFT);
        motor_pwm(motors.M2,MOTOR_FRONT_RIGHT);
        motor_pwm(motors.M3,MOTOR_BACK_LEFT);
        motor_pwm(motors.M4,MOTOR_BACK_RIGHT);
      }
      etimer_set(&periodic,SAMPLING_TIME_7MS);
    }else if(data == &pwm_calib_timer){
      motor_pwm(DUTY_MIN,MOTOR_ALL);
      etimer_stop(&pwm_calib_timer);
      set_yaw_axis = 1;
      hand_shake = 1;
    }else if(data == &landing_timer){
      landing();
    }
  }

  PROCESS_END();
}

static void 
send_to_master(unsigned char * data,int len)
{
  uip_udp_packet_sendto(quad_conn,data, len, &master_ipaddr, UIP_HTONS(QUAD_CONTROL_PORT));
}


static euler_t
get_angles()
{
  euler_t result;
  quaternion_t q;           // [w, x, y, z]         quaternion container
  vector_float_t gravity;    // [x, y, z]            gravity vector
  float ypr[3];
  uint8_t fifoBuffer[packetSize]; 

  fifoCount = mpu6050_getFIFOCount();
  
  while (fifoCount < packetSize) {
    fifoCount = mpu6050_getFIFOCount();
  }
   
  if (fifoCount >= 1024 || packetSize > (1024-fifoCount) ) {
    mpu6050_resetFIFO();
    PRINTF("FIFO overflow!\n");
  }else{
    mpu6050_getFIFOBytes(fifoBuffer, packetSize);
    
    fifoCount = mpu6050_getFIFOCount();
    if(fifoCount % packetSize == 0 && fifoCount/packetSize>=1){
      mpu6050_getFIFOBytes(fifoBuffer, packetSize);
    }
      
    mpu6050_dmpGetQuaternion_q(&q, fifoBuffer);
    mpu6050_dmpGetGravity(&gravity, &q);
    mpu6050_dmpGetYawPitchRoll(ypr, &q, &gravity);


    if(set_yaw_axis){
      set_yaw_axis = 0;
      set_point.z = ypr[0];
    }
    //PRINTF("yaw : %d roll %d pitch %d\n",(int16_t)ypr[0],(int16_t)-ypr[1],(int16_t)ypr[2]);
    //PRINTF("yaw : %d \n",(int16_t)ypr[0]);
    
    result.z = ypr[0];
    result.y = -ypr[1];
    result.x = ypr[2];

    return result;
  }
}
  
static void 
fly_control(euler_t set_point, euler_t reference)
{
  uint32_t now = (uint32_t)clock_time();
  float dt = ((float)(now - last_time_pid)) / 128.0f;
  last_time_pid = now;

  pidout_t rate = pid_system_calculation(set_point,reference,dt);

  motor_t motors = control_motors(rate.pidX,rate.pidY,rate.pidZ);
  motor_pwm(motors.M1,MOTOR_FRONT_LEFT);
  motor_pwm(motors.M2,MOTOR_FRONT_RIGHT);
  motor_pwm(motors.M3,MOTOR_BACK_LEFT);
  motor_pwm(motors.M4,MOTOR_BACK_RIGHT);
  //PRINTF("M1 %d M2 :%d M3 %d M4 %d\n",motors.M1,motors.M2,motors.M3,motors.M4 );
  
}

static void 
pwm_calibration()
{   
  
  INTERRUPTS_DISABLE();
  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_0, PWM_TIMER_B, GPIO_C_NUM, 5) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_1, PWM_TIMER_B, GPIO_C_NUM, 1) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_2, PWM_TIMER_B, GPIO_C_NUM, 6) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  if( pwm_enable(PWM_FREQUENCY,DUTY_MAX,PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
    PRINTF("PWM ERROR\n");
  if( pwm_start(PWM_TIMER_3, PWM_TIMER_B, GPIO_C_NUM, 7) != PWM_SUCCESS )
    PRINTF("PWM ERROR 2\n");

  INTERRUPTS_ENABLE();
}

static void 
motor_pwm(uint16_t duty,uint8_t motor)
{
  switch(motor){
    case MOTOR_FRONT_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

   case MOTOR_FRONT_RIGHT:
      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_LEFT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_BACK_RIGHT:

      INTERRUPTS_DISABLE();
      if(pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B) != PWM_SUCCESS)
        PRINTF("PWM ERROR 2\n");
      INTERRUPTS_ENABLE();
      break;

    case MOTOR_ALL:

      INTERRUPTS_DISABLE();
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_0, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_1, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_2, PWM_TIMER_B);
      pwm_update(PWM_FREQUENCY, duty, PWM_TIMER_3, PWM_TIMER_B);
      INTERRUPTS_ENABLE();
      break;
  }
}


static float 
get_altitude()
{
  uint8_t i = 0;
  float value ;
  for(; i<5; i++)
    value += sonar_get_value();
  value /= 5.0f;
  //return kalman_getFilteredValue(value,&filter_info);
  return value;
}

static void 
landing()
{
  if(set_point.altitute <= 10.0f){
     PRINTF("10 CM ALTINDAYIM MOTORLAR KAPANIYOR \n");
    pid_block = 0;
    last_time_pid = 0;
    control_set_throttle(STOP_THROTTLE);
    pid_reset();
    etimer_stop(&landing_timer);
    set_point.altitute = 0;
    hold_altitude = 0;
  }else{
    if(set_point.altitute - 10.0f >= 10.0f){
       PRINTF("YÜKSEKLİK 10 CM AZALTILIYORR \n");
      set_point.altitute -= 10.0f;
    }else{
       PRINTF("YÜKSEKLİK 3 CM AZALTILIYORR \n");
        set_point.altitute -= 3.0f;
    } 
    etimer_reset(&landing_timer);
  }
}

static uint8_t 
altitute_control()
{
  if(set_point.altitute-5 < input.altitute && input.altitute < set_point.altitute+5){
    PRINTF("altitute istenilen aralıkta \n");
    return 1;
  }
  else{
    PRINTF("altitute istenilen aralığın disinda \n");
    return 0;
  }
}

static uint8_t 
angle_control()
{
  if(set_point.z-2 < input.z && input.z < set_point.z+2){
    PRINTF("aci istenilen aralıkta \n");
    return 1;
  }
  else{
    return 0;
     PRINTF("aci istenilen araligin disinda \n");
  }
}

static uint8_t 
formation_flight(const uint8_t move, const float rssi)
{
  if(altitute_control() == 0){
    return 0;
  }
  if(angle_control() == 0){
    return 0;
  }

  if(rssi >= default_rssi){  // 1.DURUM
     PRINTF("1M ICINDE ILERLEME DURDU \n"); 
    set_point.x = 0.0f;
  }else{  // 2.DURUM
     PRINTF("1M DISINDA \n");
    if(move == MOVE_FALSE){  // 2.1 DURUM 
      if(last_rssi > rssi ){ // Master'ı geçtim  
         PRINTF("MASTERI GECTIM GERİ GIDIYORUM \n");
        set_point.x = -2.0f + 0.5f;
      }else{
         PRINTF("MASTERA YAKLASIYORUM \n");
        set_point.x = 2.0f + 0.5f;
      }

    }else if (move == MOVE_TRUE){ // 2.2 DURUMU
       PRINTF("MASTERI TAKIP EDIYORUM \n");
      set_point.x = 2.0f + 0.5f; // default 2 derece açı
    }
  }

  last_rssi = rssi;
  return 1;
}