/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A simple PCE scheduler.
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/rime/rime.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "net/rime/rimestats.h"
#include "net/rime/broadcast.h"
#include "net/ip/uip-udp-packet.h"

#include "lib/random.h"
#include "sys/rtimer.h"
#include "dev/watchdog.h"
#include "sys/node-id.h"
#include <string.h>

#if PLATFORM_HAS_LEDS
#include "dev/leds.h"
#endif
#if PLATFORM_HAS_BUTTON
#include "dev/button-sensor.h"
#endif

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac.h"

#if TSCH_CENTRAL_SCHEDULER_ENABLED
#include "4emac-central-scheduler.h"
#include "4emac-cs-request.h"
#include "4emac-cs-pair.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define MAX_PAYLOAD_LEN   (24 + (2 * CS_MAX_SLOT_NUM))

#define NUMBER_OF_SLOT_OFFSET     SLOT_FRAME_SIZE
#define NUMBER_OF_CHANNEL_OFFSET  2

#if NUMBER_OF_CHANNEL_OFFSET > NUMBER_OF_CHANNELS
#error "NUMBER_OF_CHANNEL_OFFSET must be less than NUMBER_OF_CHANNELS"
#endif

#define DELETE_CHECK_THRESHOLD    50
#define DELETE_CHECK_INTERVAL     10 * SLOT_FRAME_DURATION_ETIMER

#define REQUEST_VALID_THRESHOLD   3

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

static struct uip_udp_conn *server_conn;
static uip_ipaddr_t node_ipaddr;
static uip_ipaddr_t to_ipaddr;

static struct scheduling_msg sch_msg;

static uint8_t allocated_cells_num;
static struct cell_list allocated_cells;

static uint8_t seqno = 1;

struct scheduling_table{
  uint8_t is_used;
  struct cs_node_pair *node_pair;
};
static struct scheduling_table central_sch[NUMBER_OF_SLOT_OFFSET][NUMBER_OF_CHANNEL_OFFSET];

PROCESS(central_scheduler_server_process, "CS Server Process");

/*------------------------------------------------------------------------------
 *  scheduling table daki shared slotları scheduled olarak, diğer slotları ise unscheduled olarak setler.
 */
static void 
reset_scheduling_table()
{
  uint8_t sl_offset, ch_offset;
  struct cs_node_pair *null_pair;

  null_pair = cs_add_pair(&uip_lladdr, (uip_lladdr_t *)&linkaddr_null);
  
  for(ch_offset = 0; ch_offset < NUMBER_OF_CHANNEL_OFFSET; ch_offset++){
    for(sl_offset = 0; sl_offset < FOURE_MAX_SHARED_SLOT; sl_offset++){
      central_sch[sl_offset][ch_offset].is_used = CS_SLOT_SCHEDULED;
      central_sch[sl_offset][ch_offset].node_pair = null_pair;
    }
    for(sl_offset = FOURE_MAX_SHARED_SLOT; sl_offset < NUMBER_OF_SLOT_OFFSET; sl_offset++){
      central_sch[sl_offset][ch_offset].is_used = CS_SLOT_UNSCHEDULED;
      central_sch[sl_offset][ch_offset].node_pair = NULL;
    }
  }
}

/*------------------------------------------------------------------------------
 *  sch-msg yi buf doldurur, verilen dest_ipaddr ye gönderir
 */
static uint8_t
central_scheduler_send_packet(uip_ipaddr_t *dest_ipaddr, struct scheduling_msg *msg)
{
  uint8_t buf[MAX_PAYLOAD_LEN], buf_len;

  if((buf_len = cs_create_packet(buf, msg)) > 0){
    PRINT("Send to %u %u->%u\n", dest_ipaddr->u8[sizeof(uip_ipaddr_t) - 1], msg->transmitter.addr[7], msg->receiver.addr[7]);
    uip_udp_packet_sendto(server_conn, buf, buf_len, dest_ipaddr, UIP_HTONS(CS_LISTEN_PORT));
    return 1;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *  scheduling table daki slot ve channel offseti verilen cell i slot_status ve allocated_node_pair e setler
 */
static void
central_scheduler_set_slot(uint8_t sl_offset, uint8_t ch_offset, uint8_t slot_status, struct cs_node_pair *allocated_node_pair)
{
  central_sch[sl_offset][ch_offset].is_used = slot_status;
  central_sch[sl_offset][ch_offset].node_pair = allocated_node_pair;
}

/*------------------------------------------------------------------------------
 *  Scheduling table da verilen sl_offset ve ch_offset e karşı gelen hücrenin 
 *  transmitter ı tr, receiver ı rc ve slot schedule ise return 1 else return 0 
 */
static uint8_t
central_scheduler_is_cell_scheduled(uint8_t sl_offset, uint8_t ch_offset, uip_lladdr_t *tr, uip_lladdr_t *rc)
{
  if(tr == NULL){
    tr = &central_sch[sl_offset][ch_offset].node_pair->transmitter;
  }
  if(rc == NULL){
    rc = &central_sch[sl_offset][ch_offset].node_pair->receiver;
  }

  if((central_sch[sl_offset][ch_offset].is_used == CS_SLOT_SCHEDULED) && \
     (memcmp(&central_sch[sl_offset][ch_offset].node_pair->transmitter, tr, sizeof(uip_lladdr_t)) == 0) && \
     (memcmp(&central_sch[sl_offset][ch_offset].node_pair->receiver, rc, sizeof(uip_lladdr_t)) == 0)){
    return 1;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *
 */
static uint8_t
central_scheduler_is_slot_used(uint8_t sl_offset, uint8_t ch_offset, uip_lladdr_t *sender, uip_lladdr_t *receiver)
{
  if((memcmp(&central_sch[sl_offset][ch_offset].node_pair->transmitter, sender, sizeof(uip_lladdr_t)) == 0) || \
     (memcmp(&central_sch[sl_offset][ch_offset].node_pair->transmitter, receiver, sizeof(uip_lladdr_t)) == 0) || \
     (memcmp(&central_sch[sl_offset][ch_offset].node_pair->receiver, receiver, sizeof(uip_lladdr_t)) == 0) || \
     (memcmp(&central_sch[sl_offset][ch_offset].node_pair->receiver, sender, sizeof(uip_lladdr_t)) == 0)){
    return 1;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *  Verilen pair için sahip olduğu slotları allocated_cells e doldurur.
 */
static uint8_t
central_scheduler_get_slots(uip_lladdr_t *sender, uip_lladdr_t *receiver)
{
  uint8_t sl, ch;

  allocated_cells_num = 0;

  for(sl = 0; sl < NUMBER_OF_SLOT_OFFSET; sl++){
    for(ch = 0; ch < NUMBER_OF_CHANNEL_OFFSET; ch++){
      if(central_scheduler_is_cell_scheduled(sl, ch, sender, receiver)){
        allocated_cells.slot_offsets[allocated_cells_num] = sl;
        allocated_cells.channel_offsets[allocated_cells_num] = ch;
        allocated_cells_num++;
      }
    }
  }
  return allocated_cells_num;
}

/*------------------------------------------------------------------------------
 *
 */
static uint8_t
central_scheduler_get_slot_id(uip_lladdr_t *ipaddr, uint8_t slot_type)
{
  int8_t sl, ch;

  for(sl = NUMBER_OF_SLOT_OFFSET; sl > -1; sl--){
    for(ch = 0; ch < NUMBER_OF_CHANNEL_OFFSET; ch++){
      if(slot_type == CS_SLOT_TYPE_TRANSMIT){
        if(central_scheduler_is_cell_scheduled(sl, ch, ipaddr, NULL)){
          return sl;
        }
      }else if(slot_type == CS_SLOT_TYPE_RECEIVE){
        if(central_scheduler_is_cell_scheduled(sl, ch, NULL, ipaddr)){
          return sl;
        }
      }
    }
  }
  return 0; 
}

/*------------------------------------------------------------------------------
 *  Verilen pair için cell_num tane slot allocate edip verilen cell_list dizisine doldurur.
 */
static int8_t
central_scheduler_allocate_slot(uip_lladdr_t *sender, uip_lladdr_t *receiver, struct cell_list *cells, uint8_t cell_num)
{ 
  struct cs_node_pair *pair;
  int8_t direction;
  uint8_t sl, ch, sl_cntr, ch_cntr;
  uint8_t sl_offset, ch_offset, cell_index;
  uint8_t is_slot_allocatable, will_be_allocated, used_cell_num = 0;
  uint8_t max_ch_retry, channel, next_channel, channels[NUMBER_OF_CHANNELS];
  uint8_t not_found_cells_num = 0;
  struct cell_list not_found_cells;
  memset(channels, 0, NUMBER_OF_CHANNELS * sizeof(uint8_t));

  for(cell_index = 0; cell_index < cell_num; cell_index++){
    if(cells->slot_offsets[cell_index] != CS_NULL_SLOT_OFFSET && cells->channel_offsets[cell_index] != CS_NULL_CHANNEL_OFFSET){
      if(central_scheduler_is_cell_scheduled(cells->slot_offsets[cell_index], cells->channel_offsets[cell_index], sender, receiver)){
        used_cell_num++;
        channel = (cells->slot_offsets[cell_index] + cells->channel_offsets[cell_index]) % NUMBER_OF_CHANNELS;
        channels[channel]++;
        next_channel = (channel + SLOT_FRAME_SIZE) % NUMBER_OF_CHANNELS; /* in the next slot frame */
        channels[next_channel]++;
      }else{
        not_found_cells.slot_offsets[not_found_cells_num] = cells->slot_offsets[cell_index];
        not_found_cells.channel_offsets[not_found_cells_num] = cells->channel_offsets[cell_index];
        not_found_cells_num++;
      }
    }
  }
  central_scheduler_get_slots(sender, receiver); /* this function sets allocated cells */
  will_be_allocated = cell_num - allocated_cells_num - not_found_cells_num;
  for(cell_index = 0; cell_index < will_be_allocated; cell_index++){
    sl_offset = CS_NULL_SLOT_OFFSET;
    ch_offset = CS_NULL_CHANNEL_OFFSET;
    if((sl = central_scheduler_get_slot_id(receiver, CS_SLOT_TYPE_TRANSMIT)) > FOURE_MAX_SHARED_SLOT){ /* if parent has transmit slot */
      direction = -1;
    }else if((sl = central_scheduler_get_slot_id(sender, CS_SLOT_TYPE_RECEIVE)) > FOURE_MAX_SHARED_SLOT){ /* if child has receive slot */
      direction = 1;
    }else{
      sl = ((uint32_t)random_rand() * 941027) % NUMBER_OF_SLOT_OFFSET;
      direction = 1;
    }
    for(sl_cntr = 0; sl_cntr < NUMBER_OF_SLOT_OFFSET; sl_cntr++){
      sl = (uint8_t)(sl + direction + NUMBER_OF_SLOT_OFFSET) % NUMBER_OF_SLOT_OFFSET;
      is_slot_allocatable = 1;
      for(ch = 0; ch < NUMBER_OF_CHANNEL_OFFSET; ch++){
        if(central_scheduler_is_slot_used(sl, ch, sender, receiver)){ /* if slot is already using sender or receiver, can't be allocated */
          is_slot_allocatable = 0;
          break;
        }
      }
      if(is_slot_allocatable){
        for(max_ch_retry = 0; max_ch_retry <= (2 * CS_MAX_SLOT_NUM / NUMBER_OF_CHANNELS); max_ch_retry++){
          for(ch_cntr = 0, ch = ((uint32_t)random_rand() * 92627) % NUMBER_OF_CHANNEL_OFFSET; ch_cntr < NUMBER_OF_CHANNEL_OFFSET; ch_cntr++, ch = ++ch % NUMBER_OF_CHANNEL_OFFSET){
            channel = (sl + ch) % NUMBER_OF_CHANNELS;
            next_channel = (channel + SLOT_FRAME_SIZE) % NUMBER_OF_CHANNELS; /* in the next slot frame */
            if(channels[channel] <= max_ch_retry && channels[next_channel] <= max_ch_retry && central_sch[sl][ch].is_used == CS_SLOT_UNSCHEDULED){
              sl_offset = sl;
              ch_offset = ch;
              channels[channel]++;
              channels[next_channel]++;
              break;
            }
          }
          if(sl_offset != CS_NULL_SLOT_OFFSET && ch_offset != CS_NULL_CHANNEL_OFFSET)
            break;
        }

        if(sl_offset != CS_NULL_SLOT_OFFSET && ch_offset != CS_NULL_CHANNEL_OFFSET){
          pair = cs_add_pair(sender, receiver);
          if(pair != NULL){
            pair->last_seqno = sch_msg.seqnum;
            if(memcmp(receiver, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){ /* if sender is child of server */
              if(foure_mac_buf_schedule_slots((linkaddr_t *)sender, CS_SLOT_TYPE_RECEIVE, FRAME802154_DATAFRAME, &sl_offset, &ch_offset, 1) == FOURE_SLOT_SCHEDULED){
                central_scheduler_set_slot(sl_offset, ch_offset, CS_SLOT_SCHEDULED, pair);
              }else
                goto return_status;
            }else
              central_scheduler_set_slot(sl_offset, ch_offset, CS_SLOT_SCHEDULED, pair);
            PRINT("Slot %u-c%u allocated for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
            allocated_cells.slot_offsets[allocated_cells_num] = sl_offset;
            allocated_cells.channel_offsets[allocated_cells_num] = ch_offset;          
            allocated_cells_num++;
            break;
          }else
            goto return_status;
        }
      }
    } 
  }

  return_status:
    if(not_found_cells_num > 0){
      for(cell_index = 0; cell_index < not_found_cells_num; cell_index++)
        PRINT("Slot %u-c%u not found\n", not_found_cells.slot_offsets[cell_index], not_found_cells.channel_offsets[cell_index]);
    }
    if(used_cell_num >= allocated_cells_num){
      PRINT("Slot scheduling failed!\n");
      return FOURE_SLOT_UNSCHEDULED;
    }else if((will_be_allocated + used_cell_num) > allocated_cells_num){
      PRINT("Slot scheduling failed for %u slots!\n", will_be_allocated + used_cell_num - allocated_cells_num);
      return FOURE_SLOT_SCHEDULED;    
    }else
      return FOURE_SLOT_SCHEDULED;
} 

/*------------------------------------------------------------------------------
 *  Deallocate given slot's from scheduling table, 
 *  eğer o pair için slot kalmamışsa pair i pair table dan siler.
 */
static uint8_t
central_scheduler_deallocate_slot(uip_lladdr_t *sender, uip_lladdr_t *receiver, struct cell_list *cells, uint8_t cell_num)
{
  uint8_t cell_index, unused_pair;
  uint8_t sl, ch, sl_offset, ch_offset;

  for(cell_index = 0; cell_index < cell_num; cell_index++){
    sl_offset = cells->slot_offsets[cell_index];
    ch_offset = cells->channel_offsets[cell_index];
    if(sl_offset != CS_NULL_SLOT_OFFSET && ch_offset != CS_NULL_CHANNEL_OFFSET){
      if(central_scheduler_is_cell_scheduled(sl_offset, ch_offset, sender, receiver)){
        central_scheduler_set_slot(sl_offset, ch_offset, CS_SLOT_UNSCHEDULED, NULL);
        PRINT("Slot %u-c%u deallocated for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
      }else{
        PRINT("Slot %u-c%u not found for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
      }
    }
  }
  unused_pair = 1;
  for(sl = 0; sl < NUMBER_OF_SLOT_OFFSET; sl++)
    for(ch = 0; ch < NUMBER_OF_CHANNEL_OFFSET; ch++)
      if(central_scheduler_is_cell_scheduled(sl, ch, sender, receiver)){
        unused_pair = 0;
        break;
      }

  if(unused_pair){
    cs_delete_pair(sender, receiver);
  }
  if(memcmp(receiver, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){ /* if sender is child of server */
    foure_mac_buf_delete_slots((linkaddr_t *)sender, cells->slot_offsets, cell_num);
  }
  return 1;
}

/*------------------------------------------------------------------------------
 *  verilen tr için pair table da başka receiver varmı diye kontrol eder. Eğer varsa client parent ını değiştirmiş demektir.
 *  Client ın eski parent ına CLEAR mesajı gönderilir. Scheduling table da client ve eski parent ı için schedule edilmiş slotları allocated_cells
 *  dizisine doldurur. ve bu slotları scheduling table dan siler. Eğer o pair için slot kalmamışsa(normal şartlarda kalmamalı) o pair ı 
 *  pair table dan siler. Eğer deallocate işlemi başarılı ise tr destination ı için o slotları 4emac-buf dan siler.
 */
static uint8_t
central_scheduler_node_changed_parent(uip_lladdr_t *tr, uip_lladdr_t *rc)
{
  uint8_t parent_changed = 0;
  struct scheduling_msg send_msg;
  struct cs_node_pair *old_pair = cs_get_head_pair();

  while(old_pair != NULL){
    if((memcmp(&old_pair->transmitter, tr, sizeof(uip_lladdr_t)) == 0) && \
       (memcmp(&old_pair->receiver, rc, sizeof(uip_lladdr_t)) != 0)){
      parent_changed = 1;
      break;
    }
    old_pair = list_item_next(old_pair);
  }

  if(parent_changed){
    if(old_pair->last_seqno > sch_msg.seqnum && old_pair->last_seqno - sch_msg.seqnum < REQUEST_VALID_THRESHOLD){ /* Old parent's request reached after client changed parent (rc show old parent, old_pair show new parent) */
      PRINT("Client %u changed parent to %u old parent %u\n", tr->addr[7], old_pair->receiver.addr[7], rc->addr[7]);
      cs_prepare_packet(&send_msg, CMD_CLEAR, seqno++, 0, NULL, tr, rc);
      cs_set_ip_from_prefix(&to_ipaddr, &default_instance->current_dag->prefix_info, rc);
      cs_add_request(&to_ipaddr,  REQUEST_FOR_SEND, NORMAL_PRIORITY, 5, CLOCK_SECOND / 16, &send_msg);
      return 2;
    }
    PRINT("Client %u changed parent from %u to %u\n", old_pair->transmitter.addr[7], old_pair->receiver.addr[7], rc->addr[7]);
    central_scheduler_get_slots(tr, &old_pair->receiver); /* get slots for tr and old parent */
    if(allocated_cells_num > 0){
      PRINT("%u slot was found for old parent %u\n", allocated_cells_num, old_pair->receiver.addr[7]);
      if(memcmp(&old_pair->receiver, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){ /* if old pair is me */
        central_scheduler_deallocate_slot(tr, &old_pair->receiver, &allocated_cells, allocated_cells_num);
      }else{
        cs_prepare_packet(&send_msg, CMD_CLEAR, seqno++, 0, NULL, tr, &old_pair->receiver);
        cs_set_ip_from_prefix(&to_ipaddr, &default_instance->current_dag->prefix_info, &old_pair->receiver);
        cs_add_request(&to_ipaddr, REQUEST_FOR_SEND, NORMAL_PRIORITY, 5, CLOCK_SECOND / 16, &send_msg);
      }
    }
    return 1;
  }
  return 0;
}

/*------------------------------------------------------------------------------
 * paket DELETE request ise paketteki silinmesi istenen tüm slotların kendi table ında schedule edilip edilmediğini kontrol eder
 * eğer hiçbiri schedule değilse 1 döner eğer bir tanesi bile schedule ise 0 döner.
 *
 * paket ADD request ise paketin içindeki client a ait slotları sayar. sonra kendi table ında o client için tuttuğu slotları sayar
 * ve allocated_cells dizisine doldurur eğer kendi table ında daha fazla slot var ise 1 döner değil ise 0 döner.
 */
static uint8_t
central_scheduler_is_request_duplicate()
{
  uint8_t cell_ptr;

  if(sch_msg.op_code == CMD_DELETE){
    for(cell_ptr = 0; cell_ptr < sch_msg.num_cells; cell_ptr++){
      if(central_scheduler_is_cell_scheduled(sch_msg.cells.slot_offsets[cell_ptr], sch_msg.cells.channel_offsets[cell_ptr], &sch_msg.transmitter, &sch_msg.receiver))
        return 0;  
    }
    return 1;
  }else if(sch_msg.op_code == CMD_ADD){
    central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver); /* this function sets allocated cells */
    if(allocated_cells_num >= sch_msg.num_cells){ /* Duplication was detected */
      return 1;
    }else
      return 0;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *
 */
static void
handle_request(struct request_stack *sch_rqst)
{
  uint8_t is_duplicate, parent_changed;
  struct request_stack *rqst;

  memcpy(&sch_msg, &sch_rqst->msg, sizeof(struct scheduling_msg));

  if(sch_msg.op_code >= CMD_ADD && sch_msg.op_code <= CMD_CLEAR){ /* Request */
      parent_changed = central_scheduler_node_changed_parent(&sch_msg.transmitter, &sch_msg.receiver);
      if(parent_changed == 2){
        return;
      }

      rqst = cs_get_request_from_op_code(&sch_msg.transmitter, &sch_msg.receiver, 0, sch_msg.op_code); /* sch_msg.transmitter should be me or my child */
      if(rqst != NULL){ /* Request already was added */
        cs_prepare_packet(&sch_msg, RC_BUSY, sch_msg.op_code, 0, NULL, &sch_msg.transmitter, &sch_msg.receiver);
        central_scheduler_send_packet(&sch_rqst->dest_ipaddr, &sch_msg);
      }else{ /* Request don't exist */
        if(sch_msg.op_code == CMD_COUNT){
          PRINT("Count Request for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
          cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_send_packet(&sch_rqst->dest_ipaddr, &sch_msg);
          return;
        }else if(sch_msg.op_code == CMD_LIST){
          PRINT("List Request for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
          cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_send_packet(&sch_rqst->dest_ipaddr, &sch_msg);
          return;
        }else if(sch_msg.op_code == CMD_CLEAR){
          PRINT("Clear Request for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_deallocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &allocated_cells, allocated_cells_num);
          cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_send_packet(&sch_rqst->dest_ipaddr, &sch_msg);
          return;
        }

        is_duplicate = central_scheduler_is_request_duplicate(); /* duplicate function sets allocated_cells */

        if(is_duplicate){
          if(sch_msg.op_code == CMD_ADD){ /* FROM: child or parent TO: server */
            cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          }else if(sch_msg.op_code == CMD_DELETE){ /* FROM: child or parent TO: server */
            cs_prepare_packet(&sch_msg, sch_msg.op_code, RC_SUCCESS, sch_msg.num_cells, &sch_msg.cells, &sch_msg.transmitter, &sch_msg.receiver);
          }
        }else{
          if(sch_msg.op_code == CMD_ADD){ /* FROM: child or parent TO: server */
            PRINT("Add Request reached for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
            if(central_scheduler_allocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &sch_msg.cells, sch_msg.num_cells) == FOURE_SLOT_SCHEDULED){ /* duplicate function sets allocated_cells were already scheduled and allocate function add new scheduled cells to allocated_cells */
              cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
            }else{
              cs_prepare_packet(&sch_msg, RC_ERR, sch_msg.op_code, 0, NULL, &sch_msg.transmitter, &sch_msg.receiver);
            }
          }else if(sch_msg.op_code == CMD_DELETE){ /* FROM: child or parent TO: server */
            PRINT("Delete Request reached from %u->%u for %u slots\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7], sch_msg.num_cells);
            if(central_scheduler_deallocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &sch_msg.cells, sch_msg.num_cells)){
              cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, sch_msg.num_cells, &sch_msg.cells, &sch_msg.transmitter, &sch_msg.receiver);
            }else{
              cs_prepare_packet(&sch_msg, RC_ERR, sch_msg.op_code, 0, NULL, &sch_msg.transmitter, &sch_msg.receiver);
            }           
          }
        }
        central_scheduler_send_packet(&sch_rqst->dest_ipaddr, &sch_msg);
      }
  }else if(sch_msg.op_code >= RC_SUCCESS && sch_msg.op_code <= RC_ERR){ /* Response */
      rqst = cs_get_request_from_op_code(&sch_msg.transmitter, &sch_msg.receiver, 0, sch_msg.seqnum); /* in Response messages seqnum = request's op_code */
      if(rqst != NULL){ /* Request exist */
        if(sch_msg.op_code == RC_SUCCESS){
          if(rqst->msg.op_code == CMD_ADD){
            PRINT("Operation succeed for Add request from %u\n", sch_msg.receiver.addr[7]);
          }else if(rqst->msg.op_code == CMD_DELETE){
            PRINT("Operation succeed for Delete request from %u\n", sch_msg.receiver.addr[7]);
          }else if(rqst->msg.op_code == CMD_COUNT){
            PRINT("Operation succeed for Count request from %u\n", sch_msg.receiver.addr[7]);
          }else if(rqst->msg.op_code == CMD_LIST){
            PRINT("Operation succeed for List request from %u\n", sch_msg.receiver.addr[7]);
          }else if(rqst->msg.op_code == CMD_CLEAR){
            PRINT("Operation succeed for Clear request from %u\n", sch_msg.receiver.addr[7]);
            central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
            central_scheduler_deallocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &allocated_cells, allocated_cells_num);
          }
        }else if(sch_msg.op_code == RC_VER_ERR){
          PRINT("ERROR: Unsupported 6P version!\n");
        }else if(sch_msg.op_code == RC_SFID_ERR){
          PRINT("ERROR: Unsupported SFID!\n");
        }else if(sch_msg.op_code == RC_BUSY){
          PRINT("ERROR: Handling previous request!\n");
          rqst->retry = 5;
          rqst->time_out = 61 * SLOT_FRAME_DURATION_ETIMER;
          return;
        }else if(sch_msg.op_code == RC_RESET){
          PRINT("ERROR: Abort 6P transaction!\n");
        }else if(sch_msg.op_code == RC_ERR){
          PRINT("ERROR: Operation failed!\n");
        }
        cs_remove_request(rqst);
        return;
      }else{ /* If incoming packet is response message, ignore it */
        PRINT("OP %u ID %u not found for %u->%u!\n", sch_msg.op_code, sch_msg.seqnum, sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
        return;
      }
    }else{
      PRINT("bug\n");
      return;
  }
}

/*------------------------------------------------------------------------------
 *
 */
static void
tcpip_handler(void)
{
  uint8_t *recv_packet;
  struct scheduling_msg msg;
  uip_ds6_route_t *routable;

  if(uip_newdata()){
    recv_packet = uip_appdata;

    if(cs_parse_packet(&msg, recv_packet, uip_datalen()) == 0){
      PRINT("Packet parsing failed!\n");
    }else{
      routable = uip_ds6_route_lookup(&UIP_IP_BUF->srcipaddr);
      if(routable != NULL){
        cs_add_request(&UIP_IP_BUF->srcipaddr,  REQUEST_FOR_HANDLE, NORMAL_PRIORITY, 1, 0, &msg);
      }else
        PRINT("No route found for %u\n", UIP_IP_BUF->srcipaddr.u8[15]);
    }
  } 
}

/*------------------------------------------------------------------------------
 *
 */
static void 
cs_is_slot_delete_required(void *ptr)
{
  static uint8_t delete_slot_check_counter = 0;
  struct ctimer *ct_ptr = ptr;

  if(delete_slot_check_counter > DELETE_CHECK_THRESHOLD){
    delete_slot_check_counter = 0;
    foure_mac_buf_delete_unused_slots((linkaddr_t *)&linkaddr_null, CS_SLOT_TYPE_SHARED);
  }
  delete_slot_check_counter++;
  ctimer_reset(ct_ptr);
}

/*------------------------------------------------------------------------------
 *
 */
PROCESS_THREAD(central_scheduler_server_process, ev, data)
{
  static struct etimer et;
  static struct ctimer delete_timer;
  static struct request_stack *rqst;
  static uip_lladdr_t lladdr;

  PROCESS_BEGIN();

#if PLATFORM_HAS_BUTTON
  SENSORS_ACTIVATE(button_sensor);
#endif

  server_conn = udp_new(NULL, UIP_HTONS(0), NULL); /* set remote port */
  if(server_conn == NULL){
    PRINT("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(server_conn, UIP_HTONS(CS_LISTEN_PORT)); /* set local port */
  
  cs_conn = server_conn;

  etimer_set(&et, CLOCK_SECOND / 64);

  PRINT("Central Scheduler Server Process Started\n");

  ctimer_set(&delete_timer, DELETE_CHECK_INTERVAL, cs_is_slot_delete_required, (void *)&delete_timer);

  while(1){
    PROCESS_YIELD();

    if(ev == tcpip_event){
      tcpip_handler();
    }

    if(etimer_expired(&et)){
      etimer_reset(&et);
      rqst = cs_get_head_request();
      while(rqst != NULL){
        if(rqst->retry > 0){
          if(rqst->time_out > 0){
            rqst->time_out -= et.timer.interval;
          }else{ /* Send */
            if(rqst->type == REQUEST_FOR_SEND){
              cs_set_lladdr_from_ipaddr(&lladdr, &rqst->dest_ipaddr);
              if(!foure_mac_buf_is_content_available_for_packet_type((linkaddr_t *)&lladdr, FRAME802154_SCHEDULER)){
                rqst->retry--;
                rqst->time_out = 5 * SLOT_FRAME_DURATION_ETIMER * (1 << (FOURE_MAC_MIN_BE + FOURE_MAC_BE_INC));
                central_scheduler_send_packet(&rqst->dest_ipaddr, &rqst->msg);
              }
            }else{
              handle_request(rqst);
              cs_remove_request(rqst);
            }
          }
        }else{ /* Delete Request */
          cs_remove_request(rqst);
        }
        rqst = list_item_next(rqst);
      }
    }
#if PLATFORM_HAS_BUTTON
    if(ev == sensors_event && data == &button_sensor){
      PRINT("CLICK\n");
    }
#endif
  }

  PROCESS_END();
}

/*------------------------------------------------------------------------------
 *
 */
void 
cs_server_init()
{
  cs_set_ip_from_prefix(&node_ipaddr, &default_instance->current_dag->prefix_info, &uip_lladdr);

  cs_request_init();
  cs_pair_init();
  reset_scheduling_table();

  process_exit(&central_scheduler_server_process);
  process_start(&central_scheduler_server_process, NULL);
}

#endif  /* TSCH_CENTRAL_SCHEDULER_ENABLED */
#endif  /* TSCH_TIME_SYNCH */
