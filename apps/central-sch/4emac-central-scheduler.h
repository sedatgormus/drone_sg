/*
 * Copyright (c) 2015, Mavi Alp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A simple PCE scheduler.
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#ifndef FOUREMAC_CENTRAL_SCHEDULER_H_
#define FOUREMAC_CENTRAL_SCHEDULER_H_

#if TSCH_CENTRAL_SCHEDULER_ENABLED
#include "net/mac/4emac/4emac-buf.h"

#define CS_LISTEN_PORT    8764

#define CS_MAX_SLOT_NUM   FOURE_MAX_SLOTS_PER_DESTINATION

#define CS_SLOT_UNSCHEDULED 	0
#define CS_SLOT_SCHEDULED       1

#define CS_SLOT_TYPE_TRANSMIT 	SLOT_TYPE_TRANSMIT
#define CS_SLOT_TYPE_RECEIVE 	SLOT_TYPE_RECEIVE
#define CS_SLOT_TYPE_SHARED 	SLOT_TYPE_SHARED
#define CS_SLOT_TYPE_ALL        (CS_SLOT_TYPE_TRANSMIT | CS_SLOT_TYPE_RECEIVE | CS_SLOT_TYPE_SHARED)

#define CS_NULL_SLOT_OFFSET 	255
#define CS_NULL_CHANNEL_OFFSET	255

#define SLOT_FRAME_DURATION_MSEC       (SLOT_FRAME_SIZE * (TSCH_DEFAULT_TS_TIMESLOT_LENGTH / 1000)) /* msec */
#define SLOT_FRAME_DURATION_ETIMER     ((((uint32_t)SLOT_FRAME_DURATION_MSEC * CLOCK_SECOND) + 500) / 1000) /* etimer second */

struct uip_udp_conn *cs_conn;

void cs_server_init();

void cs_client_init();

void post_central_scheduler_parent_switch_event(void *);

#endif /* TSCH_CENTRAL_SCHEDULER_ENABLED */
#endif /* FOUREMAC_CENTRAL_SCHEDULER_H_ */