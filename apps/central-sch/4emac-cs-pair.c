/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Information Element Processing Routines for Central Scheduler 
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"


#if TSCH_TIME_SYNCH
#include "net/mac/4emac/4emac-private.h"
#include "4emac-cs-pair.h"
#include "net/ip/uip.h"
#include "lib/memb.h"
#include "lib/list.h"

#include <string.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#define PAIR_SIZE      SLOT_FRAME_SIZE

MEMB(cs_node_pair_mem, struct cs_node_pair, PAIR_SIZE);
LIST(cs_node_pair_list);

#define GET_PAIR_HEAD()  list_head(cs_node_pair_list)
#define GET_PAIR_COUNT() list_length(cs_node_pair_list)

/*------------------------------------------------------------------------------
 *  
 */
struct cs_node_pair *
cs_get_head_pair()
{
  return GET_PAIR_HEAD();
}

/*------------------------------------------------------------------------------
 *  If pair exist return pointer to pair else return NULL
 */
struct cs_node_pair *
cs_get_pair(uip_lladdr_t *sender, uip_lladdr_t *receiver)
{
  struct cs_node_pair *pair = GET_PAIR_HEAD();

  while(pair != NULL){
    if((memcmp(&pair->transmitter, sender, sizeof(uip_lladdr_t)) == 0) && \
       (memcmp(&pair->receiver, receiver, sizeof(uip_lladdr_t)) == 0))
        return pair; 
    pair = list_item_next(pair);
  }
  return NULL;
}

/*------------------------------------------------------------------------------
 *  Adds new pair for given transmitter 'sender' and receiver 'receiver' and returns pointer to pair, 
 *  pair already exist returns pointer to it. If table 'cs_node_pair_mem' is full return NULL.
 */
struct cs_node_pair *
cs_add_pair(uip_lladdr_t *sender, uip_lladdr_t *receiver)
{
  struct cs_node_pair *pair = cs_get_pair(sender, receiver);

  if(pair != NULL){ /* if pair is exist */
    return pair;
  }

  pair = memb_alloc(&cs_node_pair_mem);
  if(pair == NULL){ /* no more space left in the buffer */
    PRINT("CS:Pair List full!\n");
    return NULL;
  }

  memcpy(&pair->transmitter, sender, sizeof(uip_lladdr_t));
  memcpy(&pair->receiver, receiver, sizeof(uip_lladdr_t));
  pair->last_seqno = 0;

  list_add(cs_node_pair_list, pair);
  PRINT("CS:Pair %u->%u added\n", sender->addr[7], receiver->addr[7]);

  return pair;
}

/*------------------------------------------------------------------------------
 *  If pair exist delete pair
 */
void
cs_delete_pair(uip_lladdr_t *sender, uip_lladdr_t *receiver)
{
  struct cs_node_pair *pair = cs_get_pair(sender, receiver);

  if(pair != NULL){
    PRINT("CS:Pair %u->%u deleted\n", sender->addr[7], receiver->addr[7]);
    list_remove(cs_node_pair_list, pair);
    memb_free(&cs_node_pair_mem, pair);
  }
}

/*------------------------------------------------------------------------------
 *  
 */
uint8_t
cs_get_pair_count()
{
  return GET_PAIR_COUNT();
}

/*------------------------------------------------------------------------------
 *  
 */
void
cs_pair_init()
{
  memb_init(&cs_node_pair_mem);
  list_init(cs_node_pair_list);
}

#endif /*TSCH_TIME_SYNCH*/
