
/*
 * Copyright (c) 2015, Mavi Alp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A simple PCE scheduler.
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#ifndef FOUREMAC_REQUEST_H_
#define FOUREMAC_REQUEST_H_

#if TSCH_CENTRAL_SCHEDULER_ENABLED
#include "4emac-central-scheduler-ies.h"

#define REQUEST_FOR_SEND    0
#define REQUEST_FOR_HANDLE  1

struct request_stack{
	struct request_stack *next;
	uip_ipaddr_t dest_ipaddr;
	struct scheduling_msg msg;
	uint8_t type;
	uint8_t priority;
	int8_t retry;
	int16_t time_out;
};

static const char *const request_str[] = {
	"ADD",
	"DELETE",
	"COUNT",
	"LIST",
	"CLEAR",
	"SUCCESS",
	"VER_ERR",
	"SFID_ERR",
	"BUSY",
	"RESET",
	"ERR",
};

void cs_request_init();

uint8_t cs_add_request(uip_ipaddr_t *dest, uint8_t type, uint8_t priority, uint8_t retry, uint16_t time_out, struct scheduling_msg *sch_msg);

struct request_stack *cs_get_head_request();

struct request_stack *cs_get_request_from_op_code(uip_lladdr_t *transmitter, uip_lladdr_t *receiver, uint8_t type, uint8_t op_code);

struct request_stack *cs_get_request_from_uniq_id(uip_lladdr_t *transmitter, uip_lladdr_t *receiver, uint8_t type, uint8_t seqnum);

void cs_remove_request(struct request_stack *rqst);

uint8_t cs_get_request_count();

void cs_set_ip_from_prefix(uip_ipaddr_t *ipaddr, rpl_prefix_t *prefix, uip_lladdr_t *lladdr);

void cs_set_lladdr_from_ipaddr(uip_lladdr_t *lladdr, uip_ipaddr_t *ipaddr);

#endif /* TSCH_CENTRAL_SCHEDULER_ENABLED */
#endif /* FOUREMAC_REQUEST_H_ */