/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *        Information Element routines 
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */
#include "contiki.h"

#if TSCH_TIME_SYNCH
#include "net/mac/4emac/4emac-private.h"
#include "4emac-cs-request.h"
#include "net/ip/uip.h"
#include "lib/memb.h"
#include "lib/list.h"

#include <string.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#define REQUEST_SIZE 3

MEMB(request_stack_mem, struct request_stack, REQUEST_SIZE);
LIST(request_stack_list);

#define GET_HEAD_REQUEST()  list_head(request_stack_list)
#define GET_REQUEST_COUNT() list_length(request_stack_list)

/*------------------------------------------------------------------------------
 *
 */
struct request_stack *
cs_get_head_request()
{
  return GET_HEAD_REQUEST();
}

/*------------------------------------------------------------------------------
 *
 */
struct request_stack *
cs_get_request_from_uniq_id(uip_lladdr_t *transmitter, uip_lladdr_t *receiver, uint8_t type, uint8_t seqnum)
{
  struct request_stack *rqst = GET_HEAD_REQUEST();

  while(rqst != NULL){
    if(memcmp(&rqst->msg.transmitter, transmitter, sizeof(uip_lladdr_t)) == 0 && \
       memcmp(&rqst->msg.receiver, receiver, sizeof(uip_lladdr_t)) == 0 && \
       rqst->type == type && \
       rqst->msg.seqnum == seqnum){
      return rqst;
    }
    rqst = list_item_next(rqst);
  }
  return NULL;
}

/*------------------------------------------------------------------------------
 *
 */
struct request_stack *
cs_get_request_from_op_code(uip_lladdr_t *transmitter, uip_lladdr_t *receiver, uint8_t type, uint8_t op_code)
{
  struct request_stack *rqst = GET_HEAD_REQUEST();

  while(rqst != NULL){
    if(memcmp(&rqst->msg.transmitter, transmitter, sizeof(uip_lladdr_t)) == 0 && \
       memcmp(&rqst->msg.receiver, receiver, sizeof(uip_lladdr_t)) == 0 && \
       rqst->type == type && \
       rqst->msg.op_code == op_code){
      return rqst;
    }
    rqst = list_item_next(rqst);
  }
  return NULL;
}

/*------------------------------------------------------------------------------
 *
 */
uint8_t
cs_add_request(uip_ipaddr_t *dest, uint8_t type, uint8_t priority, uint8_t retry, uint16_t time_out, struct scheduling_msg *sch_msg)
{
  struct request_stack *new_rqst;

  new_rqst = cs_get_request_from_op_code(&sch_msg->transmitter, &sch_msg->receiver, type, sch_msg->op_code);
  if(new_rqst != NULL){ /* already added the same request */
    if(sch_msg->seqnum > new_rqst->msg.seqnum){
      PRINT("6P:OP %s id %u replaced with %u tr %u!\n", request_str[new_rqst->msg.op_code - 5], new_rqst->msg.seqnum, sch_msg->seqnum, new_rqst->msg.transmitter.addr[7]);
      new_rqst->priority = priority;
      new_rqst->retry = retry;
      new_rqst->time_out = time_out;
      new_rqst->msg.seqnum = sch_msg->seqnum;
    }else
      PRINT("6P:OP %s id %u already exist tr %u!\n", request_str[new_rqst->msg.op_code - 5], new_rqst->msg.seqnum, new_rqst->msg.transmitter.addr[7]);
    return 1;
  }

  new_rqst = memb_alloc(&request_stack_mem);
  if(new_rqst == NULL){ /* no more space left in the buffer */
    PRINT("6P:Request buffer full!\n");
    memb_free(&request_stack_mem, new_rqst);
    return 0;
  }
  
  memcpy(&new_rqst->dest_ipaddr, dest, sizeof(uip_ipaddr_t));
  memcpy(&new_rqst->msg, sch_msg, sizeof(struct scheduling_msg));
  new_rqst->type = type;
  new_rqst->priority = priority;
  new_rqst->retry = retry;
  new_rqst->time_out = time_out;
  
  list_add(request_stack_list, new_rqst);
  PRINT("6P:OP %s id %u added for %u->%u to %u\n", \
          request_str[new_rqst->msg.op_code - 5], new_rqst->msg.seqnum, new_rqst->msg.transmitter.addr[7], new_rqst->msg.receiver.addr[7], new_rqst->dest_ipaddr.u8[15]);
  return 1;
}

/*------------------------------------------------------------------------------
 *  Remove request 'rqst' from list and deallocate
 */
void
cs_remove_request(struct request_stack *rqst)
{
  if(rqst != NULL){
    PRINT("6P:OP %s id %u deleted for %u->%u to %u\n", \
            request_str[rqst->msg.op_code - 5], rqst->msg.seqnum, rqst->msg.transmitter.addr[7], rqst->msg.receiver.addr[7], rqst->dest_ipaddr.u8[15]);
    list_remove(request_stack_list, rqst);
    memb_free(&request_stack_mem, rqst);
  }
}

/*------------------------------------------------------------------------------
 *
 */
uint8_t
cs_get_request_count()
{
  return GET_REQUEST_COUNT();
}

/*------------------------------------------------------------------------------
 *  set ipaddr from given lladdr and prefix
 */
void
cs_set_ip_from_prefix(uip_ipaddr_t *ipaddr, rpl_prefix_t *prefix, uip_lladdr_t *lladdr)
{
  memset(ipaddr, 0, sizeof(uip_ipaddr_t));
  memcpy(ipaddr, &prefix->prefix, (prefix->length + 7) / 8);
  uip_ds6_set_addr_iid(ipaddr, lladdr);
}

/*------------------------------------------------------------------------------
 *  set lladdr from given ipaddr
 */
void
cs_set_lladdr_from_ipaddr(uip_lladdr_t *lladdr, uip_ipaddr_t *ipaddr)
{
  /* We consider only links with IEEE EUI-64 identifier or
   * IEEE 48-bit MAC addresses */
#if (UIP_LLADDR_LEN == 8)
  memcpy(lladdr, ipaddr->u8 + 8, UIP_LLADDR_LEN);
  lladdr->addr[0] ^= 0x02;
#elif (UIP_LLADDR_LEN == 6)
  memcpy(lladdr, ipaddr->u8 + 8, 3);
  memcpy((uint8_t *)lladdr + 3, ipaddr->u8 + 13, 3);
  lladdr->addr[0] ^= 0x02;
#else
#error uip-ds6.c cannot build interface address when UIP_LLADDR_LEN is not 6 or 8
#endif
}

/*------------------------------------------------------------------------------
 *
 */
void 
cs_request_init()
{
  memb_init(&request_stack_mem);
  list_init(request_stack_list);
}
#endif /*TSCH_TIME_SYNCH*/
