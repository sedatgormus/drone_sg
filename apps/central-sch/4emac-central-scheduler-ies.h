/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Information Element Processing Routines for Central Scheduler
 *
 *         https://tools.ietf.org/html/draft-ietf-6tisch-6top-protocol-00
 *
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#ifndef FOUREMAC_CS_IES_H_
#define FOUREMAC_CS_IES_H_

#define IANA_6TOP_METADATA      0x0000
#define IANA_6TOP_6P_VERSION    1
#define IANA_6TOP_SFID_VERSION  0xFF

#define IANA_6TOP_CMD_ADD       0x05
#define IANA_6TOP_CMD_DELETE    0x06
#define IANA_6TOP_CMD_COUNT     0x07
#define IANA_6TOP_CMD_LIST      0x08
#define IANA_6TOP_CMD_CLEAR     0x09

#define IANA_6TOP_RC_SUCCESS    0x0A
#define IANA_6TOP_RC_VER_ERR    0x0B
#define IANA_6TOP_RC_SFID_ERR   0x0C
#define IANA_6TOP_RC_BUSY       0x0D
#define IANA_6TOP_RC_RESET      0x0E
#define IANA_6TOP_RC_ERR        0x0F

#define CS_MAX_CELL_LIST        FOURE_MAX_SLOTS_PER_DESTINATION

enum sixp_command_identifiers{
	CMD_ADD = IANA_6TOP_CMD_ADD,
	CMD_DELETE = IANA_6TOP_CMD_DELETE,
	CMD_COUNT = IANA_6TOP_CMD_COUNT,
	CMD_LIST = IANA_6TOP_CMD_LIST,
	CMD_CLEAR = IANA_6TOP_CMD_CLEAR,
};

enum sixp_return_codes{
	RC_SUCCESS = IANA_6TOP_RC_SUCCESS,
	RC_VER_ERR = IANA_6TOP_RC_VER_ERR,
	RC_SFID_ERR = IANA_6TOP_RC_SFID_ERR,
	RC_BUSY = IANA_6TOP_RC_BUSY,
	RC_RESET = IANA_6TOP_RC_RESET,
	RC_ERR = IANA_6TOP_RC_ERR,
};

struct cell_list{
	uint8_t slot_offsets[CS_MAX_CELL_LIST];
	uint8_t channel_offsets[CS_MAX_CELL_LIST];
};

struct scheduling_msg{
	uip_lladdr_t transmitter;
	uip_lladdr_t receiver;
	uint8_t ver;
	uint8_t op_code;
	uint8_t sfid;
	uint8_t seqnum;
	uint8_t num_cells;
	uint16_t metadata;
	struct cell_list cells;
};

void cs_prepare_packet(struct scheduling_msg *sch_msg, uint8_t op_code, uint8_t seqnum, uint8_t num_cells, struct cell_list *list_cells, uip_lladdr_t *tr, uip_lladdr_t *rc);

uint8_t cs_create_packet(uint8_t *buf, struct scheduling_msg *sch_msg);

uint8_t cs_parse_packet(struct scheduling_msg *sch_msg, uint8_t *buf, uint8_t buf_len);

#endif /* FOUREMAC_CS_IES_H_ */