/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *        Information Element routines 
 *
 *        https://tools.ietf.org/html/draft-ietf-6tisch-6top-protocol-00
 *
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

/*
 *	ADD and DELETE Request Format:
 *
 *     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |Version|  Code |    SFID       |     SeqNum    |   NumCells    |
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |           Metadata            | CellList ...
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 *
 * 	COUNT and LIST and CLEAR Request Format:
 *
 *     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |Version|  Code |    SFID       |     SeqNum    |   Metadata
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        Metadata    |
 *    +-+-+-+-+-+-+-+-+
 *
 *
 *	Response Format:
 *
 *     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |Version|  Code |    SFID       |     SeqNum    | Other Fields...
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 *
 *	CellList:
 * 
 *     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *    |          slotOffset           |         channelOffset         |
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
#include "contiki.h"

#if TSCH_TIME_SYNCH
#include "net/ip/uip.h"
#include "net/mac/4emac/4emac-private.h"
#include "4emac-central-scheduler-ies.h"

#include <string.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

/*------------------------------------------------------------------------------
 *
 */
void 
cs_prepare_packet(struct scheduling_msg *sch_msg, uint8_t op_code, uint8_t seqnum, uint8_t num_cells, struct cell_list *list_cells, uip_lladdr_t *tr, uip_lladdr_t *rc)
{
	uint8_t cur_position;

	memcpy(&sch_msg->transmitter, tr, sizeof(uip_lladdr_t));
	memcpy(&sch_msg->receiver, rc, sizeof(uip_lladdr_t));
	sch_msg->ver = IANA_6TOP_6P_VERSION;
	sch_msg->op_code = op_code;
	sch_msg->sfid = IANA_6TOP_SFID_VERSION;
	sch_msg->seqnum = seqnum;
	sch_msg->num_cells = num_cells;
	sch_msg->metadata = IANA_6TOP_METADATA;
	if(list_cells != NULL){
		for(cur_position = 0; cur_position < num_cells; cur_position++){
			sch_msg->cells.slot_offsets[cur_position] = list_cells->slot_offsets[cur_position];
			sch_msg->cells.channel_offsets[cur_position] = list_cells->channel_offsets[cur_position];
		}
	}
}

/*------------------------------------------------------------------------------
 *
 */
uint8_t 
cs_create_packet(uint8_t *buf, struct scheduling_msg *sch_msg)
{
	uint8_t temp, cur_position = 0;

	/* Ver | OpCode */
	temp = ((sch_msg->ver << 4) & 0xF0) | (sch_msg->op_code & 0x0F);
	buf[cur_position] = temp;
	cur_position++;

	/* SFID */
	buf[cur_position] = sch_msg->sfid;
	cur_position++;

	/* SeqNum */
	buf[cur_position] = sch_msg->seqnum;
	cur_position++;

	if(sch_msg->op_code >= CMD_ADD && sch_msg->op_code <= CMD_CLEAR){ /* Request  Format */
		if(sch_msg->op_code == CMD_ADD || sch_msg->op_code == CMD_DELETE){
			/* NumCells */
			buf[cur_position] = sch_msg->num_cells;
			cur_position++;
		}
		/* Metadata */
		memcpy(&buf[cur_position], &sch_msg->metadata, sizeof(uint16_t));
		cur_position += sizeof(uint16_t);
	}

	for(temp = 0; temp < sch_msg->num_cells; temp++){ /* if packet type does not contain CellList sch_msg->num_cells must be set to zero */
		memcpy(&buf[cur_position], &sch_msg->cells.slot_offsets[temp], sizeof(uint8_t));
		cur_position += sizeof(uint8_t);
		memcpy(&buf[cur_position], &sch_msg->cells.channel_offsets[temp], sizeof(uint8_t));
		cur_position += sizeof(uint8_t);
	}

	memcpy(&buf[cur_position], &sch_msg->transmitter, sizeof(uip_lladdr_t));
	cur_position += sizeof(uip_lladdr_t);
	memcpy(&buf[cur_position], &sch_msg->receiver, sizeof(uip_lladdr_t));
	cur_position += sizeof(uip_lladdr_t);

	return cur_position;
}

/*------------------------------------------------------------------------------
 *
 */
uint8_t 
cs_parse_packet(struct scheduling_msg *sch_msg, uint8_t *buf, uint8_t buf_len)
{
	uint8_t num_cells = 0, cur_position = 0;

	/* Ver */
	sch_msg->ver = (buf[cur_position] >> 4) & 0x0F;
	if(sch_msg->ver != IANA_6TOP_6P_VERSION){
		PRINT("CS-IES:Unsupported Version Number!\n");
		return 0;
	}

	/* OpCode */
	sch_msg->op_code = buf[cur_position] & 0x0F;
	cur_position++;

	/* SFID */
	sch_msg->sfid = buf[cur_position];
	cur_position++;

	/* SeqNum */
	sch_msg->seqnum = buf[cur_position];
	cur_position++;

	if(sch_msg->op_code >= CMD_ADD && sch_msg->op_code <= CMD_CLEAR){ /* Request  Format */
		if(sch_msg->op_code == CMD_ADD || sch_msg->op_code == CMD_DELETE){
			/* NumCells */
			sch_msg->num_cells = buf[cur_position];
			cur_position++;
		}
		/* Metadata */
		memcpy(&sch_msg->metadata, &buf[cur_position], sizeof(uint16_t));
		cur_position += sizeof(uint16_t);
	}

	while(cur_position < buf_len - 2 * sizeof(uip_lladdr_t)){
		memcpy(&sch_msg->cells.slot_offsets[num_cells], &buf[cur_position], sizeof(uint8_t));
		cur_position += sizeof(uint8_t);
		memcpy(&sch_msg->cells.channel_offsets[num_cells], &buf[cur_position], sizeof(uint8_t));
		cur_position += sizeof(uint8_t);
		num_cells++;
	}

	memcpy(&sch_msg->transmitter, &buf[cur_position], sizeof(uip_lladdr_t));
	cur_position += sizeof(uip_lladdr_t);
	memcpy(&sch_msg->receiver, &buf[cur_position], sizeof(uip_lladdr_t));
	cur_position += sizeof(uip_lladdr_t);

	if(sch_msg->op_code == CMD_ADD || sch_msg->op_code == CMD_DELETE){
		if(sch_msg->num_cells != num_cells){
			PRINT("CS-IES:Failed to parse!\n");
			return 0;
		}
	}else
		sch_msg->num_cells = num_cells;

	return 1;
}

#endif /*TSCH_TIME_SYNCH*/
