/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A simple PCE scheduler.
 * \author
 *         Ahmet Faruk Yavuz <ahmfrk61@gmail.com>
 *
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/rime/rime.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "net/rime/rimestats.h"
#include "net/rime/broadcast.h"
#include "net/ip/uip-udp-packet.h"

#include "lib/random.h"
#include "sys/rtimer.h"
#include "dev/watchdog.h"
#include "sys/node-id.h"
#include <string.h>

#if PLATFORM_HAS_LEDS
#include "dev/leds.h"
#endif
#if PLATFORM_HAS_BUTTON
#include "dev/button-sensor.h"
#endif

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac.h"

#if TSCH_CENTRAL_SCHEDULER_ENABLED
#include "4emac-central-scheduler.h"
#include "4emac-cs-request.h"
#include "4emac-cs-pair.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define MAX_PAYLOAD_LEN   (24 + (2 * CS_MAX_SLOT_NUM))

#define NUMBER_OF_SLOT_OFFSET     SLOT_FRAME_SIZE

#define UPDATE_TRANSMISSIONS_FACTOR     5
#define UPDATE_TRANSMISSIONS_INTERVAL   (UPDATE_TRANSMISSIONS_FACTOR * SLOT_FRAME_DURATION_ETIMER)
#define WAIT_DAO_TRANSMISSION_INTERVAL  (4 * CLOCK_SECOND)

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#if RPL_OF == rpl_of_tisch
process_event_t rpl_parent_switch_event;
#endif

static struct uip_udp_conn *client_conn;

static uip_ipaddr_t node_ipaddr;

static uip_ipaddr_t server_ipaddr;
static uip_ipaddr_t to_ipaddr;

static uip_lladdr_t *old_parent_lladdr;
static uip_lladdr_t *parent_lladdr;

static uint8_t allocated_cells_num;
static struct cell_list allocated_cells;

static struct scheduling_msg sch_msg;

static uint8_t seqno = 1;
static uint8_t pass_add_op, pass_del_op, pass_it;

struct scheduling_table{
  uint8_t is_used;
  uint8_t channel_offset;
  struct cs_node_pair *node_pair;
};
static struct scheduling_table central_sch[NUMBER_OF_SLOT_OFFSET];

PROCESS(central_scheduler_client_process, "CS Client Process");

/*------------------------------------------------------------------------------
 *  Scheduling table daki shared slotları scheduled olarak, diğer slotları ise unscheduled olarak setler.
 */
static void 
reset_scheduling_table()
{
  uint8_t sl;
  struct cs_node_pair *null_pair;

  null_pair = cs_add_pair(&uip_lladdr, (uip_lladdr_t *)&linkaddr_null);

  for(sl = 0; sl < FOURE_MAX_SHARED_SLOT; sl++){
    central_sch[sl].is_used = CS_SLOT_SCHEDULED;
    central_sch[sl].channel_offset = 0;
    central_sch[sl].node_pair = null_pair;
  }
  for(sl = FOURE_MAX_SHARED_SLOT; sl < NUMBER_OF_SLOT_OFFSET; sl++){
    central_sch[sl].is_used = CS_SLOT_UNSCHEDULED;
    central_sch[sl].channel_offset = CS_NULL_CHANNEL_OFFSET;
    central_sch[sl].node_pair = NULL;
  }
}

/*------------------------------------------------------------------------------
 *  sch-msg yi buf doldurur, verilen dest_ipaddr ye gönderir
 */
static uint8_t
central_scheduler_send_packet(uip_ipaddr_t *dest_ipaddr, struct scheduling_msg *msg)
{
  uint8_t buf[MAX_PAYLOAD_LEN], buf_len;

  if((buf_len = cs_create_packet(buf, msg)) > 0){
    PRINT("Send to %u %u->%u\n", dest_ipaddr->u8[sizeof(uip_ipaddr_t) - 1], msg->transmitter.addr[7], msg->receiver.addr[7]);
    uip_udp_packet_sendto(client_conn, buf, buf_len, dest_ipaddr, UIP_HTONS(CS_LISTEN_PORT));
    return 1;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *  
 */
static void
central_scheduler_set_slot(uint8_t sl_offset, uint8_t ch_offset, uint8_t slot_status, struct cs_node_pair *allocated_node_pair)
{
  central_sch[sl_offset].is_used = slot_status;
  central_sch[sl_offset].channel_offset = ch_offset;
  central_sch[sl_offset].node_pair = allocated_node_pair;
}

/*------------------------------------------------------------------------------
 *  Verilen pair için sahip olduğu slotları allocated_cells e doldurur.
 */
static uint8_t
central_scheduler_get_slots(uip_lladdr_t *sender, uip_lladdr_t *receiver)
{
  uint8_t sl;

  allocated_cells_num = 0;

  for(sl = 0; sl < NUMBER_OF_SLOT_OFFSET; sl++){
    if((central_sch[sl].is_used == CS_SLOT_SCHEDULED) && \
       (memcmp(&central_sch[sl].node_pair->transmitter, sender, sizeof(uip_lladdr_t)) == 0) && \
       (memcmp(&central_sch[sl].node_pair->receiver, receiver, sizeof(uip_lladdr_t)) == 0)){
      allocated_cells.slot_offsets[allocated_cells_num] = sl;
      allocated_cells.channel_offsets[allocated_cells_num] = central_sch[sl].channel_offset;
      allocated_cells_num++;
    }
  }
  return allocated_cells_num;
}

/*------------------------------------------------------------------------------
 *  Scheduling table da verilen sl_offset ve ch_offset e karşı gelen hücrenin 
 *  transmitter ı tr, receiver ı rc ve slot schedule ise return 1 else return 0 
 */
static uint8_t
central_scheduler_is_cell_scheduled(uint8_t sl_offset, uint8_t ch_offset, uip_lladdr_t *tr, uip_lladdr_t *rc)
{
  if((central_sch[sl_offset].is_used == CS_SLOT_SCHEDULED) && \
     (central_sch[sl_offset].channel_offset == ch_offset) && \
     (memcmp(&central_sch[sl_offset].node_pair->transmitter, tr, sizeof(uip_lladdr_t)) == 0) && \
     (memcmp(&central_sch[sl_offset].node_pair->receiver, rc, sizeof(uip_lladdr_t)) == 0)){
        return 1;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *  Verilen cell listesindeki cell_num slot u verilen pair için allocate eder.
 */
static int8_t
central_scheduler_allocate_slot(uip_lladdr_t *sender, uip_lladdr_t *receiver, struct cell_list *cells, uint8_t cell_num, uint8_t slot_type)
{ 
  struct cs_node_pair *pair;
  int8_t  cell_index, ret_val = 0;
  uint8_t sl_offset, ch_offset, allocation_num = 0;
  uint8_t failed_cells_num = 0;
  struct cell_list failed_cells;
  struct scheduling_msg send_msg;

  central_scheduler_get_slots(sender, receiver); /* this function sets allocated cells */

  for(cell_index = 0; cell_index < cell_num; cell_index++){ /* adds cell_num slots */
    sl_offset = cells->slot_offsets[cell_index];
    ch_offset = cells->channel_offsets[cell_index];
    if(sl_offset != CS_NULL_SLOT_OFFSET && ch_offset != CS_NULL_CHANNEL_OFFSET){
      if(central_sch[sl_offset].is_used == CS_SLOT_UNSCHEDULED){
        pair = cs_add_pair(sender, receiver);
        if(pair != NULL){
          if(slot_type == CS_SLOT_TYPE_TRANSMIT){
            ret_val = foure_mac_buf_schedule_slots((linkaddr_t *)receiver, CS_SLOT_TYPE_TRANSMIT, FRAME802154_DATAFRAME, &sl_offset, &ch_offset , 1);
          }else if(slot_type == CS_SLOT_TYPE_RECEIVE){
            ret_val = foure_mac_buf_schedule_slots((linkaddr_t *)sender, CS_SLOT_TYPE_RECEIVE, FRAME802154_DATAFRAME, &sl_offset, &ch_offset, 1);
          }else{
            ret_val = foure_mac_buf_schedule_slots((linkaddr_t *)&linkaddr_null, CS_SLOT_TYPE_SHARED, FRAME802154_DATAFRAME, &sl_offset, &ch_offset, 1);
          }
          if(ret_val == FOURE_SLOT_SCHEDULED){
            central_scheduler_set_slot(sl_offset, ch_offset, CS_SLOT_SCHEDULED, pair);
            allocated_cells.slot_offsets[allocated_cells_num] = sl_offset;
            allocated_cells.channel_offsets[allocated_cells_num] = ch_offset;    
            allocated_cells_num++;
            allocation_num++;
            PRINT("Slot %u-c%u was scheduled for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
          }else{
            failed_cells.slot_offsets[failed_cells_num] = sl_offset;
            failed_cells.channel_offsets[failed_cells_num] = ch_offset;
            failed_cells_num++;
          }
        }
      }else if(central_scheduler_is_cell_scheduled(sl_offset, ch_offset, sender, receiver)){
        PRINT("Slot %u-c%u was already scheduled for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
      }else{
        PRINT("Slot %u-c%u wasn't found for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
      }
    }
  }
  if(failed_cells_num > 0){ /* TODO: Check the request before forwarding to the root node so that this does not happen */
    for(cell_index = 0; cell_index < failed_cells_num; cell_index++)
      PRINT("Slot %u-c%u failed\n", failed_cells.slot_offsets[cell_index], failed_cells.channel_offsets[cell_index]);

    cs_prepare_packet(&send_msg, CMD_DELETE, seqno++, failed_cells_num, &failed_cells, sender, receiver);
    cs_add_request(&UIP_IP_BUF->srcipaddr, REQUEST_FOR_SEND, NORMAL_PRIORITY, 5, CLOCK_SECOND / 16, &send_msg);
  }

  if(allocated_cells_num == cell_num){
    return FOURE_SLOT_SCHEDULED;
  }else if(allocation_num > 0){
    PRINT("Slot scheduling failed for %u slots!\n", cell_num - allocated_cells_num);
    return FOURE_SLOT_SCHEDULED;    
  }else{
    PRINT("Slot scheduling failed!\n");
    return FOURE_SLOT_UNSCHEDULED;
  }
}

/*------------------------------------------------------------------------------
 *  Deallocate given slot's from scheduling table, 
 *  eğer o pair için slot kalmamışsa pair i pair table dan siler.
 */
static uint8_t
central_scheduler_deallocate_slot(uip_lladdr_t *sender, uip_lladdr_t *receiver, struct cell_list *cells, uint8_t cell_num)
{
  uint8_t cell_index, unused_pair;
  uint8_t sl, sl_offset, ch_offset;

  for(cell_index = 0; cell_index < cell_num; cell_index++){
    sl_offset = cells->slot_offsets[cell_index];
    ch_offset = cells->channel_offsets[cell_index];
    if(sl_offset != CS_NULL_SLOT_OFFSET && ch_offset != CS_NULL_CHANNEL_OFFSET){
      if(central_scheduler_is_cell_scheduled(sl_offset, ch_offset, sender, receiver)){
        central_scheduler_set_slot(sl_offset, CS_NULL_CHANNEL_OFFSET, CS_SLOT_UNSCHEDULED, NULL);
        PRINT("Slot %u-c%u deallocated for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
      }else{
        PRINT("Slot %u-c%u not found for %u->%u\n", sl_offset, ch_offset, sender->addr[7], receiver->addr[7]);
      }
    }
  }
  unused_pair = 1;
  for(sl = 0; sl < NUMBER_OF_SLOT_OFFSET; sl++){
    if((central_sch[sl].is_used == CS_SLOT_SCHEDULED) && \
       (memcmp(&central_sch[sl].node_pair->transmitter, sender, sizeof(uip_lladdr_t)) == 0) && \
       (memcmp(&central_sch[sl].node_pair->receiver, receiver, sizeof(uip_lladdr_t)) == 0)){
      unused_pair = 0;
      break;
    }
  }
  if(unused_pair){
    cs_delete_pair(sender, receiver);
  }
  if((memcmp(sender, &uip_lladdr, sizeof(uip_lladdr_t)) == 0)){ /* Transmit slots for receiver */
    foure_mac_buf_delete_slots((linkaddr_t *)receiver, cells->slot_offsets, cell_num);
  }else if((memcmp(receiver, &uip_lladdr, sizeof(uip_lladdr_t)) == 0)){ /* Receive slots for sender */
    foure_mac_buf_delete_slots((linkaddr_t *)sender, cells->slot_offsets, cell_num);
  }
  return 1;
}

/*------------------------------------------------------------------------------
 *
 */
static uint8_t
central_scheduler_is_request_duplicate()
{
  uint8_t cell_ptr, used_cell_num = 0;

  if(sch_msg.op_code == CMD_DELETE){
    for(cell_ptr = 0; cell_ptr < sch_msg.num_cells; cell_ptr++){
      if(central_scheduler_is_cell_scheduled(sch_msg.cells.slot_offsets[cell_ptr], sch_msg.cells.channel_offsets[cell_ptr], &sch_msg.transmitter, &sch_msg.receiver))
        return 0;  
    }
    return 1;
  }else if(sch_msg.op_code == CMD_ADD){
    for(cell_ptr = 0; cell_ptr < sch_msg.num_cells; cell_ptr++){
      if(sch_msg.cells.slot_offsets[cell_ptr] != CS_NULL_SLOT_OFFSET && sch_msg.cells.channel_offsets[cell_ptr] != CS_NULL_CHANNEL_OFFSET)
        used_cell_num++;
    }
    central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver); /* this function sets allocated cells */
    if(allocated_cells_num > used_cell_num){ /* Duplication was detected */
      return 1;
    }else
      return 0;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 *
 */
void
tsch_rpl_callback_leaving_network()
{
  rpl_dag_t *dag = rpl_get_any_dag();
  if(dag != NULL) {
    rpl_local_repair(dag->instance);
  }
}

/*------------------------------------------------------------------------------
 *
 */
static void
tcpip_handler(void)
{
  uint8_t *recv_packet, is_duplicate;
  uip_ds6_nbr_t *nbr = NULL;
  struct request_stack *rqst;

  if(uip_newdata()){
    recv_packet = uip_appdata;

    if(cs_parse_packet(&sch_msg, recv_packet, uip_datalen()) == 0){
      PRINT("Packet parsing failed!\n");
      return;
    }

    if(memcmp(&sch_msg.receiver, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){
      nbr = uip_ds6_nbr_ll_lookup(&sch_msg.transmitter);
      if(nbr == NULL){
        PRINT("Neighbour %u not found!\n", sch_msg.transmitter.addr[7]);
        dis_output(NULL);
        return;
      }
    }

    if(sch_msg.op_code >= CMD_ADD && sch_msg.op_code <= CMD_CLEAR){ /* Request */
      rqst = cs_get_request_from_op_code(&sch_msg.transmitter, &sch_msg.receiver, 0, sch_msg.op_code); /* sch_msg.transmitter should be me or my child */
      if(rqst != NULL){ /* Request already was added */
        cs_prepare_packet(&sch_msg, RC_BUSY, sch_msg.op_code, 0, NULL, &sch_msg.transmitter, &sch_msg.receiver);
        central_scheduler_send_packet(&UIP_IP_BUF->srcipaddr, &sch_msg);
      }else{ /* Request don't exist */
        if(sch_msg.op_code == CMD_COUNT){
          PRINT("Count Request for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
          cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_send_packet(&UIP_IP_BUF->srcipaddr, &sch_msg);
          return;
        }else if(sch_msg.op_code == CMD_LIST){
          PRINT("List Request for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
          cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_send_packet(&UIP_IP_BUF->srcipaddr, &sch_msg);
          return;
        }else if(sch_msg.op_code == CMD_CLEAR){
          PRINT("Clear Request for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_deallocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &allocated_cells, allocated_cells_num);
          rqst = cs_get_request_from_op_code(&sch_msg.transmitter, &sch_msg.receiver, 0, CMD_ADD);
          if(rqst != NULL){
            cs_remove_request(rqst);
          }        
          cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          central_scheduler_send_packet(&UIP_IP_BUF->srcipaddr, &sch_msg);
          return;
        }
        
        is_duplicate = central_scheduler_is_request_duplicate(); /* duplicate function sets allocated_cells */

        if(is_duplicate){
          if(sch_msg.op_code == CMD_ADD){
            cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
          }else if(sch_msg.op_code == CMD_DELETE){
            cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.op_code, sch_msg.num_cells, &sch_msg.cells, &sch_msg.transmitter, &sch_msg.receiver);
          }
          central_scheduler_send_packet(&UIP_IP_BUF->srcipaddr, &sch_msg);
        }else{
          if(sch_msg.op_code == CMD_ADD){
            PRINT("Add Request reached to Parent for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          }else if(sch_msg.op_code == CMD_DELETE){
            PRINT("Delete Request reached to Parent for %u->%u\n", sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
          }
          cs_add_request(&server_ipaddr, REQUEST_FOR_SEND, NORMAL_PRIORITY, 5, 0, &sch_msg);
          return;
        }
      }
    }else if(sch_msg.op_code >= RC_SUCCESS && sch_msg.op_code <= RC_ERR){ /* Response */
      rqst = cs_get_request_from_op_code(&sch_msg.transmitter, &sch_msg.receiver, 0, sch_msg.seqnum); /* in Response messages seqnum = request's op_code */
      if(rqst != NULL){ /* Request exist */
        if(sch_msg.op_code == RC_SUCCESS){
          if(rqst->msg.op_code == CMD_ADD){
            if(memcmp(&sch_msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){ /* Response for me */
              central_scheduler_allocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &sch_msg.cells, sch_msg.num_cells, CS_SLOT_TYPE_TRANSMIT);
              pass_it = 1;
              pass_add_op = 0;
            }else if(memcmp(&sch_msg.receiver, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){ /* Response for my child */
              PRINT("Add reached from Server for Client %u\n", sch_msg.transmitter.addr[7]);
              if(central_scheduler_allocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &sch_msg.cells, sch_msg.num_cells, CS_SLOT_TYPE_RECEIVE) == FOURE_SLOT_SCHEDULED){
                cs_prepare_packet(&sch_msg, RC_SUCCESS, sch_msg.seqnum, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
              }else{
                cs_prepare_packet(&sch_msg, RC_ERR, sch_msg.seqnum, 0, NULL, &sch_msg.transmitter, &sch_msg.receiver);
              }
            }
          }else if(rqst->msg.op_code == CMD_DELETE){
            central_scheduler_deallocate_slot(&sch_msg.transmitter, &sch_msg.receiver, &sch_msg.cells, sch_msg.num_cells);
            pass_del_op = 0;
          }else if(rqst->msg.op_code == CMD_COUNT){
            PRINT("Operation succeed from %u for Count request\n", sch_msg.receiver.addr[7]);
          }else if(rqst->msg.op_code == CMD_LIST){
            PRINT("Operation succeed from %u for List request\n", sch_msg.receiver.addr[7]);
          }else if(rqst->msg.op_code == CMD_CLEAR){
            PRINT("Operation succeed from %u for Clear request\n", sch_msg.receiver.addr[7]);
          }
        }else if(sch_msg.op_code == RC_VER_ERR){
          PRINT("ERROR: Unsupported 6P version!\n");
        }else if(sch_msg.op_code == RC_SFID_ERR){
          PRINT("ERROR: Unsupported SFID!\n");
        }else if(sch_msg.op_code == RC_BUSY){
          PRINT("ERROR: Handling previous request!\n");
          rqst->retry = 5;
          rqst->time_out = 180 * SLOT_FRAME_DURATION_ETIMER; /* retry after one minute */
          return;
        }else if(sch_msg.op_code == RC_RESET){
          PRINT("ERROR: Abort 6P transaction!\n");
        }else if(sch_msg.op_code == RC_ERR){
          PRINT("ERROR: Operation failed!\n");
          if(memcmp(&sch_msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t)) == 0){ /* if transmitter of packet is me */
            if(central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver) < 1){
              tsch_rpl_callback_leaving_network();
            }
          }
        }
        cs_remove_request(rqst);
        if(memcmp(&sch_msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t)) != 0){ /* if transmitter of packet is not me */
          cs_set_ip_from_prefix(&to_ipaddr, &default_instance->current_dag->prefix_info, &sch_msg.transmitter);
          central_scheduler_send_packet(&to_ipaddr, &sch_msg);
        }
      }else{ /* If incoming packet is response message, ignore it */
        PRINT("OP %u ID %u not found for %u->%u!\n", sch_msg.op_code, sch_msg.seqnum, sch_msg.transmitter.addr[7], sch_msg.receiver.addr[7]);
        return;
      }
    }else{
      PRINT("bug\n");
      return;
    }
  }
}

/*------------------------------------------------------------------------------*/
#define TRANSMISSIONS_RATE_SCALE 100
#define TRANSMISSIONS_RATE_ALPHA 40

#define BUFFER_OCCUPANCY_SCALE 100
#define BUFFER_OCCUPANCY_ALPHA 70

#define EXPONENTIAL_AVERAGES(average, value, alpha, scale)                                    \
                             average = ((uint32_t)average * alpha +                           \
                                        (uint32_t)value * (scale - alpha) + scale / 2) / scale
#define SLOT_DELETE_THRESHOLD 120

static uint16_t av_arrival_rate;
static uint16_t av_service_rate;
static uint16_t av_noack_rate;
static uint16_t av_collision_rate;
static uint8_t av_buf_occupancy;
static uint8_t slot_delete_threshold;
static destination_stats_t parent_stat;
/*------------------------------------------------------------------------------
 *
 */
static void
cs_update_transmissions_ratio()
{
  uint16_t arrival_num, service_num, noack_num, collision_num, arrival_service_ratio;
  uint8_t buf_occupancy, free_content_num, av_arrival_slot_frame, av_service_slot_frame, slot_add_required, slot_num;
  int8_t tx_slot_num, needed_tx_slot_num;

  arrival_num = foure_mac_buf_get_destination_inserted_content_num((linkaddr_t *)parent_lladdr, &parent_stat);
  service_num = foure_mac_buf_get_destination_slot_tx_ok((linkaddr_t *)parent_lladdr, (CS_SLOT_TYPE_TRANSMIT | CS_SLOT_TYPE_SHARED));
  noack_num = foure_mac_buf_get_destination_slot_tx_noack((linkaddr_t *)parent_lladdr, CS_SLOT_TYPE_TRANSMIT);
  collision_num = foure_mac_buf_get_destination_slot_tx_collisions((linkaddr_t *)parent_lladdr, CS_SLOT_TYPE_TRANSMIT);
  
  EXPONENTIAL_AVERAGES(av_arrival_rate, arrival_num, TRANSMISSIONS_RATE_ALPHA, TRANSMISSIONS_RATE_SCALE);
  av_arrival_rate = (av_arrival_rate > 0) ? av_arrival_rate : 1;
  EXPONENTIAL_AVERAGES(av_service_rate, service_num, TRANSMISSIONS_RATE_ALPHA, TRANSMISSIONS_RATE_SCALE);
  av_service_rate = (av_service_rate > 0) ? av_service_rate : 1;

  EXPONENTIAL_AVERAGES(av_noack_rate, noack_num, TRANSMISSIONS_RATE_ALPHA, TRANSMISSIONS_RATE_SCALE);
  EXPONENTIAL_AVERAGES(av_collision_rate, collision_num, TRANSMISSIONS_RATE_ALPHA, TRANSMISSIONS_RATE_SCALE);

  arrival_service_ratio = (100 * av_arrival_rate) / av_service_rate;

  buf_occupancy = foure_mac_buf_get_buffer_occupancy((linkaddr_t *)parent_lladdr);
  free_content_num = foure_mac_buf_get_free_content_num((linkaddr_t *)parent_lladdr);
  tx_slot_num = foure_mac_buf_get_slot_num_from_lladdr((linkaddr_t *)parent_lladdr, CS_SLOT_TYPE_TRANSMIT);
 
  av_arrival_slot_frame = (av_arrival_rate + (UPDATE_TRANSMISSIONS_FACTOR - 1)) / UPDATE_TRANSMISSIONS_FACTOR;
  av_service_slot_frame = (av_service_rate + av_noack_rate + av_collision_rate + (UPDATE_TRANSMISSIONS_FACTOR - 1)) / UPDATE_TRANSMISSIONS_FACTOR;
  needed_tx_slot_num = (tx_slot_num > 0) ? ((av_arrival_rate * tx_slot_num + av_service_rate - 1) / av_service_rate) - tx_slot_num : av_arrival_slot_frame;

  slot_add_required = (tx_slot_num < 1) || (arrival_service_ratio > 100 && \
                       (int16_t)((free_content_num + av_service_rate) - av_arrival_rate) < 0 && ((int8_t)buf_occupancy - av_buf_occupancy) > 0);

  EXPONENTIAL_AVERAGES(av_buf_occupancy, buf_occupancy, BUFFER_OCCUPANCY_ALPHA, BUFFER_OCCUPANCY_SCALE);
  PRINTO("n%u c%u ", noack_num, collision_num);
  PRINTO("F%u I/O:%u/%u ", free_content_num, arrival_num, service_num);
  PRINTO("av I/O:%u/%u %u ", av_arrival_rate, av_service_rate, av_service_slot_frame);
  PRINTO("B%u avB%u n%d %u\n", buf_occupancy, av_buf_occupancy, needed_tx_slot_num, slot_add_required);

  if(pass_it){
    pass_it = 0;
  }else if(!pass_add_op && slot_add_required && needed_tx_slot_num > 0 && \
           foure_mac_buf_get_slot_num_from_lladdr((linkaddr_t *)parent_lladdr, CS_SLOT_TYPE_ALL) < (CS_MAX_SLOT_NUM - 1)){

    PRINT("slot need %u\n", needed_tx_slot_num);
    memcpy(&sch_msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t));
    memcpy(&sch_msg.receiver, parent_lladdr, sizeof(uip_lladdr_t));
    central_scheduler_get_slots(&sch_msg.transmitter, &sch_msg.receiver); /* this function sets allocated cells */
    slot_num = allocated_cells_num;
    while(needed_tx_slot_num > 0 && allocated_cells_num < (CS_MAX_SLOT_NUM - FOURE_MAX_SHARED_SLOT)){
      allocated_cells.slot_offsets[allocated_cells_num] = CS_NULL_SLOT_OFFSET;
      allocated_cells.channel_offsets[allocated_cells_num] = CS_NULL_CHANNEL_OFFSET;
      allocated_cells_num++;
      needed_tx_slot_num--;
    }
    if(allocated_cells_num > slot_num){
      cs_prepare_packet(&sch_msg, CMD_ADD, seqno++, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
      cs_set_ip_from_prefix(&to_ipaddr, &default_instance->current_dag->prefix_info, parent_lladdr);
      cs_add_request(&to_ipaddr, REQUEST_FOR_SEND, NORMAL_PRIORITY, 5, 0, &sch_msg);
      pass_add_op = 1;
    }
  }else if(!pass_del_op && arrival_service_ratio < 150 && tx_slot_num > 1 && ((int8_t)tx_slot_num - av_service_slot_frame) > 0 && \
           ((buf_occupancy * free_content_num + (100 - buf_occupancy) / 2) / (100 - buf_occupancy)) < 2){
    uint8_t i, slot_tx[5];
    slot_delete_threshold++;
    if(slot_delete_threshold > SLOT_DELETE_THRESHOLD){
      slot_delete_threshold = 0;
      allocated_cells_num = foure_mac_buf_get_least_used_slots((linkaddr_t *)parent_lladdr, allocated_cells.slot_offsets, allocated_cells.channel_offsets, \
                                                               slot_tx, tx_slot_num - av_service_slot_frame, 2);
      for(i= 0; i < allocated_cells_num; i++){
        PRINT("del s%u-c%u TX %u\n", allocated_cells.slot_offsets[i], allocated_cells.channel_offsets[i], slot_tx[i]);
      }

      if(allocated_cells_num > 0){
        foure_mac_buf_set_slots_status((linkaddr_t *)parent_lladdr, allocated_cells.slot_offsets, allocated_cells_num, SLOT_NOT_USABLE);
        memcpy(&sch_msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t));
        memcpy(&sch_msg.receiver, parent_lladdr, sizeof(uip_lladdr_t));
        cs_prepare_packet(&sch_msg, CMD_DELETE, seqno++, allocated_cells_num, &allocated_cells, &sch_msg.transmitter, &sch_msg.receiver);
        cs_set_ip_from_prefix(&to_ipaddr, &default_instance->current_dag->prefix_info, parent_lladdr);
        cs_add_request(&to_ipaddr, REQUEST_FOR_SEND, NORMAL_PRIORITY, 5, CLOCK_SECOND / 16, &sch_msg);
        pass_del_op = 1;
      }
    }
  }else{
    slot_delete_threshold = 0;
    PRINTO("reset\n");
  }
    
  foure_mac_buf_update_slot_stats((linkaddr_t *)parent_lladdr, (CS_SLOT_TYPE_TRANSMIT | CS_SLOT_TYPE_SHARED));
}

/*------------------------------------------------------------------------------
 *
 */
PROCESS_THREAD(central_scheduler_client_process, ev, data)
{
  static struct etimer et, stats_timer;
  static struct request_stack *rqst;
  static rpl_parent_t *parent;
  static uip_lladdr_t lladdr;

  PROCESS_BEGIN();

#if PLATFORM_HAS_BUTTON
  SENSORS_ACTIVATE(button_sensor);
#endif

  client_conn = udp_new(NULL, UIP_HTONS(0), NULL); /* first argument is remote ipaddr, second argument is remote port of connection */

  if(client_conn == NULL){
    PRINT("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(client_conn, UIP_HTONS(CS_LISTEN_PORT));/* Set local port given connection */
  
  cs_conn = client_conn;

  etimer_set(&et, CLOCK_SECOND / 64);
  while(!foure_control.secured || default_instance == NULL){
    PROCESS_WAIT_UNTIL(etimer_expired(&et));
    etimer_reset(&et);
  }

  PRINT("Central Scheduler Client Process Started\n");

  while(nbr_table_get_lladdr(rpl_parents, default_instance->current_dag->preferred_parent) == NULL){
    PROCESS_WAIT_UNTIL(etimer_expired(&et));
    etimer_reset(&et);
  }
  memcpy(&server_ipaddr, &default_instance->current_dag->dag_id, sizeof(uip_ipaddr_t));
  PRINT("CS:Server IP is %u\n", server_ipaddr.u8[15]);
  parent_lladdr = (uip_lladdr_t *)nbr_table_get_lladdr(rpl_parents, default_instance->current_dag->preferred_parent);
  PRINT("CS:Parent was set to %u\n", parent_lladdr->addr[7]);
  parent = default_instance->current_dag->preferred_parent;

  etimer_set(&stats_timer, WAIT_DAO_TRANSMISSION_INTERVAL);
  
  while(1){
    PROCESS_YIELD();

    if(parent != default_instance->current_dag->preferred_parent){
      parent = default_instance->current_dag->preferred_parent;
    }

    if(ev == tcpip_event){
      tcpip_handler();
    }

    if(ev == rpl_parent_switch_event){
      PRINT("CS:Preferred Parent Changed to %u.\n", parent_lladdr->addr[7]);
      foure_mac_buf_parent_changed((linkaddr_t *)old_parent_lladdr, (linkaddr_t *)parent_lladdr);
      central_scheduler_get_slots(&uip_lladdr, old_parent_lladdr);
      central_scheduler_deallocate_slot(&uip_lladdr, old_parent_lladdr, &allocated_cells, allocated_cells_num);

      rqst = cs_get_request_from_op_code(&uip_lladdr, old_parent_lladdr, 0, CMD_ADD);
      while(rqst != NULL){
        cs_remove_request(rqst);
        rqst = cs_get_request_from_op_code(&uip_lladdr, old_parent_lladdr, 0, CMD_ADD);
      }
    }

    if(etimer_expired(&et)){
      etimer_reset(&et);
      if(foure_control.synched && foure_control.secured && default_instance != NULL){
        rqst = cs_get_head_request();
        while(rqst != NULL){
          if(rqst->retry > 0){
            if(rqst->time_out > 0){
              rqst->time_out -= et.timer.interval;
            }else{ /* Send */
              cs_set_lladdr_from_ipaddr(&lladdr, &rqst->dest_ipaddr);
              if(!foure_mac_buf_is_content_available_for_packet_type((linkaddr_t *)&lladdr, FRAME802154_SCHEDULER)){
                rqst->retry--;
                rqst->time_out = 5 * SLOT_FRAME_DURATION_ETIMER * (1 << (FOURE_MAC_MIN_BE + FOURE_MAC_BE_INC));
                central_scheduler_send_packet(&rqst->dest_ipaddr, &rqst->msg);
              }
            }
          }else{ /* Delete Request */
            if(rqst->msg.op_code == CMD_ADD){
              if(memcmp(&rqst->msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t)) == 0)
                pass_add_op = 0;
            }else if(rqst->msg.op_code == CMD_DELETE){
              if(memcmp(&rqst->msg.transmitter, &uip_lladdr, sizeof(uip_lladdr_t)) == 0)
                pass_del_op = 0;
            }
            cs_remove_request(rqst);
          }
          rqst = list_item_next(rqst);
        }
      }
    }
    if(etimer_expired(&stats_timer)){
      cs_update_transmissions_ratio();
      etimer_set(&stats_timer, UPDATE_TRANSMISSIONS_INTERVAL);
    }
#if PLATFORM_HAS_BUTTON
    if(ev == sensors_event && data == &button_sensor){
      PRINT("CLICK\n");
    }
#endif
  }
  PROCESS_END();
}

/*------------------------------------------------------------------------------
 *
 */
void
post_central_scheduler_parent_switch_event(void *data)
{
  rpl_parent_t *new_parent = (rpl_parent_t *)data;

  if(new_parent != NULL && \
     memcmp(parent_lladdr, nbr_table_get_lladdr(rpl_parents, new_parent), sizeof(uip_lladdr_t)) != 0){
    old_parent_lladdr = parent_lladdr;
    parent_lladdr = (uip_lladdr_t *)nbr_table_get_lladdr(rpl_parents, new_parent);
    process_post(&central_scheduler_client_process, rpl_parent_switch_event, (process_data_t)data);
    av_arrival_rate = 0;
    av_service_rate = 0;
    av_noack_rate = 0;
    av_collision_rate = 0;
    av_buf_occupancy = 0;
    slot_delete_threshold = 0;
    pass_add_op = 0; 
    pass_del_op = 0; 
    pass_it = 0;
  }
}

/*------------------------------------------------------------------------------
 *
 */
void 
cs_client_init()
{
  allocated_cells_num = 0;
  av_arrival_rate = 0;
  av_service_rate = 0;
  av_noack_rate = 0;
  av_collision_rate = 0;
  av_buf_occupancy = 0;
  slot_delete_threshold = 0;
  pass_add_op = 0; 
  pass_del_op = 0; 
  pass_it = 0;
  
  cs_set_ip_from_prefix(&node_ipaddr, &default_instance->current_dag->prefix_info, &uip_lladdr);
  
  cs_request_init();
  cs_pair_init();
  reset_scheduling_table();
#if RPL_OF == rpl_of_tisch
  rpl_parent_switch_event = process_alloc_event();
#endif
  process_exit(&central_scheduler_client_process);
  process_start(&central_scheduler_client_process, NULL);
}

#endif  /* TSCH_CENTRAL_SCHEDULER_ENABLED */
#endif  /* TSCH_TIME_SYNCH */
