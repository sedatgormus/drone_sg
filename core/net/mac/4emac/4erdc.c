/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A null RDC implementation that uses framer for headers.
 * \author
 *         Adam Dunkels <adam@sics.se>
 *         Niclas Finne <nfi@sics.se>
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "net/rpl/rpl-private.h"
//#include "net/rime/rimestats.h"
#include "net/mac/framer-802154.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "sys/rtimer.h"
#include "dev/watchdog.h"

#if TSCH_TIME_SYNCH

#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/4erdc.h"
#include "net/mac/4emac/4emac-sequence.h"
#include "net/mac/4emac/4emac-eack.h"

#include <stdlib.h>
#include <string.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#define COOJA_DEBUG 0
#if COOJA_DEBUG
#include "net/mac/4emac/cooja-debug.h"
#else
#define COOJA_DEBUG_STR(str)
#define COOJA_DEBUG_INT(val)
#endif

#ifdef FOUR_ERDC_CONF_ADDRESS_FILTER
#define FOUR_ERDC_ADDRESS_FILTER FOUR_ERDC_CONF_ADDRESS_FILTER
#else
#define FOUR_ERDC_ADDRESS_FILTER 1
#endif /* FOUR_ERDC_CONF_ADDRESS_FILTER */

#ifndef FOUR_ERDC_802154_AUTOACK
#ifdef FOUR_ERDC_CONF_802154_AUTOACK
#define FOUR_ERDC_802154_AUTOACK FOUR_ERDC_CONF_802154_AUTOACK
#else
#define FOUR_ERDC_802154_AUTOACK 0
#endif /* FOUR_ERDC_CONF_802154_AUTOACK */
#endif /* FOUR_ERDC_802154_AUTOACK */

#ifndef FOUR_ERDC_802154_AUTOACK_HW
#ifdef FOUR_ERDC_CONF_802154_AUTOACK_HW
#define FOUR_ERDC_802154_AUTOACK_HW FOUR_ERDC_CONF_802154_AUTOACK_HW
#else
#define FOUR_ERDC_802154_AUTOACK_HW 0
#endif /* FOUR_ERDC_CONF_802154_AUTOACK_HW */
#endif /* FOUR_ERDC_802154_AUTOACK_HW */

#ifndef FOUR_ERDC_SEND_802154_ACK
#ifdef FOURE_CONF_SEND_802154_ACK
#define FOUR_ERDC_SEND_802154_ACK FOURE_CONF_SEND_802154_ACK
#else /* FOURE_CONF_SEND_802154_ACK */
#define FOUR_ERDC_SEND_802154_ACK 0
#endif /* FOURE_CONF_SEND_802154_ACK */
#endif /* FOUR_ERDC_SEND_802154_ACK */

#if (FOUR_ERDC_802154_AUTOACK || FOUR_ERDC_SEND_802154_ACK)

#ifdef FOUR_ERDC_CONF_ACK_WAIT_TIME
#define ACK_WAIT_TIME FOUR_ERDC_CONF_ACK_WAIT_TIME
#else /* FOUR_ERDC_CONF_ACK_WAIT_TIME */
#define ACK_WAIT_TIME                      RTIMER_SECOND / 5000
#endif /* FOUR_ERDC_CONF_ACK_WAIT_TIME */
#ifdef FOUR_ERDC_CONF_AFTER_ACK_DETECTED_WAIT_TIME
#define AFTER_ACK_DETECTED_WAIT_TIME FOUR_ERDC_CONF_AFTER_ACK_DETECTED_WAIT_TIME
#else /* FOUR_ERDC_CONF_AFTER_ACK_DETECTED_WAIT_TIME */
#define AFTER_ACK_DETECTED_WAIT_TIME       RTIMER_SECOND / 2500
#endif /* FOUR_ERDC_CONF_AFTER_ACK_DETECTED_WAIT_TIME */
#endif /* FOUR_ERDC_802154_AUTOACK */

#if FOUR_ERDC_SEND_802154_ACK
#include "net/mac/frame802154.h"
#endif /* FOUR_ERDC_SEND_802154_ACK */
#if (!TSCH_TIME_SYNCH || FOUR_ERDC_802154_AUTOACK)
#define ACK_LEN 3
#endif

#ifndef FOUR_ERDC_CCA_ENABLED
#ifdef FOUR_ERDC_CONF_CCA_ENABLED
#define FOUR_ERDC_CCA_ENABLED     FOUR_ERDC_CONF_CCA_ENABLED
#define CHANNEL_NOISE_SCALE      100
#define CHANNEL_NOISE_ALPHA      80
#define CCA_MARGIN               7 
#define MIN_NOISE_LEVEL          -90
#define MAX_NOISE_LEVEL          -75
#else
#define FOUR_ERDC_CCA_ENABLED     0
#endif /* FOUR_ERDC_CONF_CCA_ENABLED */
#endif /* FOUR_ERDC_CCA_ENABLED */

extern struct rtimer stx;

static PT_THREAD(four_erdc_tx_slot(struct rtimer *t, void *ptr));
struct pt slot_tx_pt;

static PT_THREAD(four_erdc_rx_slot(struct rtimer *t, void *ptr));
struct pt slot_rx_pt;

static int average_channel_noise[NUMBER_OF_CHANNELS]; 

PROCESS(four_erdc_process_pending, "Pending Process");

/*---------------------------------------------------------------------------*/
#define TSCH_SCHEDULE_AND_YIELD(pt, tm, ref_time, offset, callback, cur_packetbuf, ptr, yielded, str) \
  do{                                                                                     \
    if(tsch_schedule_slot_operation(tm, ref_time, offset - RTIMER_GUARD, callback, ptr)){ \
      packetbuf_select(cur_packetbuf);                                                    \
      yielded = 1;                                                                        \
      PT_YIELD(pt);                                                                       \
    }                                                                                     \
    BUSYWAIT_UNTIL_ABS(0, ref_time, offset);                                              \
  }while(0);


#if FOUR_ERDC_CCA_ENABLED
/*---------------------------------------------------------------------------
 *   Get current channel index to average the noise in the channel
 *   This can be used to set the CCA threshold on the fly
 */
static uint8_t 
get_current_channel_index(uint8_t channel_offset)
{
  return (current_asn.ls4b + channel_offset) % NUMBER_OF_CHANNELS;
}

/*---------------------------------------------------------------------------
 *   Calculate average noise at each channel to set propoper CCA threshold
 */
static void 
calculate_average_channel_noise(struct foure_mac_item *mac_item)
{
  if (mac_item == NULL) return; //should not happen, mac_item should not be null.

  radio_value_t radio_noise_rssi;
  NETSTACK_RADIO.get_value(RADIO_PARAM_RSSI, &radio_noise_rssi);

  uint8_t channel_offset = mac_item->sl->channel_offset;
  int8_t current_channel_index = get_current_channel_index(channel_offset);

  if(current_channel_index >= NUMBER_OF_CHANNELS) return; //something wrong

  if(radio_noise_rssi < MAX_NOISE_LEVEL && radio_noise_rssi > MIN_NOISE_LEVEL)
     average_channel_noise[current_channel_index] = (average_channel_noise[current_channel_index] * CHANNEL_NOISE_ALPHA + \
                                                     radio_noise_rssi * (CHANNEL_NOISE_SCALE - CHANNEL_NOISE_ALPHA))/CHANNEL_NOISE_SCALE;

  PRINTO("r %d %d %d\n", average_channel_noise[current_channel_index], radio_noise_rssi, current_channel_index);
}

/*---------------------------------------------------------------------------
 *   Get CCA threshold for the current channel
 */
static void 
set_channel_CCA_threshold(struct foure_mac_item *mac_item)
{
  if (mac_item == NULL) return; //should not happen, mac_item should not be null.
        
  uint8_t channel_offset = mac_item->sl->channel_offset;
  int8_t current_channel_index = get_current_channel_index(channel_offset);
  NETSTACK_RADIO.set_value(RADIO_PARAM_CCA_THRESHOLD, average_channel_noise[current_channel_index] + CCA_MARGIN);

}
#endif /* FOUR_ERDC_CCA_ENABLED */

/*---------------------------------------------------------------------------*/
int8_t
four_erdc_peek_put(uint8_t bitmap, uint8_t buf_size)
{
  int8_t i;

  for(i = 0; i < buf_size; i++){
    if((bitmap & (1 << i)) == 0) return i;
  }
  return -1;
}

/*---------------------------------------------------------------------------*/
int8_t
four_erdc_peek_get(uint8_t bitmap, uint8_t buf_size)
{
  int8_t i;

  for(i = 0; i < buf_size; i++){
    if(bitmap & (1 << i)) return i;
  }
  return -1;
}

/*---------------------------------------------------------------------------*/
struct foure_log_t{
  char message[24];
};

#define LOG_NUM          1
#define LOG_PROCESS_MASK 0xFF
static uint32_t log_process_index;
static struct foure_log_t foure_logs[LOG_NUM];

#define FOURE_LOG_ADD(init_code) \
do{ \
    int8_t log_index = four_erdc_peek_put(log_process_index, LOG_NUM); \
    if(log_index != -1){ \
      log_process_index |= (1 << log_index); \
      struct foure_log_t *log = &foure_logs[log_index]; \
      init_code; \
    } \
}while(0);

void
four_erdc_log_process()
{
  static int8_t log_index;

  while(log_process_index & LOG_PROCESS_MASK){
    log_index = four_erdc_peek_get(log_process_index, LOG_NUM);
    if(log_index != -1){
      PRINT("%s\n", foure_logs[log_index].message);
      log_process_index &= ~(1 << log_index);
    }
  }  
}

/*---------------------------------------------------------------------------*/
#define TX_PACKET_NUM    5
#define TX_PROCESS_MASK 0x1F /* b00011111 */
static uint8_t tx_process_index;
static struct foure_mac_packet tx_process_queue[TX_PACKET_NUM];

void
four_erdc_tx_pending_process()
{
  static int8_t tx_index;

  while(tx_process_index & TX_PROCESS_MASK){
    tx_index = four_erdc_peek_get(tx_process_index, TX_PACKET_NUM);
    if(tx_index != -1){
      packetbuf_select(tx_process_queue[tx_index].packetbuf);
      mac_call_sent_callback(tx_process_queue[tx_index].sent, tx_process_queue[tx_index].ptr, tx_process_queue[tx_index].status, tx_process_queue[tx_index].num_transmissions);
      packetbuf_free(tx_process_queue[tx_index].packetbuf);
      tx_process_queue[tx_index].packetbuf = NULL;
      tx_process_index &= ~(1 << tx_index);
      PRINTO("TX %u unlock ret %u\n", tx_index, tx_process_queue[tx_index].status);
      //FOURE_LOG_ADD(sprintf(log->message, "TX %u unlock ret %u", tx_index, tx_process_queue[tx_index].status));
    }
  }  
}

/*---------------------------------------------------------------------------*/
#define RX_PACKET_NUM    5
#define RX_PROCESS_MASK 0x1F /* b00011111 */
struct foure_mac_input_packet{
  uint32_t asn;
  packetbuf_t *p;
};

static uint8_t rx_process_index;
static struct foure_mac_input_packet rx_process_queue[RX_PACKET_NUM];

void
four_erdc_rx_pending_process()
{
  static int8_t rx_index;

  while(rx_process_index & RX_PROCESS_MASK){
    rx_index = four_erdc_peek_get(rx_process_index, RX_PACKET_NUM);
    if(rx_index != -1){
      packetbuf_select(rx_process_queue[rx_index].p);
#if RPL_ADD_ROUTE_ENABLED
      cur_asn = rx_process_queue[rx_index].asn % (NUMBER_OF_CHANNELS * slot_frame_size);
#endif
      NETSTACK_MAC.input();
      packetbuf_free(rx_process_queue[rx_index].p);
      rx_process_queue[rx_index].p = NULL;
      rx_process_index &= ~(1 << rx_index);
      PRINTO("RX %u unlock\n", rx_index);
      //FOURE_LOG_ADD(snprintf(log->message, sizeof(log->message),"RX %u unlock", rx_index));
    }
  }
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(four_erdc_process_pending, ev, data)
{
  PROCESS_BEGIN();

  PRINT("4erdc: Pending Process Started\n");

  while(1){
    PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
    four_erdc_rx_pending_process();
    //four_erdc_tx_pending_process();
    //four_erdc_log_process();
  }
  PROCESS_END();
}

/*---------------------------------------------------------------------------
 *
 */
PT_THREAD(four_erdc_tx_slot(struct rtimer *t, void *ptr))
{
  static uint8_t ret, yielded;
  static uint8_t tx_status;
  static struct foure_mac_packet *tx_packet = NULL;

#if ENHANCED_PACKETBUF_ENABLE
  packetbuf_t *cur_packetbuf = packetbuf_get_current(); /* backup current Packetbuf pointer */
  if(cur_packetbuf == NULL){
    PRINT("4erdc: current packetbuf TX not found!\n");
  }

  if(tx_packet != NULL)
    packetbuf_select(tx_packet->packetbuf);
#endif /* ENHANCED_PACKETBUF_ENABLE */

  PT_BEGIN(&slot_tx_pt);
  yielded = 0;
  tx_packet = (struct foure_mac_packet *)ptr;
  
  if(tx_packet == NULL){
    ret = MAC_TX_ERR_FATAL;
    PRINT("4erdc: fatal error!\n");
  }else{
    packetbuf_select(tx_packet->packetbuf);
#ifdef NETSTACK_ENCRYPT
    NETSTACK_ENCRYPT();
#endif /* NETSTACK_ENCRYPT */

    static int is_broadcast;

#if IPV6_RPL_UDP_NOISE
    static uint8_t channel_hopping_pattern[] = {15, 4, 10, 14, 0, 11};
    static uint8_t pass = 0;
    static uint8_t ch = 0;
    static rtimer_clock_t loop_start;
    uint8_t *hdr_ptr = packetbuf_hdrptr();
    
    hdr_ptr[0] = 0xFF;
    NETSTACK_RADIO.prepare(packetbuf_hdrptr(), 2 * packetbuf_totlen());

    while(1){
      loop_start = RTIMER_NOW();
      pass = random_rand() % 3;
      if(pass == 0){
        NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, channel_hopping_pattern[ch % NUMBER_OF_CHANNELS] + 11);
        TSCH_SCHEDULE_AND_YIELD(&slot_tx_pt, t, loop_start, \
                                US_TO_RTIMERTICKS(3000), \
                                four_erdc_tx_slot, cur_packetbuf, ptr, yielded, "loop");   
        ch++;
      }else if(pass == 1){
        NETSTACK_RADIO.transmit(2 * packetbuf_totlen());
        loop_start = RTIMER_NOW();
        BUSYWAIT_UNTIL_ABS(0, loop_start, \
                           US_TO_RTIMERTICKS(10));
        NETSTACK_RADIO.transmit(2 * packetbuf_totlen());
        loop_start = RTIMER_NOW();
        BUSYWAIT_UNTIL_ABS(0, loop_start, \
                           US_TO_RTIMERTICKS(10));
        NETSTACK_RADIO.transmit(2 * packetbuf_totlen());
        NETSTACK_RADIO.off();
      }else{
        TSCH_SCHEDULE_AND_YIELD(&slot_tx_pt, t, loop_start, \
                                US_TO_RTIMERTICKS(3000), \
                                four_erdc_tx_slot, cur_packetbuf, ptr, yielded, "loop");       
      }
    }
#endif /* !IPV6_RPL_UDP_NOISE */
    NETSTACK_RADIO.prepare(packetbuf_hdrptr(), packetbuf_totlen());

    is_broadcast = linkaddr_cmp(packetbuf_addr(PACKETBUF_ADDR_RECEIVER), &linkaddr_null);

#if FOUR_ERDC_CCA_ENABLED
    static uint8_t cca_status;
    cca_status = 1;

    TSCH_SCHEDULE_AND_YIELD(&slot_tx_pt, t, current_slot_start, \
                            US_TO_RTIMERTICKS(time_slot_timing.ts_cca_offset), \
                            four_erdc_tx_slot, cur_packetbuf, ptr, yielded, "cca");

    /*  if channel_clear() == 1, channel is empty */
    BUSYWAIT_UNTIL_ABS(!(cca_status &= NETSTACK_RADIO.channel_clear()), \
                       current_slot_start, \
                       US_TO_RTIMERTICKS(time_slot_timing.ts_cca_offset) \
                       + US_TO_RTIMERTICKS(time_slot_timing.ts_cca));

    if(cca_status == 0){
      int8_t current_channel_index = get_current_channel_index(((struct foure_mac_item *)(tx_packet->ptr))->sl->channel_offset);
       radio_value_t radio_noise_rssi;
       NETSTACK_RADIO.get_value(RADIO_PARAM_RSSI, &radio_noise_rssi);
      
      PRINT("col %d %d\n", current_channel_index, radio_noise_rssi);
      NETSTACK_RADIO.off();
      ret = MAC_TX_COLLISION;
    }else
#endif /* FOUR_ERDC_CCA_ENABLED */
    {

#if FOUR_ERDC_CCA_ENABLED //set adaptive CCA threshold for the channel
      set_channel_CCA_threshold((struct foure_mac_item *)tx_packet->ptr);
#endif
      /* Delay before TX */
      TSCH_SCHEDULE_AND_YIELD(&slot_tx_pt, t, current_slot_start, \
                              US_TO_RTIMERTICKS(time_slot_timing.ts_tx_offset) - RADIO_DELAY_BEFORE_TX, \
                              four_erdc_tx_slot, cur_packetbuf, ptr,  yielded, "beforeTX");

      static rtimer_clock_t tx_finish_time;
      
      tx_status = NETSTACK_RADIO.transmit(packetbuf_totlen());
      tx_finish_time = RTIMER_NOW();

#if FOUR_ERDC_CCA_ENABLED //Sample and calculate channel noise for adaptive CCA thresholding
      if(tx_status == RADIO_TX_OK){
         calculate_average_channel_noise((struct foure_mac_item *)tx_packet->ptr); 
      }
#endif

      NETSTACK_RADIO.off();
      if(tx_status == RADIO_TX_OK){
          if(!is_broadcast){           
            watchdog_periodic();
#if 0//FOUR_ERDC_CCA_ENABLED
            cca_status = 1;
            BUSYWAIT_UNTIL_ABS(0, tx_finish_time \
                               , US_TO_RTIMERTICKS(time_slot_timing.ts_rx_ack_delay)
                               - US_TO_RTIMERTICKS(time_slot_timing.ts_cca));

            BUSYWAIT_UNTIL_ABS(!(cca_status &= NETSTACK_RADIO.channel_clear()) \
                                , tx_finish_time \
                                , US_TO_RTIMERTICKS(time_slot_timing.ts_rx_ack_delay));
#endif
            /* Wait for ACK to come */
            TSCH_SCHEDULE_AND_YIELD(&slot_tx_pt, t, tx_finish_time, \
                                    US_TO_RTIMERTICKS(time_slot_timing.ts_rx_ack_delay) - RADIO_DELAY_BEFORE_RX, \
                                    four_erdc_tx_slot, cur_packetbuf, ptr,  yielded, "beforeACK");
            NETSTACK_RADIO.on();
            //static linkaddr_t sender;
            static frame802154_t frame;
            static uint8_t ack_len, ack_hdrlen, frame_valid, time_source;
            static linkaddr_t source_address;
            static linkaddr_t destination_address;
            static struct eack_ts_ie eack_ies;

            BUSYWAIT_UNTIL_ABS(NETSTACK_RADIO.receiving_packet(), \
                               tx_finish_time, \
                               US_TO_RTIMERTICKS(time_slot_timing.ts_rx_ack_delay) \
                               + US_TO_RTIMERTICKS(time_slot_timing.ts_ack_wait));

#if !SIX_TISCH_MINIMAL
            static rtimer_clock_t ack_start_time;
            ack_start_time = RTIMER_NOW();
#endif
            /* Wait for ACK to finish */
            watchdog_periodic();
            BUSYWAIT_UNTIL_ABS(!NETSTACK_RADIO.receiving_packet(), \
                               tx_finish_time, \
                               US_TO_RTIMERTICKS(time_slot_timing.ts_rx_ack_delay) \
                               + US_TO_RTIMERTICKS(time_slot_timing.ts_ack_wait) \
                               + US_TO_RTIMERTICKS(time_slot_timing.ts_max_ack));

            ack_len = 0;
            if(NETSTACK_RADIO.pending_packet()){
              ack_len = NETSTACK_RADIO.read(packetbuf_dataptr(), FOURE_MAC_MAX_PACKET_LEN);
              NETSTACK_RADIO.off();
              if(ack_len > 0) {
                if(decode_eack((uint8_t*)packetbuf_dataptr(), ack_len, packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO),
                   &frame, &eack_ies, &ack_hdrlen) == 0){
                  ack_len = 0;
                  PRINTO("4erdc: failed to decode\n");
                }

#if LLSEC802154_SECURITY_LEVEL //Place holder for security
                if(ack_len != 0){
                  if(!security_parse_frame(ackbuf, ack_hdrlen, ack_len - ack_hdrlen - tsch_security_mic_len(&frame),
                     &frame, &current_neighbor->addr, &current_asn)){
                    PRINTO("4erdc: !failed to authenticate ACK\n");
                    ack_len = 0;
                  }
                }else{
                  PRINTO("4erdc: !failed to parse ACK\n");
                }
#endif /* LLSEC802154_SECURITY_LEVEL */
                
                frame_valid = ack_hdrlen > 0 && frame802154_extract_linkaddr(&frame, &source_address, &destination_address);

                if(frame_valid){
                  if(linkaddr_cmp(&destination_address, &linkaddr_node_addr) || \
                     linkaddr_cmp(&destination_address, &linkaddr_null)){
                    time_source = 0;
                    if(!foure_control.synched || (default_instance->current_dag->preferred_parent != NULL \
                       && (default_instance->current_dag->preferred_parent == rpl_get_parent((uip_lladdr_t *)&source_address)))){ 
                      ack_counter++;
                      time_source = 1;
                    }
                  }
                }
              }
            }else{
              NETSTACK_RADIO.off();
            } 
             
            if(ack_len != 0){
              if(time_source){
#if SIX_TISCH_MINIMAL
                extern int32_t adjust_by_eack;
                extern uint8_t time_correction_received;
                adjust_by_eack = US_TO_RTIMERTICKS(eack_ies.ts);
                time_correction_received = FOURE_MAC_TIME_CORRECTION_EACK;
#else /* !SIX_TISCH_MINIMAL */
                extern uint8_t time_correction_received;
                //TODO: authority offset is not received here. Further, if the function takes too long time it de-synch the node. Do a message passing
                eack_ts_container.ie_timestamp = eack_ies.ts;
                eack_ts_container.eack_receive_time = ack_start_time;
                time_correction_received = FOURE_MAC_TIME_CORRECTION_EACK;

                PRINTO("eack ts %x:%x:%x\n",eack_ts_container.ie_timestamp, eack_ts_container.eack_receive_time);
#endif /* SIX_TISCH_MINIMAL */
              }
              ret = MAC_TX_OK;
            }else{
#if 0//FOUR_ERDC_CCA_ENABLED
              if(cca_status == 0){
                ret = MAC_TX_COLLISION;
              }else
#endif
                ret = MAC_TX_NOACK;
            }
            /*  Restore the sender address as the receiver of the sent frame */
            //linkaddr_copy(packetbuf_addr(PACKETBUF_ADDR_SENDER), &sender);
          }else{ /* If broadcast */
            ret = MAC_TX_OK;
          }
      }else if(tx_status == RADIO_TX_COLLISION){
        NETSTACK_RADIO.off();
        ret = MAC_TX_COLLISION;
      }else if(tx_status == RADIO_TX_NOACK){
        NETSTACK_RADIO.off();
        ret = MAC_TX_NOACK;
      }else{
        NETSTACK_RADIO.off();
        ret = MAC_TX_ERR;
      }
    }
  }

#if 0 /* transmit callbac leri üst katmanlara process ile çıkarılmalı*/
  uint8_t index = four_erdc_peek_put(tx_process_index, TX_PACKET_NUM);
  if(index != -1){
    tx_process_index |= (1 << index);
    tx_process_queue[index].packetbuf = tx_packet->packetbuf;
    tx_process_queue[index].sent = tx_packet->sent;
    tx_process_queue[index].ptr = tx_packet->ptr;
    tx_process_queue[index].status = ret;
    tx_process_queue[index].num_transmissions = 1;
    process_poll(&four_erdc_process_pending);
    FOURE_LOG_ADD(snprintf(log->message, sizeof(log->message), "TX %u lock", index));
  }else{
#endif
    mac_call_sent_callback(tx_packet->sent, tx_packet->ptr, ret, 1);
    //PRINT("4erdc: TX queue full!\n");
  ///}

#if ENHANCED_PACKETBUF_ENABLE
  packetbuf_select(cur_packetbuf);
  packetbuf_free(tx_packet->packetbuf);
#endif
  tx_packet = NULL;

  if(yielded){
    sixtop_slot_callback(t, NULL);
  }

  PT_END(&slot_tx_pt);
}

/*---------------------------------------------------------------------------
 * Transmit one packet over the radio interface. This code is called during
 * transmit slots and retransmissions are handled at 4eMAC layer. Slot timings
 * are handled here. And the eack processing happens after a successful transmission
 */
static void
send_packet(mac_callback_t sent, void *ptr)
{
  PT_INIT(&slot_tx_pt);
  uint8_t ret = four_erdc_tx_slot(&stx, ptr);
  if(ret > PT_YIELDED){ /* pt exited or ended */
    PRINT("4erdc: TX pt ended!\n");
  }
}

/*---------------------------------------------------------------------------
 * 
 */
PT_THREAD(four_erdc_rx_slot(struct rtimer *t, void *ptr))
{
  static uint8_t packet_seen, yielded;
  static rtimer_clock_t rx_start_time;
  static rtimer_clock_t packet_duration;
  static uint8_t duplicate = 1;
#if ENHANCED_PACKETBUF_ENABLE
  static packetbuf_t *rx_packet = NULL;

  packetbuf_t *cur_packetbuf = packetbuf_get_current(); /* backup current Packetbuf pointer */
  if(cur_packetbuf == NULL){
    PRINT("4erdc: current packetbuf RX not found!\n");
  }

  if(rx_packet != NULL)
    packetbuf_select(rx_packet);
#endif /* ENHANCED_PACKETBUF_ENABLE */
  PT_BEGIN(&slot_rx_pt);
  yielded = 0;

  if(NETSTACK_RADIO.pending_packet()){
    PRINTO("4erdc: already pending a packet\n");
    PRINT("x\n");
  }

  /* Wait before starting to listen */
  TSCH_SCHEDULE_AND_YIELD(&slot_rx_pt, t, current_slot_start, \
                          US_TO_RTIMERTICKS(time_slot_timing.ts_rx_offset) - RADIO_DELAY_BEFORE_RX, \
                          four_erdc_rx_slot, cur_packetbuf, ptr,  yielded, "beforeRX");

  NETSTACK_RADIO.on();

  if((rx_packet = packetbuf_alloc()) != NULL){
    packetbuf_select(rx_packet);
    packetbuf_clear();
  }else{
    NETSTACK_RADIO.off();
    rx_packet = NULL;
    packetbuf_select(cur_packetbuf);
    sixtop_slot_callback(t, NULL);
    PRINT("4erdc: packetbuf RX full!\n");
    PT_EXIT(&slot_rx_pt);
  }

  packet_seen = NETSTACK_RADIO.receiving_packet();

  if(!packet_seen){
    BUSYWAIT_UNTIL_ABS((packet_seen = NETSTACK_RADIO.receiving_packet()), \
                       current_slot_start, \
                       US_TO_RTIMERTICKS(time_slot_timing.ts_rx_offset) \
                       + US_TO_RTIMERTICKS(time_slot_timing.ts_rx_wait));
  }

  if(!packet_seen){
    NETSTACK_RADIO.off();
  }else{
    rx_start_time = RTIMER_NOW();
    packetbuf_set_attr(PACKETBUF_ATTR_TIMESTAMP, rx_start_time);
    /* Wait until packet is received, turn radio off */
    BUSYWAIT_UNTIL_ABS(!NETSTACK_RADIO.receiving_packet(), \
                       current_slot_start, \
                       US_TO_RTIMERTICKS(time_slot_timing.ts_rx_offset) \
                       + US_TO_RTIMERTICKS(time_slot_timing.ts_rx_wait) \
                       + US_TO_RTIMERTICKS(time_slot_timing.ts_max_tx));

    if(NETSTACK_RADIO.pending_packet()){
      static int datalen;
      static uint8_t *dataptr;

      dataptr = packetbuf_dataptr();

      radio_received_time = RTIMER_NOW();
      datalen = NETSTACK_RADIO.read(dataptr, PACKETBUF_SIZE);
      packetbuf_set_datalen(datalen);
      
      NETSTACK_RADIO.off();
      packet_duration = TSCH_PACKET_DURATION(datalen);
      //radio_received_time = rx_start_time + packet_duration;
#ifdef NETSTACK_DECRYPT
      NETSTACK_DECRYPT();
#endif /* NETSTACK_DECRYPT */
      if(NETSTACK_FRAMER.parse() < 0){
        PRINTO("4erdc: failed to parse %u %d\n", packetbuf_datalen(), datalen);
#if FOUR_ERDC_ADDRESS_FILTER
      }else if(!linkaddr_cmp(packetbuf_addr(PACKETBUF_ADDR_RECEIVER), &linkaddr_node_addr) &&
               !linkaddr_cmp(packetbuf_addr(PACKETBUF_ADDR_RECEIVER), &linkaddr_null)){
        PRINTO("4erdc: not for us\n");
#endif /* FOUR_ERDC_ADDRESS_FILTER */
      }else if(packetbuf_attr(PACKETBUF_ATTR_FRAME_TYPE) == FRAME802154_BEACONFRAME){
        PRINTO("4erdc: Time Stamp\n");
        tsch_eb_input();
      }else if(packetbuf_attr(PACKETBUF_ATTR_FRAME_TYPE) == FRAME802154_ACKFRAME){
        PRINTO("4erdc: Ign ACK\n");
      }else{
#if FOUR_ERDC_SEND_802154_ACK
        /* Check for duplicate packet. */     
        duplicate = foure_mac_sequence_is_duplicate();
        if(duplicate){
          /* Drop the packet. */
          PRINTO("4erdc: drop duplicate link layer packet %u\n", packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO));
        }else{
          foure_mac_sequence_register_seqno();
        }

        static frame802154_t info154;
        frame802154_parse(dataptr, datalen, &info154);

        PRINTO("FOR US %d %d %d\n", info154.fcf.frame_type == FRAME802154_DATAFRAME,
                                    info154.fcf.ack_required != 0,
                                    linkaddr_cmp((linkaddr_t *)&info154.dest_addr, &linkaddr_node_addr));

        if(info154.fcf.frame_type == FRAME802154_DATAFRAME &&  
           info154.fcf.ack_required != 0 && 
           linkaddr_cmp((linkaddr_t *)&info154.dest_addr, &linkaddr_node_addr))
        {
          static uint8_t ack_buf[FOURE_MAC_MAX_PACKET_LEN];
          static int ack_len;

#if SIX_TISCH_MINIMAL 
          static rtimer_clock_t expected_rx_time;
          static int32_t estimated_drift;
          uint32_t tx_offset_in_ticks = US_TO_RTIMERTICKS(time_slot_timing.ts_tx_offset);


          expected_rx_time = current_slot_start + tx_offset_in_ticks;
          estimated_drift = (int32_t)expected_rx_time - (int32_t)packetbuf_attr(PACKETBUF_ATTR_TIMESTAMP);
          if(abs(estimated_drift) > tx_offset_in_ticks) estimated_drift = 0;
          ack_len = tsch_packet_create_eack(ack_buf, sizeof(ack_buf), (linkaddr_t *)packetbuf_addr(PACKETBUF_ADDR_SENDER), packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO), (int16_t)RTIMERTICKS_TO_US(estimated_drift), 1);
          PRINTO("4erdc: est drift %d %d\n", estimated_drift, abs(estimated_drift) > tx_offset_in_ticks);
#else /* !SIX_TISCH_MINIMAL */
          ack_len = tsch_packet_create_eack(ack_buf, sizeof(ack_buf), (linkaddr_t *)packetbuf_addr(PACKETBUF_ADDR_SENDER), packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO), 0, 1);
#endif /* SIX_TISCH_MINIMAL */
          NETSTACK_RADIO.prepare((void *)&ack_buf[0], ack_len);

          /* Wait for time to ACK and transmit ACK */
          TSCH_SCHEDULE_AND_YIELD(&slot_rx_pt, t, radio_received_time, \
                                  US_TO_RTIMERTICKS(time_slot_timing.ts_tx_ack_delay) - RADIO_DELAY_BEFORE_TX, \
                                  four_erdc_rx_slot, cur_packetbuf, ptr,  yielded, "beforeAck");

          packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_ACKFRAME);
          NETSTACK_RADIO.on();
          NETSTACK_RADIO.transmit(ack_len);
          NETSTACK_RADIO.off();
        }else
          PRINTO("4erdc: unknown frame type %d \n", info154.fcf.ack_required);

        if(!duplicate){
          uint8_t index = four_erdc_peek_put(rx_process_index, RX_PACKET_NUM);
          if(index != -1){
            rx_process_index |= (1 << index);
            rx_process_queue[index].p = rx_packet;
            rx_process_queue[index].asn = current_asn.ls4b;
#if PACKETBUF_LOCK_ENABLED
            if(packetbuf_attr(PACKETBUF_ATTR_LOCK) == PACKETBUF_LOCK)
              PRINT("4erdc: packetbuf RX %p lock!\n", rx_packet); 
            packetbuf_set_attr(PACKETBUF_ATTR_LOCK, PACKETBUF_LOCK);
#endif /* PACKETBUF_LOCK_ENABLED */
            process_poll(&four_erdc_process_pending);
            rx_packet = NULL;
            //FOURE_LOG_ADD(sprintf(log->message, "RX %u lock", index));
          }else{
            PRINT("4erdc: RX queue full!\n");
          }
        }
#endif /* FOUR_ERDC_SEND_802154_ACK */
      }
    }else{
      NETSTACK_RADIO.off();
    }
  }

  packetbuf_select(cur_packetbuf);
  if(rx_packet != NULL){
    packetbuf_free(rx_packet);
    rx_packet = NULL;
  }
  if(yielded){
    sixtop_slot_callback(t, NULL);
  }

  PT_END(&slot_rx_pt);
}

/*-------------------------------------------------------------------------------------
 * Receive and parse the packet received by the radio. If the packet is not intended
 * for the receivng node or an ACK frame, discard it. If it is a time stamp (enhanced
 * beacon), process it. If the packet is for the receiving node, estimate the time drift
 * and an enhanced ACK is sent. Consequently, the parsed frame is passed to the upper layers,
 * if it is not a duplicate of previously received frames.
 */
static void
packet_input(void)
{
  uint8_t poll_mode = 0;
  radio_value_t radio_rx_mode;

  /* Radio Rx mode */
  NETSTACK_RADIO.get_value(RADIO_PARAM_RX_MODE, &radio_rx_mode);
  poll_mode = (radio_rx_mode & RADIO_RX_MODE_POLL_MODE);

  if(poll_mode){
    PT_INIT(&slot_rx_pt);
    uint8_t ret = four_erdc_rx_slot(&stx, NULL);
    if(ret > PT_YIELDED){ /* pt exited or ended */
      PRINT("4erdc: RX pt ended!\n");
    }
  }else{
    int datalen;
    uint8_t *dataptr;

    datalen = packetbuf_datalen();
    dataptr = packetbuf_dataptr();
#ifdef NETSTACK_DECRYPT
    NETSTACK_DECRYPT();
#endif /* NETSTACK_DECRYPT */
    if(NETSTACK_FRAMER.parse() < 0){
      PRINTO("4erdc: failed to parse %u %d\n", packetbuf_datalen(), datalen);
#if FOUR_ERDC_ADDRESS_FILTER
    }else if(!linkaddr_cmp(packetbuf_addr(PACKETBUF_ADDR_RECEIVER), &linkaddr_node_addr) &&
             !linkaddr_cmp(packetbuf_addr(PACKETBUF_ADDR_RECEIVER), &linkaddr_null)){
      PRINTO("4erdc: not for us %x\n", tsch_timesynch_time());
#endif /* FOUR_ERDC_ADDRESS_FILTER */
    }else if(packetbuf_attr(PACKETBUF_ATTR_FRAME_TYPE) == FRAME802154_BEACONFRAME){ 
        PRINTO("4erdc: Time Stamp\n");
        tsch_eb_input();
    }else if(packetbuf_attr(PACKETBUF_ATTR_FRAME_TYPE) == FRAME802154_ACKFRAME){
        PRINTO("4erdc: Ign ACK\n");
    }else{
#if FOUR_ERDC_SEND_802154_ACK
      /* Check for duplicate packet. */ 
      int duplicate = 0;    
      duplicate = foure_mac_sequence_is_duplicate();
      if(duplicate){
        /* Drop the packet. */
        PRINTO("4erdc: drop duplicate link layer packet %u\n", packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO));
      }else{
        foure_mac_sequence_register_seqno();
      }

      frame802154_t info154;
      frame802154_parse(dataptr, datalen, &info154);

      PRINTO("FOR US %d %d %d\n", info154.fcf.frame_type == FRAME802154_DATAFRAME,
                                  info154.fcf.ack_required != 0,
                                  linkaddr_cmp((linkaddr_t *)&info154.dest_addr, &linkaddr_node_addr));

      if(info154.fcf.frame_type == FRAME802154_DATAFRAME &&  
         info154.fcf.ack_required != 0 && 
         linkaddr_cmp((linkaddr_t *)&info154.dest_addr, &linkaddr_node_addr)){

        static uint8_t ack_buf[FOURE_MAC_MAX_PACKET_LEN];
        static int ack_len;
#if SIX_TISCH_MINIMAL 
        static rtimer_clock_t expected_rx_time;
        extern rtimer_clock_t current_slot_start;
        static int32_t estimated_drift;
        uint32_t tx_offset_in_ticks = US_TO_RTIMERTICKS(time_slot_timing.ts_tx_offset);

        expected_rx_time = current_slot_start + tx_offset_in_ticks;
        estimated_drift = (int32_t)expected_rx_time - (int32_t)packetbuf_attr(PACKETBUF_ATTR_TIMESTAMP);
        if(abs(estimated_drift) > tx_offset_in_ticks) estimated_drift = 0;
        ack_len = tsch_packet_create_eack(ack_buf, sizeof(ack_buf), (linkaddr_t *)packetbuf_addr(PACKETBUF_ADDR_SENDER), packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO), (int16_t)RTIMERTICKS_TO_US(estimated_drift), 1);
        PRINTO("4erdc: est drift %d %d\n", estimated_drift, abs(estimated_drift) > tx_offset_in_ticks);
#else /* !SIX_TISCH_MINIMAL */
        ack_len = tsch_packet_create_eack(ack_buf, sizeof(ack_buf), (linkaddr_t *)packetbuf_addr(PACKETBUF_ADDR_SENDER), packetbuf_attr(PACKETBUF_ATTR_MAC_SEQNO), 0, 1);
#endif /* SIX_TISCH_MINIMAL */
        NETSTACK_RADIO.prepare((void *)&ack_buf[0], ack_len);

        BUSYWAIT_UNTIL_ABS(0, radio_received_time, \
                           US_TO_RTIMERTICKS(time_slot_timing.ts_tx_ack_delay) - RADIO_DELAY_BEFORE_TX);
     
        packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, FRAME802154_ACKFRAME);
        NETSTACK_RADIO.transmit(ack_len);
      }else
        PRINTO("4erdc: unknown frame type %d \n", info154.fcf.ack_required);

#endif /* FOUR_ERDC_SEND_802154_ACK */
      if(!duplicate){
        NETSTACK_MAC.input();
      }
    }
  }
}

/*---------------------------------------------------------------------------*/
static int
on(void)
{
  return NETSTACK_RADIO.on();
}

/*---------------------------------------------------------------------------*/
static int
off(int keep_radio_on)
{
  if(keep_radio_on){
    return NETSTACK_RADIO.on();
  }else{
    return NETSTACK_RADIO.off();
  }
}

/*---------------------------------------------------------------------------*/
static unsigned short
channel_check_interval(void)
{
  return 0;
}

/*---------------------------------------------------------------------------*/
static void
init(void)
{
#if TSCH_TIME_SYNCH
  sixtop_init();
  process_exit(&four_erdc_process_pending);
  process_start(&four_erdc_process_pending, NULL);
  
  //Initialise the cca values for individual values to the preset value. 
  uint8_t ch_noise_index;
  radio_value_t cca_threshold;
  NETSTACK_RADIO.get_value(RADIO_PARAM_CCA_THRESHOLD, &cca_threshold);
  for(ch_noise_index = 0; ch_noise_index < NUMBER_OF_CHANNELS; ch_noise_index ++)
      average_channel_noise[ch_noise_index] = cca_threshold;

#endif
  on();
}

/*---------------------------------------------------------------------------*/
const struct rdc_driver foure_driver = {
  "four_e",
  init,
  send_packet,
  NULL, //we do not implement send_list interface
  packet_input,
  on,
  off,
  channel_check_interval,
};
/*---------------------------------------------------------------------------*/
#endif
