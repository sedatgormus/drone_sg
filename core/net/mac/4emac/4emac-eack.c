
/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Enhanced ACK frame creation
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/packetbuf.h"
#include "net/mac/framer-802154.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-eack.h"
#include "net/mac/4emac/4emac-buf.h"

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_NONE
#endif
#include "net/mac/4emac/4emac-debug.h"

/*-----------------------------------------------------------
 * Header IE. ACK/NACK time correction. Used in enhanced ACKs 
 */
int8_t
create_ie_header_ack_nack_time_correction(uint8_t *buf, int len, struct eack_ts_ie *eack_ies)
{
  int ie_len = 2;
  if(len >= 2 + ie_len && eack_ies != NULL) {
    int16_t drift_us;
    uint16_t time_sync_field;
    drift_us = eack_ies->ts;
    time_sync_field = drift_us & 0x0fff;
    if( eack_ies->ie_is_nack) {
      time_sync_field |= 0x8000;
    }
#if SIX_TISCH_MINIMAL
    WRITE16(buf + 2, time_sync_field);
    create_header_ie_descriptor(buf, &eack_ies->ie_hdr);
    return 2 + ie_len;
#else
    WRITE16(buf + 2, 0);
    create_header_ie_descriptor(buf, &eack_ies->ie_hdr);
    return 4 + ie_len;
#endif
  } else {
    return -1;
  }
}

/*-----------------------------------------------------------------
 * Parse a header IE in the received enhanced ACKs.
 */
int8_t
decode_eack_ies(uint8_t *buf, int len, struct eack_ts_ie *eack_ies)
{
  uint16_t local_ie_bytes;
  uint8_t  current_position = 0;

  memcpy(&local_ie_bytes, &buf[current_position], sizeof(uint16_t));
  get_ie_header_parts(local_ie_bytes, &eack_ies->ie_hdr, 1);

  current_position += 2;


  switch(eack_ies->ie_hdr.id) {
    case HEADER_IE_ACK_NACK_TIME_CORRECTION:
#if SIX_TISCH_MINIMAL
      if(len - current_position == 2) {
        /* If the originator was a time source neighbor, the receiver adjust
        * its own clock by incorporating the received drift correction */
        uint16_t time_sync_field = 0;
        /* Extract drift correction from Sync-IE, cast from 12 to 16-bit,
         * and convert it to RTIMER ticks.
         * See page 88 in IEEE Std 802.15.4e-2012. */
        memcpy(&time_sync_field, &buf[current_position], sizeof(uint16_t));
        int16_t drift_us = 0;
        /* First extract NACK */
        eack_ies->ie_is_nack = (time_sync_field & (uint16_t)0x8000) ? 1 : 0;
        /* Then cast from 12 to 16 bit signed */
        if(time_sync_field & 0x0800) { /* Negative integer */
          drift_us = time_sync_field | 0xf000;
        } else { /* Positive integer */
          drift_us = time_sync_field & 0x0fff;
        }
        /* Convert to RTIMER ticks */
        eack_ies->ts = drift_us;
#else
      if(len - current_position == 4) {
        uint32_t time_sync_field;
#if OPENMOTE || MAHMOTE
        memcpy(&time_sync_field, &buf[current_position], sizeof(uint32_t));
#else
        memcpy(&time_sync_field, &buf[current_position + 2], sizeof(uint16_t));
#endif
        //drift_us = time_sync_field;
        eack_ies->ts = time_sync_field;//US_TO_RTIMERTICKS(drift_us);
#endif
        /*Save hader length*/
        eack_ies->ie_payload_ie_offset = current_position;
        return len;
      }
      break;
  }
  return -1;
}

/*------------------------------------------------------------------------------------- 
 * Decode enhanced ACK packet, extract drift and nack. 
 */
int8_t
decode_eack(uint8_t *buf, int buf_size,
    uint8_t seqno, frame802154_t *frame,  struct  eack_ts_ie * eack_ies, uint8_t *hdr_len)
{
  uint8_t curr_len = 0;
  int ret;
  linkaddr_t dest;

  if(frame == NULL || buf_size < 0) {
    return 0;
  }

  /* Parse 802.15.4-2006 frame, i.e. all fields before Information Elements */
  if((ret = frame802154_parse(buf, buf_size, frame)) < 3) {
    return 0;
  }
  if(hdr_len != NULL) {
    *hdr_len = ret;
  }
  curr_len += ret;

  /* Check seqno */
  if(seqno != frame->seq) {
    return 0;
  }
  /* Check destination address (if any) */
  ret = frame802154_extract_linkaddr(frame, NULL, &dest); // frame802154_packet_extract_addresses
  if(ret == 0 ||
      (!linkaddr_cmp(&dest, &linkaddr_node_addr)
          && !linkaddr_cmp(&dest, &linkaddr_null))) {
    return 0;
  }
  if(eack_ies != NULL) {
    memset(eack_ies, 0, sizeof(struct eack_ts_ie));
  }
#if FRAME802154_VERSION == FRAME802154_IEEE802154E_2012
  if(frame->fcf.ie_list_present) {
#endif
    int mic_len = 0;
#if LLSEC802154_SECURITY_LEVEL
    /* Check if there is space for the security MIC (if any) */
    mic_len = tsch_security_mic_len(frame);
    if(buf_size < curr_len + mic_len) {
      return 0;
    }
#endif
    /* Parse information elements. We need to substract the MIC length, as the exact payload len is needed while parsing */
    if((ret = decode_eack_ies(buf + curr_len, buf_size - curr_len - mic_len, eack_ies)) == -1) {
      return 0;
    }
    curr_len += ret;
#if FRAME802154_VERSION == FRAME802154_IEEE802154E_2012
  }
#endif
  if(hdr_len != NULL) {
    *hdr_len += eack_ies->ie_payload_ie_offset;
  }

  return curr_len;
}

/*-----------------------------------------------------------------------
 * Construct enhanced ACK packet and return ACK length 
 */
int
tsch_packet_create_eack(uint8_t *buf, int buf_size,
    linkaddr_t *dest_addr, uint8_t seqno, int16_t drift, int nack)
{ 
  int ret;
  uint8_t curr_len = 0;
  frame802154_t p;
  struct eack_ts_ie eack_ies;
  
  memset(&p, 0, sizeof(p));
  p.fcf.frame_type = FRAME802154_ACKFRAME;
  p.fcf.frame_version = FRAME802154_VERSION; //FRAME802154_IEEE802154E_2012;
#if FRAME802154_VERSION == FRAME802154_IEEE802154E_2012
  p.fcf.ie_list_present = 1;
#endif
  /* Compression unset. According to IEEE802.15.4e-2012:
   * - if no address is present: elide PAN ID
   * - if at least one address is present: include exactly one PAN ID (dest by default) */
  p.fcf.panid_compression = 0;
  p.dest_pid = IEEE802154_PANID;
  p.seq = seqno;
#if FOURE_PACKET_DEST_ADDR_IN_ACK
  if(dest_addr != NULL) {
    p.fcf.dest_addr_mode = FRAME802154_LONGADDRMODE;
    linkaddr_copy((linkaddr_t*)&p.dest_addr, dest_addr);
  }
#endif
#if FOURE_PACKET_SRC_ADDR_IN_ACK
  p.fcf.src_addr_mode = FRAME802154_LONGADDRMODE;
  p.src_pid = IEEE802154_PANID;
  linkaddr_copy((linkaddr_t*)&p.src_addr, &linkaddr_node_addr);
#endif
#if LLSEC802154_SECURITY_LEVEL
  if(tsch_is_pan_secured) {
    p.fcf.security_enabled = 1;
    p.aux_hdr.security_control.security_level = TSCH_SECURITY_KEY_SEC_LEVEL_ACK;
    p.aux_hdr.security_control.key_id_mode = 1;
#if FRAME802154_VERSION == FRAME802154_IEEE802154E_2012
    p.aux_hdr.security_control.frame_counter_suppression = 1;
    p.aux_hdr.security_control.frame_counter_size = 1;
#endif
    p.aux_hdr.key_index = TSCH_SECURITY_KEY_INDEX_ACK;
  }
#endif

  if((curr_len = frame802154_create(&p, buf)) == 0) {
    return 0;
  }

  /* Append IE timesync */
  memset(&eack_ies, 0, sizeof(eack_ies));
  eack_ies.ts = drift;
  eack_ies.ie_is_nack = nack;
  eack_ies.ie_hdr.id = HEADER_IE_ACK_NACK_TIME_CORRECTION;
  eack_ies.ie_hdr.len = 2;
  eack_ies.ie_hdr.type = 0;

  if((ret = create_ie_header_ack_nack_time_correction(buf+curr_len, buf_size-curr_len, &eack_ies)) == -1) {
    return -1;
  }
  curr_len += ret;

  return curr_len;
}
