/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A Time Synchronisation mechanism that uses ACK frames and a Keep alive timer.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */
#ifndef FOUREMAC_EACK_H
#define FOUREMAC_EACK_H

#include "net/mac/4emac/4emac-ies.h"

struct eack_ts_ie {
  struct ie_header ie_hdr; //length (b0-b7) = 0x06, Sub-id (b8-b14) = 0x1a, Type(b15) = 0x00 (short)
#if SIX_TISCH_MINIMAL
  int16_t ts;
#else
  int32_t ts;
#endif
  uint8_t ie_is_nack;
  uint8_t ie_payload_ie_offset;
};


#if 0
#define PADDING_LEN 10
struct _enhanced_ack {
  uint8_t type;
  uint8_t dummy;
  uint8_t seqno;
  uint8_t authority_level;
  /* We need some padding so that the radio has time to update the
     timestamp at the end of the packet, after the transmission has
     started. */
  uint8_t padding[PADDING_LEN];
  /* The timestamp must be the last two bytes. */
  struct eack_ts_ie eack_timestamp;
};
struct _enhanced_ack msg_eack;
#endif

int tsch_packet_create_eack(uint8_t *buf, int buf_size, linkaddr_t *dest_addr, uint8_t seqno, int16_t drift, int nack);

/**
 * \brief      Enhanced beacon decoder
 * \return     The size of packet length
 *
 *             This function decodes the incoming enhanced ack and stores the timestamp and relevant information in enhanced_ack structure
 *
 */
int8_t decode_eack(uint8_t *buf, int buf_size, uint8_t seqno, frame802154_t *frame, struct  eack_ts_ie * eack_ies , uint8_t *hdr_len);

int8_t decode_eack_ies(uint8_t *buf, int len, struct eack_ts_ie *eack_ies);

/**
 * \brief      This function copies the fields of  _enhance_ack structure to the enhanced beacon message buffer
 * \return     The size of packet length
 *
 */
int8_t insert_eack_content(uint8_t *msg_buffer);

int8_t create_ie_header_ack_nack_time_correction(uint8_t *buf, int len, struct eack_ts_ie *eack_ies);

#endif
