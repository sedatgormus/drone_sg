/*
 * Copyright (c) 2015, Mavi Alp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A simple scheduler.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */
#include "contiki.h"
#include "net/rime/rime.h"
#include "net/rime/rimestats.h"
#include "net/rime/broadcast.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "sys/rtimer.h"
#include "sys/node-id.h"

#if PLATFORM_HAS_LEDS
#include "dev/leds.h"
#endif

#include <stdlib.h>
#include <string.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/4emac.h"
#include "net/mac/4emac/4emac-buf.h"
#include "net/mac/4emac/cooja-debug.h"

#if !SIX_TISCH_MINIMAL

rtimer_clock_t current_slot_start; //accessed by rdc layer
uint8_t time_correction_received; //accessed by rdc layer
uint16_t slot_duration_threshold;

static rtimer_clock_t rtimer_previous;

extern uint8_t time_master;

static uint8_t rtimer_now_first_call;
static struct rtimer stx;
static int8_t slots_until_next_scheduled_slot;

PROCESS(sixtop_process, "TSCH 6top");
PROCESS(sixtop_synch_process, "TSCH 6top resynch");

//const uint8_t default_channel_hopping_pattern[] = {5, 6, 12, 7, 15, 4, 14, 11, 8, 0, 1, 2, 13, 3, 9, 10};
const uint8_t default_channel_hopping_pattern[] = { 15, 11};

/*---------------------------------------------------------------------------
 * Return the current channel depending on the channel offset and ASN
 */
uint8_t 
current_channel(uint8_t channel_offset)
{
  return default_channel_hopping_pattern[(current_asn.ls4b + channel_offset) % NUMBER_OF_CHANNELS] + 11;
}

/*---------------------------------------------------------------------------
 * Return the current slot using the ASN value and slot frame size.
 */
uint8_t 
current_slot()
{
  return (current_asn.ls4b) % slot_frame_size;
}

/*---------------------------------------------------------------------------
 * The callback function for the slots. Function is scheduled recursively for
 * each scheduled slot. If a number of slots are not scheduled, the function
 * skips over them to let the cpu sleep longer durations to save energy.
 * The function also checks for any missed slot deadlines due to glitches, 
 * and corrects the ASN counters if there is any. The next slot start time is
 * calcualted using the time offset based time value received by the time master.  
 * The radio is turned on for each and every receive and shared slots. 
 * The radio is kept off if there is no packet in the 
 * 6Top buffer to be transmitted.
 */
void 
sixtop_slot_callback(struct rtimer *t, void *ptr)
{
  rtimer_clock_t fixed_time;
  struct content *cn;
  struct slot *a_slot;
  struct destination *dn;
  static int8_t ret_val, slot_type;
  static uint16_t time_since;

  // ASN check: checks if the real time timer missed any slots.
  current_slot_start = RTIMER_NOW();
  time_since = abs(current_slot_start - rtimer_previous);
  rtimer_previous = RTIMER_NOW();

  //Turn Radio off first, if not root and not duty cycled
  if(time_master != 1){
    NETSTACK_RDC.off(0);
  }
  
  // if the encountered delay is greater than the expected delay, re-adjust the asn counter
  if(time_since > ((slots_until_next_scheduled_slot - 1) * slot_duration + slot_duration_threshold) && !rtimer_now_first_call){
    uint8_t deadline_missed_by = time_since/slot_duration;
    ASN_INC(current_asn, deadline_missed_by);
    rtimer_now_first_call = 1;
  }else{
    rtimer_now_first_call = 0;
  }

  //ASN COUNTER MANAGEMENT : if there are unscheduled slots, skip over them so that the cpu can sleep longer.
  //You need to make sure that you re-adjust the ASN counter
  if(time_master != 1){//if time master you may want to listen every slot, if required can be changed 
    slots_until_next_scheduled_slot = foure_mac_buf_get_next_scheduled_slot(current_slot(), slot_frame_size);
    if(slots_until_next_scheduled_slot == 0){
      slots_until_next_scheduled_slot = 1;
    }
  }

  if(time_correction_received == FOURE_MAC_TIME_CORRECTION_EACK){
     adjust_offset(eack_ts_container.ie_timestamp, eack_ts_container.eack_receive_time, 0);
     time_correction_received = 0;
  }

  ASN_INC(current_asn, slots_until_next_scheduled_slot);

  fixed_time = tsch_timesynch_time_to_rtimer((uint32_t)(current_asn.ls4b) * slot_duration + slot_duration);

  /* If synchronisation is lost, exit */
  if(!foure_control.synched){
    NETSTACK_RDC.on();
    return; 
  }else{
    if(rtimer_set(t,  fixed_time, 1,  sixtop_slot_callback, ptr) != 0){
      PRINT("4emac-6top: Fatal Error\n");
    }
  }
  
  //INFO : Go over all the slots to find if there is any packet scheduled for the current slot.
  ret_val = foure_mac_buf_get_scheduled_content(&dn, &cn, &a_slot, current_slot());

  if(ret_val == FOURE_SLOT_UNSCHEDULED){
    NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, current_channel(0));
    return;
  }

  slot_type = a_slot->slot_type & 0x0F;

  /* set channel using asn */
  NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, current_channel(a_slot->channel_offset));
  if(slot_type == SLOT_TYPE_TRANSMIT || slot_type == SLOT_TYPE_SHARED){

    if(slot_type == SLOT_TYPE_SHARED && ret_val == FOURE_SLOT_SCHEDULED_FOR_RECEIVE){
       NETSTACK_RDC.on();
       return; /* /if there is no content in the buffer and is a shared slot return. */
    }else if(slot_type == SLOT_TYPE_TRANSMIT && cn == NULL){
       return; /* if this is a transmit slot and there is no content in the content buffer or the packet buffer is locked, 
                * return without turning the radio on. */
    }

    // Do we really need this for drift calculation ?
    if(cn->c_type == ((a_slot->slot_type & 0xF0) >> 4)){
      if(cn->no_of_tx > 0){
        if(cn->last_transmit_ch == current_channel(a_slot->channel_offset)){
          //printf("Same channel %u\n", current_channel(a_slot->channel_offset));
          return;
        }
      }      

       NETSTACK_RDC.on();
       if(cn->c_type == FRAME802154_BEACONFRAME) //Update the EB asn and other time critical parameters prior to the transmission
           memcpy(&cn->msg[6], &current_asn, 5 * sizeof(uint8_t));
        
       if(cn != NULL){
          NETSTACK_LLSEC.send(dn->sent, (void *)dn, (void *)cn);
       }
    } 
  }else if(slot_type == SLOT_TYPE_RECEIVE && ret_val == FOURE_SLOT_SCHEDULED_FOR_RECEIVE){ //Turn on radio to receive transmitted frames
    NETSTACK_RDC.on();
  }

#if PLATFORM_HAS_LEDS
  if(asn[0] % 2 == 0){
    leds_on(LEDS_RED);  
    P23_1();
  }else{
    leds_off(LEDS_RED);
    P23_0();
  }
#endif
}

/*----------------------------------------------------------------------------
 * Synchronize the timesynch with 6top process when an enhanced beacon received
 * or when the current node is a time master.
 */
void 
post_sixtop_synch_event(char *msg)
{
  process_post_synch(&sixtop_process, PROCESS_EVENT_CONTINUE, msg);
}

/*---------------------------------------------------------------------------
 * Start the sixtop slot callback (if not already started) when a 
 * synchronisation event happens. 
 */
PROCESS_THREAD(sixtop_process, ev, data)
{
  PROCESS_BEGIN();
  
  printf("6top Started\n");

  while(1){
    PROCESS_WAIT_EVENT();    
    if(ev == PROCESS_EVENT_CONTINUE){//do this with event detection to start the real time timer.
      if(foure_control.synched){
        if(time_master == 1) slot_frame_size = SLOT_FRAME_SIZE;

        rtimer_now_first_call = 1; //ignore first two callbacks until the callback durations are settled
        rtimer_previous = tsch_timesynch_time_to_rtimer(RTIMER_NOW());
        rtimer_set(&stx, tsch_timesynch_time_to_rtimer(RTIMER_NOW() + slot_duration), 1,  sixtop_slot_callback, NULL);
      }
    }
  }

  PROCESS_END();
}

/*---------------------------------------------------------------------------
 * Restart the time synchronisation process, if something goes wrong.
 */
void 
restart_time_synchronisation()
{
  PRINT("restart\n");
  if(tsch_timesynch_authority_level() == 1) //root node
    foure_timesynch_init(1);
  else
    foure_timesynch_init(0);

  sixtop_init();
}

/*---------------------------------------------------------------------------
 * Check and restart the sixtop process if the ASN values do not make sense.
 * This means a problem with slot callbacks and realtime timers.
 */
PROCESS_THREAD(sixtop_synch_process, ev, data)
{
  static struct etimer et;
  static uint32_t asn_previous;
  uint32_t asn_now;
  static rtimer_clock_t check_period_rtimer_ticks = TSCH_CLOCK_TO_TICKS(CLOCK_SECOND);

  PROCESS_BEGIN();
  /* Delay 1 second */
  etimer_set(&et, CLOCK_SECOND);
  asn_previous = 0;
  while(1){
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    /* Reset the etimer to trig again in 1 second */
    etimer_reset(&et);
   
    asn_now = current_asn.ls4b;
    uint32_t asn_difference_rtimer_ticks = (asn_now - asn_previous) * slot_duration;
    //If the asn counter is lower than expected, there is an rtimer problem, resynchronize
    if((asn_difference_rtimer_ticks < check_period_rtimer_ticks - 2 * slot_frame_size * slot_duration) && asn_previous != 0 && foure_control.synched){ 
      PRINT("4eMAC: Re-synch\n");
      restart_time_synchronisation();
      asn_previous = 0;
    }else
      asn_previous = current_asn.ls4b;
  }
  PROCESS_END();
}

/*---------------------------------------------------------------------------
 * Initialise the sixtop process with the default values.
 */
void 
sixtop_init() 
{
  rtimer_previous = 0;
  rtimer_now_first_call = 0;
  slot_duration = US_TO_RTIMERTICKS(TSCH_DEFAULT_TIMESLOT_LENGTH);
  slot_duration_threshold = slot_duration + (slot_duration / 10); // 10 % longer than slot_duration, dont forget to update this when EB slot duration used
  slots_until_next_scheduled_slot = 1;
  memset(&current_asn, 0, 5 * sizeof(char));
  process_exit(&sixtop_process);
  process_exit(&sixtop_synch_process);
  process_start(&sixtop_process, NULL);
  process_start(&sixtop_synch_process, NULL);
}

#endif /* !SIX_TISCH_MINIMAL */

#endif /* TSCH_TIME_SYNCH */

