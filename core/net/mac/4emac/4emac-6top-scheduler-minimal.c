/*
 * Copyright (c) 2015, Mavi Alp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A simple scheduler.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/rime/rime.h"
#include "net/rime/rimestats.h"
#include "net/rime/broadcast.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "sys/rtimer.h"
#include "sys/node-id.h"
#include "dev/watchdog.h"

#if PLATFORM_HAS_LEDS
#include "dev/leds.h"
#endif

#include <stdlib.h>
#include <string.h>
 
#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/4emac.h"
#include "net/mac/4emac/4emac-buf.h"
#include "net/mac/4emac/cooja-debug.h"

#if SIX_TISCH_MINIMAL
rtimer_clock_t current_slot_start; //accessed by rdc layer.
int32_t adjust_by_eack; //accessed by rdc layer.
uint8_t time_correction_received; //accessed by rdc layer

static rtimer_clock_t prev_slot_start;
static rtimer_clock_t current_link_start;
static rtimer_clock_t fixed_time;

extern struct pt slot_tx_pt;
extern struct pt slot_rx_pt;

struct rtimer stx;
static uint8_t rtimer_created;
static uint8_t slots_until_next_scheduled_slot;
static uint16_t timestamp_valid_threshold;

PROCESS(sixtop_process, "TSCH 6top");
PROCESS(sixtop_synch_process, "TSCH 6top resynch");

static struct pt sixtop_slot_callback_pt;

//const uint8_t default_channel_hopping_pattern[] = {5, 6, 12, 7, 15, 4, 14, 11, 8, 0, 1, 2, 13, 3, 9, 10};
const uint8_t default_channel_hopping_pattern[] = { 15, 4, 10, 14, 0, 11};

/*---------------------------------------------------------------------------
 * Return the current channel depending on the channel offset and ASN
 */
uint8_t 
current_channel(uint8_t channel_offset)
{
  return default_channel_hopping_pattern[(current_asn.ls4b + channel_offset) % NUMBER_OF_CHANNELS] + 11;
}

/*---------------------------------------------------------------------------
 * Return the current slot using the ASN value and slot frame size.
 */
uint8_t 
current_slot()
{
  return (current_asn.ls4b) % slot_frame_size;
}

/*------------------------------------------------------------------------------
 * 
 */
static uint8_t
check_timer_miss(rtimer_clock_t ref_time, rtimer_clock_t offset, rtimer_clock_t now)
{
  rtimer_clock_t target = ref_time + offset;
  uint8_t now_has_overflowed = now < ref_time;
  uint8_t target_has_overflowed = target < ref_time;

  if(now_has_overflowed == target_has_overflowed){
    /* Both or none have overflowed, just compare now to the target */
    return target <= now;
  }else{
    /* Either now or target of overflowed.
     * If it is now, then it has passed the target.
     * If it is target, then we haven't reached it yet.
     */
    return now_has_overflowed;
  }
}

/*------------------------------------------------------------------------------
 * 
 */
uint8_t
tsch_schedule_slot_operation(struct rtimer *tm, rtimer_clock_t ref_time, rtimer_clock_t offset, char (*callback)(struct rtimer *, void *), void *ptr)
{
  rtimer_clock_t now = RTIMER_NOW();
  int r;
  /* Subtract RTIMER_GUARD before checking for deadline miss
   * because we can not schedule rtimer less than RTIMER_GUARD in the future 
   */
  uint8_t missed = check_timer_miss(ref_time, offset - RTIMER_GUARD, now);

  if(missed){
    return 0;
  }
  ref_time += offset;
  r = rtimer_set(tm, ref_time, 1, (void (*)(struct rtimer *, void *))callback, ptr);
  if(r != RTIMER_OK){
    return 0;
  }
  return 1;
}

/*------------------------------------------------------------------------------
 * 
 */
static uint8_t
is_slot_already_failed(uint8_t current_channel, struct content *cn)
{
  uint8_t i, max_failed_channel;
  if(cn->no_of_tx > 1)
    max_failed_channel = FOURE_MAX_FAILED_CHANNEL;
  else
    max_failed_channel = cn->no_of_tx;
  
  for(i = 0; i < max_failed_channel; i++){
    if(current_channel == cn->last_transmit_ch[i])
      return 1;
  }
  return 0;
}

/*---------------------------------------------------------------------------
 * The callback function for the slots. Function is scheduled recursively for
 * each scheduled slot. If a number of slots are not scheduled, the function
 * skips over them to let the cpu sleep longer durations to save energy.
 * The function also checks for any missed slot deadlines due to glitches, 
 * and corrects the ASN counters if there is any. The next slot start time is
 * calcualted using the current time.  The radio is turned on for each and every
 * receive and shared slots. The radio is kept off if there is no packet in the 
 * 6Top buffer to be transmitted.
 */
PT_THREAD(sixtop_slot_callback(struct rtimer *t, void *ptr))
{
  static struct foure_mac_item tx_item;
  static int8_t ret_val, slot_type;
  static int16_t adjust_by;

  PT_BEGIN(&sixtop_slot_callback_pt);
  
  /* If synchronisation is lost, exit */
  if(!foure_control.synched){
    NETSTACK_RDC.on();
  }else{
    //INFO : Go over all the slots to find if there is any packet scheduled for the current slot.
    ret_val = foure_mac_buf_get_scheduled_content(&tx_item.dn, &tx_item.cn, &tx_item.sl, current_slot());

    if(ret_val == FOURE_SLOT_UNSCHEDULED){
      NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, current_channel(0));
    }else{
      uint8_t current_ch = current_channel(tx_item.sl->channel_offset);;
      slot_type = tx_item.sl->slot_type;
      /* set channel using asn */
      NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, current_ch);
      if(ret_val == FOURE_SLOT_SCHEDULED && (slot_type & (SLOT_TYPE_TRANSMIT | SLOT_TYPE_SHARED))){
        if((slot_type & SLOT_TYPE_TRANSMIT) && is_slot_already_failed(current_ch, tx_item.cn)){
          PRINT("Same channel %u sq %u\n", current_ch, tx_item.cn->seqno);
        }else{
          if(tx_item.cn->c_type == FRAME802154_BEACONFRAME){ //Update the EB asn and other time critical parameters prior to the transmission
            memcpy(&tx_item.cn->msg[6], &current_asn, 5 * sizeof(uint8_t));
            set_eb_slot_channel_offset(tx_item.cn->msg, tx_item.sl->slot_id, tx_item.sl->channel_offset);
          }
          if(tx_item.cn != NULL){
            NETSTACK_RDC.on();
            NETSTACK_LLSEC.send(tx_item.dn->sent, (void *)&tx_item);
            if(slot_tx_pt.lc != 0){
              PT_YIELD(&sixtop_slot_callback_pt);
            }else{
              NETSTACK_RDC.off(0);
              PRINT("4emac-6top: slot_tx_pt ended\n");
            }   
          }else
            PRINT("4emac-6top: bug\n");
        }
      }else if(ret_val == FOURE_SLOT_SCHEDULED_FOR_RECEIVE && (slot_type & (SLOT_TYPE_SHARED | SLOT_TYPE_RECEIVE))){
        uint8_t poll_mode = 0;
        radio_value_t radio_rx_mode;

        /* Radio Rx mode */
        NETSTACK_RADIO.get_value(RADIO_PARAM_RX_MODE, &radio_rx_mode);
        poll_mode = (radio_rx_mode & RADIO_RX_MODE_POLL_MODE);
        if(poll_mode){
          NETSTACK_RDC.input();
          if(slot_rx_pt.lc != 0){
            PT_YIELD(&sixtop_slot_callback_pt);
          }else{
            NETSTACK_RDC.off(0);
            PRINT("4emac-6top: slot_rx_pt ended\n");
          }        
        }else{
          NETSTACK_RDC.on();
        }
      }
    }

    if(time_correction_received == FOURE_MAC_TIME_CORRECTION_EACK || time_correction_received == FOURE_MAC_TIME_CORRECTION_EB){
      static int received_drift;
      if(time_correction_received == FOURE_MAC_TIME_CORRECTION_EACK) received_drift = adjust_by_eack; 
      else received_drift = current_link_start - current_slot_start; // I don't like this
#if TRUNCATE_SYNC_IE
      if(received_drift > TRUNCATE_SYNC_IE_BOUND){
        adjust_by += TRUNCATE_SYNC_IE_BOUND;
      } else if(received_drift < -TRUNCATE_SYNC_IE_BOUND){
        adjust_by += -TRUNCATE_SYNC_IE_BOUND;
      } else {
        adjust_by += received_drift;
      }
#else
      if(abs(received_drift) < (TRUNCATE_SYNC_IE_BOUND)){
        adjust_by += received_drift;
      } else adjust_by += received_drift >> 1; 
#endif
      time_correction_received = 0;
      adjust_by_eack = 0;
    }

    do{
      slots_until_next_scheduled_slot =  foure_mac_buf_get_next_scheduled_slot(current_slot(), slot_frame_size);
      ASN_INC(current_asn, slots_until_next_scheduled_slot);
      fixed_time = slots_until_next_scheduled_slot * slot_duration + adjust_by;
      prev_slot_start = current_slot_start;
      current_slot_start += fixed_time;
      adjust_by = 0;
    }while(tsch_schedule_slot_operation(t, prev_slot_start, fixed_time, sixtop_slot_callback, ptr) != 1);

#if PLATFORM_HAS_LEDS
    if(asn[0] % 2 == 0){
      leds_on(LEDS_RED);
      P23_1();
    }else {
      leds_off(LEDS_RED);
      P23_0();
    }
#endif /* PLATFORM_HAS_LEDS */
  }

  PT_END(&sixtop_slot_callback_pt);
}

/*----------------------------------------------------------------------------
 * Synchronize the timesynch with 6top process when an enhanced beacon received
 * or when the current node is a time master.
 */
void
post_sixtop_synch_event(char *msg)
{
  process_post_synch(&sixtop_process, PROCESS_EVENT_CONTINUE, msg);
}

/*---------------------------------------------------------------------------
 * Start the sixtop slot callback (if not already started) when a 
 * synchronisation event happens. If an enahanced beacon with a valid time stamp 
 * is received adjust the local slot callback time.
 */
PROCESS_THREAD(sixtop_process, ev, data)
{
  PROCESS_BEGIN();

  PRINT("4emac-6top: Started\n");

  while(1){
    PROCESS_WAIT_EVENT();
    if(ev == PROCESS_EVENT_CONTINUE){ /* do this with event detection to start the real time timer. */
      if(foure_control.synched){

        timestamp_valid_threshold = slot_duration >> 1;

        if(abs(RTIMER_NOW() - packetbuf_attr(PACKETBUF_ATTR_TIMESTAMP)) < timestamp_valid_threshold){ /* Check if the timestamp is valid. */
          time_correction_received = FOURE_MAC_TIME_CORRECTION_EB;
          current_link_start = packetbuf_attr(PACKETBUF_ATTR_TIMESTAMP) - US_TO_RTIMERTICKS((int32_t)tsch_msg.tsch_slot_template.time_slot_timing.ts_tx_offset);
        } 

        if(!rtimer_created){
          rtimer_created = 1;
          time_correction_received = 0;
          slots_until_next_scheduled_slot = foure_mac_buf_get_next_scheduled_slot(current_slot(), slot_frame_size);
          ASN_INC(current_asn, slots_until_next_scheduled_slot);
          current_link_start += slots_until_next_scheduled_slot * slot_duration;
          current_slot_start = current_link_start;
          rtimer_set(&stx, current_link_start, 1,  (void (*)(struct rtimer *, void *))sixtop_slot_callback, NULL);
        }
      }
    }
  }

  PROCESS_END();
}

/*---------------------------------------------------------------------------
 * Restart the time synchronisation process, if something goes wrong.
 */
void 
restart_time_synchronisation()
{
  PRINT("4emac-6top: Restart!\n");
  if(tsch_timesynch_authority_level() == 1) //root node
    foure_timesynch_init(1);
  else
    foure_timesynch_init(0);

  sixtop_init();
}

/*---------------------------------------------------------------------------
 * Check and restart the sixtop process if the ASN values do not make sense.
 * This means a problem with slot callbacks and realtime timers.
 */
PROCESS_THREAD(sixtop_synch_process, ev, data)
{
  static struct etimer et;
  static uint32_t asn_previous;
  uint32_t asn_now;
  static rtimer_clock_t check_period_rtimer_ticks = TSCH_CLOCK_TO_TICKS(CLOCK_SECOND);

  PROCESS_BEGIN();
  /* Delay 1 second */
  etimer_set(&et, CLOCK_SECOND);
  asn_previous = 0;
  while(1){
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    /* Reset the etimer to trig again in 1 second */
    etimer_reset(&et);

    asn_now = current_asn.ls4b;
    uint32_t asn_difference_rtimer_ticks = (asn_now - asn_previous) * slot_duration;
    /* If the asn counter is lower than expected, there is an rtimer problem, resynchronize */
    if((asn_difference_rtimer_ticks < check_period_rtimer_ticks - 2 * slot_frame_size * slot_duration) && asn_previous != 0 && foure_control.synched){
      PRINT("4emac-6top: Re-synch %lu %u\n", asn_difference_rtimer_ticks, check_period_rtimer_ticks - 2 * slot_frame_size * slot_duration);
      restart_time_synchronisation();
      asn_previous = 0;
    }else
      asn_previous = current_asn.ls4b;
  }
  PROCESS_END();
}

/*---------------------------------------------------------------------------
 * Initialise the sixtop process with the default values.
 */
void
sixtop_init()
{
  rtimer_created = 0;
  prev_slot_start = 0;
  current_slot_start = 0;
  fixed_time = 0;
  slot_duration = US_TO_RTIMERTICKS(TSCH_DEFAULT_TIMESLOT_LENGTH);
  slots_until_next_scheduled_slot = 1;
  current_link_start = RTIMER_NOW() + 5 * slot_duration;
  memset(&current_asn, 0, 5 * sizeof(char));
  process_exit(&sixtop_process);
  process_exit(&sixtop_synch_process);
  process_start(&sixtop_process, NULL);
  process_start(&sixtop_synch_process, NULL);
}

#endif /* SIX_TISCH_MINIMAL */

#endif /* TSCH_TIME_SYNCH */
