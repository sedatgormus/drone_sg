/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A Time Synchronisation mechanism that uses ACK frames and a Keep alive timer.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */
#ifndef TIMESYNCH_H
#define TIMESYNCH_H

#include "net/rpl/rpl.h"

#define CONTROL_CHANNEL       26
#define PARENT_LOST_TRESHOLD  10
#define PADDING_LEN           10

#if !SIX_TISCH_MINIMAL
struct eack_timestamp_container{ //This is used to pass timestamp to the scheduler process
   rtimer_clock_t ie_timestamp;
   rtimer_clock_t eack_receive_time;
};

struct eack_timestamp_container eack_ts_container;
#endif /* !SIX_TISCH_MINIMAL */

rpl_rank_t get_current_instance_rank();

/**
 * \brief      Initialize the timesynch module
 *
 *             This function initializes the timesynch module. This
 *             function must not be called before rime_init().
 *
 */
void foure_timesynch_init(int8_t);

/**
 * \brief      Get the current time-synchronized time
 * \return     The current time-synchronized time
 *
 *             This function returns the current time-synchronized
 *             time.
 *
 */
rtimer_clock_t tsch_timesynch_time(void);

/**
 * \brief      Get the current time-synchronized time, suitable for use with the rtimer module
 * \return     The current time-synchronized rtimer time
 *
 *             This function returns the (local) rtimer-equivalent
 *             time corresponding to the current time-synchronized
 *             (global) time. The rtimer-equivalent time is used for
 *             setting rtimer timers that are synchronized to other
 *             nodes in the network.
 *
 */
rtimer_clock_t tsch_timesynch_time_to_rtimer(rtimer_clock_t synched_time);

/**
 * \brief      Get the synchronized equivalent of an rtimer time
 * \return     The synchronized equivalent of an rtimer time
 *
 *             This function returns the time synchronized equivalent
 *             time corresponding to a (local) rtimer time.
 *
 */
rtimer_clock_t tsch_timesynch_rtimer_to_time(rtimer_clock_t rtimer_time);


/**
 * \brief      Get the current time-synchronized offset from the rtimer clock, which is used mainly for debugging
 * \return     The current time-synchronized offset from the rtimer clock
 *
 *             This function returns the current time-synchronized
 *             offset from the rtimer arch clock. This is mainly
 *             useful for debugging the timesynch module.
 *
 */
rtimer_clock_t tsch_timesynch_offset(void);



/**
 * \brief      Get the current authority level of the time-synchronized time
 * \return     The current authority level of the time-synchronized time
 *
 *             This function returns the current authority level of
 *             the time-synchronized time. A node with a lower
 *             authority level is defined to have a better notion of
 *             time than a node with a higher authority
 *             level. Authority level 0 is best and should be used by
 *             a sink node that has a connection to an outside,
 *             "true", clock source.
 *
 */
int tsch_timesynch_authority_level(void);


/**
 * \brief      Set the authority level of the current time
 * \param level The authority level
 */
void tsch_timesynch_set_authority_level(int level);
 

/**
 * \brief Receive the timing update from the parent authority 
 * \param node
 */
void tsch_eb_input();

/**
 * \brief adjust the offset value between the time master and the slave node.
 * \param master's time, local time and a parameter indicating drift calculation
 *  if the drift calculation parameter is 1, the slave node estimation its drift relative
 *  to its time master. The slave node then compansates the time drift value programatically.
 */
void adjust_offset(rtimer_clock_t, rtimer_clock_t, uint8_t);

#define P23_OUT() P2DIR |= BV(3)
#define P23_IN() P2DIR &= ~BV(3)
#define P23_SEL() P2SEL &= ~BV(3)
#define P23_IS_1  (P2OUT & BV(3))
#define P23_WAIT_FOR_1() do{}while (!P23_IS_1)
#define P23_IS_0  (P2OUT & ~BV(3))
#define P23_WAIT_FOR_0() do{}while (!P23_IS_0)
#define P23_1() P2OUT |= BV(3)
#define P23_0() P2OUT &= ~BV(3)
#endif /* TIMESYNCH_H */
