/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A slot based  MAC layer
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "net/netstack.h"
#include "net/packetbuf.h"
#include "sys/ctimer.h"
#include "sys/clock.h"
#include "dev/watchdog.h"
#include "lib/random.h"
#include "lib/list.h"
#include "lib/memb.h"

#if TSCH_TIME_SYNCH
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/4emac-buf.h"
#include "net/mac/4emac/4emac-asn.h"


#include <string.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#ifndef FOUR_EMAC_SEND_802154_ACK
#ifdef FOURE_CONF_SEND_802154_ACK
#define FOUR_EMAC_SEND_802154_ACK FOURE_CONF_SEND_802154_ACK
#else /* FOURE_CONF_SEND_802154_ACK */
#define FOUR_EMAC_SEND_802154_ACK 0
#endif /* FOURE_CONF_SEND_802154_ACK */
#endif /* FOUR_EMAC_SEND_802154_ACK */

#ifndef FOUR_EMAC_MAX_MAC_TRANSMISSIONS
#ifdef FOUR_EMAC_CONF_MAX_MAC_TRANSMISSIONS
#define FOUR_EMAC_MAX_MAC_TRANSMISSIONS FOUR_EMAC_CONF_MAX_MAC_TRANSMISSIONS
#else
#define FOUR_EMAC_MAX_MAC_TRANSMISSIONS 5
#endif /* FOUR_EMAC_CONF_MAX_MAC_TRANSMISSIONS */
#endif /* FOUR_EMAC_MAX_MAC_TRANSMISSIONS */

#if FOUR_EMAC_MAX_MAC_TRANSMISSIONS < 1
#error FOUR_EMAC_CONF_MAX_MAC_TRANSMISSIONS must be at least 1.
#error Change FOUR_EMAC_CONF_MAX_MAC_TRANSMISSIONS in contiki-conf.h or in your Makefile.
#endif /* FOUR_EMAC_CONF_MAX_MAC_TRANSMISSIONS < 1 */

uint16_t seqno;
static  uint8_t will_retry;
extern  struct asn_t current_asn;
extern  uint16_t slot_frame_size;

static void packet_sent(void *ptr, int status, int num_transmissions);

/*---------------------------------------------------------------------------
 * The callback function to let MAC layer know about the status of the 
 * transmitted frame.
 */
static void 
packet_sent(void *ptr, int status, int num_transmissions)
{
  struct foure_mac_item *callback_item = (struct foure_mac_item *)ptr;
  uint8_t local_no_of_txs = 0;
  linkaddr_t *linkaddr;

  if(callback_item->dn == NULL){
    return;
  }

  linkaddr = (linkaddr_t *)foure_mac_buf_get_destination_lladdr(callback_item->dn);

  switch(status){
  case MAC_TX_OK:
       callback_item->sl->slot_tx_ok++;
       callback_item->dn->transmissions++;
       callback_item->cn->no_of_tx = 1;
       local_no_of_txs = callback_item->cn->no_of_tx;
       will_retry = 0;
       PRINTO("ok\n");
       PRINTO("RSSI: %d c %u s %u\n", packetbuf_attr(PACKETBUF_ATTR_RSSI) \
                                    , default_channel_hopping_pattern[(current_asn.ls4b + cs->channel_offset) % NUMBER_OF_CHANNELS] + 11, cn->seqno);
       foure_mac_buf_free_content(callback_item->dn, callback_item->cn);
    break;
  case MAC_TX_NOACK:
       callback_item->sl->slot_tx_noack++;
       callback_item->dn->transmissions++;
       foure_mac_buf_backoff_inc(callback_item->dn); //Increment destination's backoff exponent
       if(callback_item->sl->slot_type & SLOT_TYPE_TRANSMIT)
        callback_item->cn->last_transmit_ch[callback_item->cn->no_of_tx % FOURE_MAX_FAILED_CHANNEL] = default_channel_hopping_pattern[(current_asn.ls4b + callback_item->sl->channel_offset) % NUMBER_OF_CHANNELS] + 11;

       callback_item->cn->no_of_tx++;
       will_retry = 1;
       if(callback_item->cn->no_of_tx >= FOUR_EMAC_MAX_MAC_TRANSMISSIONS){ 
        callback_item->cn->no_of_tx = 1;
        will_retry = 0;
        callback_item->dn->packet_drop++;
        PRINT("drop %u dn %u cn %2x\n", callback_item->dn->packet_drop, linkaddr->u8[7], callback_item->cn->c_type); 
        foure_mac_buf_free_content(callback_item->dn, callback_item->cn); 
       }else
        PRINT("noack\n");
       local_no_of_txs = callback_item->cn->no_of_tx;
    break;
  case MAC_TX_COLLISION:
       callback_item->sl->slot_tx_collisions++;
       callback_item->dn->transmissions++;
       will_retry = 0;
       local_no_of_txs = 1;
       PRINT("col\n");
    break;
  case MAC_TX_DEFERRED:
       callback_item->sl->slot_tx_deferred++;
       callback_item->dn->transmissions++;
       will_retry = 0;
       local_no_of_txs = 1;
    break;
  }
  //TODO:Update destination and slot statistics
  if(!will_retry){
    mac_call_sent_callback(callback_item->dn->sent, callback_item->dn, status, local_no_of_txs);
  }
}

/*---------------------------------------------------------------------------
 * Update relevant fields in the packet buffer prior to transmission
 * If the frame is a FRAME802154_IEEE802154E_2012 frame, set IE list preset 
 * field to 1
 */
static void
send_packet(mac_callback_t sent, void *ptr)
{
  static struct foure_mac_packet tx_packet;
  struct foure_mac_item *tx_item = (struct foure_mac_item *)ptr;
  uint8_t ret = MAC_TX_DEFERRED;
  linkaddr_t *linkaddr;

#if ENHANCED_PACKETBUF_ENABLE
  packetbuf_t *cur_packetbuf = packetbuf_get_current(); /* backup current Packetbuf pointer */
  if(cur_packetbuf == NULL){
    PRINT("4emac: current packetbuf not found!\n");
    ret = MAC_TX_ERR_FATAL;
  }else{
    packetbuf_t *p = packetbuf_alloc();
    if(p != NULL){
      packetbuf_select(p);
#endif /* ENHANCED_PACKETBUF_ENABLE */
      linkaddr = (linkaddr_t *)foure_mac_buf_get_destination_lladdr(tx_item->dn);

      packetbuf_copyfrom(tx_item->cn->msg, tx_item->cn->size);

      packetbuf_set_addr(PACKETBUF_ADDR_RECEIVER, linkaddr);

      packetbuf_set_addr(PACKETBUF_ADDR_SENDER, &linkaddr_node_addr);

      packetbuf_set_attr(PACKETBUF_ATTR_MAC_SEQNO, tx_item->cn->seqno);

      packetbuf_set_attr(PACKETBUF_ATTR_FRAME_TYPE, tx_item->cn->c_type);

#if FOUR_EMAC_SEND_802154_ACK
      packetbuf_set_attr(PACKETBUF_ATTR_MAC_ACK, 1);
#endif /* FOUR_EMAC_SEND_802154_ACK */

    /* set to PACKETBUF_ATTR_IE_LIST_PRESENT to 1 for beacon frames */
#if FRAME802154_VERSION == FRAME802154_IEEE802154E_2012
      if(tx_item->cn->c_type == FRAME802154_BEACONFRAME){
        packetbuf_set_attr(PACKETBUF_ATTR_IE_LIST_PRESENT, 1);
      }
#endif

#if PACKETBUF_LOCK_ENABLED
      if(packetbuf_attr(PACKETBUF_ATTR_LOCK) == PACKETBUF_LOCK)
        PRINT("4emac: packetbuf %p lock!\n", p); 
      packetbuf_set_attr(PACKETBUF_ATTR_LOCK, PACKETBUF_LOCK);
#endif /* PACKETBUF_LOCK_ENABLED */

      watchdog_periodic();

      if(NETSTACK_FRAMER.create() < 0){
        /* Failed to allocate space for headers */
        PRINT("4emac: send failed, too large header\n");
        ret = MAC_TX_ERR_FATAL;
#if ENHANCED_PACKETBUF_ENABLE
        packetbuf_select(cur_packetbuf);
        packetbuf_free(p);
#endif /* ENHANCED_PACKETBUF_ENABLE */
      }else{
        tx_packet.sent = packet_sent;
        tx_packet.ptr = (void *)tx_item;
#if ENHANCED_PACKETBUF_ENABLE
        tx_packet.packetbuf = p;
        packetbuf_select(cur_packetbuf);
#endif /* ENHANCED_PACKETBUF_ENABLE */
        NETSTACK_RDC.send(packet_sent, (void *)&tx_packet);
      }
#if ENHANCED_PACKETBUF_ENABLE
    }else{
      PRINT("4emac: packetbuf alloc failed!\n");
      ret = MAC_TX_ERR;
    }
  }
#endif /* ENHANCED_PACKETBUF_ENABLE */
  if(ret != MAC_TX_DEFERRED)
    mac_call_sent_callback(packet_sent, (void *)tx_item, ret, 1);
}

/*---------------------------------------------------------------------------
 * Pass packet to network layer.
 */
static void
input_packet(void)
{
  PRINTO("4emac: Input\n");
  foure_mac_buf_inc_destination_slot_rx((linkaddr_t *)packetbuf_addr(PACKETBUF_ADDR_SENDER), current_slot());
  NETSTACK_LLSEC.input();
}

/*---------------------------------------------------------------------------*/
static int
on(void)
{
  return NETSTACK_RDC.on();
}

/*---------------------------------------------------------------------------*/
static int
off(int keep_radio_on)
{
  return NETSTACK_RDC.off(keep_radio_on);
}

/*---------------------------------------------------------------------------*/
static unsigned short
channel_check_interval(void)
{
  if(NETSTACK_RDC.channel_check_interval) {
    return NETSTACK_RDC.channel_check_interval();
  }
  return 0;
}

/*---------------------------------------------------------------------------*/
static void
init(void)
{
  seqno = random_rand();
}

const char *mac_ret_str[] = {
  "TX_OK",
  "TX_COLLISION",
  "TX_NOACK",
  "TX_DEFERRED",
  "TX_ERR",
  "TX_ERR_FATAL",
};

/*---------------------------------------------------------------------------*/
const struct mac_driver foure_mac_driver = {
  "4eMac",
  init,
  send_packet,
  input_packet,
  on,
  off,
  channel_check_interval,
};
/*---------------------------------------------------------------------------*/
#endif /*TSCH_TIME_SYNCH*/
