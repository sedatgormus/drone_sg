/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 */

/**
 * \file
 *         A Time Synchronisation mechanism that uses ACK frames and a Keep alive timer.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/packetbuf.h"
#include "net/netstack.h"
#include "net/rime/rime.h"
#include "net/rime/rimestats.h"
#include "net/rime/broadcast.h"
#include "sys/rtimer.h"
#include "lib/random.h"
#include "dev/watchdog.h"
#include "sys/node-id.h"

#include <stdlib.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#if TSCH_TIME_SYNCH
#include "net/rpl/rpl-private.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/4emac-buf.h"
#include "net/mac/4emac/4emac-eb.h"
#include "net/mac/4emac/cooja-debug.h"

PROCESS(foure_timesynch_process, "TSCH Synch");
PROCESS(foure_timesynch_hopping_process, "Ch. Hop");

#if FOUREMAC_COMPANSATE_DRIFT 
PROCESS(foure_timesynch_compansate_drift, "Comp. Drift");
#endif

static uint8_t      msg_buffer[FOURE_MAC_MAX_PACKET_LEN];
static uint8_t      authority_level;
static int          check_counter; // to check how many EBs received within a given period.
static int          eb_counter; //the number of EBs received during a given period
linkaddr_t         *timesynch_destination = (linkaddr_t *)&linkaddr_null;

#if !SIX_TISCH_MINIMAL
#if FOURE_MAC_COMPANSATE_DRIFT
static int                avg_drift, avg_drift_;
static int                high_p_steps, high_p_steps_remainder, high_p_steps_remainder_remainder, offset_counter;
#endif /* FOURE_MAC_COMPANSATE_DRIFT */
static rtimer_clock_t     clock_offset, clock_offset_, received_clock_offset;
static uint8_t            confidence;
#endif /* !SIX_TISCH_MINIMAL */

uint8_t                   time_master;
extern uint16_t           slot_frame_size;

/*------------------------------------------------------------------------------*/
rpl_rank_t 
get_current_instance_rank()
{
  if (default_instance != NULL){
    if (!rpl_dag_root_is_root())
       return default_instance->current_dag->preferred_parent->rank;
    else
       return ROOT_RANK(default_instance);
  }else
    return 0;
}

#if !SIX_TISCH_MINIMAL
/*------------------------------------------------------------------------------
 * When the minimal draft synchronisation approach is not used, this routine 
 * adjusts the local time based on the received clock offset from the time master
 * (default parent). 
 */
void
adjust_offset(rtimer_clock_t authoritative_time, rtimer_clock_t local_time, uint8_t calc_drift)
{ 
  static int16_t offset_difference;

  received_clock_offset = authoritative_time - local_time;
  if(clock_offset == 0) offset_difference = 0;
  else offset_difference = received_clock_offset - clock_offset; //compansate this value if there is a delay, we need to keep the time between the updates ??

  if ((abs(offset_difference) < CLOCK_OFFSET_DRIFT_THRESHOLD) || clock_offset == 0 || confidence == 0){
    clock_offset_ = clock_offset = received_clock_offset;  
#if FOURE_MAC_COMPANSATE_DRIFT
    if (calc_drift) {
      offset_difference = (offset_difference * MAX_INTERVAL_MUL) / (offset_counter * MIN_INTERVAL_MUL);

      if (avg_drift == 0 ) avg_drift = avg_drift_ = offset_difference; //start condition, change, not correct, what if avg_drift is really 0
      else if (offset_difference != 0) avg_drift_ = (avg_drift * 60 + offset_difference * 40) / 100;

      if(avg_drift > 0 && offset_difference > avg_drift)  avg_drift = avg_drift_ + 1;
      else if (avg_drift < 0 && offset_difference < avg_drift)  avg_drift = avg_drift_ - 1;
      else avg_drift = avg_drift_;
    }
    //reset counters and stuff
    high_p_steps = 0;
    high_p_steps_remainder = 0;
    offset_counter = 0;
#endif /* FOURE_MAC_COMPANSATE_DRIFT */
    confidence = 3;
    return;
  } else confidence --;
}

#if FOURE_MAC_COMPANSATE_DRIFT
/*------------------------------------------------------------------------------
 * If the relative drifts of the time master - time slave is known, then the 
 * drift can be compansated using this routine which will enable longer sleeps.
 */
void 
compansate_drift()
{
  int16_t step;
   
  step = (100 * MIN_INTERVAL_MUL * avg_drift) / MAX_INTERVAL_MUL;

  high_p_steps += step;

  if(abs(high_p_steps) >= 100) {

    clock_offset_ += (high_p_steps / 100);
    high_p_steps_remainder += high_p_steps % 100;
        
    if (abs(high_p_steps_remainder) >= 100){

      clock_offset_ += (high_p_steps_remainder / 100);
      high_p_steps_remainder_remainder += high_p_steps_remainder % 100;
          
      if (abs(high_p_steps_remainder_remainder) >= 100) {

        clock_offset_ += (high_p_steps_remainder_remainder / 100);
        high_p_steps_remainder_remainder = 0;
      }
      high_p_steps_remainder = 0;
         
    }
    high_p_steps = 0;
  }

  if((offset_counter % (MAX_INTERVAL_MUL/MIN_INTERVAL_MUL) == 0 ) && ((abs(high_p_steps) + abs(high_p_steps_remainder) + abs(high_p_steps_remainder_remainder))  >= 100))
  { 
    clock_offset_ += (high_p_steps + high_p_steps_remainder + high_p_steps_remainder_remainder) / 100;
    high_p_steps_remainder = 0;
    high_p_steps_remainder_remainder = 0;
    high_p_steps = 0;
  }
  offset_counter ++;
}
#endif /* FOURE_MAC_COMPANSATE_DRIFT */

/*------------------------------------------------------------------------------
 * Return the synched real time.
 */
rtimer_clock_t
tsch_timesynch_time(void)
{
  return RTIMER_NOW() + clock_offset_;
}

/*------------------------------------------------------------------------------
 * Return rtimer value from the synched time.
 */
rtimer_clock_t
tsch_timesynch_time_to_rtimer(rtimer_clock_t synched_time)
{
  return synched_time - clock_offset_;
}

/*------------------------------------------------------------------------------
 * Return the synched time in the future
 */
rtimer_clock_t
tsch_timesynch_rtimer_to_time(rtimer_clock_t rtimer_time)
{
  return rtimer_time + clock_offset_;
}

/*------------------------------------------------------------------------------
 * Return the offset of the node in relation to its time source parent
 */
rtimer_clock_t
tsch_timesynch_offset(void)
{
  return clock_offset_;
}
#endif /* !SIX_TISCH_MINIMAL */

/*------------------------------------------------------------------------------
 * Return the authority level of the node
 */
int
tsch_timesynch_authority_level(void)
{
  #if(1)
    return authority_level;
  #else
    return DAG_RANK(get_current_instance_rank(), default_instance);
  #endif
}

/*------------------------------------------------------------------------------
 * Set the authority level of the node. The authorithy level is obtained from
 * the rank of the node.
 */
void
tsch_timesynch_set_authority_level(int level)
{
  int old_level = tsch_timesynch_authority_level();

  authority_level = level;

  if(old_level != authority_level && authority_level != 0) {
    /* Restart the timesynch process to restart with a low
       transmission interval. */
    process_exit(&foure_timesynch_process);
    process_start(&foure_timesynch_process, NULL);
  }
}

/*------------------------------------------------------------------------------
 * Receive and process the enhanced beacon. The beacon is used for time synchronisation
 * in non-minimal setting. In the minimal draft implementation, beacon receive time is 
 * used to estimate the start of the slot.
 */
void 
tsch_eb_input()
{
  decode_enhanced_beacon(packetbuf_dataptr(), packetbuf_datalen(), &tsch_msg);

  slot_frame_size = tsch_msg.frame_link_information.slot_frame_size;
  //TODO: Are you going to synch to the first EB frame?
  //      Or are you going to wait for a while prior to synchronising?
  if(!foure_control.synched || (default_instance->current_dag->preferred_parent != NULL && 
    (default_instance->current_dag->preferred_parent == rpl_get_parent((uip_lladdr_t *)packetbuf_addr(PACKETBUF_ADDR_SENDER)))))
  { 
#if !SIX_TISCH_MINIMAL
    if(!foure_control.synched){
      foure_control.synched = 1;
#if FRAME802154_SECURITY_LEVEL_AES_192
      if(tsch_msg.has_security == 0)
#endif
        foure_control.secured = 1;
      post_sixtop_synch_event("S");
    }
#if MAHMOTE || OPENMOTE //32 bit time offset
    adjust_offset(tsch_msg.timestamp << 16 | tsch_msg.authority_offset, packetbuf_attr(PACKETBUF_ATTR_TIMESTAMP), 1);
    PRINTO("ts recv %x\n", tsch_msg.timestamp << 16 | tsch_msg.authority_offset);
#else
    adjust_offset(tsch_msg.timestamp, packetbuf_attr(PACKETBUF_ATTR_TIMESTAMP), 1);
#endif
#else /* SIX_TISCH_MINIMAL */
    foure_control.synched = 1;
#if FRAME802154_SECURITY_LEVEL_AES_192
    if(tsch_msg.has_security == 0)
#endif
      foure_control.secured = 1;
    post_sixtop_synch_event("S");
#endif /* !SIX_TISCH_MINIMAL */
    if (default_instance != NULL) {
      if (tsch_msg.synch_information.join_priority == DAG_RANK(INFINITE_RANK, default_instance)) {
        restart_time_synchronisation();
      }
    }
    eb_counter++;

    //if not part of RPL, send a dis immediately.
    if (rpl_get_any_dag() == NULL && foure_control.synched && foure_control.secured){ 
      dis_output(NULL);
    }else if (default_instance != NULL){
      tsch_timesynch_set_authority_level(DAG_RANK(get_current_instance_rank(), default_instance) + 1);
    }

    radio_value_t radio_rx_mode;
    if(foure_control.synched){
      /* Radio Rx mode */
      NETSTACK_RADIO.get_value(RADIO_PARAM_RX_MODE, &radio_rx_mode);
      /* Set radio in poll mode */
      NETSTACK_RADIO.set_value(RADIO_PARAM_RX_MODE, radio_rx_mode | RADIO_RX_MODE_POLL_MODE);
    }
  }
}

/*------------------------------------------------------------------------------
 * Send enhanced beacon periodically (currently 1 beacon every 10 seconds).
 */
void 
tsch_send_eb()
{
  uint8_t eb_msg_len;

  set_eb_content(&tsch_msg);

  /*  add other parameter to msg_eb   */
  tsch_msg.synch_information.join_priority = DAG_RANK(get_current_instance_rank(), default_instance);
#if !SIX_TISCH_MINIMAL
  tsch_msg.authority_offset = clock_offset;
#endif
  eb_msg_len = insert_eb_content(msg_buffer, &tsch_msg); 
  //the root is assumed to be on continiously
  foure_mac_buf_content_insert_data(FRAME802154_BEACONFRAME, (uint8_t *)msg_buffer, eb_msg_len * sizeof(uint8_t), timesynch_destination, HIGH_PRIORITY, NULL);
}

/*-------------------------------------------------------------------------------------------
 * Insert enhanced beacon to the 6TOP buffer for transmission, Check the node parent 
 * periodically to see if the node is still synched. If you have lost synchronisation, resynch.
 */
PROCESS_THREAD(foure_timesynch_process, ev, data)
{
  static struct etimer intervaltimer;
  uint32_t delay;
  
  PROCESS_BEGIN();

  PRINTO("Update Started\n");
  etimer_set(&intervaltimer, CLOCK_SECOND / 10);
  while(!foure_control.synched) {
    PROCESS_WAIT_UNTIL(etimer_expired(&intervaltimer));
    etimer_reset(&intervaltimer);
  }

  if(!time_master){
    etimer_set(&intervaltimer, random_rand() % MAX_BEACON_INTERVAL);
    PROCESS_WAIT_UNTIL(etimer_expired(&intervaltimer));
  }

  while(1){
    if(foure_control.synched && foure_control.secured){   
      check_counter ++;
      tsch_send_eb();
    }
    
    //If the parent is not heard for PARENT_LOST_TRESHOLD EB intervals than try to resynchronise
    if(check_counter >= PARENT_LOST_TRESHOLD) {
      if(ack_counter == 0 && eb_counter == 0 && !time_master) {
        rpl_free_instance(default_instance);
        restart_time_synchronisation();
      } 
      check_counter = 0;
      eb_counter = 0;
      ack_counter = 0;
    }

    delay = (MAX_BEACON_INTERVAL - MAX_BEACON_INTERVAL / 4) + random_rand() % (MAX_BEACON_INTERVAL / 4);
    etimer_set(&intervaltimer, delay);
    PROCESS_WAIT_UNTIL(etimer_expired(&intervaltimer));
  }

  PROCESS_END();
}
/*------------------------------------------------------------------------------
 * Re-adjust the local clock based on the estimated drift periodicall.
 */
#if FOUREMAC_COMPANSATE_DRIFT 
PROCESS_THREAD(foure_timesynch_compansate_drift, ev, data){
  static struct etimer intervaltimer;

  PROCESS_BEGIN();

  etimer_set(&intervaltimer, CLOCK_SECOND);

  while(1) {
    PROCESS_YIELD();
    if(etimer_expired(&intervaltimer) && foure_control.synched){
      etimer_reset(&intervaltimer);
      compansate_drift(); 
    } 
  }
  PROCESS_END();
}
#endif

/*------------------------------------------------------------------------------
 * Channel Hopping Process hops between the channels to receive the beacon frame 
 * and synchronise with the network.
 */
PROCESS_THREAD(foure_timesynch_hopping_process, ev, data)
{
  static struct etimer intervaltimer;
  static uint8_t hopping_counter = 0;

  PROCESS_BEGIN();

  PRINTO("4eMAC : Hopping Process Started\n");

  etimer_set(&intervaltimer, CLOCK_SECOND);
  while(1) {
    PROCESS_YIELD();
    if(etimer_expired(&intervaltimer) && !foure_control.synched){     
      etimer_reset(&intervaltimer);
      NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, default_channel_hopping_pattern[hopping_counter++ % NUMBER_OF_CHANNELS] + 11);      
    } 
  }
  PROCESS_END();
}

/*------------------------------------------------------------------------------
 * Initialisation of time synchronisation process. The beacons are scheduled to
 * be sent in the first slot of the slot frame. 
 */
void
foure_timesynch_init(int8_t synched_to_master)
{

  check_counter = 0;
  eb_counter = 0;
  ack_counter = 0;
  foure_control.synched = time_master = synched_to_master;
  foure_control.secured = synched_to_master;

  PRINT("4eMAC : Timesynch init\n");

  foure_mac_buf_init();

  radio_value_t radio_rx_mode;
  /* Radio Rx mode */
  if(NETSTACK_RADIO.get_value(RADIO_PARAM_RX_MODE, &radio_rx_mode) != RADIO_RESULT_OK) {
    PRINT("4eMAC : radio does not support getting RADIO_PARAM_RX_MODE. Abort init.\n");
    return;
  }
  /* Disable radio in poll mode */
  radio_rx_mode &= ~RADIO_RX_MODE_POLL_MODE;
  if(NETSTACK_RADIO.set_value(RADIO_PARAM_RX_MODE, radio_rx_mode) != RADIO_RESULT_OK) {
    PRINT("4eMAC : radio does not support setting required RADIO_PARAM_RX_MODE. Abort init.\n");
    return;
  }

  if(synched_to_master) {
    /* Set radio in poll mode */
    radio_rx_mode |= RADIO_RX_MODE_POLL_MODE;
    NETSTACK_RADIO.set_value(RADIO_PARAM_RX_MODE, radio_rx_mode);

    slot_frame_size = SLOT_FRAME_SIZE;
    fouremac_buf_reset_timeslot_timing(&time_slot_timing);
    tsch_timesynch_set_authority_level(synched_to_master);
    post_sixtop_synch_event("S");
  }

#if !SIX_TISCH_MINIMAL
  clock_offset = 0;
  clock_offset_ = 0;
  confidence = 0;
#if FOUREMAC_COMPANSATE_DRIFT
  offset_counter = 0;
  avg_drift = 0;
  avg_drift_ = 0;
  high_p_steps = 0;
  high_p_steps_remainder = 0;
  high_p_steps_remainder_remainder = 0;
#endif /* FOUREMAC_COMPANSATE_DRIFT */
#endif /* !SIX_TISCH_MINIMAL */

#if PIN_DEBUF  
  P23_OUT();
  P23_SEL();
#endif

  NETSTACK_RADIO.on(); //If the process already exist, kill it and restart it
  NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, CONTROL_CHANNEL);
  process_exit(&foure_timesynch_process);
  process_start(&foure_timesynch_process, NULL);
  process_exit(&foure_timesynch_hopping_process);
  process_start(&foure_timesynch_hopping_process, NULL);
#if FOUREMAC_COMPANSATE_DRIFT 
  process_exit(&foure_timesynch_compansate_drift);
  process_start(&foure_timesynch_compansate_drift, NULL);
#endif
#if TSCH_CENTRAL_SCHEDULER_ENABLED
  if(synched_to_master){
    cs_server_init();
  }else{
    cs_client_init();
  }
#endif
}

#endif /* TSCH_TIME_SYNCH */

