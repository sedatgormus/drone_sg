/*
 * Copyright (c) 2014, Mavialp Research Limited
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * Author: Sedat Gormus <sedatgormus@gmail.com>
 *
 */

#include "contiki.h"
#include "sys/node-id.h"
#include "net/packetbuf.h"
#include "lib/random.h"

#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-buf.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/cooja-debug.h"

#include <stdio.h>
#include <string.h>
#include <stddef.h>

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_PRINT
#endif
#include "net/mac/4emac/4emac-debug.h"

#define CN_RATE_SCALE           100
#define CN_RATE_ALPHA           70
#define RATE_REFRESH_THRESHOLD  100

#define DEST_HEAD()       list_head(destination_list)
#define DEST_TAIL()       list_tail(destination_list)
#define DEST_COUNT()      list_length(destination_list)
#define DEST_NEXT(dn)     dn = list_item_next(dn)

#define SLOT_HEAD(dn)     list_head(dn->slot_list)
#define SLOT_TAIL(dn)     list_tail(dn->slot_list)
#define SLOT_COUNT(dn)    list_length(dn->slot_list)
#define SLOT_NEXT(sl)     sl = list_item_next(sl)

#define CONTENT_HEAD(dn)  list_head(dn->content_list)
#define CONTENT_TAIL(dn)  list_tail(dn->content_list)
#define CONTENT_COUNT(dn) list_length(dn->content_list)
#define CONTENT_NEXT(cn)  cn = list_item_next(cn)

#if FOURE_MAC_CLEAR_UNUSED_DESTINATIONS
#define DESTINATION_CHECK_INTERVAL (120 * CLOCK_SECOND)
PROCESS(foure_buf_clear_unused_destinations, "Clear Destinations");
#endif

#if TSCH_TIME_SYNCH
MEMB(slot_mem, slot_t, NBR_TABLE_MAX_NEIGHBORS * FOURE_MAX_SHARED_SLOT + SLOT_FRAME_SIZE - FOURE_MAX_SHARED_SLOT);
MEMB(content_mem, content_t, FOURE_MAX_CONTENT + 1);
LIST(destination_list);
NBR_TABLE_GLOBAL(destination_t, destinations);

extern uint16_t seqno;

static uint16_t inserted_content_num;

/*------------------------------------------------------------------------------
 * Slotted Aloha Backoff
 */
static uint8_t
foure_mac_buf_backoff_expired(destination_t *dn)
{
  if(dn != NULL)
    return dn->backoff_window == 0;

  return 0;
}

/*------------------------------------------------------------------------------
 * Reset destination backoff
 */
static void
foure_mac_buf_backoff_reset(destination_t *dn)
{
  if(dn != NULL){
    dn->backoff_window = 0;
    dn->backoff_exponent = FOURE_MAC_MIN_BE;
  }
}

/*-------------------------------------------------------------------------------
 * Increment backoff exponent, pick a new window
 */
void
foure_mac_buf_backoff_inc(destination_t *dn)
{
  if(dn != NULL){
    /* Increment exponent */
    dn->backoff_exponent = MIN(dn->backoff_exponent + FOURE_MAC_BE_INC, FOURE_MAC_MAX_BE);
    /* Pick a window (number of sharered slots to skip) */
    dn->backoff_window = ((uint32_t)random_rand() * 941027 * uip_lladdr.addr[7]) % (1 << dn->backoff_exponent);
    /* Add one to the window as we will decrement it at the end of the current slot
     * through tsch_queue_update_all_backoff_windows */
    dn->backoff_window++;
  }
}

/*---------------------------------------------------------------------------
 * Decrement backoff window for all destination.
 */
static void
foure_mac_buf_update_all_backoff_windows()
{
  destination_t *dn;
  
  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    if(dn->backoff_window != 0){
      dn->backoff_window--;
    }
  }
}

static void scale_all_rate_of_destinations(destination_t *ignore_dest, uint8_t numerator, uint8_t denominator);
static void share_out_rate_to_all_destination(uint8_t rate);

/*---------------------------------------------------------------------------
 * Get the pointer of the destination for the neighbour with link address 'linkaddr'
 *
 * RETURN : NULL, if node existing destination found, 'destination' otherwise
 */
static destination_t *
foure_mac_buf_get_destination(linkaddr_t *linkaddr)
{
  destination_t *dn = nbr_table_get_from_lladdr(destinations, linkaddr);
  return dn;
}

/*---------------------------------------------------------------------------
 *
 */
uip_ipaddr_t *
foure_mac_buf_get_destination_ipaddr(destination_t *dn)
{
  linkaddr_t *linkaddr = nbr_table_get_lladdr(destinations, dn);
  return uip_ds6_nbr_ipaddr_from_lladdr((uip_lladdr_t *)linkaddr);
}

/*---------------------------------------------------------------------------
 *
 */
uip_lladdr_t *
foure_mac_buf_get_destination_lladdr(destination_t *dn)
{
  return (uip_lladdr_t *)nbr_table_get_lladdr(destinations, dn);
}

/*---------------------------------------------------------------------------
 *
 */
static uint8_t
foure_mac_buf_destination_num(void)
{
  destination_t *dn;
  uint8_t num = 0;

  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    num++;
  }
  return num;
}

/*---------------------------------------------------------------------------
 *
 */
static void
foure_mac_buf_set_destination_slots(linkaddr_t *linkaddr, uint8_t sl_type, uint8_t cn_type, uint8_t ch_offset, uint8_t sl_num)
{
  uint8_t slot_offsets[sl_num], channel_offsets[sl_num];
  uint8_t sl;

  for(sl = 0; sl < sl_num; sl++){
    slot_offsets[sl] = sl;
    channel_offsets[sl] = ch_offset;
  }

  foure_mac_buf_schedule_slots(linkaddr, sl_type, cn_type, slot_offsets, channel_offsets, sl_num);
}

/*-------------------------------------------------------------------------------------------------------
 * Add a destination to destination memory block prior to transmitting to it. When destination is created,
 * the content list and slot list for the destination should also be initialised.
 *
 * The slotted ALOHA like backoff windows shall be reset here.
 *
 * RETURN : destination *  if successful, NULL otherwise
 *
 * The default shared slots for the destination shall be scheduled here to allow DIO/DAO transmissions.
 */
static destination_t *
foure_mac_buf_add_destination(linkaddr_t *linkaddr, mac_callback_t sent)
{
  destination_t *dn = NULL;
  uip_ds6_nbr_t *nbr = NULL;
  uint8_t num;

  /* Is the destination known by ds6? Drop this request if not. */
  nbr = uip_ds6_nbr_ll_lookup((const uip_lladdr_t *)linkaddr);
  if(nbr != NULL || memcmp(&linkaddr_null, linkaddr, sizeof(linkaddr_t)) == 0){
    dn = foure_mac_buf_get_destination(linkaddr);
    if(dn != NULL){ /* already added the same destination */
      if(dn->sent == NULL) dn->sent = sent; /* if sent callback was not set, set it */
      return dn;
    }
    /* Add destination in destinations */
    dn = nbr_table_add_lladdr(destinations, linkaddr);
    if(dn == NULL){
      PRINT("6TOP-BUF:Destination failed!\n");
    }else{
      num = foure_mac_buf_destination_num();

      dn->sent = sent;
      dn->cn_rate = 100 / (num + 1);

      dn->inserted_cn_num = 0;
      dn->transmissions = 0;
      dn->packet_drop = 0;

      LIST_STRUCT_INIT(dn, slot_list);
      LIST_STRUCT_INIT(dn, content_list);

      foure_mac_buf_backoff_reset(dn);

      scale_all_rate_of_destinations(dn, num, num + 1);
      list_add(destination_list, dn);

      PRINT("6TOP-BUF:Create Destination %u\n", linkaddr->u8[7]);

      foure_mac_buf_set_destination_slots(linkaddr, SLOT_TYPE_SHARED, FRAME802154_DATAFRAME, 0, FOURE_MAX_SHARED_SLOT);
    }
  }else
    PRINT("6TOP-BUF:Neighbour %u does not exist!\n", linkaddr->u8[7]);

  return dn;
}

/*---------------------------------------------------------------------------------------
 * Delete destination 'dn' and free the memory allocated in destination_mem memory block
 */
static void 
foure_mac_buf_delete_destination(destination_t *dn)
{
  uint8_t cn_rate;

  if(dn != NULL){
    cn_rate = dn->cn_rate;
    list_remove(destination_list, dn);
    nbr_table_remove(destinations, dn);
    share_out_rate_to_all_destination(cn_rate);
  }
}

/*---------------------------------------------------------------------------------------
 * Delete destination and free the memory allocated in destination_mem memory block with link address 'linkaddr'
 */
void
foure_mac_buf_delete_destination_from_lladdr(linkaddr_t *linkaddr)
{
  destination_t *dn = foure_mac_buf_get_destination(linkaddr);

  if(dn != NULL){
    PRINT("6TOP-BUF:Delete Destination %u\n", linkaddr->u8[7]);
    foure_mac_buf_delete_destination(dn);
  }
}

/*---------------------------------------------------------------------------------------
 * 
 */
static void
scale_all_rate_of_destinations(destination_t *ignore_dn, uint8_t numerator, uint8_t denominator)
{
  destination_t *dn;

  /* Scale other destination's cn_rate and then will set new destination's cn_rate default */
  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    if(dn != ignore_dn){
      dn->cn_rate = (uint16_t)(dn->cn_rate * numerator + denominator / 2) / denominator; /* round up cn_rate */
    }
  }
}

/*---------------------------------------------------------------------------------------
 * 
 */
static void
share_out_rate_to_all_destination(uint8_t rate)
{
  destination_t *dn;
  uint8_t num = foure_mac_buf_destination_num();
  uint8_t rate_a_dn = (rate + num / 2) / num;

  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    dn->cn_rate += rate_a_dn; /* add cn_rate of the dest to be deleted to other dest */
  }
}

static destination_stats_t dest_stats[NBR_TABLE_MAX_NEIGHBORS];
static uint8_t dest_stats_num;
/*---------------------------------------------------------------------------------------
 *
 */
static void
update_all_content_rate_of_destination()
{
  destination_stats_t *stat;
  destination_t *dn, *max_rate_dn = NULL;
  uint8_t i, new_cn_rate, min_rate, max_rate;
  int8_t waste_rate = 0;

  min_rate = (CN_RATE_SCALE + FOURE_MAX_CONTENT / 2) / FOURE_MAX_CONTENT;
  max_rate = CN_RATE_SCALE - min_rate * (foure_mac_buf_destination_num() - 1);
  
  dn = DEST_HEAD();
  if(dn != NULL) max_rate_dn = dn;
  while(dn != NULL){
    stat = NULL;
    for(i = 0 ; i < dest_stats_num; i++){
      if(dest_stats[i].dn == dn){
        stat = &dest_stats[i];
        break;
      }
    }
    if(stat == NULL){
      dest_stats[dest_stats_num].dn = dn;
      dest_stats[dest_stats_num].stat = 0;
      stat = &dest_stats[dest_stats_num];
      dest_stats_num++;
    }
    new_cn_rate = (CN_RATE_SCALE / 2 + (uint16_t)dn->cn_rate * CN_RATE_ALPHA + 
                  (uint16_t)(dn->inserted_cn_num - stat->stat) * (CN_RATE_SCALE - CN_RATE_ALPHA)) / CN_RATE_SCALE; /* calculate and round up new cn_rate */   

    if(new_cn_rate < min_rate){ /* each destination must has minimum one content */
      new_cn_rate = min_rate;
    }else if(new_cn_rate > max_rate){
      new_cn_rate = max_rate;
    }
    stat->stat = dn->inserted_cn_num;

    dn->cn_rate = new_cn_rate;
    if(dn->cn_rate > max_rate_dn->cn_rate)
      max_rate_dn = dn;
    waste_rate += dn->cn_rate - min_rate;
    DEST_NEXT(dn);
  }
  if(max_rate_dn != NULL){
    max_rate_dn->cn_rate += (int8_t)max_rate - (waste_rate + min_rate);
  }
}

/*---------------------------------------------------------------------------
 * Get the pointer of the slot with 'sl_pointer' for the destination 'dn'
 *
 * RETURN : current slot pointer for the destination.
 */
static slot_t *
foure_mac_buf_get_slot(destination_t *dn, uint8_t sl_pointer)
{ 
  slot_t *sl;

  if(dn != NULL){
    sl = SLOT_HEAD(dn);
    while(sl != NULL){
      if(sl_pointer == sl->slot_id)
        return sl;
      SLOT_NEXT(sl);
    }
  }
  return NULL;
}

/*-----------------------------------------------------------------------------------
 * Schedule given number of 'sl_num' for destination designated by 'linkaddr_t dest_addr'.
 * 
 * 'c_type' : Designate the type of the content to be scheduled. Some content types may be 
 * scheduduled for a designated slot type.
 *
 * 'allocate_slots' : point to the array of 'sl_num' to be allocated and the size of the array
 * is given as 'sl_num' variable.
 *
 * 'sl_type' : can be one of the following : TRANSMIT, RECEIVE, SHARED
 *
 *  TODO: move to another file for 
*/
uint8_t 
foure_mac_buf_schedule_slots(linkaddr_t *linkaddr, uint8_t sl_type, uint8_t cn_type, uint8_t *slot_offsets, uint8_t *channel_offsets, uint8_t sl_num)
{
  destination_t *dn;
  slot_t *sl, *new_sl;
  uint8_t i, assigned;

  dn = foure_mac_buf_add_destination(linkaddr, NULL);
  if(dn == NULL) return FOURE_BUFFERS_FULL;

  i = 0;
  while(i < sl_num){
    assigned = 0;
    sl = SLOT_HEAD(dn); //first entry in the list
    while(sl != NULL){
      if(sl->slot_id == slot_offsets[i]){ 
        assigned = 1;
        sl->slot_type = sl_type;
        sl->channel_offset = channel_offsets[i];
      }
      SLOT_NEXT(sl);
    }

    if(!assigned){ //slot is not assigned before
      if(SLOT_COUNT(dn) > FOURE_MAX_SLOTS_PER_DESTINATION - 1){
        PRINT("6TOP-BUF:Slot buffer full!\n");
        return FOURE_SLOT_UNSCHEDULED;
      }

      new_sl = memb_alloc(&slot_mem);
      if(new_sl == NULL){ //no more space left in the buffer
        PRINT("6TOP-BUF:Slot memory allocation failed!\n");
        return FOURE_SLOT_UNSCHEDULED;
      }

      new_sl->slot_id = slot_offsets[i];
      new_sl->slot_type = sl_type;
      new_sl->channel_offset = channel_offsets[i];
      new_sl->slot_tx = 0;
      new_sl->slot_tx_ok = 0;
      new_sl->slot_tx_noack = 0;
      new_sl->slot_tx_collisions = 0;
      new_sl->slot_tx_deferred = 0;
      new_sl->slot_rx = 0;
      new_sl->slot_black_list = 0;
      list_push(dn->slot_list, new_sl);
    }   
    i++; 
  }
  return FOURE_SLOT_SCHEDULED; 
}

/*---------------------------------------------------------------------------
 * Return the number of 'sl_type' slots for the destination 'dn'.
 */
static uint8_t 
foure_mac_buf_get_slot_num(destination_t *dn, uint8_t sl_type)
{
  slot_t *sl;
  uint8_t sl_num = 0;
  
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((sl->slot_type & SLOT_TYPE_MASK) & sl_type)
        sl_num++;
    }
  }
  return sl_num;
}

/*---------------------------------------------------------------------------
 *
 */
uint8_t
foure_mac_buf_get_slot_num_from_lladdr(linkaddr_t *linkaddr, uint8_t sl_type)
{
  destination_t *dn;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    return foure_mac_buf_get_slot_num(dn, sl_type);
  }
  return 0;
}

/*---------------------------------------------------------------------------
 * Delete slot 'sl' from given destination 'dn' and free the memory allocated in 'slot_list' memory block
 */
static void
foure_mac_buf_delete_slot(destination_t *dn, slot_t *sl)
{
  if(dn != NULL && sl != NULL){
    list_remove(dn->slot_list, sl);
    memb_free(&slot_mem, sl);  
  }
}

/*---------------------------------------------------------------------------
 * Delete 'sl_num' slots of destination 'linkaddr' from given 'sl_id' array and free the memory allocated in 'slot_list' memory block
 */
void
foure_mac_buf_delete_slots(linkaddr_t *linkaddr, uint8_t *sl_id, uint8_t sl_num)
{
  destination_t *dn;
  slot_t *sl;
  uint8_t sl_index;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn == NULL)  return;

  for(sl_index = 0; sl_index < sl_num; sl_index++){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if(sl->slot_id == sl_id[sl_index]){
        list_remove(dn->slot_list, sl);
        memb_free(&slot_mem, sl);
        PRINT("6TOP-BUF:Slot %u was deleted\n", sl_id[sl_index]);
        break;
      }
    }
  }
}

/*-----------------------------------------------------------------------------------
 *                                                                              
 */
void
foure_mac_buf_set_slots_status(linkaddr_t * linkaddr, uint8_t *sl_id, uint8_t sl_num, uint8_t sl_status)
{
  destination_t *dn;
  slot_t *sl;
  uint8_t sl_index;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn == NULL)  return;

  for(sl_index = 0; sl_index < sl_num; sl_index++){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if(sl->slot_id == sl_id[sl_index]){
        sl->slot_type = (sl->slot_type & SLOT_TYPE_MASK) | sl_status;
        PRINT("6TOP-BUF:Slot %u status changed to %2x\n", sl->slot_id, sl->slot_type);
        break;
      }
    }
  }
}


/*-----------------------------------------------------------------------------------
 * If the slot is scheduler return slot type, otherwise return FOURE_SLOT_UNSCHEDULED                                                                              
 */
static int8_t 
foure_mac_buf_is_slot_scheduled(uint8_t sl_pointer)
{
  destination_t *dn;
  slot_t *sl;

  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){ /* check for all destination if there exist a schedule */
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if(sl->slot_id == sl_pointer){
        return(sl->slot_type & 0xFF);
      }
    }
  }
  return FOURE_SLOT_UNSCHEDULED;
}

/*-------------------------------------------------------------------------------
 * Get the number of scheduled slots between start_point and end_point within the
 * slot_frame duration.
 */
uint8_t 
foure_mac_buf_get_next_scheduled_slot(uint8_t start_point, uint8_t frame_size)
{

  uint8_t i, end_point, number_of_unscheduled_slots = 1;

  end_point = frame_size - start_point;

  for(i = 0; i < end_point; i++){
    if(foure_mac_buf_is_slot_scheduled((start_point + i + 1) % frame_size) == -1) 
      number_of_unscheduled_slots++;
    else 
      break;
  }
  return MAX(1, number_of_unscheduled_slots);
}

/*------------------------------------------------------------------------------
 * Delete unused 'sl_type' slots from all destination apart from given 'ignore_dest'.
 */
void
foure_mac_buf_delete_unused_slots(linkaddr_t *ignore_dest, uint8_t sl_type)
{
  destination_t *dn;
  slot_t *sl, *temp_sl;
  linkaddr_t *linkaddr; 

  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    linkaddr = (linkaddr_t *)foure_mac_buf_get_destination_lladdr(dn);
    if(memcmp(linkaddr, ignore_dest, sizeof(linkaddr_t)) != 0){
      PRINTO("Dest addr %u\n", linkaddr->u8[7]);
      sl = SLOT_HEAD(dn);
      while(sl != NULL){
        PRINTO("6TOP-BUF:S%u-c%u tx %u rx %u\n", sl->slot_id, sl->channel_offset, sl->slot_tx_ok, sl->slot_rx);
        if((sl->slot_type & SLOT_TYPE_MASK) & sl_type){
          if((sl->slot_tx_ok - sl->slot_tx) == 0){
            if(foure_mac_buf_get_slot_num(dn, sl_type) > 1){
              temp_sl = sl;
              SLOT_NEXT(sl);
              PRINT("6TOP-BUF:Dest %u Slot %u was deleted\n", linkaddr->u8[7], temp_sl->slot_id);
              list_remove(dn->slot_list, temp_sl);
              memb_free(&slot_mem, temp_sl);
              continue;
            }
          }
          sl->slot_tx = sl->slot_tx_ok;
        }
        SLOT_NEXT(sl);
      }
    }
  }
}

/*------------------------------------------------------------------------------
 * Get the least used 'sl_num' transmit slot ids for given 'linkaddr'
 */
uint8_t
foure_mac_buf_get_least_used_slots(linkaddr_t *linkaddr, uint8_t *slot_offsets, uint8_t *channel_offsets, uint8_t *sl_tx, uint8_t sl_num, uint8_t delete_threshold)
{
  destination_t *dn;
  slot_t *sl;
  uint8_t i, most_used_sl_id, most_used_sl_tx, sl_ctr = 0;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((sl->slot_type & SLOT_TYPE_MASK) & SLOT_TYPE_TRANSMIT){
        if(sl_ctr < sl_num){
          if((sl->slot_tx_ok - sl->slot_tx) < delete_threshold){
            slot_offsets[sl_ctr] = sl->slot_id;
            channel_offsets[sl_ctr] = sl->channel_offset;
            sl_tx[sl_ctr] = sl->slot_tx_ok - sl->slot_tx;
            sl_ctr++;
          }       
        }else{
          most_used_sl_id = 0;
          most_used_sl_tx = sl_tx[0];
          for(i = 1; i < sl_num; i++){
            if(sl_tx[i] > most_used_sl_tx){
              most_used_sl_id = i;
              most_used_sl_tx = sl_tx[i];
            }
          }
          if(most_used_sl_tx > (sl->slot_tx_ok - sl->slot_tx)){
            slot_offsets[most_used_sl_id] = sl->slot_id;
            channel_offsets[most_used_sl_id] = sl->channel_offset;
            sl_tx[most_used_sl_id] = sl->slot_tx_ok - sl->slot_tx;
          }
        }
      }
    }
  }

  return sl_ctr;
}

/*------------------------------------------------------------------------------
 * 
 */
uint16_t  
foure_mac_buf_get_destination_slot_tx_ok(linkaddr_t *linkaddr, uint8_t sl_type)
{
  destination_t *dn;
  slot_t *sl;
  uint16_t dest_tx_ok = 0;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((sl->slot_type & SLOT_TYPE_MASK) & sl_type){
        dest_tx_ok += (sl->slot_tx_ok - sl->slot_tx);
      }
    }
  }
  return dest_tx_ok;
}

/*------------------------------------------------------------------------------
 * 
 */
uint16_t  
foure_mac_buf_get_destination_slot_tx_noack(linkaddr_t *linkaddr, uint8_t sl_type)
{
  destination_t *dn;
  slot_t *sl;
  uint16_t dest_tx_noack = 0;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((sl->slot_type & SLOT_TYPE_MASK) & sl_type){
        dest_tx_noack += sl->slot_tx_noack;
      }
    }
  }
  return dest_tx_noack;
}

/*------------------------------------------------------------------------------
 * 
 */
uint16_t  
foure_mac_buf_get_destination_slot_tx_collisions(linkaddr_t *linkaddr, uint8_t sl_type)
{
  destination_t *dn;
  slot_t *sl;
  uint16_t dest_tx_collisions = 0;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((sl->slot_type & SLOT_TYPE_MASK) & sl_type){
        dest_tx_collisions += sl->slot_tx_collisions;
      }
    }
  }
  return dest_tx_collisions;
}

/*------------------------------------------------------------------------------
 * 
 */
uint16_t  
foure_mac_buf_inc_destination_slot_rx(linkaddr_t *linkaddr, uint8_t sl_id)
{
  destination_t *dn;
  slot_t *sl;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    sl = foure_mac_buf_get_slot(dn, sl_id);
    if(sl != NULL){
      sl->slot_rx++;
      return sl->slot_rx;
    }
  }
  return 0;
}

/*------------------------------------------------------------------------------
 * 
 */
void 
foure_mac_buf_update_slot_stats(linkaddr_t *linkaddr, uint8_t sl_type)
{
  destination_t *dn;
  slot_t *sl;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((sl->slot_type & SLOT_TYPE_MASK) & sl_type){
        sl->slot_tx = sl->slot_tx_ok;
        sl->slot_tx_noack = 0;
        sl->slot_tx_collisions = 0;
      }
    }
  }
}

/*------------------------------------------------------------------------------
 * Get the  transmit slot ids for given 'linkaddr'
 */
uint8_t
foure_mac_buf_get_slot_frame_from_lladdr(linkaddr_t *linkaddr, uint8_t *slot_frame)
{
  destination_t *dn;
  slot_t *sl;
  linkaddr_t *dn_linkaddr;
  uint8_t slot, slot_num = 0;

  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    dn_linkaddr = (linkaddr_t *)foure_mac_buf_get_destination_lladdr(dn);
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if((((sl->slot_type & SLOT_TYPE_MASK) & SLOT_TYPE_TRANSMIT) && (memcmp(dn_linkaddr, linkaddr, sizeof(linkaddr_t)) == 0)) || \
         (((sl->slot_type & SLOT_TYPE_MASK) & SLOT_TYPE_SHARED) && (memcmp(dn_linkaddr, &linkaddr_null, sizeof(linkaddr_t)) == 0)) || \
         ((sl->slot_type & SLOT_TYPE_MASK) & SLOT_TYPE_RECEIVE)){

        /* 1 slot unused(default), 0 slot used */
        slot = (sl->channel_offset & 0x0F) | ((sl->slot_type & SLOT_TYPE_MASK) << 4);
        slot_frame[sl->slot_id] = slot;
        slot_num++;
      }
    }
  }
  return slot_num;
}

/*------------------------------------------------------------------------------
 * Get the content from given 'cn_type'
 */
static content_t *
foure_mac_buf_get_content_from_packet_type(destination_t *dn, uint8_t cn_type)
{
  content_t *cn;

  if(dn != NULL){
    for(cn = CONTENT_HEAD(dn); cn != NULL; CONTENT_NEXT(cn)){
      if(cn->c_type == cn_type)
        return cn;
    }
  }
  return NULL;
}

/*---------------------------------------------------------------------------
 * Free given content from given destination`s content buffer
 */
void
foure_mac_buf_free_content(destination_t *dn, content_t *cn)
{ 
  if(dn != NULL && cn != NULL){
    /* Remove packet from list and deallocate */
    list_remove(dn->content_list, cn);
    memb_free(&content_mem, cn);
    PRINTO("6TOP-BUF:Free queued packet from %u queue length %d\n", dn->dest_addr.u8[7], CONTENT_COUNT(dn));
  }
}

/*---------------------------------------------------------------------------
 * Free content buffer for given destination 
 */
static void
foure_mac_buf_free_all_content(destination_t *dn)
{
  content_t *cn, *temp_cn;

  if(dn != NULL){
    cn = CONTENT_HEAD(dn);
    while(cn != NULL){
      temp_cn = cn;
      CONTENT_NEXT(cn);
      PRINT("6TOP-BUF: Seqno %u c_type %2x\n", temp_cn->seqno, temp_cn->c_type);
      /* Remove packet from list and deallocate */
      list_remove(dn->content_list, temp_cn);
      memb_free(&content_mem, temp_cn);
    }
  }
}

/*---------------------------------------------------------------------------------------
 * Add the content given by 'buf' with size 'size' to the content buffer of desntionat 'dn' 
 *
 * 'priority' : signifies the priority of the content and the highest priority content is always 
 * is always inserted to the first location of the list
 *
 * Return FOURE_BUFFERS_FULL, if there no more buffer space in the content buffer of the destination
 *
 * Return FOURE_CONTENT_INSERTED, if operation is successfull.
 */
static int8_t 
foure_mac_buf_add_content(destination_t *dn, uint8_t *c_buf, uint8_t c_size, uint8_t c_type, uint8_t priority)
{
  content_t *prev_cn, *cur_cn, *new_cn;
  linkaddr_t *linkaddr;
  uint8_t max_cn, least_priority = HIGH_PRIORITY;

  if(dn == NULL) return FOURE_BUFFERS_FULL;

  if(c_type == FRAME802154_BEACONFRAME || c_type >= FRAME802154_RPLFRAME){
    cur_cn = foure_mac_buf_get_content_from_packet_type(dn, c_type);
    if(cur_cn != NULL){
      foure_mac_buf_free_content(dn, cur_cn);
    }
  }

  linkaddr = (linkaddr_t *)foure_mac_buf_get_destination_lladdr(dn);

  max_cn = (uint16_t)(dn->cn_rate * FOURE_MAX_CONTENT + CN_RATE_SCALE / 2) / CN_RATE_SCALE;
  if(CONTENT_COUNT(dn) > max_cn - 1){
    PRINT("6TOP-BUF:Dest %u content num %u rate %u\n", linkaddr->u8[7], CONTENT_COUNT(dn), dn->cn_rate);
    cur_cn = CONTENT_TAIL(dn);
    if(cur_cn == NULL) 
      return FOURE_BUFFERS_FULL;

    least_priority = cur_cn->priority; /* get least priority in the content buffer */

    if(priority <= least_priority){
      cur_cn = CONTENT_HEAD(dn);
      while(cur_cn != NULL){
        if(cur_cn->priority == least_priority){
          break;
        }else
          CONTENT_NEXT(cur_cn);
      }
      if(cur_cn != NULL){
        foure_mac_buf_free_content(dn, cur_cn);
      }else 
        PRINT("6TOP-BUF:bug\n");
    }else{
      PRINT("6TOP-BUF:Content buffer full! %u\n", CONTENT_COUNT(dn));
      return FOURE_BUFFERS_FULL;
    }
  }

  new_cn = memb_alloc(&content_mem);
  if(new_cn == NULL){ /* no more space left in the buffer */
    PRINT("6TOP-BUF:Content memory allocation failed!\n");
    return FOURE_BUFFERS_FULL;
  }

  memset(new_cn, 0, sizeof(content_t));

  new_cn->size = c_size;
  new_cn->c_type = c_type;
  new_cn->no_of_tx = 0;
  new_cn->priority = priority;
  
  if(++seqno == 0){
    seqno++;
  }

  new_cn->seqno = seqno; 
  memcpy(new_cn->msg, c_buf, c_size * sizeof(char));
  dn->inserted_cn_num++;
  PRINTO("6TOP-BUF:Add Content to Dest %u\n", linkaddr->u8[7]);

  /* check the priority and order the list according to priority */
  prev_cn = NULL;
  cur_cn = CONTENT_HEAD(dn);

  while(cur_cn != NULL){
    if(new_cn->priority >= cur_cn->priority){
      prev_cn = cur_cn;
      CONTENT_NEXT(cur_cn);
    } else {
      list_insert(dn->content_list, prev_cn, new_cn);
      return FOURE_CONTENT_INSERTED; 
    }
  }
  list_add(dn->content_list, new_cn);

  return FOURE_CONTENT_INSERTED;
}

/*---------------------------------------------------------------------------
 * Return the number of contents for the destination 'dn'.
 */
static uint8_t 
foure_mac_buf_get_content_num(destination_t *dn, uint8_t priority)
{
  content_t *cn;
  uint8_t cn_num = 0;

  if(dn != NULL){
    for(cn = CONTENT_HEAD(dn); cn != NULL; CONTENT_NEXT(cn)){
      if(cn->priority & priority)
        cn_num++;
    }
  }
  return cn_num;
}

/*------------------------------------------------------------------------------
 * Return the number of contents for the destination.
 */
uint8_t
foure_mac_buf_get_content_num_from_lladdr(linkaddr_t *linkaddr)
{
  destination_t *dn;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    return CONTENT_COUNT(dn);
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 * 
 */
uint8_t
foure_mac_buf_get_free_content_num(linkaddr_t *linkaddr)
{
  destination_t *dn;
  uint8_t max_cn;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    max_cn = (uint16_t)(dn->cn_rate * FOURE_MAX_CONTENT + CN_RATE_SCALE / 2) / CN_RATE_SCALE;
    return (max_cn - CONTENT_COUNT(dn));
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 * Get the content from given 'cn_type'
 */
uint8_t
foure_mac_buf_is_content_available_for_packet_type(linkaddr_t *linkaddr, uint8_t cn_type)
{
  content_t *cn;
  destination_t *dn;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    cn = foure_mac_buf_get_content_from_packet_type(dn, cn_type);
    if(cn != NULL)
      return 1;
  }
  return 0;
}

/*---------------------------------------------------------------------------------------
 * Insert data to the destination node buffer. Create the destination if it does not exist
 *
 * Return FOURE_BUFFERS_FULL, if either destination or content buffers are full.
 *
 * Return FOURE_CONTENT_INSERTED, if the peration is successfull.
 */
int8_t
foure_mac_buf_content_insert_data(const uint8_t c_type, void *c_buf, uint8_t c_size, linkaddr_t *linkaddr, uint8_t priority, mac_callback_t sent)
{
  destination_t *dn;

  inserted_content_num++;
  if(inserted_content_num > RATE_REFRESH_THRESHOLD){
    inserted_content_num = 0;
    update_all_content_rate_of_destination();
  }

  dn = foure_mac_buf_add_destination(linkaddr, sent);
  if(dn == NULL) return FOURE_BUFFERS_FULL;

  if(foure_mac_buf_add_content(dn, c_buf, c_size, c_type, priority) == FOURE_BUFFERS_FULL){
    return FOURE_BUFFERS_FULL;
  }

  return FOURE_CONTENT_INSERTED;
}

/*---------------------------------------------------------------------------
 *  Get scheduled content for the current slot
 *  'Destination dn', 'content cn' and 'slot sl'  are returned.
 *
 *  'Destination' : Points to the destination scheduled for the current slot
 *
 *  'Content' : Points to the content of the destination scheduled for the slot
 *
 *  'Slot' : Points to the slot structure which stores slot statistics
 */
int8_t 
foure_mac_buf_get_scheduled_content(destination_t **dn, content_t **cn, slot_t **sl, uint8_t sl_ptr)
{
  destination_t *cur_dn = DEST_HEAD(); //get pointer to the first destination
  content_t *cur_cn;
  slot_t *cur_sl;
  uint8_t slot_type, max_cn;

  if(cur_dn == NULL){
    *dn = NULL;
    *cn = NULL;
    *sl = NULL;
    return FOURE_SLOT_UNSCHEDULED;
  }

  while(cur_dn != NULL){
    cur_cn = CONTENT_HEAD(cur_dn);
    cur_sl = SLOT_HEAD(cur_dn);
    while(cur_sl != NULL){
      if(cur_sl->slot_id == sl_ptr && (cur_sl->slot_type & 0x80) == SLOT_USABLE){

        slot_type = cur_sl->slot_type;

        *dn = cur_dn;
        *cn = cur_cn;
        *sl = cur_sl;

        if(cur_cn != NULL){
          if(slot_type & (SLOT_TYPE_SHARED | SLOT_TYPE_TRANSMIT)){ 
            foure_mac_buf_update_all_backoff_windows();// Decrement all destinations backoff_windows
          }

          max_cn = ((uint16_t)cur_dn->cn_rate * FOURE_MAX_CONTENT) / CN_RATE_SCALE;
          if((slot_type & SLOT_TYPE_SHARED) && /*(cur_cn->priority != LOW_PRIORITY) &&*/ \
             (foure_mac_buf_get_slot_num(cur_dn, SLOT_TYPE_TRANSMIT) < 1 || \
              foure_mac_buf_get_content_num(cur_dn, (NORMAL_PRIORITY | HIGH_PRIORITY)) > (max_cn / 2))){

            if(foure_mac_buf_backoff_expired(cur_dn)){
              foure_mac_buf_backoff_reset(cur_dn); 
              return FOURE_SLOT_SCHEDULED;
            }
          }else if(slot_type & SLOT_TYPE_TRANSMIT)
            return FOURE_SLOT_SCHEDULED;
        } 
      }
      SLOT_NEXT(cur_sl);
    }
    DEST_NEXT(cur_dn);
  }

  /* if slot scheduled, but there is no content to transmit (and if the slot is shared or receive) */
  if(foure_mac_buf_is_slot_scheduled(sl_ptr) == SLOT_TYPE_SHARED || foure_mac_buf_is_slot_scheduled(sl_ptr) == SLOT_TYPE_RECEIVE){
    return FOURE_SLOT_SCHEDULED_FOR_RECEIVE;
  }else{
    *dn = NULL;
    *cn = NULL;
    *sl = NULL;
    return FOURE_SLOT_UNSCHEDULED;
  }
}

/*------------------------------------------------------------------------------
 * Get the total buffer dept for all the destinations apart from the ignored one. 
 */
uint8_t
foure_mac_buf_get_total_buffer_depth(linkaddr_t *ignore_dest)
{
  destination_t *dn;
  content_t *cn;
  linkaddr_t *linkaddr;
  uint8_t buffer_depth = 0;

  for(dn = DEST_HEAD(); dn != NULL; DEST_NEXT(dn)){
    linkaddr = (linkaddr_t *)foure_mac_buf_get_destination_lladdr(dn);
    if(memcmp(linkaddr, ignore_dest, sizeof(linkaddr_t)) != 0){
      for(cn = CONTENT_HEAD(dn); cn != NULL; CONTENT_NEXT(cn)){
        buffer_depth++;
      } 
    }
  }
  return buffer_depth;
}

/*------------------------------------------------------------------------------
 * 
 */
uint8_t
foure_mac_buf_get_buffer_occupancy(linkaddr_t *linkaddr)
{
  destination_t *dn;
  uint8_t max_cn;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    max_cn = (uint16_t)(dn->cn_rate * FOURE_MAX_CONTENT + CN_RATE_SCALE / 2) / CN_RATE_SCALE;
    return (CONTENT_COUNT(dn) * 100) / max_cn;
  }else
    return 0;
}

/*------------------------------------------------------------------------------
 * 
 */
 uint16_t
 foure_mac_buf_get_destination_inserted_content_num(linkaddr_t *linkaddr, destination_stats_t *s)
 {
  destination_t *dn;
  uint16_t cur_inserted_cn_num = 0;
  
  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    if(s->dn != dn){
      s->dn = dn;
      s->stat = 0;
    }
    cur_inserted_cn_num = dn->inserted_cn_num - s->stat;
    s->stat = dn->inserted_cn_num;
  }
  return cur_inserted_cn_num;
 }

/*------------------------------------------------------------------------------
 * 
 */
uint8_t
foure_mac_buf_get_slot_black_list(linkaddr_t *linkaddr, uint8_t *slot_offsets, uint8_t *channel_offsets, uint8_t black_list_threshold)
{
  destination_t *dn;
  slot_t *sl;
  uint16_t slot_black_list_num = 0;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    for(sl = SLOT_HEAD(dn); sl != NULL; SLOT_NEXT(sl)){
      if(sl->slot_black_list > black_list_threshold){
        slot_offsets[slot_black_list_num] = sl->slot_id;
        channel_offsets[slot_black_list_num] = sl->channel_offset;
        slot_black_list_num++;
      }
      sl->slot_black_list = 0;
    }
  }
  return slot_black_list_num;
}

/*------------------------------------------------------------------------------
 * 
 */
int8_t
foure_mac_buf_parent_changed(linkaddr_t *old_parent, linkaddr_t *new_parent)
{
  destination_t *old_parent_dn, *new_parent_dn;
  content_t *cn, *temp_cn;
  slot_t *sl, *temp_sl;
  uint8_t i, cn_rate;

  old_parent_dn = foure_mac_buf_get_destination(old_parent);

  if(old_parent_dn != NULL){
    cn = CONTENT_HEAD(old_parent_dn);
    while(cn != NULL){
      if(cn->c_type != FRAME802154_DATAFRAME){
        temp_cn = cn;
        CONTENT_NEXT(cn);
        PRINT("del cn %2x\n", temp_cn->c_type);
        foure_mac_buf_free_content(old_parent_dn, temp_cn);
      }else
        CONTENT_NEXT(cn);
    }

    sl = SLOT_HEAD(old_parent_dn);
    while(sl != NULL){
      if((sl->slot_type & SLOT_TYPE_MASK) != SLOT_TYPE_SHARED){
        temp_sl = sl;
        SLOT_NEXT(sl);
        PRINT("del sl %u\n", temp_sl->slot_id);
        foure_mac_buf_delete_slot(old_parent_dn, temp_sl);
      }else
        SLOT_NEXT(sl);
    }

    new_parent_dn = foure_mac_buf_get_destination(new_parent);

    if(new_parent_dn != NULL){
      foure_mac_buf_free_all_content(new_parent_dn);

      new_parent_dn->inserted_cn_num = 0;
      new_parent_dn->transmissions = 0;
      new_parent_dn->packet_drop = 0;

      LIST_STRUCT_INIT(new_parent_dn, content_list);

      foure_mac_buf_backoff_reset(new_parent_dn);
    }else{
      new_parent_dn = foure_mac_buf_add_destination(new_parent, NULL);
      if(new_parent_dn == NULL) return FOURE_BUFFERS_FULL;      
    }

    *(new_parent_dn->content_list) = *(old_parent_dn->content_list);
    LIST_STRUCT_INIT(old_parent_dn, content_list);
    cn_rate = new_parent_dn->cn_rate;
    new_parent_dn->cn_rate = old_parent_dn->cn_rate;
    old_parent_dn->cn_rate = cn_rate; //(CN_RATE_SCALE + FOURE_MAX_CONTENT / 2) / FOURE_MAX_CONTENT;

    PRINT("6TOP-BUF:Old Parent cn num %u %u\n", CONTENT_COUNT(old_parent_dn), CONTENT_COUNT(new_parent_dn));

    old_parent_dn = DEST_HEAD();
    while(old_parent_dn != NULL){
      for(i = 0; i < dest_stats_num; i++){
        if(dest_stats[i].dn == old_parent_dn){
          dest_stats[i].stat = old_parent_dn->inserted_cn_num;
          break;
        }
      }
      DEST_NEXT(old_parent_dn);
    }
    inserted_content_num = 0;

    return 1;
  }
  return 0;
}

/*------------------------------------------------------------------------------
 * Get number of lost packet for given destinations.
 */
uint8_t
foure_mac_buf_get_total_lost_packet(linkaddr_t *linkaddr, destination_stats_t *s)
{
  destination_t *dn;
  uint8_t cur_lost = 0;

  dn = foure_mac_buf_get_destination(linkaddr);
  if(dn != NULL){
    if(s->dn != dn){
      s->dn = dn;
      s->stat = 0;
    }
    cur_lost = dn->packet_drop - s->stat;
    s->stat = dn->packet_drop;
  }
  return cur_lost;
}

#if FOURE_MAC_CLEAR_UNUSED_DESTINATIONS 
/*---------------------------------------------------------------------------------------
 * Go through the destination list and find the ones that has not been used for the last
 * DESTINATION_CHECK_INTERVAL seconds. If there is unused destinations, remove them from 
 * the list and free the buffers.
 */
static void 
foure_mac_buf_check_unused_destination()
{
  destination_t *dn = DEST_HEAD(); /* get pointer to the first destination */
  destination_t *temp_dn;

  while(dn != NULL){
    if(dn->is_used != 0){
      dn->is_used = 0;
      DEST_NEXT(dn);
    }else if(linkaddr_cmp(&dn->dest_addr, &linkaddr_null) == 0){ /* Non-zero if the addresses are the same, zero if they are different */
      temp_dn = dn;
      DEST_NEXT(dn);
      foure_mac_buf_delete_destination(temp_dn);
    }else
      DEST_NEXT(dn);
  }
}

/*---------------------------------------------------------------------------------------
 * If any destination is not used for a pre determined period, remove it from the 
 * destination buffer space.
 */
PROCESS_THREAD(foure_buf_clear_unused_destinations, ev, data)
{
  static struct etimer et;

  PROCESS_BEGIN();

  etimer_set(&et, DESTINATION_CHECK_INTERVAL);

  while(1){
    PROCESS_YIELD();
    if(etimer_expired(&et)){
      etimer_reset(&et);
      foure_mac_buf_check_unused_destination();
    } 
  }
  PROCESS_END();
}
#endif /* FOURE_MAC_CLEAR_UNUSED_DESTINATIONS */

/*---------------------------------------------------------------------------------------
 *
 */
static void
foure_mac_buf_callback(void *ptr)
{
  foure_mac_buf_delete_destination(ptr);
}

/*---------------------------------------------------------------------------------------
 * Initialize 4emac Buffer
 */
void 
foure_mac_buf_init()
{
  dest_stats_num = 0;
  inserted_content_num = 0;
  nbr_table_register(destinations, (nbr_table_callback *)foure_mac_buf_callback);
  list_init(destination_list);
  memb_init(&slot_mem);
  memb_init(&content_mem);
#if FOURE_MAC_CLEAR_UNUSED_DESTINATIONS 
  process_exit(&foure_buf_clear_unused_destinations);
  process_start(&foure_buf_clear_unused_destinations, NULL);
#endif
}

#endif /* TSCH_TIME_SYNCH */
