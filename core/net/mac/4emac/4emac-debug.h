#ifndef FOUREMAC_DEBUG_H
#define FOUREMAC_DEBUG_H

#include <stdio.h>

#define DEBUG_NONE      0
#define DEBUG_PRINT     1
#define DEBUG_PRINTO    2

#if (FOURE_DEBUG) & DEBUG_PRINT
#define PRINT(...) printf(__VA_ARGS__)
#else
#define PRINT(...)
#endif /* (FOURE_DEBUG) & DEBUG_PRINT */

#if (FOURE_DEBUG) & DEBUG_PRINTO
#define PRINTO(...) printf(__VA_ARGS__)
#else
#define PRINTO(...)
#endif /* (FOURE_DEBUG) & DEBUG_PRINTO */

#endif /* FOUREMAC_DEBUG_H */
