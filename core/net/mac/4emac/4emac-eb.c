/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A Time Synchronisation mechanism that uses ACK frames and a Keep alive timer.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/mac/4emac/4emac-private.h"
#include "net/mac/4emac/4emac-eb.h"
#include "net/mac/4emac/4emac-6top-scheduler.h"
#include "net/mac/4emac/4emac-buf.h"

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_NONE
#endif
#include "net/mac/4emac/4emac-debug.h"

/*---------------------------------------------------------------------------------
 * Reset slot timings to the defined default value (depends on the configuration). 
 * The function gets a pointer to the slot timing structure and fills it 
 * with the default values.
 */

void
fouremac_buf_reset_timeslot_timing(struct tsch_time_slot_timing *tsch_timing)
{
  tsch_timing->ts_cca_offset = TSCH_DEFAULT_TS_CCA_OFFSET;
  tsch_timing->ts_cca = TSCH_DEFAULT_TS_CCA;
  tsch_timing->ts_tx_offset = TSCH_DEFAULT_TS_TX_OFFSET;
  tsch_timing->ts_rx_offset = TSCH_DEFAULT_TS_RX_OFFSET;
  tsch_timing->ts_rx_ack_delay = TSCH_DEFAULT_TS_RX_ACK_DELAY;
  tsch_timing->ts_tx_ack_delay = TSCH_DEFAULT_TS_TX_ACK_DELAY;
  tsch_timing->ts_rx_wait = TSCH_DEFAULT_TS_RX_WAIT;
  tsch_timing->ts_ack_wait = TSCH_DEFAULT_TS_ACK_WAIT;
  tsch_timing->ts_rx_tx = TSCH_DEFAULT_TS_RX_TX;
  tsch_timing->ts_max_ack = TSCH_DEFAULT_TS_MAX_ACK;
  tsch_timing->ts_max_tx = TSCH_DEFAULT_TS_MAX_TX;
  tsch_timing->ts_duration = TSCH_DEFAULT_TS_TIMESLOT_LENGTH;
}


/*---------------------------------------------------------------------------------
 * This function decodes the received enhanced beacon packet in the 'packetbuf' array 
 * The result is stored in to the timesynch_msg_eb structure. 
 * If the decoding process fails, -1 is returned. If the decoding succeeds, 
 * the final index within the 'packetbuf' array is returned.
 */

int8_t 
decode_enhanced_beacon(char *packetbuf, uint8_t size, struct timesynch_msg_eb *tsch_timesynch_msg)
{
  uint16_t local_ie_bytes;
  uint8_t current_position;
  struct ie_header temp_ie_header;

  #if DEBUG
    PRINTO("decode_enhanced_beacon\n");
    for (current_position = 0; current_position < 36; current_position++)
    {
      PRINTO("%x ", packetbuf[current_position]);
    }
    PRINTO("\n");
  #endif
   
  if(size < 2) { /* Not enough space for IE descriptor */
    return -1;
  }
  
  //get rid of termination characters and retrive the payload size
  current_position = 0;

  /*  Header IE Header  */
  memcpy(&local_ie_bytes, &packetbuf[current_position], sizeof(uint16_t));
  tsch_timesynch_msg->enhanced_beacon_header.type = local_ie_bytes & 0x8000 ? 1 : 0; /* b15 */;
  tsch_timesynch_msg->enhanced_beacon_header.len = local_ie_bytes & 0x007f; /* b0-b6 */
  tsch_timesynch_msg->enhanced_beacon_header.id = (local_ie_bytes & 0x7f80) >> 7; /* b7-b14 */
  current_position += 2;
   
  if (tsch_timesynch_msg->enhanced_beacon_header.type  != 0 ) {
     PRINTO("4emac-eb: wrong type %04x\n", local_ie_bytes);
     return -1;
  } 

  PRINTO("4emac:len  : %04x\n", tsch_timesynch_msg->enhanced_beacon_header.len);
  PRINTO("4emac:id   : %04x\n", tsch_timesynch_msg->enhanced_beacon_header.id);
  PRINTO("4emac:type : %04x\n", tsch_timesynch_msg->enhanced_beacon_header.type);

  if(tsch_timesynch_msg->enhanced_beacon_header.id == HEADER_IE_LIST_TERMINATION_1) { //beacon starts correctly, get size field
    /*  Payload IE Header(MLME)   */
    memcpy(&local_ie_bytes, &packetbuf[current_position], sizeof(uint16_t));
    get_ie_header_parts(local_ie_bytes, &tsch_timesynch_msg->payload_ie, 0);
    current_position += 2;
    PRINTO("4emac-eb: Length is %u bytes, frame groupID is %x\n", tsch_timesynch_msg->payload_ie.len, tsch_timesynch_msg->payload_ie.id);
  }else{
    return -1;  //error unexpected enhanced beacon start
  }
  
  if( tsch_timesynch_msg->payload_ie.type != PAYLOAD_IE_MLME && tsch_timesynch_msg->payload_ie.type != PAYLOAD_IE_LIST_TERMINATION) {
    return -1; 
  }

  while(current_position < size){
    memcpy(&local_ie_bytes, &packetbuf[current_position], sizeof(uint16_t));
    get_ie_header_parts(local_ie_bytes, &temp_ie_header, 0);

    if(temp_ie_header.id == MLME_SHORT_IE_TSCH_SYNCHRONIZATION){            /*  MLME-SubIE TSCH Synchronization   */
 
      tsch_timesynch_msg->synch_information.ie_hdr = temp_ie_header;
      current_position += 2;
      memcpy(&tsch_timesynch_msg->synch_information.asn, &packetbuf[current_position], 5 * sizeof(uint8_t));
      current_position += 5;
      //Maybe change this to a simple function for readability purposes
      if(tsch_msg.synch_information.asn.ls4b > current_asn.ls4b) /*Update asn immediately if its value is logical*/
        memcpy(&current_asn, &tsch_msg.synch_information.asn, sizeof(struct asn_t));

      memcpy(&tsch_timesynch_msg->synch_information.join_priority, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;

    }else if(temp_ie_header.id == MLME_SHORT_IE_TSCH_TIMESLOT){      /*  MLME-SubIE TSCH TimeSlot  */

      tsch_timesynch_msg->tsch_slot_template.ie_hdr = temp_ie_header;
      current_position += 2;
      memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_template_id, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;

      if(tsch_timesynch_msg->tsch_slot_template.time_slot_template_id == 0x00){  //default

        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_cca_offset = TSCH_DEFAULT_TS_CCA_OFFSET;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_cca = TSCH_DEFAULT_TS_CCA;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_tx_offset = TSCH_DEFAULT_TS_TX_OFFSET;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_offset = TSCH_DEFAULT_TS_RX_OFFSET;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_ack_delay = TSCH_DEFAULT_TS_RX_ACK_DELAY;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_tx_ack_delay = TSCH_DEFAULT_TS_TX_ACK_DELAY;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_wait = TSCH_DEFAULT_TS_RX_WAIT;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_ack_wait = TSCH_DEFAULT_TS_ACK_WAIT;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_tx = TSCH_DEFAULT_TS_RX_TX;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_max_ack = TSCH_DEFAULT_TS_MAX_ACK;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_max_tx = TSCH_DEFAULT_TS_MAX_TX;
        tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_duration = TSCH_DEFAULT_TS_TIMESLOT_LENGTH; // 10ms

      }else{  //timeslot timing using non-default(!=10ms) timeslot.

        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_cca_offset, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_cca, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_tx_offset, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_offset, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_ack_delay, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_tx_ack_delay, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_wait, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_ack_wait, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_rx_tx, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_max_ack, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_max_tx, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        memcpy(&tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_duration, &packetbuf[current_position], 2 * sizeof(uint8_t));
        current_position += 2;
        
        slot_duration = US_TO_RTIMERTICKS(tsch_timesynch_msg->tsch_slot_template.time_slot_timing.ts_duration);
      }

    }else if(temp_ie_header.id == MLME_LONG_IE_TSCH_CHANNEL_HOPPING_SEQUENCE){      /*  MLME-SubIE TSCH Channel Hopping   */

      tsch_timesynch_msg->tsch_channel_hopping_template.ie_hdr = temp_ie_header;
      current_position += 2;
      memcpy(&tsch_timesynch_msg->tsch_channel_hopping_template.channel_hopping_template_id, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;

    }else if(temp_ie_header.id == MLME_SHORT_IE_TSCH_SLOFTRAME_AND_LINK){       /*  MLME-SubIE TSCH Slotframe and Link  */
      tsch_timesynch_msg->frame_link_information.ie_hdr = temp_ie_header;
      current_position += 2;
      memcpy(&tsch_timesynch_msg->frame_link_information.slot_frames, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;
      memcpy(&tsch_timesynch_msg->frame_link_information.slot_frame_id, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;
      memcpy(&tsch_timesynch_msg->frame_link_information.slot_frame_size, &packetbuf[current_position], 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&tsch_timesynch_msg->frame_link_information.links, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;
      memcpy(&tsch_timesynch_msg->frame_link_information.slot_number, &packetbuf[current_position], 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&tsch_timesynch_msg->frame_link_information.channel_offset, &packetbuf[current_position], 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&tsch_timesynch_msg->frame_link_information.link_option, &packetbuf[current_position], sizeof(uint8_t));
      current_position += 1;
    }else if (temp_ie_header.id == PAYLOAD_IE_LIST_TERMINATION) {
      memcpy(&tsch_timesynch_msg->termination_ie, &temp_ie_header, sizeof(struct ie_header));
      current_position += 2; 
      break;
    }
    else {
      PRINTO("4emac-eb:unexpected ie id\n");
      return -1;
    }
  }

#if !SIX_TISCH_MINIMAL 
  if (current_position < size) {
     /*------------------------Extra parameters for synchronisation (4 bytes) --------------------------*/  
     //printf("ie id %d\n", tsch_timesynch_msg->termination_ie.id);
     memcpy(&tsch_timesynch_msg->authority_offset, &packetbuf[current_position], 2 * sizeof(uint8_t));
     current_position += 2;
     memcpy(&tsch_timesynch_msg->timestamp, &packetbuf[current_position], 2 * sizeof(uint8_t));
     current_position += 2;
  }
#endif
#if FRAME802154_SECURITY_LEVEL_AES_192
  if(current_position < size){
    memcpy(&tsch_timesynch_msg->public_key, &packetbuf[current_position], 2 * sizeof(uint8_t));
    current_position += 2;
    memcpy(&tsch_timesynch_msg->mod, &packetbuf[current_position], 2 * sizeof(uint8_t));
    current_position += 2;
    tsch_timesynch_msg->has_security = 1;
  }
#endif
  // store the received node timing
  memcpy(&time_slot_timing, &tsch_timesynch_msg->tsch_slot_template.time_slot_timing, sizeof(struct tsch_time_slot_timing));
  
  return current_position;
}



/*---------------------------------------------------------------------------------
 * This function sets the default values of the enhanced beacon.
 * The result is stored in to the timesynch_msg_eb structure. 
 */
void 
set_eb_content(struct timesynch_msg_eb *tsch_eb_msg)
{
  /*  Header IE Header  */
  tsch_eb_msg->enhanced_beacon_header.len = 0x0000;
  tsch_eb_msg->enhanced_beacon_header.id = HEADER_IE_LIST_TERMINATION_1;
  tsch_eb_msg->enhanced_beacon_header.type = 0x0000;  

  /*  Payload IE Header(MLME)   */
  if(slot_duration == US_TO_RTIMERTICKS(10000))
    tsch_eb_msg->payload_ie.len = 0x001A; // 26 bytes
  else
    tsch_eb_msg->payload_ie.len = 0x0032; // 53 bytes
  tsch_eb_msg->payload_ie.id = PAYLOAD_IE_MLME;
  tsch_eb_msg->payload_ie.type = 0x0001;

  /*  MLME-SubIE TSCH Synchronization   */
  tsch_eb_msg->synch_information.ie_hdr.len = 0x0006; // length of the sub information element (6 bytes)
  tsch_eb_msg->synch_information.ie_hdr.id = MLME_SHORT_IE_TSCH_SYNCHRONIZATION;  //  0x1a is the sub ID 7-bits
  tsch_eb_msg->synch_information.ie_hdr.type = 0x0000;

  /*  these values will be updated in 4emac-timesynch.c  */
  //memcpy(tsch_eb_msg->synch_information.asn, asn, 5 * sizeof(uint8_t));//this value must be updated before transmission.
  //tsch_eb_msg->synch_information.join_priority = tsch_timesynch_authority_level();

  /*  MLME-SubIE TSCH TimeSlot  */
  if(slot_duration == US_TO_RTIMERTICKS(10000)) //if timeslot length is 10ms
  {
    tsch_eb_msg->tsch_slot_template.ie_hdr.len = 0x0001;  //length of the sub information element
    tsch_eb_msg->tsch_slot_template.ie_hdr.id = MLME_SHORT_IE_TSCH_TIMESLOT;   //0x1c is the sub ID 7-bits
    tsch_eb_msg->tsch_slot_template.ie_hdr.type = 0x0000;
    tsch_eb_msg->tsch_slot_template.time_slot_template_id = 0x00; //default
  } else {
    tsch_eb_msg->tsch_slot_template.ie_hdr.len = 0x0019;  //length of the sub information element
    tsch_eb_msg->tsch_slot_template.ie_hdr.id = MLME_SHORT_IE_TSCH_TIMESLOT;   //0x1c is the sub ID 7-bits
    tsch_eb_msg->tsch_slot_template.ie_hdr.type = 0x0000;
    tsch_eb_msg->tsch_slot_template.time_slot_template_id = 0x01; //non-default
  }

  if(tsch_eb_msg->tsch_slot_template.time_slot_template_id != 0x00)// if not default (not 10ms), should be set to the value received from the parent.
  {
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_cca_offset = time_slot_timing.ts_cca_offset; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_cca = time_slot_timing.ts_cca; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_tx_offset = time_slot_timing.ts_tx_offset; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_offset = time_slot_timing.ts_rx_offset; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_ack_delay = time_slot_timing.ts_rx_ack_delay; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_tx_ack_delay = time_slot_timing.ts_tx_ack_delay; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_wait = time_slot_timing.ts_rx_wait; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_ack_wait = time_slot_timing.ts_ack_wait; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_tx = time_slot_timing.ts_rx_tx;
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_max_ack = time_slot_timing.ts_max_ack;
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_max_tx = time_slot_timing.ts_max_tx; 
    tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_duration = time_slot_timing.ts_duration; 
  }

  /*  MLME-SubIE TSCH Channel Hopping   */
  tsch_eb_msg->tsch_channel_hopping_template.ie_hdr.len = 0x0001;
  tsch_eb_msg->tsch_channel_hopping_template.ie_hdr.id = MLME_LONG_IE_TSCH_CHANNEL_HOPPING_SEQUENCE;
  tsch_eb_msg->tsch_channel_hopping_template.ie_hdr.type = 0x0001;
  tsch_eb_msg->tsch_channel_hopping_template.channel_hopping_template_id = 0x00; //default 0x00, 2.4Ghz 0x00 template = [5,6,12,7,15,4,14,11,8,0,1,2,13,3,9,10]

  /*  MLME-SubIE TSCH Slotframe and Link  */
  tsch_eb_msg->frame_link_information.ie_hdr.len = 0x000A; //(10 bytes)
  tsch_eb_msg->frame_link_information.ie_hdr.id = MLME_SHORT_IE_TSCH_SLOFTRAME_AND_LINK;
  tsch_eb_msg->frame_link_information.ie_hdr.type = 0x0000;
  tsch_eb_msg->frame_link_information.slot_frames = 0x01; /*Number of slot frames*/
  tsch_eb_msg->frame_link_information.slot_frame_id = 0x01;  // with the reasonable assumption that the slot frame size is not larger than 256
  tsch_eb_msg->frame_link_information.slot_frame_size = slot_frame_size;
  
  tsch_eb_msg->frame_link_information.links = 0x01; //what is this?
  tsch_eb_msg->frame_link_information.slot_number = 0x0000;
  tsch_eb_msg->frame_link_information.channel_offset = 0x0000;
  tsch_eb_msg->frame_link_information.link_option = 0x0F; //transmit, receive, shared, timekeeping. EBs are used for time keeping in this implementation

  /*  Payload IE Header(IE Termination Header)  */
  tsch_eb_msg->termination_ie.len = 0x0000;
  tsch_eb_msg->termination_ie.id = PAYLOAD_IE_LIST_TERMINATION; //termination
  tsch_eb_msg->termination_ie.type = 0x0001;  

  /*   Optional Parameters  */
  tsch_eb_msg->timestamp = 0x0000;
#if FRAME802154_SECURITY_LEVEL_AES_192
  tsch_eb_msg->has_security = 0;
  tsch_eb_msg->public_key = 0x0007;
  tsch_eb_msg->mod = 0x0015;
#endif
}

/*---------------------------------------------------------------------------------
 * This function inserts enhanced beacon content in to the message buffer prior to 
 * transmission. The unction should be called with a timesynch_msg_eb structure
 * with default values set. The return value is -1, if the values in timesynch_msg_eb
 * are not within expected range. Otherwise, the position of the buffer index(size) is returned.
 */
int8_t 
insert_eb_content(uint8_t *msg_buffer, struct timesynch_msg_eb *tsch_eb_msg)
{
  uint16_t current_position = 0;

  /*  Header IE Header  */
  if(!tsch_eb_msg->enhanced_beacon_header.type){
    create_header_ie_descriptor(&msg_buffer[current_position], &tsch_eb_msg->enhanced_beacon_header);
    PRINTO("%04x\n", local_ie_bytes);
  }else{
    PRINTO("4emac-eb : Do nothing!\n");
  }
  current_position += 2;

  /*  Payload IE Header(MLME)   */  
  set_ie_header_parts(&msg_buffer[current_position], &tsch_eb_msg->payload_ie);
  current_position += 2;

  /*  MLME-SubIE TSCH Synchronization   */
  set_ie_header_parts(&msg_buffer[current_position], &tsch_eb_msg->synch_information.ie_hdr);
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->synch_information.asn, 5 * sizeof(uint8_t));
  current_position += 5;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->synch_information.join_priority, sizeof(uint8_t));
  current_position += 1;

  /*  MLME-SubIE TSCH TimeSlot  */
  if(tsch_eb_msg->tsch_slot_template.ie_hdr.len == 1 || tsch_eb_msg->tsch_slot_template.ie_hdr.len == 25)
  {
    set_ie_header_parts(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.ie_hdr);
    current_position += 2;
    memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_template_id, sizeof(uint8_t));
    current_position += 1;

    if(tsch_eb_msg->tsch_slot_template.time_slot_template_id != 0x00)//if not default
    {
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_cca_offset, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_cca, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_tx_offset, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_offset, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_ack_delay, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_tx_ack_delay, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_wait, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_ack_wait, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_rx_tx, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_max_ack, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_max_tx, 2 * sizeof(uint8_t));
      current_position += 2;
      memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_slot_template.time_slot_timing.ts_duration, 2 * sizeof(uint8_t));
      current_position += 2;
    }
  }else
        return -1;

  /*  MLME-SubIE TSCH Channel Hopping   */
  set_ie_header_parts(&msg_buffer[current_position], &tsch_eb_msg->tsch_channel_hopping_template.ie_hdr);
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->tsch_channel_hopping_template.channel_hopping_template_id, sizeof(uint8_t));
  current_position += 1;

  /*  MLME-SubIE TSCH Slotframe and Link  */
  set_ie_header_parts(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.ie_hdr);
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.slot_frames, sizeof(uint8_t));
  current_position += 1;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.slot_frame_id, sizeof(uint8_t));
  current_position += 1;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.slot_frame_size, 2 * sizeof(uint8_t));
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.links, sizeof(uint8_t));
  current_position += 1;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.slot_number, 2 * sizeof(uint8_t));
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.channel_offset, 2 * sizeof(uint8_t));
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->frame_link_information.link_option, sizeof(uint8_t));
  current_position += 1;

  /*  Payload IE Header(IE Termination Header)  */
  set_ie_header_parts(&msg_buffer[current_position], &tsch_eb_msg->termination_ie);
  current_position += 2;

#if !SIX_TISCH_MINIMAL
  /*  Extra parameters(4 bytes)  */
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->authority_offset, 2 * sizeof(uint8_t));
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->timestamp, 2 * sizeof(uint8_t));
  current_position += 2;
#endif

#if FRAME802154_SECURITY_LEVEL_AES_192
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->public_key, 2 * sizeof(uint8_t));
  current_position += 2;
  memcpy(&msg_buffer[current_position], &tsch_eb_msg->mod, 2 * sizeof(uint8_t));
  current_position += 2;
#endif

  #if DEBUG
    int local_ie_bytes;
    printf("insert_eb_content\n");
    for (local_ie_bytes = 0; local_ie_bytes < current_position; local_ie_bytes++)
    {
      printf("%x ", msg_buffer[local_ie_bytes]);
    }
    printf("\n");
  #endif
  
  return current_position;
}

/*---------------------------------------------------------------------------------
 *
 */
void
set_eb_slot_channel_offset(uint8_t *msg_buffer, uint16_t slot_offset, uint16_t channel_offset)
{
  uint8_t current_position = 0;

  if(msg_buffer[14] == 0x00){ /* Default timeslot length*/
    current_position = 25;
  }else if(msg_buffer[14] == 0x01){
    current_position = 49;
  }else{
    PRINTO("4emac-eb: Bug! %2x\n", msg_buffer[13]);
    return;
  }

  memcpy(&msg_buffer[current_position], &slot_offset, 2 * sizeof(uint8_t));
  current_position += 2;
  memcpy(&msg_buffer[current_position], &channel_offset, 2 * sizeof(uint8_t));
}