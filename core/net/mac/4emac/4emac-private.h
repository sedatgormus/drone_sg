#ifndef _FOURE_MAC_PRIVATE_H
#define _FOURE_MAC_PRIVATE_H

#define FRAME802154_SECURITY_LEVEL_AES_192   0

#include "sys/rtimer.h"
#include "net/packetbuf.h"
#include "net/mac/4emac/4emac-eb.h"
#include "net/mac/4emac/4emac-timesynch.h"

/* 4emac MAC parameters */
#ifdef FOURE_CONF_MAC_MAX_PACKET_LEN
#define FOURE_MAC_MAX_PACKET_LEN FOURE_CONF_MAC_MAX_PACKET_LEN
#else
#define FOURE_MAC_MAX_PACKET_LEN 127
#endif
#ifdef FOURE_CONF_PACKET_DEST_ADDR_IN_ACK
#define FOURE_PACKET_DEST_ADDR_IN_ACK FOURE_CONF_PACKET_DEST_ADDR_IN_ACK
#else
#define FOURE_PACKET_DEST_ADDR_IN_ACK 1
#endif
#ifdef FOURE_CONF_PACKET_SRC_ADDR_IN_ACK
#define FOURE_PACKET_SRC_ADDR_IN_ACK FOURE_CONF_PACKET_SRC_ADDR_IN_ACK
#else
#define FOURE_PACKET_SRC_ADDR_IN_ACK 1
#endif

#define FOURE_MAC_TIME_CORRECTION_EB 	1
#define FOURE_MAC_TIME_CORRECTION_EACK 	2

#ifdef TSCH_CONF_SIX_TISCH_MINIMAL
#define SIX_TISCH_MINIMAL TSCH_CONF_SIX_TISCH_MINIMAL
#else
#define SIX_TISCH_MINIMAL 0
#endif

#ifdef TSCH_CONF_CENTRAL_SCHEDULER_ENABLED
#define TSCH_CENTRAL_SCHEDULER_ENABLED TSCH_CONF_CENTRAL_SCHEDULER_ENABLED
#else
#define TSCH_CENTRAL_SCHEDULER_ENABLED 0
#endif

#ifdef TSCH_CONF_DISTRIBUTED_SCHEDULER_ENABLED
#define TSCH_DISTRIBUTED_SCHEDULER_ENABLED TSCH_CONF_DISTRIBUTED_SCHEDULER_ENABLED
#else
#define TSCH_DISTRIBUTED_SCHEDULER_ENABLED 0
#endif

#if !(TSCH_CENTRAL_SCHEDULER_ENABLED || TSCH_DISTRIBUTED_SCHEDULER_ENABLED)
#ifdef TSCH_CONF_SHARED_SCHEDULING_ENABLED
#define TSCH_SHARED_SCHEDULING_ENABLED TSCH_CONF_SHARED_SCHEDULING_ENABLED
#else
#define TSCH_SHARED_SCHEDULING_ENABLED 1
#endif
#endif /* !TSCH_CENTRAL_SCHEDULER_ENABLED */

#ifdef FOURE_CONF_MAC_COMPANSATE_DRIFT
#define FOURE_MAC_COMPANSATE_DRIFT FOURE_CONF_MAC_COMPANSATE_DRIFT
#else
#define FOURE_MAC_COMPANSATE_DRIFT 0
#endif

#ifdef FOURE_CONF_MAC_CLEAR_UNUSED_DESTINATIONS
#define FOURE_MAC_CLEAR_UNUSED_DESTINATIONS  FOURE_CONF_MAC_CLEAR_UNUSED_DESTINATIONS
#else
#define FOURE_MAC_CLEAR_UNUSED_DESTINATIONS  0
#endif

#ifdef FOURE_CONF_NUMBER_OF_CHANNELS
#define NUMBER_OF_CHANNELS FOURE_CONF_NUMBER_OF_CHANNELS
#else
#define NUMBER_OF_CHANNELS 2
#endif

/* Foure Buffer */
#ifdef FOURE_CONF_MAX_CONTENT
#define FOURE_MAX_CONTENT FOURE_CONF_MAX_CONTENT
#else                           
#define FOURE_MAX_CONTENT 5 
#endif                          

#ifdef FOURE_CONF_MAX_SLOTS
#define FOURE_MAX_SLOTS_PER_DESTINATION FOURE_CONF_MAX_SLOTS
#else                           
#define FOURE_MAX_SLOTS_PER_DESTINATION 10
#endif                                               

#ifdef FOURE_CONF_MAX_SHARED_SLOT
#define FOURE_MAX_SHARED_SLOT FOURE_CONF_MAX_SHARED_SLOT
#else
#define FOURE_MAX_SHARED_SLOT 2
#endif

#if FOURE_MAX_SHARED_SLOT > FOURE_MAX_SLOTS_PER_DESTINATION
#error "Number of shared slot must be fewer than number of slot memory!"
#endif

#ifdef FOURE_CONF_MAC_MIN_BE
#define FOURE_MAC_MIN_BE TSCH_CONF_MAC_MIN_BE
#else
#define FOURE_MAC_MIN_BE 1
#endif

#ifdef FOURE_CONF_MAC_MAX_BE
#define FOURE_MAC_MAX_BE TSCH_CONF_MAC_MAX_BE
#else
#define FOURE_MAC_MAX_BE 13 //maximum 5 retries * 2 increments + min_be
#endif

#ifdef FOURE_CONF_MAC_BE_INC
#define FOURE_MAC_BE_INC TSCH_CONF_MAC_BE_INC
#else
#define FOURE_MAC_BE_INC 2
#endif

/*Foure Buffer Priorities */
#define HIGH_PRIORITY     1 // b0001
#define NORMAL_PRIORITY   2 // b0010
#define LOW_PRIORITY      4 // b0100

/* Timesynch */
#ifdef SLOT_CONF_FRAME_SIZE
#define SLOT_FRAME_SIZE SLOT_CONF_FRAME_SIZE
#else
#define SLOT_FRAME_SIZE 31
#endif

#ifdef MIN_CONF_INTERVAL_MUL
#define MIN_INTERVAL_MUL MIN_CONF_INTERVAL_MUL
#else
#define MIN_INTERVAL_MUL 1
#endif
#define MIN_BEACON_INTERVAL (CLOCK_SECOND * MIN_INTERVAL_MUL) 

#ifdef MAX_CONF_INTERVAL_MUL
#define MAX_INTERVAL_MUL MAX_CONF_INTERVAL_MUL
#else
#define MAX_INTERVAL_MUL 30
#endif

#define MAX_BEACON_INTERVAL (CLOCK_SECOND * MAX_INTERVAL_MUL)

#define CLOCK_OFFSET_DRIFT_THRESHOLD (0x4 * MAX_BEACON_INTERVAL)/CLOCK_SECOND

/* Timeslot timing */
#ifdef TSCH_CONF_DEFAULT_TIMESLOT_LENGTH
#define TSCH_DEFAULT_TIMESLOT_LENGTH TSCH_CONF_DEFAULT_TIMESLOT_LENGTH
#else
#define TSCH_DEFAULT_TIMESLOT_LENGTH 10000
#endif

#if TSCH_DEFAULT_TIMESLOT_LENGTH == 10000
/* Default timeslot timing as per IEEE 802.15.4e */

#define TSCH_DEFAULT_TS_CCA_OFFSET         1800
#define TSCH_DEFAULT_TS_CCA                128
#define TSCH_DEFAULT_TS_TX_OFFSET          1800
/* By standard, TS_RX_OFFSET is 1120us by default. To have the guard time
 * equally spent before and after the expected reception, use
 * (TS_TX_OFFSET - (TS_RX_WAIT / 2)) instead */
#define TSCH_DEFAULT_TS_RX_OFFSET          1120
#define TSCH_DEFAULT_TS_RX_ACK_DELAY       2800
#define TSCH_DEFAULT_TS_TX_ACK_DELAY       3000
#define TSCH_DEFAULT_TS_RX_WAIT            2200
#define TSCH_DEFAULT_TS_ACK_WAIT           800
#define TSCH_DEFAULT_TS_RX_TX              192
#define TSCH_DEFAULT_TS_MAX_ACK            1000
#define TSCH_DEFAULT_TS_MAX_TX             4256
#define TSCH_DEFAULT_TS_TIMESLOT_LENGTH    10000

#elif TSCH_DEFAULT_TIMESLOT_LENGTH == 15000
/* Default timeslot timing for platfroms requiring 15ms slots */

#define TSCH_DEFAULT_TS_CCA_OFFSET         2200
#define TSCH_DEFAULT_TS_CCA                128
#define TSCH_DEFAULT_TS_TX_OFFSET          3180 /* tsCCAOffset + tsCCA + tsRxTx */ 
#define TSCH_DEFAULT_TS_RX_OFFSET          1680
#define TSCH_DEFAULT_TS_RX_ACK_DELAY       3600 /* 1200 */
#define TSCH_DEFAULT_TS_TX_ACK_DELAY       4000 /* 1500 */
#define TSCH_DEFAULT_TS_RX_WAIT            3300
#define TSCH_DEFAULT_TS_ACK_WAIT           800  /* 600 */
#define TSCH_DEFAULT_TS_RX_TX              192
#define TSCH_DEFAULT_TS_MAX_ACK            2400
#define TSCH_DEFAULT_TS_MAX_TX             4256
#define TSCH_DEFAULT_TS_TIMESLOT_LENGTH    15000

#elif TSCH_DEFAULT_TIMESLOT_LENGTH == 65000
/* Default timeslot timing for platfroms requiring 65ms slots
 * (e.g. sky/z1 with security enabled) */

#define TSCH_DEFAULT_TS_CCA_OFFSET         1800
#define TSCH_DEFAULT_TS_CCA                128
#define TSCH_DEFAULT_TS_TX_OFFSET          38000
#define TSCH_DEFAULT_TS_RX_OFFSET          36900
#define TSCH_DEFAULT_TS_RX_ACK_DELAY       37600
#define TSCH_DEFAULT_TS_TX_ACK_DELAY       38000
#define TSCH_DEFAULT_TS_RX_WAIT            2200
#define TSCH_DEFAULT_TS_ACK_WAIT           800
#define TSCH_DEFAULT_TS_RX_TX              2072
#define TSCH_DEFAULT_TS_MAX_ACK            2400
#define TSCH_DEFAULT_TS_MAX_TX             4256
#define TSCH_DEFAULT_TS_TIMESLOT_LENGTH    65000

#else
#error "TSCH: Unsupported default timeslot length"
#endif

#ifndef MIN
#define MIN(n, m)   (((n) < (m)) ? (n) : (m))
#endif

#if RTIMER_SECOND >= 200000
#define RTIMER_GUARD (RTIMER_SECOND / 100000)
#else
#define RTIMER_GUARD 2u
#endif

/* Truncate received drift correction information to maximum half
 * of the guard time (one fourth of TSCH_DEFAULT_TS_RX_WAIT). */
#define TRUNCATE_SYNC_IE 1
#define TRUNCATE_SYNC_IE_BOUND ((int)US_TO_RTIMERTICKS(TSCH_DEFAULT_TS_RX_WAIT/4))

/* Convert rtimer ticks to clock and vice versa */
#define TSCH_CLOCK_TO_TICKS(c) (((c)*RTIMER_SECOND)/CLOCK_SECOND)
#define TSCH_CLOCK_TO_SLOTS(c, timeslot_length) (TSCH_CLOCK_TO_TICKS(c)/timeslot_length)

#define SLOT_DURATION_THRESHOLD US_TO_RTIMERTICKS(TSCH_DEFAULT_TIMESLOT_LENGTH + TSCH_DEFAULT_TIMESLOT_LENGTH/10)

/* Calculate packet tx/rx duration in rtimer ticks based on sent
 * packet len in bytes with 802.15.4 250kbps data rate.
 * One byte = 32us. Add two bytes for CRC and one for len field */
#define TSCH_PACKET_DURATION(len) US_TO_RTIMERTICKS(32 * ((len) + 3))

/* Wait for a condition with timeout t0+offset. */
#define BUSYWAIT_UNTIL_ABS(cond, t0, offset) \
    while(!(cond) && RTIMER_CLOCK_LT(RTIMER_NOW(), (t0) + (offset))); 
/* #define RTIMER_CLOCK_LT(a,b)     ((signed short)((a)-(b)) < 0)   */

struct foure_mac_control {
	uint8_t secured;
	uint8_t	synched;
};

extern rtimer_clock_t current_slot_start;
extern rtimer_clock_t radio_received_time;

struct foure_mac_control     foure_control;
struct asn_t                 current_asn;
struct timesynch_msg_eb      tsch_msg;
struct tsch_time_slot_timing time_slot_timing;
int                          ack_counter;

struct foure_mac_packet {
	mac_callback_t sent;
	void *ptr;
	uint8_t status;
	uint8_t num_transmissions;
#if ENHANCED_PACKETBUF_ENABLE
	packetbuf_t *packetbuf;
#endif
};

struct foure_mac_item {
	struct destination *dn;
	struct content *cn;
	struct slot *sl;
	void *ptr;
};

#if TSCH_CENTRAL_SCHEDULER_ENABLED
extern struct uip_udp_conn *cs_conn;
#endif

#if RPL_ADD_ROUTE_ENABLED
uint16_t cur_asn;
#endif

#endif /* _FOURE_MAC_PRIVATE_H */
