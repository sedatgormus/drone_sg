/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *        Information Element routines 
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#include "contiki.h"
#include "net/mac/4emac/4emac-private.h"

#ifndef FOURE_DEBUG
#define FOURE_DEBUG DEBUG_NONE
#endif
#include "net/mac/4emac/4emac-debug.h"
/*--------------------------------------------------------------------------------------------
 * decode information elements and retrive length field, id, and type. The results will be
 * stored in ie_header structure.
 */
void
get_ie_header_parts(uint16_t local_ie_bytes, struct ie_header *ie_hdr, uint8_t short_header_ie)
{

  ie_hdr->type = local_ie_bytes & 0x8000 ? 1 : 0;
  if(!ie_hdr->type){
    ie_hdr->len = local_ie_bytes & 0x007f; /* b0-b6 */
    if(short_header_ie)
      ie_hdr->id = (local_ie_bytes & 0x7f80) >> 7; /* b7-b14 */
    else
      ie_hdr->id = (local_ie_bytes & 0x7f80) >> 8; /* b7-b14 */
  }else{
    ie_hdr->len = local_ie_bytes & 0x7ff; /* b0-b10 */
    ie_hdr->id = (local_ie_bytes & 0x7800) >> 11; /* b11-b14 */
  }

  PRINTO("len  : %04x\n", ie_hdr->len);
  PRINTO("id   : %04x\n", ie_hdr->id );
  PRINTO("type : %04x\n", ie_hdr->type);
}


/*--------------------------------------------------------------------------------------------
 * Write information element fields stored in ie_header structure to packet buffer 
 * prior to transmission. The result is written to the buffer in little endian format.
 */
uint16_t
set_ie_header_parts(uint8_t * buf, struct ie_header *ie_hdr)
{

  uint16_t local_ie_bytes;

  if(!ie_hdr->type){
    /* Create a header IE 2-byte descriptor */
    /* Header IE descriptor: b0-6: len, b7-14: element id:, b15: type: 0 */
    local_ie_bytes = (ie_hdr->len & 0xff) + ((ie_hdr->id & 0x7f) << 8);
    WRITE16(buf, local_ie_bytes);
    PRINTO("%04x\n", local_ie_bytes);
  }else{
    /* MLME Long IE descriptor: b0-10: len, b11-14: group id:, b15: type: 1 */
    local_ie_bytes = (ie_hdr->len & 0x07ff) + ((ie_hdr->id & 0x0f) << 11) + (1 << 15);
    WRITE16(buf, local_ie_bytes);
    PRINTO("%04x\n", local_ie_bytes);
  }

  return local_ie_bytes;
}

/*--------------------------------------------------------------------------
 * Create an MLME short IE 2-byte descriptor 
*/
void
create_mlme_short_ie_descriptor(uint8_t *buf, struct ie_header *ie_hdr)
{
  uint16_t local_ie_bytes;
  /* MLME Short IE descriptor: b0-7: len, b8-14: sub id:, b15: type: 0 */
  local_ie_bytes = (ie_hdr->len & 0x7f) + ((ie_hdr->id & 0xff) << 8);
  WRITE16(buf, local_ie_bytes);
}

/*---------------------------------------------------------------------------
 * Create a header IE 2-byte descriptor 
 */
void
create_header_ie_descriptor(uint8_t *buf, struct ie_header *ie_hdr)
{
  uint16_t local_ie_bytes;
  /* Header IE descriptor: b0-6: len, b7-14: element id:, b15: type: 0 */
  local_ie_bytes = (ie_hdr->len & 0x7f) + ((ie_hdr->id & 0xff) << 7);
  WRITE16(buf, local_ie_bytes);
}

