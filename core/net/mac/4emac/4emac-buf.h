/*
 * Copyright (c) 2010, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *
 * Author: Sedat Gormus <sedatgormus@gmail.com>
 *
 */
#ifndef FOUREMAC_BUF_H_
#define FOUREMAC_BUF_H_

#include "net/linkaddr.h"
#include "net/mac/mac.h"
#include "net/ip/uip.h"
#include "lib/memb.h"
#include "lib/list.h"

/**
 * define how many content types you can store
 */
#if MAHMOTE || OPENMOTE
#define FOURE_CONTENT_SIZE 127
#else
#define FOURE_CONTENT_SIZE 100
#endif
 
#define SLOT_USABLE         0x00
#define SLOT_NOT_USABLE     0x80
/* slot types */
#define SLOT_TYPE_TRANSMIT    1 // b0001
#define SLOT_TYPE_RECEIVE     2 // b0010
#define SLOT_TYPE_SHARED      4 // b0100
#define SLOT_TYPE_TIMEKEEPING 8 // b1000
#define SLOT_TYPE_MASK        0x0F

/* return types for slots */
#define FOURE_SLOT_SCHEDULED_FOR_RECEIVE  -2
#define FOURE_SLOT_UNSCHEDULED            -1
#define FOURE_SLOT_SCHEDULED               1

/* return types for contents */
#define FOURE_BUFFERS_FULL        -3
#define FOURE_CONTENT_INSERTED     2

#define FOURE_MAX_FAILED_CHANNEL   2

typedef struct slot{
  struct slot *next;
  uint8_t  slot_id;    /* slot_id represents the slot number within the slot frame */
  uint8_t  slot_type;  /* it seems like slot_type manipulations are spilling over to slot_tx and making the system crash */
  uint8_t  channel_offset;
  uint8_t  rssi;       /* Average rssi of the slot */
  //uint8_t etx;
  uint16_t slot_tx;
  uint16_t slot_tx_ok;
  uint16_t slot_tx_noack;
  uint16_t slot_tx_collisions;
  uint16_t slot_tx_deferred;
  uint16_t slot_rx;
  uint8_t  slot_black_list;
} slot_t;

typedef struct content {
  struct content *next;
  uint8_t  msg[FOURE_CONTENT_SIZE];
  uint8_t  size;
  uint8_t  no_of_tx; /* number of txs for the content */
  uint8_t  last_transmit_ch[FOURE_MAX_FAILED_CHANNEL];
  uint8_t  c_type;
  uint8_t  priority;
  uint16_t seqno;
} content_t;

typedef struct destination {
  struct destination *next;
  LIST_STRUCT(content_list);
  LIST_STRUCT(slot_list);
  mac_callback_t sent;
  uint8_t  backoff_window;
  uint8_t  backoff_exponent;
  uint8_t  cn_rate;
  uint16_t inserted_cn_num;
  uint16_t transmissions; /* total number of transmissions to the destination */
  uint16_t packet_drop;
} destination_t;

typedef struct destination_stats{
  destination_t *dn;
  uint16_t stat;
} destination_stats_t;

/**
 * \brief  
 */
void foure_mac_buf_init();

/**
 * \brief   
 */
void foure_mac_buf_backoff_inc(destination_t *);

/**
 * \brief
 */
uip_ipaddr_t * foure_mac_buf_get_destination_ipaddr(destination_t *dn);

/**
 * \brief
 */
uip_lladdr_t * foure_mac_buf_get_destination_lladdr(destination_t *dn);

/**
 * \brief     
 */
uint8_t foure_mac_buf_get_slot_frame_from_lladdr(linkaddr_t *linkaddr, uint8_t *slot_frame);

/**
 * \brief 
 */
uint8_t foure_mac_buf_is_content_available_for_packet_type(linkaddr_t *linkaddr, uint8_t cn_type);

/**
 * \brief 
 */
uint8_t foure_mac_buf_get_slot_black_list(linkaddr_t *linkaddr, uint8_t *slot_offsets, uint8_t *channel_offsets, uint8_t black_list_threshold);

/**
 * \brief 
 */
uint8_t foure_mac_buf_get_least_used_slots(linkaddr_t *linkaddr, uint8_t *slot_offsets, uint8_t *channel_offsets, uint8_t *slot_tx, uint8_t slot_num, uint8_t delete_threshold);

/**
 * \brief 
 */
uint8_t foure_mac_buf_schedule_slots(linkaddr_t *addr, uint8_t cn_type, uint8_t sl_type, uint8_t *slot_offsets, uint8_t *channel_offsets, uint8_t sl_num);

/**
 * \brief 
 */
void foure_mac_buf_set_slots_status(linkaddr_t * linkaddr, uint8_t *sl_id, uint8_t sl_num, uint8_t sl_status);

/**
 * \brief     get the number of slots until the next scheduled slot 
 */
uint8_t foure_mac_buf_get_next_scheduled_slot(uint8_t slot_pointer, uint8_t frame_size);

/**
 * \brief 
 */
uint8_t foure_mac_buf_get_slot_num_from_lladdr(linkaddr_t *linkaddr, uint8_t slot_type);

/**
 * \brief     remove given slot from the destination list. 
 */
void foure_mac_buf_delete_slots(linkaddr_t *linkaddr, uint8_t *slot_id, uint8_t slot_num);

/**
 * \brief 
 */
void foure_mac_buf_delete_unused_slots(linkaddr_t *ignore_dest, uint8_t slot_type);

/**
 * \brief      Copy data with content type to the destinations content buffer
 * \retval     FOURE_CONTENT_INSERTED if successful, FOURE_BUFFERSFULL if not
 */
int8_t foure_mac_buf_content_insert_data(const uint8_t c_type, void *c_buf, uint8_t c_size, linkaddr_t *linkaddr, uint8_t priority, mac_callback_t sent);

/**
 * \brief     get the content and destination for the current slot 
 */
int8_t foure_mac_buf_get_scheduled_content(destination_t**, content_t**, slot_t**, uint8_t slot_pointer);

/**
 * \brief     Remove given content from the destination list.
 */
void foure_mac_buf_free_content(destination_t *, content_t *);

/**
* \return      
* \retval     
*/
uint8_t foure_mac_buf_get_free_content_num(linkaddr_t *linkaddr);

/**
* \return      
* \retval     
*/
uint8_t foure_mac_buf_get_buffer_occupancy(linkaddr_t *linkaddr);

/**
* \return      
* \retval     
*/
int8_t foure_mac_buf_parent_changed(linkaddr_t *old_parent, linkaddr_t *new_parent);

/**
 * \brief     
 * \retval    the total number of lost packets.
 */
uint8_t foure_mac_buf_get_total_lost_packet(linkaddr_t *linkaddr, destination_stats_t *s);

/**
 * \brief     
 * \retval    
 */
uint16_t  foure_mac_buf_get_destination_inserted_content_num(linkaddr_t *linkaddr, destination_stats_t *s);

/**
 * \brief     
 * \retval    
 */
uint16_t  foure_mac_buf_get_destination_slot_tx_ok(linkaddr_t *linkaddr, uint8_t slot_type);

/**
 * \brief     
 * \retval    
 */
uint16_t  foure_mac_buf_get_destination_slot_tx_noack(linkaddr_t *linkaddr, uint8_t slot_type);

/**
 * \brief     
 * \retval    
 */
uint16_t  foure_mac_buf_get_destination_slot_tx_collisions(linkaddr_t *linkaddr, uint8_t slot_type);

/**
 * \brief     
 * \retval    
 */
uint16_t foure_mac_buf_inc_destination_slot_rx(linkaddr_t *linkaddr, uint8_t sl_id);

/**
 * \brief       
 */
void foure_mac_buf_update_slot_stats(linkaddr_t *linkaddr, uint8_t slot_type);

#endif /* FOUREMAC_BUF_H_ */
