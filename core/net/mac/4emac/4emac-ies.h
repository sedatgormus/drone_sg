/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Information Element Processing Routines for 4eMAC 
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#ifndef FOUREMAC_IES_H
#define FOUREMAC_IES_H

#define BEACON_TERMINATION_HEADER 0x007E
#define EACK_HEADER 0x001E

struct ie_header{
  uint16_t len;
  uint16_t id;
  uint16_t type;
};

/* c.f. IEEE 802.15.4e Table 4b */
enum ieee802154e_header_ie_id {
  HEADER_IE_LE_CSL = 0x1a,
  HEADER_IE_LE_RIT,
  HEADER_IE_DSME_PAN_DESCRIPTOR,
  HEADER_IE_RZ_TIME,
  HEADER_IE_ACK_NACK_TIME_CORRECTION,
  HEADER_IE_GACK,
  HEADER_IE_LOW_LATENCY_NETWORK_INFO,
  HEADER_IE_LIST_TERMINATION_1 = 0x7e,
  HEADER_IE_LIST_TERMINATION_2 = 0x7f,
};

/* c.f. IEEE 802.15.4e Table 4c */
enum ieee802154e_payload_ie_id {
  PAYLOAD_IE_ESDU = 0,
  PAYLOAD_IE_MLME,
  PAYLOAD_IE_LIST_TERMINATION = 0xf,
};

/* c.f. IEEE 802.15.4e Table 4d */
enum ieee802154e_mlme_short_subie_id {
  MLME_SHORT_IE_TSCH_SYNCHRONIZATION = 0x1a,
  MLME_SHORT_IE_TSCH_SLOFTRAME_AND_LINK,
  MLME_SHORT_IE_TSCH_TIMESLOT,
  MLME_SHORT_IE_TSCH_HOPPING_TIMING,
  MLME_SHORT_IE_TSCH_EB_FILTER,
  MLME_SHORT_IE_TSCH_MAC_METRICS_1,
  MLME_SHORT_IE_TSCH_MAC_METRICS_2,
};

/* c.f. IEEE 802.15.4e Table 4e */
enum ieee802154e_mlme_long_subie_id {
  MLME_LONG_IE_TSCH_CHANNEL_HOPPING_SEQUENCE = 0x9,
};

#define WRITE16(buf, val) \
  do { ((uint8_t*)(buf))[0] = (val) & 0xff; \
  ((uint8_t*)(buf))[1] = ((val) >> 8) & 0xff; } while(0);

#define READ16(buf, var) \
  (var) = ((uint8_t*)(buf))[0] | ((uint8_t*)(buf))[1] << 8
#endif

uint16_t set_ie_header_parts(uint8_t *buf, struct ie_header *ie_hdr);

void get_ie_header_parts(uint16_t local_ie_bytes, struct ie_header *ie_hdr, uint8_t short_header_ie);

void create_mlme_short_ie_descriptor(uint8_t *buf, struct ie_header *ie_hdr);

void create_header_ie_descriptor(uint8_t *buf, struct ie_header *ie_hdr);
