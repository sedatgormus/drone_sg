/*
 * Copyright (c) 2015, Mavialp Research Limited.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A Time Synchronisation mechanism that uses ACK frames and a Keep alive timer.
 * \author
 *         Sedat Gormus <sedatgormus@gmail.com>
 */

#ifndef FOUREMAC_EB_H
#define FOUREMAC_EB_H

#include "net/mac/4emac/4emac-ies.h"
#include "net/mac/4emac/4emac-asn.h"
#include "net/llsec/llsec802154.h"

struct synch_ie {
  struct ie_header ie_hdr; //length (b0-b7) = 0x06, Sub-id (b8-b14) = 0x1a, Type(b15) = 0x00 (short)
  struct asn_t asn;
  uint8_t join_priority;
};

struct tsch_time_slot_timing{
  uint16_t ts_cca_offset;
  uint16_t ts_cca;
  uint16_t ts_tx_offset;
  uint16_t ts_rx_offset;
  uint16_t ts_rx_ack_delay;
  uint16_t ts_tx_ack_delay;
  uint16_t ts_rx_wait;
  uint16_t ts_ack_wait;
  uint16_t ts_rx_tx;
  uint16_t ts_max_ack;
  uint16_t ts_max_tx;
  uint16_t ts_duration;
};

struct tsch_time_slot_ie{
  struct ie_header ie_hdr; //length (b0-b7) = 0x01, Sub-id (b8-b14) = 0x1c, Type(b15) = 0x00 (short)
  uint8_t time_slot_template_id; // 0x00
  struct tsch_time_slot_timing time_slot_timing;
};

struct channel_hopping_ie{
  struct ie_header ie_hdr; //length (b0-b7) = 0x01, Sub-id (b8-b14) = 0x1d, Type(b15) = 0x00 (short)
  uint8_t channel_hopping_template_id;//default 0x00, 2.4Ghz 0x00 template = [5,6,12,7,15,4,14,11,8,0,1,2,13,3,9,10]
};

struct frame_link_ie {
  struct ie_header ie_hdr; /*Length (b0-b7) (variable-Why?), sub-id (id of the IE = 0x1b) (b8-b14), type(b15) = 0x00 (short, 1 probably long)*/
  uint8_t slot_frames; //0x01 default
  uint8_t slot_frame_id; //0x00 slot frame template id
  uint16_t slot_frame_size; //variable
  uint8_t links; // 0x01

  //active cell minimal draft ???
  uint16_t slot_number; //0x00
  uint16_t channel_offset; //0x00 default
  uint8_t link_option; //b0 for transmit, b1 for Receive, b2 = Shared, b3 = Time keeping, b4-b7 reserved
};

struct timesynch_msg_eb{
  struct ie_header enhanced_beacon_header;                  //2  bytes
  struct ie_header payload_ie;                              //2  bytes
  struct synch_ie synch_information;                        //8  bytes
  struct tsch_time_slot_ie tsch_slot_template;              //3  bytes
  struct channel_hopping_ie tsch_channel_hopping_template;  //3  bytes
  struct frame_link_ie frame_link_information;              //12 bytes
  struct ie_header termination_ie;                          //2  bytes
#if !SIX_TISCH_MINIMAL
  uint16_t authority_offset;                                //2  bytes
  /*  The timestamp must be the last two bytes. in cc2420.c write timestamp to last two bytes of packet in TXFIFO  */
  uint16_t timestamp;                                       //2  bytes
#endif
#if FRAME802154_SECURITY_LEVEL_AES_192
  uint8_t  has_security;
  uint16_t public_key;
  uint16_t mod;
#endif
};//                                                      _____________ = 36 bytes


void fouremac_buf_set_join_priority(uint8_t);

void fouremac_buf_set_authority_offset(uint16_t);

void fouremac_buf_set_asn(struct asn_t *);

void set_eb_slot_channel_offset(uint8_t *msg_buffer, uint16_t sl_offset, uint16_t ch_offset);

void fouremac_buf_reset_timeslot_timing(struct tsch_time_slot_timing *);

/**
 * \brief      Enhanced beacon decoder
 * \return     The size of packet length
 *
 *             This function decodes the incoming beacon and sets the data into the relevant fields in timesynch_msg_eb structure
 *
 */
int8_t decode_enhanced_beacon(char *, uint8_t, struct timesynch_msg_eb *);

/**
 * \brief      The transmitter is required to set the fields of the timesynch_msg_eb prior to transmission
 * 
 *             
 *             This function must not be called before insert_eb_content().
 *
 */
void set_eb_content(struct timesynch_msg_eb *);

/**
 * \brief      This function copies the fields of the timesynch_msg_eb to the enhanced beacon message buffer
 * \return     The size of packet length
 *
 */
int8_t insert_eb_content( uint8_t *, struct timesynch_msg_eb *);

#endif  /*FOUREMAC_EB*/
