#include "dev/cc2592.h"
#include "dev/gpio.h"
#include "dev/cc2538-rf.h"

/*---------------------------------------------------------------------------*/
void
cc2592_selectMode(int mode)
{
	if(mode == RX_LOW_GAIN_MODE){
		GPIO_WRITE_PIN(GPIO_C_BASE, CC2592_CBASE_MASK, LNA_EN);
  		GPIO_WRITE_PIN(GPIO_D_BASE, CC2592_DBASE_MASK, 0);
	}else if(mode == RX_HIGH_GAIN_MODE){
		//GPIO_WRITE_PIN(GPIO_C_BASE, CC2592_CBASE_MASK, LNA_EN);
  		GPIO_WRITE_PIN(GPIO_D_BASE, CC2592_DBASE_MASK, HGM);
	}else if(mode == TX){
		GPIO_WRITE_PIN(GPIO_C_BASE, CC2592_CBASE_MASK, PA_EN);
  		GPIO_WRITE_PIN(GPIO_D_BASE, CC2592_DBASE_MASK, 0);
	}else{
		GPIO_WRITE_PIN(GPIO_C_BASE, CC2592_CBASE_MASK, 0);
  		GPIO_WRITE_PIN(GPIO_D_BASE, CC2592_DBASE_MASK, 0);
	}
}

/*---------------------------------------------------------------------------*/
void
cc2592_init(void)
{
	//cc2538_rf_set_promiscous_mode(1); //disable frame filtering
	
	GPIO_SET_OUTPUT(GPIO_C_BASE, CC2592_CBASE_MASK);
	GPIO_SET_OUTPUT(GPIO_D_BASE, CC2592_DBASE_MASK);

	GPIO_WRITE_REG(CCTEST_OBSSEL2, 0x80);
	GPIO_WRITE_REG(CCTEST_OBSSEL3, 0x81);
	GPIO_WRITE_REG(RFCORE_XREG_RFC_OBS_CTRL0, 0x6A);
	GPIO_WRITE_REG(RFCORE_XREG_RFC_OBS_CTRL1, 0x68);	
}


