#include "contiki.h"
#include "reg.h"
#include "dev/leds.h"
#include "dev/gpio.h"

#define DEBUG 0

#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define LEDS_GPIO_PIN_MASK   LEDS_ALL
#define GPIO_BASE 			 GPIO_C_BASE

/*---------------------------------------------------------------------------*/
void
leds_arch_init(void)
{
  PRINTF("leds_arch_init\n");	
  GPIO_SET_OUTPUT(GPIO_BASE, LEDS_GPIO_PIN_MASK);
}
/*---------------------------------------------------------------------------*/
unsigned char
leds_arch_get(void)
{
  PRINTF("leds_arch_get\n");
  return GPIO_READ_PIN(GPIO_BASE, LEDS_GPIO_PIN_MASK);
}
/*---------------------------------------------------------------------------*/
void
leds_arch_set(unsigned char leds)
{
  PRINTF("leds_arch_set\n");		
  GPIO_WRITE_PIN(GPIO_BASE, LEDS_GPIO_PIN_MASK, leds);
}

