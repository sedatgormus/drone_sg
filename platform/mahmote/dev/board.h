#ifndef BOARD_H_
#define BOARD_H_

#include "dev/gpio.h"
#include "dev/nvic.h"
/*---------------------------------------------------------------------------*/
/* Some files include leds.h before us, so we need to get rid of defaults in
 * leds.h before we provide correct definitions */
#undef LEDS_GREEN
#undef LEDS_YELLOW
#undef LEDS_RED
#undef LEDS_CONF_ALL

#define PIN0 1
#define PIN1 2
#define PIN2 4
#define PIN3 8
#define PIN4 16
#define PIN5 32
#define PIN6 64
#define PIN7 128

#define LEDS_MASK               PIN4

#if USB_SERIAL_CONF_ENABLE
#define LEDS_CONF_ALL           LEDS_MASK & 0xFE
#else
#define LEDS_CONF_ALL           LEDS_MASK
#endif

/*---------------------------------------------------------------------------*/
/** \name USB configuration
 *
 * The USB pullup is driven by PC0 and is shared with LED1
 */
#define USB_PULLUP_PORT         GPIO_C_NUM
#define USB_PULLUP_PIN          0

/*---------------------------------------------------------------------------*/
/** \name UART configuration
 *
 * - RX:  PA0
 * - TX:  PA1
 * - CTS: PB0 (Can only be used with UART1)
 * - RTS: PD3 (Can only be used with UART1)
 */
#define UART0_RX_PORT           GPIO_A_NUM
#define UART0_RX_PIN            0

#define UART0_TX_PORT           GPIO_A_NUM
#define UART0_TX_PIN            1

#define UART1_CTS_PORT          GPIO_B_NUM
#define UART1_CTS_PIN           0

#define UART1_RTS_PORT          GPIO_D_NUM
#define UART1_RTS_PIN           3

/*---------------------------------------------------------------------------*/
/**
 * \name ADC configuration
 *
 * These values configure which CC2538 pins and ADC channels to use for the ADC
 * inputs.
 *
 * ADC inputs can only be on port A.
 * @{
 */
#define ADC_THERMISTOR_PWR_PORT        GPIO_A_NUM  /**< THERMISTOR power GPIO control port */
#define ADC_THERMISTOR_PWR_PIN         7           /**< THERMISTOR power GPIO control pin */
#define ADC_THERMISTOR_OUT_PIN         6           /**< THERMISTOR output ADC input pin on port A */

#define ADC_ALS_PWR_PORT               GPIO_A_NUM /**< ALS power GPIO control port */
#define ADC_ALS_PWR_PIN                5          /**< ALS power GPIO control pin */
#define ADC_ALS_OUT_PIN                4          /**< ALS output ADC input pin on port A */
/*---------------------------------------------------------------------------*/
/**
 * \name SPI configuration
 *
 * These values configure which CC2538 pins to use for the SPI lines. Both
 * SPI instances can be used independently by providing the corresponding
 * port / pin macros.
 * @{
 */
#define SPI0_IN_USE             0
#define SPI1_IN_USE             0

#if SPI0_IN_USE

#define SPI0_CLK_PORT           GPIO_A_NUM		/** Clock port SPI0 */
#define SPI0_CLK_PIN            2				/** Clock pin SPI0 */
#define SPI0_TX_PORT            GPIO_A_NUM		/** TX port SPI0 (master mode: MOSI) */
#define SPI0_TX_PIN             4				/** TX pin SPI0 */
#define SPI0_RX_PORT            GPIO_A_NUM		/** RX port SPI0 (master mode: MISO */
#define SPI0_RX_PIN             5				/** RX pin SPI0 */
#endif  /* #if SPI0_IN_USE */

#if SPI1_IN_USE

#define SPI1_CLK_PORT           GPIO_A_NUM		/** Clock port SPI1 */
#define SPI1_CLK_PIN            2				/** Clock pin SPI1 */
#define SPI1_TX_PORT            GPIO_A_NUM		/** TX port SPI1 (master mode: MOSI) */
#define SPI1_TX_PIN             4				/** TX pin SPI1 */
#define SPI1_RX_PORT            GPIO_A_NUM		/** RX port SPI1 (master mode: MISO) */
#define SPI1_RX_PIN             5				/** RX pin SPI1 */
#endif  /* #if SPI1_IN_USE */

/*---------------------------------------------------------------------------*/
/**
 * \name Device string used on startup
 */
#define BOARD_STRING "Mahmote V1.0"


/*---------------------------------------------------------------------------*/
/**
 * \name Radio Delay Parameters for TSCH Synchronisation Code
 */

/* Delay between GO signal and SFD: radio fixed delay + 4Bytes preample + 1B SFD -- 1Byte time is 32us
 * ~327us + 129preample = 456 us */
#define RADIO_DELAY_BEFORE_TX ((unsigned)US_TO_RTIMERTICKS(456))
/* Delay between GO signal and start listening
 * ~50us delay + 129preample + ?? = 183 us */
#define RADIO_DELAY_BEFORE_RX ((unsigned)US_TO_RTIMERTICKS(183))

#endif /* BOARD_H_ */

