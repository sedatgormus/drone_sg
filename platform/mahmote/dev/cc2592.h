#ifndef CC2592_H_
#define CC2592_H_

#include "dev/board.h"
#include "dev/rfcore-xreg.h"
#include "reg.h"
#include "dev/cctest.h"

#define LNA_EN  PIN2	//PC2
#define PA_EN   PIN3	//PC3
#define HGM     PIN2	//PD2

#define CC2592_CBASE_MASK LNA_EN | PA_EN
#define CC2592_DBASE_MASK HGM

#define POWER_DOWN          0
#define RX_LOW_GAIN_MODE    1
#define RX_HIGH_GAIN_MODE   2
#define TX                  3

#define GPIO_WRITE_REG(REG_ADDRESS, value) \
  do { REG(REG_ADDRESS) = (value); } while(0)

void cc2592_init(void);
void cc2592_selectMode(int mode);

#endif /* CC2592_H_ */