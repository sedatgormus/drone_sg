#include <stdio.h>
#include "dev/uart1.h"

#if WITH_LCD
#include "lcd.h"
#endif

int
putchar(int c)
{
  uart1_writeb((char)c);
#if WITH_LCD
  lcd_write_char((char)c);
#endif
  return c;
}
